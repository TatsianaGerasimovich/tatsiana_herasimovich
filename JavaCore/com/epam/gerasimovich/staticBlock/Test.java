package com.epam.gerasimovich.staticBlock;

/**
 * @author Tatiana
 * @version 1.00 12/10/2015
 */
class Test2 {
    public final static int VALUABLE_STATIC;

    static {
        //VALUABLE=12; not allowed
        VALUABLE_STATIC = 12;
    }

    public final int VALUABLE;

    {
        VALUABLE = 15;
        //   VALUABLE_STATIC=15; not allowed
    }

    private Test2() {
    }
    public Test2(int a) {
    }

    protected Test2(String b) {
    }

    Test2(double d) {
    }
}

public class Test {
    static String s;
    static String str;

    static {
        s = "new";
        System.out.println("Work in static block");
        Test2 t = new Test2(5);
        try {
            throw new Exception("Work!");
        } catch (Exception e) {
            System.out.println("We can write try/catch in block");
        }
    }

    {
        str = "new";
        System.out.println("Work in block");
        Test2 t = new Test2(5);
        try {
            throw new Exception("Work!");
        } catch (Exception e) {
            System.out.println("We can write try/catch in block");
        }
    }

    public static void main(String[] args) {
        Test t = new Test();
        System.out.println("Dima");
    }
}
