package com.epam.gerasimovich.accessModifiers.package1;

import com.epam.gerasimovich.accessModifiers.package2.TestClass2;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class TestClass1 {
    public int variable_public;
    protected int variable_protected;
    int variable_friendly;
    private int variable_private;

    public static void main(String[] args) throws IllegalAccessException {
        TestClass1 testClass1 = new TestClass1();
        TestClass3 testClass3 = new TestClass3();
        TestClass4 testClass4 = new TestClass4();
        TestClass2 testClas2 = new TestClass2();

        testClass1.testMethod();
        testClass3.testMethod();
        testClass4.testMethod();
        testClas2.testMethod();

    }

    public void testMethod() {
        variable_public = 1;
        variable_protected = 1;
        variable_private = 1;
        variable_friendly = 1;
        System.out.println("Available from class:\n*private\n*public\n*protected\n*friendly");
    }
}

class TestClass3 {
    public void testMethod() throws IllegalAccessException {
        TestClass1 testClass1 = new TestClass1();
        testClass1.variable_public = 2;
        testClass1.variable_protected = 2;
        testClass1.variable_friendly = 2;
        System.out.println("Available from in the same package:\n*public\n*protected\n*friendly");
        /*Field[] fields = testClass1.getClass().getDeclaredFields();
        for (Field f : fields) {
            if(f.isAccessible())
            System.out.println(f.getGenericType() + " " + f.getName() + " = " + f.get(testClass1));
        }*/

    }
}


class TestClass4 extends TestClass1 {
    public void testMethod() {
        TestClass1 testClass1 = new TestClass1();
        testClass1.variable_public = 2;
        testClass1.variable_protected = 2;
        testClass1.variable_friendly = 2;
        System.out.println("Available from subclass in the same package:\n*public\n*protected\n*friendly");
    }
}