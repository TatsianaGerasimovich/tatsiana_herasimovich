package com.epam.gerasimovich.accessModifiers.package2;

import com.epam.gerasimovich.accessModifiers.package1.TestClass1;

public class TestClass2 {
    TestClass1 testClass1 = new TestClass1();

    public void testMethod() {
        testClass1.variable_public = 3;
        System.out.println("Available from subclass in the same package:\n*public");
    }
}

class TestClass5 extends TestClass1 {
    TestClass1 testClass1 = new TestClass1();

    public void testMethod() {
        variable_public = 5;
        variable_protected = 5;
        testClass1.variable_public = 6;
        System.out.println("Available from subclass in other package:\n*public\n*protected\n*friendly");
    }
}