package com.epam.gerasimovich.local.inner.classes;

/**
 * @author Tatiana
 * @version 1.00 12/14/2015
 */
public class Test {
    {
         class Inner {
            //allowed
             private int field;
             public int get (){
                 return field;
             }
        }
    }

    static{
        class Inner {
            //allowed
            private int field;
            public int get (){
                return field;
            }
        }
    }
}
