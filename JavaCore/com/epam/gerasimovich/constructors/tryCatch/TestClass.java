package com.epam.gerasimovich.constructors.tryCatch;

/**
 * @author Tatiana
 * @version 1.00 12/10/2015
 */
public class TestClass {
    public static void main(String[] args) {
        try {
            Child2 c = new Child2();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Parent {
    Parent() throws Exception {
        System.out.println("Parent constructor");
    }
}

//not allowed
/*class Child extends Parent {
    Child(){
        try {
          super();
        } catch (Exception ex) {
        }
    }
}*/

//allowed
class Child2 extends Parent {
    int x, y, z;

    public Child2() throws Exception {
        super();//can not specify
        System.out.println("Child");
        x = 12;
    }
}