package by.bsuir.informationhandling.operationwithtext;

import by.bsuir.informationhandling.entity.ISentencePart;
import by.bsuir.informationhandling.entity.Sentence;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class SortingIncreasingNumberOfWords implements Comparator<Sentence> {

    public int compare(Sentence obj1, Sentence obj2) {
        List<ISentencePart> parts1=obj1.getComponents();
        List<ISentencePart> parts2=obj2.getComponents();

        Iterator it = parts1.iterator();
        int count1=0;
        while (it.hasNext()) {
            it.next();
           count1++;
        }
        it = parts2.iterator();
        int count2=0;
        while (it.hasNext()) {
            it.next();
            count2++;
        }

        if (count1 > count2) {
            return 1;
        } else if (count1 < count2) {
            return -1;
        } else {
            return 0;
        }
    }
}