package by.bsuir.informationhandling.operationwithtext;

import by.bsuir.informationhandling.entity.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class OperationWithText {
    public static List<Sentence> getSortedByCountOfWords(Text text){
        List<Paragraph> paragraph= withoutCode(text.getComponents());
        List<Sentence> result = new ArrayList<Sentence>();
        for (Paragraph o: paragraph) {
            List<ITextPart> newList = new ArrayList<ITextPart>(o.getComponents());
            List<Sentence> newList2=withoutUnknownComponent(newList);
            result.addAll(newList2);
        }
        Collections.sort(result, new SortingIncreasingNumberOfWords());
        return result;
    }

    public static List<Sentence> getSwapTwoWords(Text text){
      /*  List<Paragraph> paragraph= withoutCode(text.getComponents());
        ArrayList<Sentence> result = new ArrayList<Sentence>();
        for (Paragraph o: paragraph) {
            List<ITextPart> newList = new ArrayList<ITextPart>(o.getComponents());
            List<Sentence> newList2=withoutUnknownComponent(newList);
            result.addAll(newList2);
        }*/
        List<Sentence> result = getSortedByCountOfWords(text);
        for(Sentence o: result){

            Collections.swap(o.getComponents(), firstWord(o.getComponents())-1,lastWord(o.getComponents()));
        }
        return result;
    }

    public static void printInAlphabeticalOrder(Text text){
        List<Paragraph> paragraph= withoutCode(text.getComponents());
        List<Sentence> result = new ArrayList<Sentence>();
        for (Paragraph o: paragraph) {
            List<ITextPart> newList = new ArrayList<ITextPart>(o.getComponents());
            List<Sentence> newList2=withoutUnknownComponent(newList);
            result.addAll(newList2);
        }
        List<Word> allWords=new ArrayList<Word>();
        for(Sentence o: result){
            allWords.addAll(justWords(o.getComponents()));
        }
        Collections.sort(allWords, new SortedInAlphabeticalOrder());
        System.out.println("************************************************************************************************************************************************");
        System.out.println("Words sorted in alphabetical order:");
        char letter=getLetter(allWords.get(0));
        for (Word o : allWords) {
            if(letter!=getLetter(o)) {
                System.out.println("\t"+o.toString() + "\n");
                letter=getLetter(o);
            }
            else System.out.println(o.toString() + "\n");
        }
    }
    private static char
    getLetter(Word word) {
        char result;
        result=word.getComponent(0).getValue();
        return result;
    }

    private static List<Paragraph>
    withoutCode(List<ITextPart> partsOfText) {
        List<Paragraph> result = new ArrayList<Paragraph>();
        Iterator it=partsOfText.listIterator();
        while (it.hasNext()){
            ITextPart temp1=  (ITextPart)it.next();
            if(temp1.getClass()==Paragraph.class) {
                result.add((Paragraph)temp1);
            }
        }
        return result;
    }
    private static List<Sentence>
    withoutUnknownComponent(List<ITextPart> partsOfParagraf) {
        List<Sentence> result = new ArrayList<Sentence>();
        Iterator it=partsOfParagraf.listIterator();
        while (it.hasNext()){
            ITextPart temp=  (ITextPart)it.next();
            if(temp.getClass()==Sentence.class) {
                result.add((Sentence)temp);
            }
        }
        return result;
    }
    private static List<Word>
    justWords(List<ISentencePart> partsOfSentence) {
        List<Word> result = new ArrayList<Word>();
        Iterator it=partsOfSentence.listIterator();
        while (it.hasNext()){
            ISentencePart temp=  (ISentencePart)it.next();
            if(temp.getClass()==Word.class) {
                result.add((Word)temp);
            }
        }
        return result;
    }
    private static int
    firstWord(List<ISentencePart> partsOfSentence) {
        Iterator it=partsOfSentence.listIterator();
        int i=0;
        boolean flag=true;
        while (flag){
            ISentencePart temp=  (ISentencePart)it.next();
            if(temp.getClass()==Word.class) {
              flag=false;
            }
            i++;
        }
        return i;
    }
    private static int
    lastWord(List<ISentencePart> partsOfSentence) {
        Iterator it=partsOfSentence.listIterator();
        int i=0;
        int j=0;
        while (it.hasNext()){
            ISentencePart temp=  (ISentencePart)it.next();
            if(temp.getClass()==Word.class) {
                j=i;
            }
            i++;
        }
        return j;
    }

}
