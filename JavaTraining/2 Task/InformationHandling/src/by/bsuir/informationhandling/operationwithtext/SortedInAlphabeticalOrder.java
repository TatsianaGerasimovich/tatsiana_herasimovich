package by.bsuir.informationhandling.operationwithtext;

import by.bsuir.informationhandling.entity.Symbol;
import by.bsuir.informationhandling.entity.Word;

import java.util.Comparator;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class SortedInAlphabeticalOrder implements Comparator<Word> {

    public int compare(Word obj1, Word obj2) {
        List<Symbol> parts1 = obj1.getComponents();
        List<Symbol> parts2 = obj2.getComponents();
        if (parts1.get(0).getValue() > parts2.get(0).getValue()) {
            return 1;
        } else if (parts1.get(0).getValue() < parts2.get(0).getValue()) {
            return -1;
        } else {
            return 0;
        }
    }
}
