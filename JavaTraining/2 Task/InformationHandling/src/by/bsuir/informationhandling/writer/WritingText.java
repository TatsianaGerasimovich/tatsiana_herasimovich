package by.bsuir.informationhandling.writer;

import by.bsuir.informationhandling.entity.Text;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * @author Tatiana
 * @version 1.00 09.04.2015.
 */
public class WritingText {
    private static final Logger LOG= Logger.getLogger(WritingText.class);
    public static final String FILE_NAME = "textAfterParsing.txt";
    public  void  write(Text text) {
        try {
            File file = new File(FILE_NAME);
            java.io.FileWriter fileWriter = new java.io.FileWriter(file);
            fileWriter.write(text.toString());
            fileWriter.close();
        } catch (IOException e) {
            LOG.fatal("error in writing to \"" + FILE_NAME +"\" file");
            e.printStackTrace();
        }
    }
}
