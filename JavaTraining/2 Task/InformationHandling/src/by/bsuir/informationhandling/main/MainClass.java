package by.bsuir.informationhandling.main;

import by.bsuir.informationhandling.entity.Sentence;
import by.bsuir.informationhandling.entity.Text;
import by.bsuir.informationhandling.operationwithtext.OperationWithText;
import by.bsuir.informationhandling.parser.ParagraphParser;
import by.bsuir.informationhandling.parser.SentenceParser;
import by.bsuir.informationhandling.parser.TextParser;
import by.bsuir.informationhandling.parser.WordParser;
import by.bsuir.informationhandling.reader.ReadingText;
import by.bsuir.informationhandling.writer.WritingText;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class MainClass {
    public static void main(String[] args) {
        ReadingText reader= new ReadingText();
        TextParser parser= new TextParser(new ParagraphParser(new SentenceParser(new WordParser())));
        Text text = (Text)parser.parse(reader.readData());
        WritingText writer = new WritingText();
        writer.write(text);

        System.out.println("************************************************************************************************************************************************");
        System.out.println("All sentence in order of increasing number of words:");
        List<Sentence> result=OperationWithText.getSortedByCountOfWords(text);
        for (Sentence o : result) {
            System.out.println(o.toString() + "\n");
        }
        System.out.println("************************************************************************************************************************************************");
        System.out.println("Replacing the first and last words:");
        List<Sentence> result2=OperationWithText.getSwapTwoWords(text);
        for (Sentence o : result2) {
            System.out.println(o.toString() + "\n");
        }

        OperationWithText.printInAlphabeticalOrder(text);

    }
}
