package by.bsuir.informationhandling.reader;

import org.apache.log4j.Logger;

import java.io.*;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class ReadingText {

    private static final Logger LOG= Logger.getLogger(ReadingText.class);
    public static final String FILE_NAME = "textFileToParse.txt";
    public String readData(){

        String result = "";
        try {
            File file = new File(FILE_NAME);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuffer text = new StringBuffer();

            while(bufferedReader.ready()) {
                text.append(bufferedReader.readLine() + "\n");
            }

            result = text.toString();
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            LOG.fatal("can't find \"" + FILE_NAME +"\" file");
            e.printStackTrace();
        } catch (IOException e) {
            LOG.fatal("error in reading \"" + FILE_NAME +"\" file");
            e.printStackTrace();
        }
        return result;
    }
}