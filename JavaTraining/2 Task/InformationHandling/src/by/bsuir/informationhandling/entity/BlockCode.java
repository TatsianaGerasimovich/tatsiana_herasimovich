package by.bsuir.informationhandling.entity;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class BlockCode implements ITextPart {
    private String code;

    public BlockCode(String code) {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=BlockCode.class){ return false;}

        BlockCode blockCode = (BlockCode) o;

        if (!code.equals(blockCode.code)){ return false;}

        return true;
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }
}
