package by.bsuir.informationhandling.entity;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class Sentence extends AbstractCompoundText<ISentencePart> implements ITextPart {
    @Override
    public ISentencePart getComponent(int index) {
        return parts.get(index);
    }

    @Override
    public List<ISentencePart> getComponents() {
        return parts;
    }

    @Override
    public void addComponent(ISentencePart component) {
        parts.add(component);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=Sentence.class){ return false;}

        Sentence sentence = (Sentence) o;

        if (!parts.equals(sentence.parts)) {return false;}

        return true;
    }

    @Override
    public int hashCode() {
        return parts.hashCode();
    }
}
