package by.bsuir.informationhandling.entity;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class Paragraph extends AbstractCompoundText<ITextPart> implements ITextPart {
    @Override
    public void addComponent(ITextPart component) {
        parts.add(component);
    }

    @Override
    public ITextPart getComponent(int index) {
        return parts.get(index);
    }

    @Override
    public List<ITextPart> getComponents() {
        return parts;
    }

    public String toString()
    {
        StringBuilder builder= new StringBuilder();
        for(ITextPart instance : parts)
        {
            builder.append(instance.toString()).append(" ");
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=Paragraph.class){ return false;}

        Paragraph paragraph = (Paragraph) o;

        if (!parts.equals(paragraph.parts)){ return false;}

        return true;
    }

    @Override
    public int hashCode() {
        return parts.hashCode();
    }

}
