package by.bsuir.informationhandling.entity;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class Word extends AbstractCompoundText<Symbol> implements ISentencePart {
    @Override
    public void addComponent(Symbol component) {
        parts.add(component);
    }

    @Override
    public Symbol getComponent(int index) {
        return parts.get(index);
    }

    @Override
    public List<Symbol> getComponents() {
        return parts;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=Word.class){ return false;}

        Word word1 = (Word) o;

        if (!parts.equals(word1.parts)){ return false;}

        return true;
    }

    @Override
    public int hashCode() {
        return parts.hashCode();
    }
}
