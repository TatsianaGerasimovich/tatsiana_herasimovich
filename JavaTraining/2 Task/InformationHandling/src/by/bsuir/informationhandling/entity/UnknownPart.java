package by.bsuir.informationhandling.entity;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class UnknownPart implements ITextPart{
    private String component;

    public UnknownPart(String component) {
        this.component = component;
    }

    public String toString()
    {
        return component;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=UnknownPart.class){ return false;}

        UnknownPart that = (UnknownPart) o;

        if (!component.equals(that.component)){ return false;}

        return true;
    }

    @Override
    public int hashCode() {
        return component.hashCode();
    }
}
