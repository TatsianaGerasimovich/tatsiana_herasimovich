package by.bsuir.informationhandling.entity;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class UnknownPartInSentence implements ISentencePart{
    private String component;

    public UnknownPartInSentence(String component) {
        this.component = component;
    }

    public String toString()
    {
        return component;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=UnknownPartInSentence.class){ return false;}

        UnknownPartInSentence that = (UnknownPartInSentence) o;

        if (!component.equals(that.component)){ return false;}

        return true;
    }

    @Override
    public int hashCode() {
        return component.hashCode();
    }
}
