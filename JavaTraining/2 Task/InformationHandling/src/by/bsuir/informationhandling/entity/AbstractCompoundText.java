package by.bsuir.informationhandling.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 09.04.2015.
 */
abstract public class AbstractCompoundText  <T extends ITextPart> {
    public List<T> parts;

    abstract void addComponent(T component);
    abstract T getComponent(int index);
    abstract List<T> getComponents();

    public AbstractCompoundText() {
        parts = new ArrayList<T>();
    }

    @Override
    public String toString() {
        String s = "";
        for (T part : parts) {
            s += part.toString();
        }
        return s;
    }

}
