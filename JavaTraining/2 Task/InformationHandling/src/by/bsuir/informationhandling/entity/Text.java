package by.bsuir.informationhandling.entity;


import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class Text extends AbstractCompoundText<ITextPart> implements ITextPart {
    @Override
    public void addComponent(ITextPart component){
        parts.add(component);
    }

    @Override
    public ITextPart getComponent(int index) {
        return parts.get(index);
    }

    @Override
    public List<ITextPart> getComponents() {
        return parts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o.getClass()!=Text.class){ return false;}

        Text text = (Text) o;

        if (!parts.equals(text.parts)){ return false;}
        return true;
    }

    @Override
    public int hashCode() {
        return parts.hashCode();
    }
}
