package by.bsuir.informationhandling.entity;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class PunctuationMark implements ISentencePart {
    char value;

    public PunctuationMark(char value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
