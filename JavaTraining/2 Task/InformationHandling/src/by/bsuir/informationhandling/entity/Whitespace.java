package by.bsuir.informationhandling.entity;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class Whitespace implements ISentencePart {
    char value;

    public Whitespace(char value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Whitespace)) return false;

        Whitespace that = (Whitespace) o;

        if (value != that.value) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}