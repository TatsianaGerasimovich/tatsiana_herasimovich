package by.bsuir.informationhandling.parser;

import by.bsuir.informationhandling.entity.ISentencePart;
import by.bsuir.informationhandling.entity.Symbol;
import by.bsuir.informationhandling.entity.Word;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class WordParser implements Parser {
    @Override
    public ISentencePart parse(String textForParsing) {
        Word word= new Word();
        Pattern symbolPattern = Pattern.compile(RegularExpressions.SYMBOL_REGEXP);
        Matcher symbolMatcher = symbolPattern.matcher(textForParsing);
        while(symbolMatcher.find())        {
                String ch = symbolMatcher.group(0);
                word.addComponent(new Symbol(ch.charAt(0)));
        }

        return word;
    }
}
