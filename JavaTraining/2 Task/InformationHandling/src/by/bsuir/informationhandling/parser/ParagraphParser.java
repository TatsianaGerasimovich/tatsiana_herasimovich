package by.bsuir.informationhandling.parser;

import by.bsuir.informationhandling.entity.ITextPart;
import by.bsuir.informationhandling.entity.Paragraph;
import by.bsuir.informationhandling.entity.UnknownPart;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class ParagraphParser implements Parser{
    private static final Logger LOG= Logger.getLogger(ParagraphParser.class);
    private Parser nextParser;

    public ParagraphParser(Parser sentenceParser) {
        this.nextParser = sentenceParser;
    }

    @Override
    public ITextPart parse(String textForParsing) {
        Paragraph paragraph = new Paragraph();

        Pattern sentenceParser = Pattern.compile(RegularExpressions.SENTENCE_REGEXP);
        Matcher sentenceMatcher = sentenceParser.matcher(textForParsing);
        nextParser = new SentenceParser();

        int start=0;
        int nextStart=0;
        int end=0;
        LOG.trace("start parsing paragraph");
        while(end!= textForParsing.length()) {
            if (sentenceMatcher.find(start)) {
                end = sentenceMatcher.start(0);
                nextStart = sentenceMatcher.end(0);
            } else {
                end = textForParsing.length();
                nextStart = -1;
            }
            String unknown = textForParsing.substring(start, end);
            if(!unknown.matches("\\s+")) {
               if(unknown.contains("\n"))
                {
                    unknown = unknown.replace("\n","");
                }
                if(unknown.contains("\t"))
                {
                    unknown = unknown.replace("\t","");
                }
                paragraph.addComponent(new UnknownPart(unknown));
            }
            start = nextStart;
            if(end!=textForParsing.length())
            {
                paragraph.addComponent(nextParser.parse(sentenceMatcher.group(0)));
            }
        }
        return paragraph;
    }
}
