package by.bsuir.informationhandling.parser;

import by.bsuir.informationhandling.entity.BlockCode;
import by.bsuir.informationhandling.entity.ITextPart;
import by.bsuir.informationhandling.entity.Text;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class TextParser implements Parser {

    private Parser nextParser;
    private static final Logger LOG= Logger.getLogger(TextParser.class);

    public TextParser(Parser ParagraphParser) {
        this.nextParser = ParagraphParser;
    }

    public ITextPart parse(String textToParsing) {
        Text text = new Text();
        Pattern codePattern = Pattern.compile(RegularExpressions.CODE_REGULAR_EXP);
        Pattern paragraphPattern = Pattern.compile(RegularExpressions.PARAGRAPH_REGEXP);
        Matcher codeMatcher = codePattern.matcher(textToParsing);
        int start = 0;
        int nextStart;
        int end = 0;
        LOG.trace("start parsing text");
        while (end != textToParsing.length()) {
            if (codeMatcher.find(start)) {
                end = codeMatcher.start();
                nextStart = codeMatcher.end(0);
            } else {
                end = textToParsing.length();
                nextStart = -1;
            }
            String paragraphs = textToParsing.substring(start, end);
            Matcher paragraphMatcher = paragraphPattern.matcher(paragraphs);
            while (paragraphMatcher.find()) {
                text.addComponent(nextParser.parse(paragraphMatcher.group(0) + "\n"));
            }
            if (end != textToParsing.length()) {
                text.addComponent(new BlockCode(codeMatcher.group(0)));
            }
            start = nextStart;
        }
        LOG.trace("end parsing text");
        return text;
    }
}
