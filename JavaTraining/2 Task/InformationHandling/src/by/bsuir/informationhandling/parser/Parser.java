package by.bsuir.informationhandling.parser;

import by.bsuir.informationhandling.entity.ITextPart;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public interface Parser {
    ITextPart parse(String textForParsing);
}
