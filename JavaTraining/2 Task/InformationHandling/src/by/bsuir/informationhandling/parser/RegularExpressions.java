package by.bsuir.informationhandling.parser;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class RegularExpressions {
    public static final String CODE_REGULAR_EXP = "(void|class)(.{5,30})[\\{](.|\\n){10,400}[\\}].";
    public static final String PARAGRAPH_REGEXP = "(.{2,}(?=[\\r\\n]))";
    public static final String SENTENCE_REGEXP = "([A-ZА-ЯЁ].{2,}?(([.?!])|(?=(\\n))))";
    public static final String SENTENCE_PART_REGEXP= "(([а-яА-ЯёЁA-Za-z]+)[^а-яА-ЯёЁA-Za-z]+)";
    public static final String WORD_REGEX = "([A-Za-zа-яА-ЯёЁ]+)";
    public static final String PUNCTUATION_REGEXP = "[:;.!?,\"']";
    public static final String SYMBOL_REGEXP = "[\\а-яА-ЯёЁa-zA-Z0-9-]";
}
