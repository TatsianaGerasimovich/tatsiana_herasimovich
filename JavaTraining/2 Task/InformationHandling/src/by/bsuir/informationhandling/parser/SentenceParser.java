package by.bsuir.informationhandling.parser;

import by.bsuir.informationhandling.entity.*;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tatiana
 * @version 1.00 06.04.2015.
 */
public class SentenceParser implements Parser {
    private Parser nextParser;
    private static final Logger LOG= Logger.getLogger(SentenceParser.class);
    public SentenceParser(Parser wordParser) {
        this.nextParser = wordParser;
    }
    public SentenceParser() {
    }
    @Override
    public ITextPart parse(String textForParsing) {
        Sentence sentence= new Sentence();
        Pattern sentencePartPattern = Pattern.compile(RegularExpressions.SENTENCE_PART_REGEXP);
        Pattern wordPattern = Pattern.compile(RegularExpressions.WORD_REGEX);
        Pattern punctuationPattern = Pattern.compile(RegularExpressions.PUNCTUATION_REGEXP);

        nextParser = new WordParser();

        Matcher sentencePartMatcher = sentencePartPattern.matcher(textForParsing);
        LOG.trace("start parsing sentence");
        while(sentencePartMatcher.find())
        {
            String sentencePart = sentencePartMatcher.group(0);
            Matcher wordMatcher = wordPattern.matcher(sentencePart);
            String word;
            if(wordMatcher.find()) {
                word = wordMatcher.group(0);
                sentence.addComponent((ISentencePart)nextParser.parse(wordMatcher.group(0)));
                sentencePart = sentencePart.replace(word, "");
                char[] sentencePartArray = sentencePart.toCharArray();
                for (Character ch : sentencePartArray) {
                    if (ch.compareTo(' ') == 0) {
                        sentence.addComponent(new Whitespace(ch));
                    } else if (punctuationPattern.matcher(ch.toString()).matches()) {
                        sentence.addComponent(new PunctuationMark(ch));
                    } else {
                        sentence.addComponent(new UnknownPartInSentence(ch.toString()));
                    }
                }
            }
        }

        return sentence;
    }
}
