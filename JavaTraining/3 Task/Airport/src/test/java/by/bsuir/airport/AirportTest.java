package by.bsuir.airport;

import by.bsuir.passenger.DepartureLounge;
import by.bsuir.passenger.Passenger;
import by.bsuir.plane.Plane;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class AirportTest extends TestCase {
    private DepartureLounge departureLounge = new DepartureLounge();
    private Airport airport=new Airport(1);
    private List<Passenger> passengerList= new ArrayList<Passenger>();

   @After
    public  void tearDownToDepartureLounge() {
       departureLounge.getAllPassengers().clear();
       passengerList.clear();
    }

  @Test
  public void testAddPassengers() throws AirportException {
      Calendar cal = Calendar.getInstance();
      Date today = cal.getTime();

      for (int i = 0; i < 5; i++) {
          try {
              passengerList.add(new Passenger(i, today.getDate()+"."+today.getMonth()+"."+today.getYear(), "Name"+i, "LastName"+i, "LastName"+i));
          } catch (ParseException e) {
              e.printStackTrace();
          }
      }
      assertTrue(departureLounge.addPassengers(passengerList));
  }
    @Test
    public void testPassengerCreate() throws AirportException {
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();

        for (int i = 0; i < 5; i++) {
            try {
                passengerList.add(new Passenger(i, today.getDate()+"."+today.getMonth()+"."+today.getYear(), "Name"+i, "LastName"+i, "LastName"+i));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
         assertFalse(passengerList.isEmpty());
    }

    @Test
    public void testAirportSetter() throws AirportException {
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();

        for (int i = 0; i < 5; i++) {
            try {
                passengerList.add(new Passenger(i, today.getDate()+"."+today.getMonth()+"."+today.getYear(), "Name"+i, "LastName"+i, "LastName"+i));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        departureLounge.addPassengers(passengerList);
        airport.setPassengersDepartureLounge(passengerList);
        assertEquals(airport.getDepartureLounge().getAllPassengers(),departureLounge.getAllPassengers());
    }

}
