package by.bsuir.airport;

import by.bsuir.passenger.DepartureLounge;
import by.bsuir.passenger.Passenger;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Test;

import java.text.ParseException;
import java.util.*;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class TerminalTest extends TestCase {
    private DepartureLounge departureLounge = new DepartureLounge();
    private Terminal terminal=new Terminal(1,departureLounge);
    private List<Passenger> passengerList= new ArrayList<Passenger>();

    @After
    public  void tearDownToDepartureLounge() {
        departureLounge.getAllPassengers().clear();

    }

    @Test
    public void testToTicketInspection() throws AirportException {
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();

        for (int i = 0; i < 5; i++) {
            try {
                passengerList.add(new Passenger(i, today.getDate()+"."+today.getMonth()+"."+today.getYear(), "Name"+i, "LastName"+i, "LastName"+i));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        departureLounge.addPassengers(passengerList);
        terminal.ticketInspection();
        assertTrue(departureLounge.getAllPassengers().isEmpty());
    }
    @Test
    public void testToGetId() throws AirportException {
        assertEquals(1,terminal.getId());
    }
}
