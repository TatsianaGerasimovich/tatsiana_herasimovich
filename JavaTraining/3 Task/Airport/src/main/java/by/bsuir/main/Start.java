package by.bsuir.main;

import by.bsuir.airport.Airport;
import by.bsuir.passenger.Passenger;
import by.bsuir.plane.Plane;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class Start {
    public static void main(String[] args) throws InterruptedException {
        int numberOfPassenger=15;
        List<Passenger> passengerList = new ArrayList<Passenger>(numberOfPassenger);
        for (int i = 0; i < numberOfPassenger; i++) {
            try {
                passengerList.add(new Passenger(i, geDate(), getName(), geLastName(), geMiddleName()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Airport airport = new Airport(2);
        airport.setPassengersDepartureLounge(passengerList);


        passengerList = new ArrayList<Passenger>(numberOfPassenger);
        for (int i = 0; i < numberOfPassenger; i++) {
            try {
                passengerList.add(new Passenger(i+numberOfPassenger, geDate(), getName(), geLastName(), geMiddleName()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Plane plane1 = new Plane("Plane1","Boeing 737-800", airport, 90);
        plane1.setPassengersOnBoard(passengerList);

        passengerList = new ArrayList<Passenger>(numberOfPassenger);
        for (int i = 0; i < numberOfPassenger; i++) {
            try {
                passengerList.add(new Passenger(i+2*numberOfPassenger, geDate(), getName(), geLastName(), geMiddleName()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Plane plane2 = new Plane("Plane2","Airbus A330-300", airport, 90);
        plane2.setPassengersOnBoard(passengerList);

        passengerList = new ArrayList<Passenger>(numberOfPassenger);
        for (int i = 0; i < numberOfPassenger; i++) {
            try {
                passengerList.add(new Passenger(i, geDate(), getName(), geLastName(), geMiddleName()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Plane plane3 = new Plane("Plane3","Ту-104", airport, 90);
        plane3.setPassengersOnBoard(passengerList);


        new Thread(plane1).start();
        new Thread(plane2).start();
        new Thread(plane3).start();


        Thread.sleep(3000);

        plane1.stopThread();
        plane2.stopThread();
        plane3.stopThread();

    }
    private static String getName() {
        Random random = new Random();
        int value = random.nextInt(4000);
        if (value < 1000) {
            return "Ivan";
        } else if (value < 2000) {
            return "Peter";
        } else if (value < 3000) {
        return "Oleg";
        }
        else return "Pavel";
    }
    private static String geLastName() {
        Random random = new Random();
        int value = random.nextInt(4000);
        if (value < 1000) {
            return "Ivanov";
        } else if (value < 2000) {
            return "Petrov";
        } else if (value < 3000) {
            return "Sidorov";
        }
        else return "Shishkin";
    }
    private static String geMiddleName() {
        Random random = new Random();
        int value = random.nextInt(4000);
        if (value < 1000) {
            return "Ivanovich";
        } else if (value < 2000) {
            return "Petrovich";
        }
        else if (value < 3000) {
            return "Olegovich";
        }
        else   return "Pavlovich";
    }
    private static String geDate() {
        Random random = new Random();
        int day = random.nextInt(28)+1;
        int month = random.nextInt(12)+1;
        int year = 2015;
        return day+"."+month+"."+year;
    }


}