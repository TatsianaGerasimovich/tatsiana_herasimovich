package by.bsuir.airport;

import by.bsuir.passenger.DepartureLounge;
import by.bsuir.passenger.Passenger;
import by.bsuir.passenger.Seats;
import by.bsuir.passenger.Ticket;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class Terminal {
    private int id;
    private DepartureLounge departureLounge;
    private final static Logger logger = Logger.getLogger(Terminal.class);

    public Terminal(int id, DepartureLounge departureLounge) {
        this.id = id;
        this.departureLounge = departureLounge;
    }

    public int getId() {
        return id;
    }
//высадка
    public boolean disembarkationOfPassengers(Seats lounge, int numberOfPassengers) throws InterruptedException {
        boolean result = false;
        Lock departureLoungeLock = departureLounge.getLock();
        boolean portLock = false;

        try{
            portLock = departureLoungeLock.tryLock(30, TimeUnit.SECONDS);
            if (portLock) {
                int numberOfPassengersToLounge=passengersCount(numberOfPassengers);
                boolean result1 = doMoveFromPlaneToLounge(lounge,numberOfPassengersToLounge);
                swapTickets();
                boolean result2 = doMoveFromPlaneAndLeaveAirport(lounge, numberOfPassengers-numberOfPassengersToLounge);
                if(result1&&result2)
                    result=true;
            }
        } finally{
            if (portLock) {
                departureLoungeLock.unlock();
            }
        }

        return result;
    }
    private int passengersCount(int between) {
        Random random = new Random();
        return random.nextInt(between) + 1;
    }
    private boolean doMoveFromPlaneAndLeaveAirport(Seats planeSeats, int numberOfPassengers) throws InterruptedException{
        Lock planeSeatsLock = planeSeats.getLock();
        boolean planeLock = false;

        try{
            planeLock = planeSeatsLock.tryLock(30, TimeUnit.SECONDS);
            if (planeLock) {
                if(planeSeats.getNumberOfPassengers() >= numberOfPassengers){
                    List<Passenger> passengers = planeSeats.getPassengers(numberOfPassengers);
                    Iterator it = passengers.iterator();
                    while (it.hasNext()) {
                        Passenger temp =  (Passenger)it.next();
                        logger.debug("Пассажир " + temp.getName() + " " + temp.getLastName() + " " + temp.getMiddleName() +
                                    " покинул аэропорт, по прилёту самолёта");
                        it.remove();
                        }

                    return true;
                }
            }
        }finally{
            if (planeLock) {
                planeSeatsLock.unlock();
            }
        }

        return false;
    }

   protected void swapTickets(){
       int numberOfPeopleChangingTicket=passengersCount(departureLounge.getNumberOfPassengers()-departureLounge.getNumberOfPassengers()/2);
       for(int i=0;i<numberOfPeopleChangingTicket/2;i++){
           int firstPerson=passengersCount(departureLounge.getNumberOfPassengers()-1);
           int secondPerson=passengersCount(departureLounge.getNumberOfPassengers()-1);
           List<Passenger> passengers=departureLounge.getAllPassengers();
           Ticket ticket1=passengers.get(firstPerson).getTicket();
           Ticket ticket2=passengers.get(secondPerson).getTicket();
           logger.debug("Пассажир " + passengers.get(firstPerson).getName() + " " + passengers.get(firstPerson).getLastName()
                   + " " + passengers.get(firstPerson).getMiddleName() +
                   " Меняется билетом "+ passengers.get(firstPerson).getTicket().getId()+
                   " c пассажиром " + passengers.get(secondPerson).getName() + " " + passengers.get(secondPerson).getLastName()
                   + " " + passengers.get(secondPerson).getMiddleName() +
                   " на билет "+ passengers.get(secondPerson).getTicket().getId());
           passengers.get(firstPerson).setTicket(ticket2);
           passengers.get(secondPerson).setTicket(ticket1);

       }
   }

    private boolean doMoveFromPlaneToLounge(Seats planeSeats, int numberOfPassengers) throws InterruptedException{
        Lock planeSeatsLock = planeSeats.getLock();
        boolean planeLock = false;

        try{
            planeLock = planeSeatsLock.tryLock(30, TimeUnit.SECONDS);
            if (planeLock) {
                if(planeSeats.getNumberOfPassengers() >= numberOfPassengers){
                    List<Passenger> passengers = planeSeats.getPassengers(numberOfPassengers);
                    departureLounge.addPassengers(passengers);
                   ticketInspection();
                    return true;
                }
            }
        }finally{
            if (planeLock) {
                planeSeatsLock.unlock();
            }
        }

        return false;
    }
//посадка
    public boolean boarding(Seats seats, int numberOfPassengers) throws InterruptedException {
        boolean result = false;
        Lock departureLoungeLock = departureLounge.getLock();
        boolean airportLock = false;
        try{
            airportLock = departureLoungeLock.tryLock(30, TimeUnit.SECONDS);
            if (airportLock) {
                ticketInspection();
                if (numberOfPassengers <= departureLounge.getNumberOfPassengers()) {
                    result = doMoveToPlane(seats, numberOfPassengers);
                }
            }
        } finally{
            if (airportLock) {
                departureLoungeLock.unlock();
            }
        }

        return result;
    }
    protected void ticketInspection(){
        List<Passenger> passengers = departureLounge.getAllPassengers();
        Iterator it = passengers.iterator();
        while (it.hasNext()) {
            Passenger temp =  (Passenger)it.next();
            Calendar cal = Calendar.getInstance();
            Date today = cal.getTime();
            if(today.compareTo(temp.getTicket().getDateOfAction())>0){
                logger.debug("Пассажир " + temp.getName() + " " + temp.getLastName() + " " + temp.getMiddleName() +
                        " покинул аэропорт, поскольку истёк срок действия билета");
               it.remove();
            }
        }
    }

    private boolean doMoveToPlane(Seats planeSeats, int numberOfPassengers) throws InterruptedException{
        Lock planeSeatsLock = planeSeats.getLock();
        boolean planeLock = false;

        try{
            planeLock = planeSeatsLock.tryLock(30, TimeUnit.SECONDS);
            if (planeLock) {
                int newPassengerCount = planeSeats.getNumberOfPassengers() + numberOfPassengers;
                if(newPassengerCount <= planeSeats.getTotalNumber()){
                    List<Passenger> passengers = departureLounge.getPassengers(numberOfPassengers);
                    planeSeats.addPassengers(passengers);
                    return true;
                }
            }
        }finally{
            if (planeLock) {
                planeSeatsLock.unlock();
            }
        }
        return false;
    }
}
