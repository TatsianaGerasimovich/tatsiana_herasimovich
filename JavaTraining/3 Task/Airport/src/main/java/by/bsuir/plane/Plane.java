package by.bsuir.plane;

import java.util.List;
import java.util.Random;

import by.bsuir.airport.Airport;
import by.bsuir.airport.AirportException;
import by.bsuir.airport.Terminal;
import by.bsuir.passenger.Passenger;
import by.bsuir.passenger.Seats;
import org.apache.log4j.Logger;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class Plane implements Runnable {

    private final static Logger logger = Logger.getLogger(Plane.class);
    private volatile boolean stopThread = false;

    private String name;
    private String typeOfAircraft;
    private Airport airport;
    private Seats seatsOnThePlane;

    public Plane(String name,String typeOfAircraft, Airport airport, int numberOfSeats) {
        this.name = name;
        this.typeOfAircraft=typeOfAircraft;
        this.airport = airport;
        seatsOnThePlane = new Seats(numberOfSeats);
    }

    public void setPassengersOnBoard(List<Passenger> passengers) {
        seatsOnThePlane.addPassengers(passengers);
    }

    public String getTypeOfAircraft() {
        return typeOfAircraft;
    }

    public String getName() {
        return name;
    }

    public void stopThread() {
        stopThread = true;
    }

    public void run() {
        try {
            while (!stopThread) {
                fly();
                landing();
            }
            logger.debug("Самолёт"+typeOfAircraft+" - "+ name+" выведен из эксплуатации.");
        } catch (InterruptedException e) {
            logger.error("Самолёт разбился", e);
        } catch (AirportException e) {
            logger.error("Самолёт"+typeOfAircraft+" - "+ name+" выведен из эксплуатации.", e);
        }
    }

    private void fly() throws InterruptedException {
        Thread.sleep(1000);
    }


    private void landing() throws AirportException, InterruptedException {

        boolean isLockedTerminal = false;
        Terminal terminal = null;
        try {
            isLockedTerminal = airport.lockTerminal(this);

            if (isLockedTerminal) {
               terminal = airport.getTerminal(this);
               logger.debug("Самолёт " +typeOfAircraft+" - "+ name + " приземлился в терминале " + terminal.getId());
                PlaneAction action = getNextAction();
                executeAction(action, terminal);
            } else {
                logger.debug("Самолёту " +typeOfAircraft+" - "+ name + " отказано в посадке ");
            }
        } finally {
            if (isLockedTerminal){
                airport.unlockTerminate(this);
               logger.debug("Самолёт " +typeOfAircraft+" - "+ name + " взлетел из терминала " + terminal.getId());
            }
        }

    }

    private void executeAction(PlaneAction action, Terminal terminal) throws InterruptedException {
        switch (action) {
            case LOAD_FROM_PLANE:
                MoveFromPlane(terminal);
                break;
            case LOAD_TO_PLANE:
                MoveToPlane(terminal);
                break;
        }
    }

    private boolean MoveFromPlane(Terminal terminal) throws InterruptedException {

        int passengersNumberToMove = passengersCount();
        boolean result = false;

        logger.debug("Самолёт " +typeOfAircraft+" - "+ name + " хочет высадить " + passengersNumberToMove
                + " человек.");

        result = terminal.disembarkationOfPassengers(seatsOnThePlane, passengersNumberToMove);

        if (!result) {
            logger.debug("На борту самолёта "
                    +typeOfAircraft+" - "+ name + "нету " + passengersNumberToMove + " человек.");
        } else {
            logger.debug("Самолёт " +typeOfAircraft+" - "+ name + " осуществил высадку " + passengersNumberToMove
                    + " человек в аэропорт.");

        }
        return result;
    }

    private boolean MoveToPlane(Terminal terminal) throws InterruptedException {

        int passengersNumberToMove = passengersCount();

        boolean result = false;

        logger.debug("Самолёт " +typeOfAircraft+" - "+ name + " хочет хочет осуществить посадку " + passengersNumberToMove
                + " человек на свой борт.");

        result = terminal.boarding(seatsOnThePlane, passengersNumberToMove);

        if (result) {
            logger.debug("Самолёт " +typeOfAircraft+" - "+ name + " осуществил посадку " + passengersNumberToMove
                    + " человек на борт.");
        } else {
            logger.debug("Недостаточно свободных мест на самолёте " +typeOfAircraft+" - "+ name
                    + " для посадки " + passengersNumberToMove + " человек из аэропорта.");
        }

        return result;
    }

    private int passengersCount() {
        Random random = new Random();
        return random.nextInt(20) + 1;
    }

    private PlaneAction getNextAction() {
        Random random = new Random();
        int value = random.nextInt(4000);
        if (value < 1000) {
            return PlaneAction.LOAD_FROM_PLANE;
        } else if (value < 2000) {
            return PlaneAction.LOAD_TO_PLANE;
        }
        return PlaneAction.LOAD_FROM_PLANE;
    }

    enum PlaneAction {
        LOAD_FROM_PLANE, LOAD_TO_PLANE
    }
}

