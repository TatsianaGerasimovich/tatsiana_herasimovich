package by.bsuir.passenger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class DepartureLounge {
    private List<Passenger> passengerList;
    private Lock lock;

    public DepartureLounge() {
        passengerList = new ArrayList<Passenger>();
        lock = new ReentrantLock();
    }

    public boolean addPassenger(Passenger passenger) {
        return passengerList.add(passenger);
    }

    public boolean addPassengers(List<Passenger> passengers) {
        return passengerList.addAll(passengers);
    }

    public List<Passenger> getPassengers(int amount) {
        if (passengerList.size() >= amount) {
            List<Passenger> cargo = new ArrayList<Passenger>(passengerList.subList(0, amount));
            passengerList.removeAll(cargo);
            return cargo;
        }
        return null;
    }
    public List<Passenger> getAllPassengers() {
        return passengerList;
    }

    public int getNumberOfPassengers(){
        return passengerList.size();
    }

    public Lock getLock(){
        return lock;
    }
}
