package by.bsuir.passenger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class Seats {
    private List<Passenger> passengerList;
    private int totalNumber;
    private Lock lock;

    public Seats(int totalNumber) {
        passengerList = new ArrayList<Passenger>(totalNumber);
        lock = new ReentrantLock();
        this.totalNumber = totalNumber;
    }

    public boolean addPassenger(Passenger passenger) {
        return passengerList.add(passenger);
    }

    public boolean addPassengers(List<Passenger> passengers) {
        boolean result = false;
        if(passengerList.size() + passengers.size() <= totalNumber){
            result = passengerList.addAll(passengers);
        }
        return result;
    }

    public Passenger getPassenger() {
        if (passengerList.size() > 0) {
            return passengerList.remove(0);
        }
        return null;
    }

    public List<Passenger> getPassengers(int amount) {
        if (passengerList.size() >= amount) {
            List<Passenger> cargo = new ArrayList<Passenger>(passengerList.subList(0, amount));
            passengerList.removeAll(cargo);
            return cargo;
        }
        return null;
    }

    public int getTotalNumber(){
        return totalNumber;
    }

    public int getNumberOfPassengers(){
        return passengerList.size();
    }

    public Lock getLock(){
        return lock;
    }
}
