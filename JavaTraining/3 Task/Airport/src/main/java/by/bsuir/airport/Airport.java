package by.bsuir.airport;

import by.bsuir.passenger.DepartureLounge;
import by.bsuir.passenger.Passenger;
import by.bsuir.plane.Plane;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class Airport {
    private final static Logger logger = Logger.getLogger(Airport.class);

    private BlockingQueue<Terminal> terminalList; // очередь терминалов
    private DepartureLounge departureLounge; // зал ожидания в аэропорте

    private Map<Plane, Terminal> usedTerminals;

    public Airport(int terminalNumber) {
        departureLounge = new DepartureLounge(); // создаем зал ожидания
        terminalList = new ArrayBlockingQueue<Terminal>(terminalNumber); // создаем очередь терминалов
        for (int i = 0; i < terminalNumber; i++) { // заполняем очередь терминалов непосредственно самими терминалами
            terminalList.add(new Terminal(i, departureLounge));
        }
        usedTerminals = new HashMap<Plane, Terminal>();
        logger.debug("Аэропорт создан.");
    }

    public void setPassengersDepartureLounge(List<Passenger> passengers){
        departureLounge.addPassengers(passengers);
    }

    public DepartureLounge getDepartureLounge() {
        return departureLounge;
    }

    public boolean lockTerminal(Plane plane) {
        Terminal terminal;
        try {
            terminal = terminalList.take();
            usedTerminals.put(plane, terminal);
        } catch (InterruptedException e) {
            logger.debug("Самолёту " + plane.getName() + " отказано в посадке.");
            return false;
        }
        return true;
    }


    public boolean unlockTerminate(Plane plane) {
        Terminal terminal = usedTerminals.get(plane);
        try {
            terminalList.put(terminal);
            usedTerminals.remove(plane);
        } catch (InterruptedException e) {
            logger.debug("Самолёт " + plane.getName() + " не смог взлететь.");
            return false;
        }
        return true;
    }

    public Terminal getTerminal(Plane plane) throws AirportException {

        Terminal terminal = usedTerminals.get(plane);
        if (terminal == null){
            throw new AirportException("Данный самолёт не находится ни в одном терминале.");
        }
        return terminal;
    }
}
