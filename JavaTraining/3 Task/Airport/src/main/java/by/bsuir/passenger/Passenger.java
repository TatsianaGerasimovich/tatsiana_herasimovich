package by.bsuir.passenger;

import java.text.ParseException;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class Passenger {
    private Ticket ticket;
    private String name;
    private String lastName;
    private String middleName;

    public Passenger(int i, String date,String name, String lastName, String middleName ) throws ParseException {
        ticket = new Ticket(i,date);
        this.name=name;
        this.lastName=lastName;
        this.middleName=middleName;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public String getName() {
        return name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Passenger)) return false;

        Passenger passenger = (Passenger) o;

        if (!lastName.equals(passenger.lastName)) return false;
        if (!middleName.equals(passenger.middleName)) return false;
        if (!name.equals(passenger.name)) return false;
        if (!ticket.equals(passenger.ticket)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ticket.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + middleName.hashCode();
        return result;
    }
}
