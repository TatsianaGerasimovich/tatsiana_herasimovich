package by.bsuir.passenger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Tatiana
 * @version 1.00 14.04.2015.
 */
public class Ticket {
    private int id;
    private Date dateOfAction;

    public Ticket(int id, String dateOfAction) throws ParseException {
        this.id = id;
        SimpleDateFormat sdf = new SimpleDateFormat ("dd.MM.yyyy");
        this.dateOfAction = sdf.parse (dateOfAction);
    }

    public int getId(){
        return id;
    }

    public Date getDateOfAction() {
        return dateOfAction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticket)) return false;

        Ticket ticket = (Ticket) o;

        if (id != ticket.id) return false;
        if (!dateOfAction.equals(ticket.dateOfAction)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + dateOfAction.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", dateOfAction=" + dateOfAction +
                '}';
    }
}
