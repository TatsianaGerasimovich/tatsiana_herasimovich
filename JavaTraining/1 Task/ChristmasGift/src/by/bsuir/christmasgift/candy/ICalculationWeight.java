package by.bsuir.christmasgift.candy;

/**
 * Interface with the method of calculating the total weight of the candy.
 * @author Tatiana
 * @version 1.00 25.03.2015.
 */
public interface ICalculationWeight {
    /**
     * Abstract method of calculating the total weight of the candy by its net weight.
     *
     * @return total weight of the candy
     */
    public int countWeight();
}
