package by.bsuir.christmasgift.candy;

/**
 * Abstract class for all sweets.
 *
 * @author Tatiana
 * @version 1.00 25.03.2015.
 */
abstract public class Candy implements ICalculationWeight {
    /**
     * Name of the candy.
     */
    protected String mName;
    /**
     * Level of sugar in candy.
     */
    protected double mBloodSugar;
    /**
     * Net weight of candy.
     */
    protected int mNetWeight;
    /**
     * The full weight of candy, including wrapper and other additions.
     */
    protected int mWeight;

    /**
     * Constructor default.
     */
    public Candy() {
        mName = new String();
        mBloodSugar = 0.0;
        mNetWeight = 0;
        mWeight = 0;
    }

    /**
     * Abstract method to establish certain name of candy.
     */

    abstract protected void generateName();

    /**
     * Abstract method to establish certain level of sugar in candy.
     */

    abstract protected void generateBloodSugar();

    /**
     * Getter for name.
     *
     * @return name of candy
     */
    public String getName() {
        return mName;
    }

    /**
     * Getter for level of sugar in candy.
     *
     * @return level of sugar
     */

    public double getBloodSugar() {
        return mBloodSugar;
    }

    /**
     * Getter for full weight of candy.
     *
     * @return full weight of candy
     */
    public int getWeight() {
        return mWeight;
    }

    /**
     * Getter for net weight of candy.
     *
     * @return net weight of candy
     */
    public int getNetWeight() {
        return mNetWeight;
    }

    /**
     * Setter for net weight of candy.
     *
     * @param netWeight value of net weight
     */
    public void setNetWeight(int netWeight) {
        mNetWeight = netWeight;
        setWeight();
    }

    /**
     * Setter for full weight of candy, value is obtained by counting.
     */
    protected void setWeight() {
        mWeight = countWeight();
    }

    /**
     * Override method of calculating the total weight of the candy by its net weight
     *
     * @return total weight of the candy
     */
    @Override
    public int countWeight() {
        int weight;
        weight = (int) ((mNetWeight * 1.07) * 1.01);
        return weight;
    }

    /**
     * Overrides equals() method. It performs general agreements on the comparison of the contents of two objects.
     *
     * @param obj
     * @return boolean value
     */
    /**
     * Returns true if and only if the argument is not null and is a Candy object that represents the same value of all fields of the class as this object.
     *
     * @param obj - the object to compare with
     * @return true if the Candy objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() == obj.getClass()) {
            Candy temp = (Candy) obj;
            return this.mBloodSugar == temp.mBloodSugar
                    && mName.equals(temp.mName)
                    && this.mNetWeight == temp.mNetWeight
                    && this.mWeight == temp.mWeight;
        } else
            return false;
    }

    /**
     * Returns a hash code for a integer value.
     *
     * @return hash code for this Tomato object
     */
    @Override
    public int hashCode() {
        return (int) (31 * mBloodSugar + mWeight + 18 * mNetWeight
                + ((mName == null) ? 0 : mName.hashCode()));
    }

    /**
     * Overrides toString() method.
     *
     * @return representation of the object as a string
     */
    @Override
    public String toString() {
        return getClass().getName() + "@name: " + mName
                + " blood sugar: " + mBloodSugar
                + " net weight: " + mNetWeight + " weight: " + mWeight;
    }

}
