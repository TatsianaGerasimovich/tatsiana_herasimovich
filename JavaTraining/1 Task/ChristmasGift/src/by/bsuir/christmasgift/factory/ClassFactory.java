package by.bsuir.christmasgift.factory;

import by.bsuir.christmasgift.candy.Candy;
import by.bsuir.christmasgift.chocolate.Jelly;
import by.bsuir.christmasgift.chocolate.LiqueurChocolate;
import by.bsuir.christmasgift.chocolate.WaffleChocolate;
import by.bsuir.christmasgift.sweetmeat.Butterscotch;
import by.bsuir.christmasgift.sweetmeat.FilledSweet;
import by.bsuir.christmasgift.sweetmeat.FruitSweetmeat;

/**
 * Factory class that generates the objects of particular classes.
 * @author Tatiana
 * @version 1.00 28.03.2015.
 */
public class ClassFactory {
    /**
     * Enumeration of classes that factory can create.
     */
    private enum  Signs {JELLY, LIQUEURCHOCOLATE, WAFFLECHOCOLATE, BUTTERSCOTCH, FILLEDSWEET, FRUITSWEETMEAT}

    /**
     * Create specified candies.
     * @param id - type of Candy to create
     * @return
     */
    public static Candy getClassFromFactory(String id) {
        Signs sign = Signs.valueOf(id.toUpperCase());
        switch(sign){
            case JELLY  : return new Jelly();
            case LIQUEURCHOCOLATE : return new LiqueurChocolate();
            case WAFFLECHOCOLATE  : return new WaffleChocolate();
            case BUTTERSCOTCH : return new Butterscotch();
            case FILLEDSWEET  : return new FilledSweet();
            case FRUITSWEETMEAT : return new FruitSweetmeat();
            default : throw new EnumConstantNotPresentException(
                    Signs.class, sign.name());
        }
    }

}
