package by.bsuir.christmasgift.controller;

import by.bsuir.christmasgift.candy.Candy;
import by.bsuir.christmasgift.chocolate.Jelly;
import by.bsuir.christmasgift.chocolate.LiqueurChocolate;
import by.bsuir.christmasgift.chocolate.WaffleChocolate;
import by.bsuir.christmasgift.factory.ClassFactory;
import by.bsuir.christmasgift.gift.ChristmasGift;
import by.bsuir.christmasgift.sweetmeat.Butterscotch;
import by.bsuir.christmasgift.sweetmeat.FilledSweet;
import by.bsuir.christmasgift.sweetmeat.FruitSweetmeat;
import org.apache.log4j.Logger;

import java.util.List;


/**
 * Main class of application
 *
 * @author Tatiana
 * @version 1.00 28.03.2015.
 */
public class MainClass {

    private static final Logger logger = Logger.getLogger(MainClass.class);

    /**
     * Output method of founded candies list.
     *
     * @param list
     */

    private static void printList(List<Candy> list) {
        if (list.isEmpty())
            System.out.println("There isn't candies with this blood sugar!");
        else for (Candy obj : list) {
            System.out.println("Found candy: " + obj.toString());
        }
    }

    /**
     * Main method of application.
     *
     * @param args - command line parameters
     */
    public static void main(String[] args) {
        try {

            Candy obJelly = ClassFactory.getClassFromFactory("Jelly");
            Candy obLiqueur = ClassFactory.getClassFromFactory("LiqueurChocolate");
            Candy obWaffle = ClassFactory.getClassFromFactory("WaffleChocolate");
            Candy obFilled = ClassFactory.getClassFromFactory("FilledSweet");
            Candy obFruit = ClassFactory.getClassFromFactory("FruitSweetmeat");
            Candy obButterscotch = ClassFactory.getClassFromFactory("Butterscotch");

            ChristmasGift gift = new ChristmasGift();

            ClassInitialization.initializationJelly((Jelly) obJelly);
            ClassInitialization.initializationLiqueurChocolate((LiqueurChocolate) obLiqueur);
            ClassInitialization.initializationWaffleChocolate((WaffleChocolate) obWaffle);
            ClassInitialization.initializationFilledSweet((FilledSweet) obFilled);
            ClassInitialization.initializationFruitSweetmeat((FruitSweetmeat) obFruit);
            ClassInitialization.initializationButterscotch((Butterscotch) obButterscotch);


            gift.addCandy(obJelly);
            gift.addCandy(obLiqueur);
            gift.addCandy(obWaffle);
            gift.addCandy(obFilled);
            gift.addCandy(obFruit);
            gift.addCandy(obButterscotch);


            gift.countWeight();
            System.out.println(gift.toString());
            gift.sortByName();
            System.out.println("Result of sort by name:");
            System.out.println(gift.toString());
            gift.sortByBloodSugar();
            System.out.println("Result of sort by blood sugar:");
            System.out.println(gift.toString());
            gift.sortByNetWeight();
            System.out.println("Result of sort by net weight:");
            System.out.println(gift.toString());

            System.out.println("Result of search:");
            printList(gift.search(7.2, 9.2));

        } catch (EnumConstantNotPresentException ex) {
            logger.error("Error in choosing constant of enum!", ex);
        } catch (Exception ex) {
            logger.error("Error!", ex);
        }
    }
}
