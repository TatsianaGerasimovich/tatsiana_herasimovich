package by.bsuir.christmasgift.controller;

import by.bsuir.christmasgift.chocolate.Jelly;
import by.bsuir.christmasgift.chocolate.LiqueurChocolate;
import by.bsuir.christmasgift.chocolate.WaffleChocolate;
import by.bsuir.christmasgift.sweetmeat.Butterscotch;
import by.bsuir.christmasgift.sweetmeat.FilledSweet;
import by.bsuir.christmasgift.sweetmeat.FruitSweetmeat;

/**
 * Class is intended to initialize objects of certain classes.
 *
 * @author Tatiana
 * @version 1.00 28.03.2015.
 */
public class ClassInitialization {
    /**
     * Static method to initialize object of class Jelly.
     *
     * @param obj - object of class Jelly, which must be initialized
     */
    public static void initializationJelly(Jelly obj) {
        obj.setNetWeight(120);
        obj.setTypeOfPacking("Candy Wrapper");
        obj.setTasteOfJelly("Apple");
    }

    /**
     * Static method to initialize object of class LiqueurChocolate.
     *
     * @param obj - object of class LiqueurChocolate, which must be initialized
     */
    public static void initializationLiqueurChocolate(LiqueurChocolate obj) {
        obj.setNetWeight(150);
        obj.setTypeOfPacking("Capsule");
        obj.setPercentageOfAlcohol(18);
        obj.setTypeOfLiqueur("Malibu");
    }

    /**
     * Static method to initialize object of class WaffleChocolate.
     *
     * @param obj - object of class WaffleChocolate, which must be initialized
     */
    public static void initializationWaffleChocolate(WaffleChocolate obj) {
        obj.setNetWeight(150);
        obj.setTypeOfPacking("Capsule");
        obj.setNumberOfWaffleLayer(4);
    }

    /**
     * Static method to initialize object of class Butterscotch.
     *
     * @param obj - object of class Butterscotch, which must be initialized
     */
    public static void initializationButterscotch(Butterscotch obj) {
        obj.setNetWeight(150);
        obj.setColorPacking("Blue");
        obj.setTypeOfSolid("Golden Key");
    }

    /**
     * Static method to initialize object of class FilledSweet.
     *
     * @param obj - object of class FilledSweet, which must be initialized
     */
    public static void initializationFilledSweet(FilledSweet obj) {
        obj.setNetWeight(150);
        obj.setColorPacking("Blue");
        obj.setTasteOfFilling("Peach");
    }

    /**
     * Static method to initialize object of class FruitSweetmeat.
     *
     * @param obj - object of class FruitSweetmeat, which must be initialized
     */
    public static void initializationFruitSweetmeat(FruitSweetmeat obj) {
        obj.setNetWeight(150);
        obj.setColorPacking("Blue");
        obj.setTasteOfFruit("Orange");
    }
}
