package by.bsuir.christmasgift.chocolate;

/**
 * Class of waffle chocolates extends class Chocolate.
 *
 * @author Tatiana
 * @version 1.00 26.03.2015.
 */
public class WaffleChocolate extends Chocolate {
    /**
     * number of layers of waffle, which is used in candy.
     */
    private int mNumberOfWaffleLayer;

    /**
     * Constructor default.
     */
    public WaffleChocolate() {
        super();
        mNumberOfWaffleLayer = 0;
        generateName();
        generateBloodSugar();

    }

    /**
     * Setter for number of layers of waffle.
     *
     * @param numberOfWaffleLayer
     */
    public void setNumberOfWaffleLayer(int numberOfWaffleLayer) {
        mNumberOfWaffleLayer = numberOfWaffleLayer;
    }

    /**
     * Overrides method to establish certain name of candy.
     */
    @Override
    protected void generateName() {
        mName = "Waffle Chocolate";
    }

    /**
     * Overrides method to establish certain level of sugar in candy.
     */
    @Override
    protected void generateBloodSugar() {
        mBloodSugar = 12.3;
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a WaffleChocolate object that represents the same int value as this object.
     *
     * @param obj - the object to compare with
     * @return true if the WaffleChocolate objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() == obj.getClass()) {
            WaffleChocolate temp = (WaffleChocolate) obj;
            return super.equals(obj)
                    && this.mNumberOfWaffleLayer == temp.mNumberOfWaffleLayer;
        } else
            return false;
    }

    /**
     * Returns a hash code for a integer value and superclass Chocolate.
     *
     * @return hash code for this WaffleChocolate object
     */
    @Override
    public int hashCode() {
        return (int) (super.hashCode() + mNumberOfWaffleLayer * 4);
    }

    /**
     * Overrides toString() method.
     *
     * @return representation of the object as a string
     */
    @Override
    public String toString() {
        return super.toString() + getClass().getName() + " @number of waffle layer: " + mNumberOfWaffleLayer;
    }

}
