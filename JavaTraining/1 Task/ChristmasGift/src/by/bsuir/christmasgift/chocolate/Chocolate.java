package by.bsuir.christmasgift.chocolate;

import by.bsuir.christmasgift.candy.Candy;

/**
 * Class of chocolates extends the class Candy.
 * @author Tatiana
 * @version 1.00 26.03.2015.
 */
public class Chocolate extends Candy {
    /**
     * Type of packaging for candy.
     */
    protected String mTypeOfPacking;

    /**
     * Constructor default.
     */
    public Chocolate() {
        super();
        mTypeOfPacking = new String();
        generateName();
        generateBloodSugar();
    }

    /**
     * Setter for type of packaging.
     *
     * @param typeOfPacking
     */
    public void setTypeOfPacking(String typeOfPacking) {
        mTypeOfPacking = typeOfPacking;
    }

    /**
     * Overrides method to establish certain name of candy.
     */
    @Override
    protected void generateName() {
        mName = "Unknown_Chocolate";
    }

    /**
     * Overrides method to establish certain level of sugar in candy.
     */
    @Override
    protected void generateBloodSugar() {
        mBloodSugar = 0.0;
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a Chocolate object that represents the same String value as this object.
     *
     * @param obj - the object to compare with
     * @return true if the Chocolate objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() == obj.getClass()) {
            Chocolate temp = (Chocolate) obj;
            return super.equals(obj)
                    && mTypeOfPacking.equals(temp.mTypeOfPacking);
        } else
            return false;
    }

    /**
     * Returns a hash code for a integer value and superclass Candy
     *
     * @return hash code for this Chocolate object
     */
    @Override
    public int hashCode() {
        return (int) (super.hashCode()
                + ((mTypeOfPacking == null) ? 0 : mTypeOfPacking.hashCode()));
    }

    /**
     * Overrides toString() method
     *
     * @return representation of the object as a string
     */
    @Override
    public String toString() {
        return super.toString() + getClass().getName() + " @type of packing: " + mTypeOfPacking;
    }

}
