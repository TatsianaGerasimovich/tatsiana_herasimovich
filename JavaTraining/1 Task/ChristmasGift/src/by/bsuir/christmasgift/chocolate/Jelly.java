package by.bsuir.christmasgift.chocolate;

/**
 * Class of jellies extends class Chocolate.
 *
 * @author Tatiana
 * @version 1.00 27.03.2015.
 */
public class Jelly extends Chocolate {
    /**
     * Taste of jelly, which is used in candy.
     */
    private String mTasteOfJelly;

    /**
     * Constructor default.
     */
    public Jelly() {
        super();
        mTasteOfJelly = new String();
        generateName();
        generateBloodSugar();

    }

    /**
     * Setter for taste of jelly.
     *
     * @param tasteOfJelly
     */
    public void setTasteOfJelly(String tasteOfJelly) {
        mTasteOfJelly = tasteOfJelly;
    }

    /**
     * Overrides method to establish certain name of candy.
     */
    @Override
    protected void generateName() {
        mName = "Jelly";
    }

    /**
     * Overrides method to establish certain level of sugar in candy.
     */
    @Override
    protected void generateBloodSugar() {
        mBloodSugar = 9.6;
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a Jelly object that represents the same String value as this object.
     *
     * @param obj - the object to compare with
     * @return true if the Jelly objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() == obj.getClass()) {
            Jelly temp = (Jelly) obj;
            return super.equals(obj) &&
                    mTasteOfJelly.equals(temp.mTasteOfJelly);
        } else
            return false;
    }

    /**
     * Returns a hash code for a integer value and superclass Chocolate.
     *
     * @return hash code for this Jelly object
     */
    @Override
    public int hashCode() {
        return (int) (super.hashCode()
                + ((mTasteOfJelly == null) ? 0 : mTasteOfJelly.hashCode()));
    }

    /**
     * Overrides toString() method.
     *
     * @return representation of the object as a string
     */
    @Override
    public String toString() {
        return super.toString() + getClass().getName() + " @taste of jelly: " + mTasteOfJelly;
    }
}
