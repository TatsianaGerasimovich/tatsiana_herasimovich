package by.bsuir.christmasgift.sweetmeat;

import by.bsuir.christmasgift.candy.Candy;

/**
 * Class for sweetmeat extends the class Candy.
 *
 * @author Tatiana
 * @version 1.00 25.03.2015.
 */
public class Sweetmeat extends Candy {
    /**
     * Color packing for candy.
     */
    protected String mColorPacking;

    /**
     * Constructor default.
     */
    public Sweetmeat() {
        super();
        mColorPacking = new String();
        generateName();
        generateBloodSugar();
    }

    /**
     * Setter for color packing.
     *
     * @param colorPacking
     */
    public void setColorPacking(String colorPacking) {
        mColorPacking = colorPacking;
    }

    /**
     * Overrides method to establish certain name of candy.
     */
    @Override
    protected void generateName() {
        mName = "Unknown_Sweetmeat";
    }

    /**
     * Overrides method to establish certain level of sugar in candy.
     */
    @Override
    protected void generateBloodSugar() {
        mBloodSugar = 0.0;
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a Sweetmeat object that represents the same String value as this object.
     *
     * @param obj - the object to compare with
     * @return true if the Sweetmeat objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() == obj.getClass()) {
            Sweetmeat temp = (Sweetmeat) obj;
            return super.equals(obj)
                    && mColorPacking.equals(temp.mColorPacking);
        } else
            return false;
    }

    /**
     * Returns a hash code for a integer value and superclass Candy.
     *
     * @return hash code for this Sweetmeat object
     */
    @Override
    public int hashCode() {
        return (int) (super.hashCode()
                + ((mColorPacking == null) ? 0 : mColorPacking.hashCode()));
    }

    /**
     * Overrides toString() method.
     *
     * @return representation of the object as a string
     */
    @Override
    public String toString() {
        return super.toString() + getClass().getName() + " @color packing: " + mColorPacking;
    }

}
