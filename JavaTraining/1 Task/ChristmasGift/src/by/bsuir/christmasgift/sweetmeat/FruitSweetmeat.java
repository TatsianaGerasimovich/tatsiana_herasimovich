package by.bsuir.christmasgift.sweetmeat;

/**
 * Class for fruit sweetmeats extends class Sweetmeat.
 *
 * @author Tatiana
 * @version 1.00 25.03.2015.
 */
public class FruitSweetmeat extends Sweetmeat {
    /**
     * Taste of the fruit, which is used in candy.
     */
    private String mTasteOfFruit;

    /**
     * Constructor default.
     */
    public FruitSweetmeat() {
        super();
        mTasteOfFruit = new String();
        generateName();
        generateBloodSugar();

    }

    /**
     * Setter for taste of the fruit.
     *
     * @param tasteOfFruit
     */
    public void setTasteOfFruit(String tasteOfFruit) {
        mTasteOfFruit = tasteOfFruit;
    }

    /**
     * Overrides method to establish certain name of candy.
     */
    @Override
    protected void generateName() {
        mName = "Fruit Sweetmeat";
    }

    /**
     * Overrides method to establish certain level of sugar in candy.
     */
    @Override
    protected void generateBloodSugar() {
        mBloodSugar = 9.1;
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a FruitSweetmeat object that represents the same String value as this object.
     *
     * @param obj - the object to compare with
     * @return true if the FruitSweetmeat objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() == obj.getClass()) {
            FruitSweetmeat temp = (FruitSweetmeat) obj;
            return super.equals(obj) &&
                    mTasteOfFruit.equals(temp.mTasteOfFruit);
        } else
            return false;
    }

    /**
     * Returns a hash code for a integer value and superclass Sweetmeat.
     *
     * @return hash code for this FruitSweetmeat object
     */
    @Override
    public int hashCode() {
        return (int) (super.hashCode()
                + ((mTasteOfFruit == null) ? 0 : mTasteOfFruit.hashCode()));
    }

    /**
     * Overrides toString() method.
     *
     * @return representation of the object as a string
     */
    @Override
    public String toString() {
        return super.toString() + getClass().getName() + " @taste of fruit: " + mTasteOfFruit;
    }

}
