package by.bsuir.christmasgift.sweetmeat;

/**
 * Class for toffee extends class Sweetmeat.
 *
 * @author Tatiana
 * @version 1.00 26.03.2015.
 */
public class Butterscotch extends Sweetmeat {
    /**
     * Type of hardness butterscotch.
     */
    private String mTypeOfSolid;

    /**
     * Constructor default.
     */
    public Butterscotch() {
        super();
        mTypeOfSolid = new String();
        generateName();
        generateBloodSugar();

    }

    /**
     * Setter for hardness of butterscotch.
     *
     * @param typeOfSolid
     */
    public void setTypeOfSolid(String typeOfSolid) {
        mTypeOfSolid = typeOfSolid;
    }

    /**
     * Overrides method to establish certain name of candy.
     */
    @Override
    protected void generateName() {
        mName = "Butterscotch";
    }

    /**
     * Overrides method to establish certain level of sugar in candy.
     */
    @Override
    protected void generateBloodSugar() {
        mBloodSugar = 7.3;
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a Butterscotch object that represents the same String value as this object.
     *
     * @param obj - the object to compare with
     * @return true if the Butterscotch objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() == obj.getClass()) {
            Butterscotch temp = (Butterscotch) obj;
            return super.equals(obj) &&
                    mTypeOfSolid.equals(temp.mTypeOfSolid);
        } else
            return false;
    }

    /**
     * Returns a hash code for a integer value and superclass Sweetmeat.
     *
     * @return hash code for this Butterscotch object
     */
    @Override
    public int hashCode() {
        return (int) (super.hashCode()
                + ((mTypeOfSolid == null) ? 0 : mTypeOfSolid.hashCode()));
    }

    /**
     * Overrides toString() method.
     *
     * @return representation of the object as a string
     */
    @Override
    public String toString() {
        return super.toString() + getClass().getName() + " @type of solid: " + mTypeOfSolid;
    }

}
