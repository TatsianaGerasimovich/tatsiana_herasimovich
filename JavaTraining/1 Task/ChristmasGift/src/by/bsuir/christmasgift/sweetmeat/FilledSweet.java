package by.bsuir.christmasgift.sweetmeat;

/**
 * Class for candies with filling extends class Sweetmeat.
 *
 * @author Tatiana
 * @version 1.00 25.03.2015.
 */
public class FilledSweet extends Sweetmeat {
    /**
     * Taste of the filling, which is used in candy.
     */
    private String mTasteOfFilling;

    /**
     * Constructor default.
     */
    public FilledSweet() {
        super();
        mTasteOfFilling = new String();
        generateName();
        generateBloodSugar();

    }

    /**
     * Setter for taste of the filling.
     *
     * @param tasteOfFilling
     */
    public void setTasteOfFilling(String tasteOfFilling) {
        mTasteOfFilling = tasteOfFilling;
    }

    /**
     * Overrides method to establish certain name of candy.
     */
    @Override
    protected void generateName() {
        mName = "Filled Sweet";
    }

    /**
     * Overrides method to establish certain level of sugar in candy.
     */
    @Override
    protected void generateBloodSugar() {
        mBloodSugar = 10.2;
    }

    /**
     * Returns true if and only if superclasses are equal, the argument is not null and is a FilledSweet object that represents the same String value as this object.
     *
     * @param obj - the object to compare with
     * @return true if the FilledSweet objects represent the same value; false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() == obj.getClass()) {
            FilledSweet temp = (FilledSweet) obj;
            return super.equals(obj) &&
                    mTasteOfFilling.equals(temp.mTasteOfFilling);
        } else
            return false;
    }

    /**
     * Returns a hash code for a integer value and superclass Sweetmeat.
     *
     * @return hash code for this FilledSweet object
     */
    @Override
    public int hashCode() {
        return (int) (super.hashCode()
                + ((mTasteOfFilling == null) ? 0 : mTasteOfFilling.hashCode()));
    }

    /**
     * Overrides toString() method.
     *
     * @return representation of the object as a string
     */
    @Override
    public String toString() {
        return super.toString() + getClass().getName() + " @taste of filling: " + mTasteOfFilling;
    }

}