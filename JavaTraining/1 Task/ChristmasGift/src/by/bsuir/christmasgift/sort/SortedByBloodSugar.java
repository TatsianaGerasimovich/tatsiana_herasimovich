package by.bsuir.christmasgift.sort;

import by.bsuir.christmasgift.candy.Candy;

import java.util.Comparator;

/**
 * Class intended for sorting by blood sugar of candy.
 *
 * @author Tatiana
 * @version 1.00 27.03.2015.
 */
public class SortedByBloodSugar implements Comparator<Candy> {
    /**
     * Compares two Candy instance.
     *
     * @param obj1 - the Candy instance to be compared
     * @param obj2 - the Candy instance to be compared
     * @return a positive value if the first objects blood sugar is higher than the second objects blood sugar; a negative value if the first objects blood sugar is lower than the second objects blood sugar;
     * zero if one object represents the same blood sugar values as the other
     */
    public int compare(Candy obj1, Candy obj2) {

        Double bloodSugar1 = obj1.getBloodSugar();
        Double bloodSugar2 = obj2.getBloodSugar();

        return bloodSugar1.compareTo(bloodSugar2);
    }
}
