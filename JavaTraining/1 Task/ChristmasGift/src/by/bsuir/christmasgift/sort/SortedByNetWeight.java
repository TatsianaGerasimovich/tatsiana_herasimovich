package by.bsuir.christmasgift.sort;

import by.bsuir.christmasgift.candy.Candy;

import java.util.Comparator;

/**
 * Class intended for sorting by weight of candy.
 *
 * @author Tatiana
 * @version 1.00 27.03.2015.
 */
public class SortedByNetWeight implements Comparator<Candy> {
    /**
     * Compares two Candy instance.
     *
     * @param obj1 - the Candy instance to be compared
     * @param obj2 - the Candy instance to be compared
     * @return a positive value if the first objects net weight is higher than the second objects net weight; a negative value if the first objects net weight is lower than the second objects net weight;
     * zero if one object represents the same net weight values as the other
     */

    public int compare(Candy obj1, Candy obj2) {

        int netWeight1 = obj1.getNetWeight();
        int netWeight2 = obj2.getNetWeight();

        if (netWeight1 > netWeight2) {
            return 1;
        } else if (netWeight1 < netWeight2) {
            return -1;
        } else {
            return 0;
        }
    }
}