package by.bsuir.christmasgift.sort;

import by.bsuir.christmasgift.candy.Candy;

import java.util.Comparator;

/**
 * Class intended for sorting by name of candy.
 *
 * @author Tatiana
 * @version 1.00 27.03.2015.
 */
public class SortedByName implements Comparator<Candy> {
    /**
     * Compares two Candy instance.
     *
     * @param obj1 - the Candy instance to be compared
     * @param obj2 - the Candy instance to be compared
     * @return a positive value if the first objects name is higher than the second objects name; a negative value if the first objects name is lower than the second objects name;
     * zero if one object represents the same name values as the other
     */
    public int compare(Candy obj1, Candy obj2) {

        String str1 = obj1.getName();
        String str2 = obj2.getName();

        return str1.compareTo(str2);
    }
}