package by.bsuir.christmasgift.gift;

import by.bsuir.christmasgift.candy.Candy;
import by.bsuir.christmasgift.candy.ICalculationWeight;
import by.bsuir.christmasgift.sort.SortedByBloodSugar;
import by.bsuir.christmasgift.sort.SortedByName;
import by.bsuir.christmasgift.sort.SortedByNetWeight;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Class describing a Christmas gift.
 *
 * @author Tatiana
 * @version 1.00 27.03.2015.
 */
public class ChristmasGift implements ICalculationWeight {
    /**
     * List of candy are in the gift.
     */
    private List<Candy> mListOfCandy;
    /**
     * The full weight of gift, including wrapper and other additions.
     */
    private int mWeight;
    /**
     * Net weight of gift.
     */
    private int mNetWeight;

    /**
     * Constructor default.
     */
    public ChristmasGift() {
        mListOfCandy = new ArrayList<Candy>();
        mWeight = 0;
        mNetWeight = 0;

    }

    /**
     * Getter for list of candy.
     *
     * @return list of candy
     */
    public List getListOfCandy() {
        return mListOfCandy;
    }

    /**
     * Getter for weight of present.
     *
     * @return full weight of present
     */
    public int getWeight() {
        return mWeight;
    }

    /**
     * Getter for net weight of present.
     *
     * @return net weight of present
     */
    public int getNetWeight() {
        return mNetWeight;
    }

    /**
     * method of adding candy in a gift.
     *
     * @param obj - candy, you want to add in a gift
     */
    public void addCandy(Candy obj) {
        mListOfCandy.add(obj);
    }

    /**
     * Sorting method chocolates in the gift by name.
     */
    public void sortByName() {
        Collections.sort(mListOfCandy, new SortedByName());
    }

    /**
     * Sorting method chocolates in the gift by blood sugar.
     */
    public void sortByBloodSugar() {
        Collections.sort(mListOfCandy, new SortedByBloodSugar());
    }

    /**
     * Sorting method chocolates in the gift by weight
     */
    public void sortByNetWeight() {
        Collections.sort(mListOfCandy, new SortedByNetWeight());
    }

    /**
     * Search method with the specified range of sweets sugar.
     *
     * @param min - sugar minimum value
     * @param max - maximum value of sugar
     * @return
     */
    public List search(double min, double max) {
        List<Candy> resultOfSearch = new ArrayList<Candy>();
        for (Candy obj : mListOfCandy) {
            if (obj.getBloodSugar() >= min && obj.getBloodSugar() <= max) {
                resultOfSearch.add(obj);
            }
        }
        return resultOfSearch;
    }

    /**
     * Overrides toString() method.
     *
     * @return representation of the object as a string
     */
    @Override
    public String toString() {
        String str = getClass().getName() + "\n";
        int i = 1;

        for (Candy obj : mListOfCandy) {
            str += i + ")" + obj.toString() + "\n";
            i++;
        }
        str += " net weight:" + mNetWeight + " weight:" + mNetWeight+"\n";
        return str;
    }

    /**
     * Override method of calculating the total weight of the candy by its net weight.
     *
     * @return total weight of the candy
     */
    @Override
    public int countWeight() {
        int tempNetWeight = 0, tempWeight = 0;
        Iterator it = mListOfCandy.iterator();
        Candy obj;
        while (it.hasNext()) {
            obj = (Candy) it.next();
            tempNetWeight += obj.getNetWeight();
            tempWeight += obj.getWeight();
        }
        mNetWeight = tempNetWeight;
        mWeight = tempWeight;
        return tempWeight;
    }
}
