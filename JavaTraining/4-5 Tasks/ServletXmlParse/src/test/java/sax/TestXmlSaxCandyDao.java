package sax;

import by.bsuir.gerasimovich.dao.CandyDao;
import by.bsuir.gerasimovich.dao.parser.sax.XmlSaxCandyDao;
import by.bsuir.gerasimovich.entity.Candy;
import org.junit.Test;

import java.util.List;

public class TestXmlSaxCandyDao {
    private CandyDao xmlSaxCandyDao;

    @org.junit.Before
    public void setUp() throws Exception {
        xmlSaxCandyDao = XmlSaxCandyDao.getInstance();
    }

    @Test
    public void testGetBeers() throws Exception {
        List<Candy> candies = xmlSaxCandyDao.getCandies();
        assert (candies!=null);
        assert (candies.size()>0);
    }
}
