package stax;

import by.bsuir.gerasimovich.dao.CandyDao;
import by.bsuir.gerasimovich.dao.parser.stax.XmlStaxCandyDao;
import by.bsuir.gerasimovich.entity.Candy;
import org.junit.Test;

import java.util.List;

public class TestXmlStaxCandyDao {
    private CandyDao xmlStaxCandyDao;

    @org.junit.Before
    public void setUp() throws Exception {
        xmlStaxCandyDao = XmlStaxCandyDao.getInstance();
    }

    @Test
    public void testGetBeers() throws Exception {
        List<Candy> candies = xmlStaxCandyDao.getCandies();
        assert (candies!=null);
        assert (candies.size()>0);
    }
}
