package dom;
import by.bsuir.gerasimovich.dao.parser.dom.XmlDomCandyDao;
import by.bsuir.gerasimovich.entity.Candy;
import org.junit.Test;

import java.util.List;

public class TestXmlDomCandyDao {

    private XmlDomCandyDao xmlDomCandyDao;

    @org.junit.Before
    public void setUp() throws Exception {
        xmlDomCandyDao = XmlDomCandyDao.getInstance();
    }

    @Test
    public void testGetBeers() throws Exception {
        List<Candy> candies = xmlDomCandyDao.getCandies();
        assert (candies!=null);
        assert (candies.size()>0);

    }
}
