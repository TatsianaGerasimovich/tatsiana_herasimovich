<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <style>
        .features-table
        {
            width: 100%;
            margin: 0 auto;
            border-collapse: separate;
            border-spacing: 0;
            border: 0;
            text-shadow: 0 1px 0 #fff;
            color: #2a2a2a;
            background: #fafafa;
            background-image: -moz-linear-gradient(top, #fff, #eaeaea, #fff); /* Firefox 3.6 */
            background-image: -webkit-gradient(linear,center bottom,center top,from(#fff),color-stop(0.5, #eaeaea),to(#fff));
            margin-top:20px;
            margin-bottom:20px;
        }

        .features-table td
        {
            height: 50px;
            padding: 0 20px;
            border-bottom: 1px solid #cdcdcd;
            box-shadow: 0 1px 0 white;
            -moz-box-shadow: 0 1px 0 white;
            -webkit-box-shadow: 0 1px 0 white;
            text-align: center;
            vertical-align: middle;
            display: table-cell;
        }

        .features-table tbody td
        {
            text-align: center;
            width: 150px;
        }


        .features-table td.grey
        {
            background: #efefef;
            background: rgba(144,144,144,0.15);
            border-right: 1px solid white;
        }

        .features-table td.green
        {
            background: #e7f3d4;
            background: rgba(184,243,85,0.3);
        }

        .features-table thead td
        {
            font-size: 120%;
            font-weight: bold;
            -moz-border-radius-topright: 10px;
            -moz-border-radius-topleft: 10px;
            border-top-right-radius: 10px;
            border-top-left-radius: 10px;
            border-top: 1px solid #eaeaea;
        }

        .features-table tfoot td
        {
            font-size: 120%;
            font-weight: bold;
            -moz-border-radius-bottomright: 10px;
            -moz-border-radius-bottomleft: 10px;
            border-bottom-right-radius: 10px;
            border-bottom-left-radius: 10px;
            border-bottom: 1px solid #dadada;
        }

    </style>
</head>
<body>
<table border="2" class="features-table">
    <tr>
        <td nowrap>type</td>
        <td>subtype</td>
        <td>name</td>
        <td>energy</td>
        <td>ingredients</td>
        <td>value</td>
        <td>production</td>
    </tr>
    <c:forEach var="candy" items="${candies}" varStatus="status">
        <tr>
            <td>
                <c:out value="${ candy.type }"/>
            </td>
            <td><c:out value="${ candy.subtype }"/></td>
            <td><c:out value="${ candy.name }"/></td>
            <td><c:out value="${ candy.energy }"/></td>
            <td>
                <ul>
                    <li>water: ${candy.ingredients.water}</li>
                    <li>sugar: ${candy.ingredients.sugar}</li>
                    <li>fructose: ${candy.ingredients.fructose}</li>
                    <li>vanillin: ${candy.ingredients.vanillin}</li>
                </ul>
            </td>
            <td>
                <ul>
                    <li>fats: ${candy.values.fats}</li>
                    <li>proteins: ${candy.values.proteins}</li>
                    <li>carbohydrates: ${candy.values.carbohydrates}</li>
                </ul>
            </td>
            <td>
                <c:out value="${ candy.production }"/>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
