package by.bsuir.gerasimovich.logic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public interface ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response);
}
