package by.bsuir.gerasimovich.logic;

import by.bsuir.gerasimovich.logic.impl.ParseICommand;
import by.bsuir.gerasimovich.logic.impl.NoSuchICommand;

import java.util.EnumMap;
import java.util.Map;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */

public class CommandHelper {
    private static CommandHelper requestHelper;
    Map<CommandName, ICommand> commands = new EnumMap<CommandName, ICommand>(CommandName.class);

    private CommandHelper(){
        commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchICommand());
        commands.put(CommandName.PARSE_DOM_COMMAND, new ParseICommand());

    }
    public static CommandHelper getInstance(){
        if(requestHelper == null )
            requestHelper = new CommandHelper();
        return requestHelper;
    }

    public ICommand getCommand(String commandName){
        CommandName action = CommandName.valueOf(commandName.toUpperCase());
        ICommand command;
        if(action==null) {
             command = commands.get(CommandName.NO_SUCH_COMMAND);
        }
        else {
            command = commands.get(action);
        }
        return command;

    }
}
