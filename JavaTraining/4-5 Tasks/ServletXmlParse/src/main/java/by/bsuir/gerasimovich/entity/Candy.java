package by.bsuir.gerasimovich.entity;

/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public class Candy {
    private String subtype;
    private String type;
    private String name;
    private String energy;
    private String production;
    private Ingredients ingredients;
    private Value values;

    public Candy() {
        ingredients = new Ingredients();
        values= new Value();
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public Ingredients getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredients ingredients) {
        this.ingredients = ingredients;
    }

    public Value getValues() {
        return values;
    }

    public void setValues(Value values) {
        this.values = values;
    }

    public class Ingredients {
        private String water;
        private String sugar;
        private String fructose;
        private String vanillin;

        public String getWater() {
            return water;
        }

        public void setWater(String water) {
            this.water = water;
        }

        public String getSugar() {
            return sugar;
        }

        public void setSugar(String sugar) {
            this.sugar = sugar;
        }

        public String getFructose() {
            return fructose;
        }

        public void setFructose(String fructose) {
            this.fructose = fructose;
        }

        public String getVanillin() {
            return vanillin;
        }

        public void setVanillin(String vanillin) {
            this.vanillin = vanillin;
        }
    }
    public class Value {
        private String fats;
        private String proteins;
        private String carbohydrates;

        public String getFats() {
            return fats;
        }

        public void setFats(String fats) {
            this.fats = fats;
        }

        public String getProteins() {
            return proteins;
        }

        public void setProteins(String proteins) {
            this.proteins = proteins;
        }

        public String getCarbohydrates() {
            return carbohydrates;
        }

        public void setCarbohydrates(String carbohydrates) {
            this.carbohydrates = carbohydrates;
        }
    }
}
