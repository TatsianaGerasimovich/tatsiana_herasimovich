package by.bsuir.gerasimovich.logic.impl;

import by.bsuir.gerasimovich.controller.JspPageName;
import by.bsuir.gerasimovich.logic.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */

public class NoSuchICommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return JspPageName.ERROR_PAGE;
    }
}
