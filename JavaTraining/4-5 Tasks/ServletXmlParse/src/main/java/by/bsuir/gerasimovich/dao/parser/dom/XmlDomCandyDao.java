package by.bsuir.gerasimovich.dao.parser.dom;

import by.bsuir.gerasimovich.dao.CandyDao;
import by.bsuir.gerasimovich.dao.CandyEnum;
import by.bsuir.gerasimovich.dao.parser.CandyDaoException;
import by.bsuir.gerasimovich.entity.Candy;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public class XmlDomCandyDao implements CandyDao {

    public static final Logger logger = Logger.getRootLogger();

    private String fileName;
    private static XmlDomCandyDao instance;
    private DocumentBuilder docBuilder;


    private XmlDomCandyDao() {

        this.fileName ="candy.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new CandyDaoException("Error in DOM PARSER configuration ");
        }
    }

    public static XmlDomCandyDao getInstance() {
        if(instance == null){
            synchronized (XmlDomCandyDao.class) {
                if (instance == null) {
                    instance = new XmlDomCandyDao();
                }
            }
                   }
        return instance;
    }

    public List<Candy> getCandies() {
        List<Candy> candies = new ArrayList<Candy>();
        Document doc = null;
        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream stream = loader.getResourceAsStream(fileName);
            doc = docBuilder.parse(stream);
            Element root = doc.getDocumentElement();
            NodeList typeList = root.getElementsByTagName("Type");
            for (int i = 0; i < typeList.getLength(); i++) {
                Element typeElement = (Element) typeList.item(i);
                NodeList subtypeList = typeElement.getElementsByTagName(CandyEnum.SUBTYPE.getValue());
                for (int j = 0; j < subtypeList.getLength(); j++) {
                    Element subtypeElement = (Element) subtypeList.item(j);
                    NodeList candyList = subtypeElement.getElementsByTagName(CandyEnum.CANDY.getValue());
                    for (int z = 0; z < candyList.getLength(); z++) {
                        Element candyElement = (Element) candyList.item(z);
                        Candy candy = new Candy();
                        candy.setType(typeElement.getAttribute(CandyEnum.NAMEAT.getValue()));
                        candy.setSubtype(subtypeElement.getAttribute(CandyEnum.APPELLATION.getValue()));
                        Candy candyMy = buildCandy(candyElement, candy);
                        candies.add(candyMy);
                    }
                }

            }
        } catch (FileNotFoundException ex){
            throw new CandyDaoException("File " + fileName + " not found");
        }
        catch (IOException e) {
            throw new CandyDaoException("I/O error while opening " + fileName);
        } catch (SAXException e) {
            throw new CandyDaoException("Parsing failure:",e);
        }
        return candies;
    }

    private Candy buildCandy(Element candyElement,Candy candy) {
        candy.setName(getElementTextContent(candyElement, CandyEnum.NAME));
        candy.setEnergy(getElementTextContent(candyElement, CandyEnum.ENERGY)+" kcal.");
        candy.setProduction(getElementTextContent(candyElement, CandyEnum.PRODUCTION));
        Element ingredientElement = (Element)candyElement.
                getElementsByTagName(CandyEnum.INGREDIENTS.getValue()).item(0);
        Candy.Ingredients ingredients =  candy.getIngredients();
        ingredients.setWater(getElementTextContent(ingredientElement, CandyEnum.WATER) + " pct.");
        ingredients.setSugar(getElementTextContent(ingredientElement, CandyEnum.SUGAR) + " pct.");
        ingredients.setFructose(getElementTextContent(ingredientElement, CandyEnum.FRUCTOSE) + " pct.");
        ingredients.setVanillin(getElementTextContent(ingredientElement, CandyEnum.VANILLIN) + " pct.");

        Element valueElement = (Element)candyElement.
                getElementsByTagName(CandyEnum.VALUE.getValue()).item(0);
        Candy.Value value =  candy.getValues();
        value.setFats(getElementTextContent(valueElement, CandyEnum.FATS) + " gr.");
        value.setProteins(getElementTextContent(valueElement, CandyEnum.PROTEINS) + " gr.");
        value.setCarbohydrates(getElementTextContent(valueElement, CandyEnum.CARBOHYDRATES) + " gr.");

        return candy;
    }

    private static String getElementTextContent(Element element,CandyEnum elementName) {
        NodeList nList = element.getElementsByTagName(elementName.getValue());
        Node node = nList.item(0);
        return node.getTextContent();
    }
}

