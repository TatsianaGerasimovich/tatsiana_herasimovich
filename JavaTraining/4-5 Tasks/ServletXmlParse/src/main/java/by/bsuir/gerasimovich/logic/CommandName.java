package by.bsuir.gerasimovich.logic;

/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public enum CommandName {
    NO_SUCH_COMMAND, PARSE_DOM_COMMAND
}
