package by.bsuir.gerasimovich.dao.parser.sax;

import by.bsuir.gerasimovich.dao.CandyEnum;
import by.bsuir.gerasimovich.entity.Candy;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public class CandyHandler extends DefaultHandler {

    private List<Candy> candies;
    private Candy currentCandy;
    private Candy.Ingredients currentIngredients;
    private CandyEnum currentEnum;
    private Candy.Value currentValue;
    private String type;
    private String subtype;

    public CandyHandler() {
        super();
        candies =new ArrayList<Candy>();
    }

    public List<Candy> getCandy() {
        return candies;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentEnum = CandyEnum.getEnum(localName);
        switch (currentEnum){
            case CANDY:
                currentCandy =new Candy();
                currentCandy.setSubtype(subtype);
                currentCandy.setType(type);
                break;
            case TYPE:
                type=attributes.getValue(0);
                break;
            case SUBTYPE:
                subtype=attributes.getValue(0);
                break;
            case INGREDIENTS:
                currentIngredients = currentCandy.getIngredients();
                break;
            case VALUE:
                currentValue = currentCandy.getValues();
                break;
            case CANDIES:
                candies = new ArrayList<Candy>();
                break;
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        CandyEnum currentElement = CandyEnum.getEnum(localName);
        switch (currentElement){
            case CANDY:
                candies.add(currentCandy);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length).trim();
        if(value.contains("\n")){return;}
        if (currentEnum != null) {
            switch (currentEnum) {
                case NAME:
                    currentCandy.setName(value);
                    break;
                case ENERGY:
                    currentCandy.setEnergy(value + " kcal.");
                    break;
                case WATER:
                    currentIngredients.setWater(value + " pct.");
                    break;
                case SUGAR:
                    currentIngredients.setSugar(value + " pct.");
                    break;
                case FRUCTOSE:
                    currentIngredients.setFructose(value + " pct.");
                    break;
                case VANILLIN:
                    currentIngredients.setVanillin(value + " pct.");
                    break;
                case FATS:
                    currentValue.setFats(value + " gr.");
                    break;
                case PROTEINS:
                    currentValue.setProteins(value + " gr.");
                    break;
                case CARBOHYDRATES:
                    currentValue.setCarbohydrates(value + " gr.");
                    break;
                case PRODUCTION:
                    currentCandy.setProduction(value);
                    break;
            }
        }
        currentEnum=null;
    }
}
