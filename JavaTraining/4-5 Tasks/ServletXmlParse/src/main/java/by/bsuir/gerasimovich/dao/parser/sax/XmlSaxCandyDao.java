package by.bsuir.gerasimovich.dao.parser.sax;

import by.bsuir.gerasimovich.dao.CandyDao;
import by.bsuir.gerasimovich.dao.parser.CandyDaoException;
import by.bsuir.gerasimovich.entity.Candy;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public class XmlSaxCandyDao implements CandyDao {


    private CandyHandler candyHandler;

    private String fileName;

    private XMLReader xmlReader;

    private static XmlSaxCandyDao instance;

    private XmlSaxCandyDao(){
        this.fileName ="candy.xml";
        candyHandler = new CandyHandler();
        try {
            xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(candyHandler);
        } catch (SAXException e) {
            throw new CandyDaoException("unable to create SAX parser");
        }
    }

    public static XmlSaxCandyDao getInstance(){
        if(instance==null){
            synchronized (XmlSaxCandyDao.class) {
                if (instance == null) {
                    instance = new XmlSaxCandyDao();
                }
            }
        }
        return instance;
    }


    @Override
    public List<Candy> getCandies() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream(fileName);
        InputSource source = new InputSource(stream);
        try {
            xmlReader.parse(source);
        } catch (SAXException e) {
            throw new CandyDaoException("error in parsing ",e);

        } catch (IOException e) {
            throw new CandyDaoException("error in IOstream while parsing");
        }
        return candyHandler.getCandy();
    }

}
