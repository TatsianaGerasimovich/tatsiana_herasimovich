package by.bsuir.gerasimovich.controller;

/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */

public class RequestParameterName {
    private RequestParameterName(){};
    public static final String COMMAND_NAME = "command";
    public static final String PARSER_TYPE = "parserType";

}
