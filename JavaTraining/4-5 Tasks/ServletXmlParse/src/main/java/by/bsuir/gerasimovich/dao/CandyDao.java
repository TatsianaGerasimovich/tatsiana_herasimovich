package by.bsuir.gerasimovich.dao;

import by.bsuir.gerasimovich.entity.Candy;

import java.util.List;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public interface CandyDao {
    List<Candy> getCandies();
}
