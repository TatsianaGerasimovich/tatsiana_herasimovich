package by.bsuir.gerasimovich.factory;

import by.bsuir.gerasimovich.dao.CandyDao;
import by.bsuir.gerasimovich.dao.parser.CandyDaoException;
import by.bsuir.gerasimovich.dao.parser.dom.XmlDomCandyDao;
import by.bsuir.gerasimovich.dao.parser.sax.XmlSaxCandyDao;
import by.bsuir.gerasimovich.dao.parser.stax.XmlStaxCandyDao;

import java.util.HashMap;
import java.util.Map;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */

public class CandyDaoFactory {
    private static CandyDaoFactory daoFactory;
    private Map<String, CandyDao> daos;

    private CandyDaoFactory() {
        daos = new HashMap<String, CandyDao>();
        daos.put("dom", XmlDomCandyDao.getInstance());
        daos.put("sax", XmlSaxCandyDao.getInstance());
        daos.put("stax", XmlStaxCandyDao.getInstance());
    }

    public static CandyDaoFactory getDaoFactory() {
        if (daoFactory == null) {
            daoFactory= new CandyDaoFactory();
        }

        return daoFactory;
    }

    public CandyDao getCandyDao(String key){
        CandyDao result = daos.get(key);
        if (result == null) {
            throw new CandyDaoException("Invalid key:" + key + " DaoFactory don't have such dao");
        }
        return result;
    }
}
