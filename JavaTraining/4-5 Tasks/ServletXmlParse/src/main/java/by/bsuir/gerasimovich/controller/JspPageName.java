package by.bsuir.gerasimovich.controller;

/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */

public class JspPageName {
    private JspPageName(){};
    public static final String ERROR_PAGE = "/jsp/error/error.jsp";
    public static final String CANDY_PAGE = "/jsp/candy.jsp";
}
