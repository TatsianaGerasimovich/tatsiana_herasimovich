package by.bsuir.gerasimovich.dao.parser.stax;

import by.bsuir.gerasimovich.dao.CandyDao;
import by.bsuir.gerasimovich.dao.CandyEnum;
import by.bsuir.gerasimovich.dao.parser.CandyDaoException;
import by.bsuir.gerasimovich.entity.Candy;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public class XmlStaxCandyDao implements CandyDao {

    public static final Logger logger = Logger.getRootLogger();
    private String fileName;
    private static XmlStaxCandyDao instance;
    private XMLInputFactory inputFactory;

    private XmlStaxCandyDao() {
        inputFactory = XMLInputFactory.newInstance();
        this.fileName = "candy.xml";
    }

    public static XmlStaxCandyDao getInstance() {
        if (instance == null) {
            synchronized (XmlStaxCandyDao.class) {
                if (instance == null) {
                    instance = new XmlStaxCandyDao();
                }
            }
        }
        return instance;
    }

    @Override
    public List<Candy> getCandies() {
        List<Candy> candyList = new ArrayList<Candy>();
        InputStream stream = null;
        XMLStreamReader reader = null;
        String name;
        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            stream = loader.getResourceAsStream(fileName);
            reader = inputFactory.createXMLStreamReader(stream);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (CandyEnum.valueOf(name.toUpperCase()) == CandyEnum.TYPE) {
                        List<Candy> candy = parse(reader);
                        candyList.addAll(candy);
                    }
                }
            }
        } catch (XMLStreamException ex) {
            throw new CandyDaoException("StAX parsing error! ",ex);
        }  finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException ex) {
                throw new CandyDaoException("Impossible close file " + fileName + " : ");
            }
        }
        return candyList;
    }
    public List<Candy> parse(XMLStreamReader reader) throws XMLStreamException {
        String typeN=reader.getAttributeValue(null, CandyEnum.NAMEAT.getValue());

        String name="";
        List<Candy> candyList = new ArrayList<Candy>();
        while (reader.hasNext()) {
            int type = reader.next();
            if (type == XMLStreamConstants.START_ELEMENT) {
                name = reader.getLocalName();
                if (CandyEnum.valueOf(name.toUpperCase()) == CandyEnum.SUBTYPE) {
                    String subtypeN = reader.getAttributeValue(null, CandyEnum.APPELLATION.getValue());
                    candyList.addAll(parseCandy(reader, typeN, subtypeN));
                }
            }
            if (type == XMLStreamConstants.END_ELEMENT) {
                name = reader.getLocalName();
                switch (CandyEnum.valueOf(name.toUpperCase())) {
                    case TYPE:
                        return candyList;
                    case SUBTYPE:
                        break;
             }
            }
        }
        return candyList;
    }

    public List<Candy> parseCandy(XMLStreamReader reader,String typeN,String subtypeN) throws XMLStreamException {
        List<Candy> candyList = new ArrayList<Candy>();
        String name;
        Candy currentCandy = new Candy();
        currentCandy.setType(typeN);
        currentCandy.setSubtype(subtypeN);
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (CandyEnum.valueOf(name.toUpperCase())) {
                        case NAME:
                            currentCandy.setName(getXMLText(reader));
                            break;
                        case ENERGY:
                            currentCandy.setEnergy(getXMLText(reader) + " kcal.");
                            break;
                        case INGREDIENTS:
                            parseIngredients(reader, currentCandy.getIngredients());
                            break;
                        case VALUE:
                            parseValue(reader, currentCandy.getValues());
                            break;
                        case PRODUCTION:
                            currentCandy.setProduction(getXMLText(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    switch (CandyEnum.valueOf(name.toUpperCase())) {
                        case CANDY:
                             candyList.add(currentCandy);
                             currentCandy = new Candy();
                             currentCandy.setType(typeN);
                             currentCandy.setSubtype(subtypeN);
                             break;
                        case SUBTYPE:
                            return candyList;
                    }
            }
        }
        throw new XMLStreamException("Unknown element");

    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }

    private void parseIngredients(XMLStreamReader reader, Candy.Ingredients ingredients)
            throws XMLStreamException {

        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (CandyEnum.getEnum(name)) {
                        case WATER:
                            ingredients.setWater(getXMLText(reader) + " pct.");
                            break;
                        case SUGAR:
                            ingredients.setSugar(getXMLText(reader) + " pct.");
                            break;
                        case FRUCTOSE:
                            ingredients.setFructose(getXMLText(reader) + " pct.");
                            break;
                        case VANILLIN:
                            ingredients.setVanillin(getXMLText(reader) + " pct.");
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    switch (CandyEnum.getEnum(name)) {
                        case INGREDIENTS:
                            return;
                    }
                    break;
            }
        }
    }

    private void parseValue(XMLStreamReader reader, Candy.Value value)
            throws XMLStreamException {

        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (CandyEnum.getEnum(name)) {
                        case FATS:
                            value.setFats(getXMLText(reader) + " gr.");
                            break;
                        case PROTEINS:
                            value.setProteins(getXMLText(reader) + " gr.");
                            break;
                        case CARBOHYDRATES:
                            value.setCarbohydrates(getXMLText(reader) + " gr.");
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    switch (CandyEnum.getEnum(name)) {
                        case VALUE:
                            return;
                    }
                    break;
            }
        }
    }
}


