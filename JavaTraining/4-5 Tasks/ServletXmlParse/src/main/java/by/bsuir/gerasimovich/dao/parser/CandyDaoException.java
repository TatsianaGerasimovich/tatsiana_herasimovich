package by.bsuir.gerasimovich.dao.parser;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public class CandyDaoException extends RuntimeException {
    public CandyDaoException(String message) {
        super(message);
    }

    public CandyDaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
