package by.bsuir.gerasimovich.logic.impl;

import by.bsuir.gerasimovich.controller.JspPageName;
import by.bsuir.gerasimovich.controller.RequestParameterName;
import by.bsuir.gerasimovich.dao.CandyDao;
import by.bsuir.gerasimovich.factory.CandyDaoFactory;
import by.bsuir.gerasimovich.entity.Candy;
import by.bsuir.gerasimovich.logic.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
/**
 * @author Tatiana
 * @version 1.00 22.04.2015.
 */
public class ParseICommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page = null;
        String parserType = request.getParameter(RequestParameterName.PARSER_TYPE);
        CandyDao dao= CandyDaoFactory.getDaoFactory().getCandyDao(parserType);
        List<Candy> candies = dao.getCandies();
        request.setAttribute("candies", candies);
        page = JspPageName.CANDY_PAGE;
        return page;
    }
}
