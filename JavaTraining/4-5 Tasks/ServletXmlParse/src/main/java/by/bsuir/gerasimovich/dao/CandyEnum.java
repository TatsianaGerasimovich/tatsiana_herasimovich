package by.bsuir.gerasimovich.dao;

public enum CandyEnum {
    //elements
    CANDIES("Candies"),
    TYPE("Type"),
    SUBTYPE("Subtype"),
    CANDY("Candy"),
    NAME("Name"),
    ENERGY("Energy"),
    INGREDIENTS("Ingredients"),
    WATER("water"),
    SUGAR("sugar"),
    FRUCTOSE("fructose"),
    VANILLIN("vanillin"),
    VALUE("Value"),
    FATS("fats"),
    PROTEINS("proteins"),
    CARBOHYDRATES("carbohydrates"),
    PRODUCTION("Production"),
    //attributes
    NAMEAT("name"),
    APPELLATION("appellation"),
    UNITS("units");


    public String getValue() {
        return value;
    }
    public static CandyEnum getEnum(String value){
        CandyEnum result=null;
        for(CandyEnum en : CandyEnum.values())
        {
            if(en.getValue().equals(value)){
                result=en;
            }
        }
        return result;
    }
    private String value;
    private CandyEnum(String value){ this.value=value;}

}
