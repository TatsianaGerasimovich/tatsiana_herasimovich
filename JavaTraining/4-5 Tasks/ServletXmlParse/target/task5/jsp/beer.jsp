<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<table border="2" cellpadding="10" cellspacing="10">
    <tr>
        <td>name</td>
        <td>type</td>
        <td>manufacture</td>
        <td>ingredients</td>
        <td>characteristics</td>
        <td>casting method</td>
    </tr>
    <c:forEach var="beer" items="${beers}" varStatus="status">
        <tr>
            <td>
                <c:out value="${ beer.name }"/>
            </td>
            <td><c:out value="${ beer.type }"/></td>
            <td><c:out value="${ beer.manufacture }"/></td>
            <td>
                <ul>
                    <c:forEach var="ingredient" items="${beer.ingredients}" varStatus="status">
                        <li><c:out value="${ingredient}"/></li>
                    </c:forEach>
                </ul>
            </td>
            <td>
                <ul>
                    <li>transparency: ${beer.characteristics.transparency}</li>
                    <li>filterd: ${beer.characteristics.filtred}</li>
                    <li>Nutritional Value: ${beer.characteristics.nutritionValue}</li>
                </ul>
            </td>
            <td>
                <c:forEach var="vessel" items="${beer.characteristics.castingMethod}" varStatus="status">
                    ${vessel.type}
                    <ul>
                        <li>volume: ${vessel.volume} (${vessel.volumeUnit})</li>
                        <li>material: ${vessel.material}</li>
                    </ul>
                </c:forEach>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
