<?xml version="1.0" encoding="WINDOWS-1251" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="html" encoding="WINDOWS-1251"/>
<xsl:template match="/Candies">
		<html>
			<head>
				<title>Candies</title>
			</head>
			<body>
		    	<xsl:apply-templates select="Type"/>
			</body> 
		</html>	
	</xsl:template>		
	<xsl:template match="Type">
	     <h2><xsl:value-of select="@name"/> (<xsl:value-of select="count(Subtype/Candy)" />)
	     </h2>
		<xsl:apply-templates select="Subtype"/>	
	</xsl:template>	
	<xsl:template match="Subtype">
	    <h3>   <xsl:value-of select="@appellation"/> (<xsl:value-of select="count(Candy)" />)
	    </h3>
		<table border="2">
				<tr>
	 				<th>Name</th>
					<th>Energy</th>
					<th>Ingregients</th>
					<th>Value</th>
					<th>Production</th>
				</tr>
				<xsl:apply-templates select="Candy">	
				<xsl:sort select="Production" order="ascending"/>
				</xsl:apply-templates>
		</table>
</xsl:template>
<xsl:template match="Candy">
    <tr><td><xsl:value-of select="Name"/></td>
	<td><xsl:value-of select="Energy"/> <xsl:value-of select="Energy/@units"/>	</td>
	<xsl:apply-templates select="Ingredients"/>	
	<xsl:apply-templates select="Value"/>	
	<td><xsl:value-of select="Production"/></td>
    </tr>
</xsl:template>	

<xsl:template match="Ingredients">   
	<td><p>water:<xsl:value-of select="water"/> <xsl:value-of select="water/@units"/>	</p>
	<p>sugar:<xsl:value-of select="sugar"/> <xsl:value-of select="sugar/@units"/>	</p>
	<p>fructose:<xsl:value-of select="fructose"/> <xsl:value-of select="fructose/@units"/></p>
	<p>vanillin:<xsl:value-of select="vanillin"/> <xsl:value-of select="vanillin/@units"/></p>
    <xsl:choose> 
		<xsl:when test="typeOfChocolate">
	       <p> typeOfChocolate:<xsl:value-of select="typeOfChocolate"/></p>
	    </xsl:when>
		<xsl:when test="milk">
	       <p> milk:<xsl:value-of select="milk"/></p>
	    </xsl:when>
		<xsl:when test="typeOfFruit">
	        <p>typeOfFruit:<xsl:value-of select="typeOfFruit"/></p>
	    </xsl:when>
	    <xsl:otherwise > 
	        <p>typeOfFilling:<xsl:value-of select="typeOfFilling"/></p>
	    </xsl:otherwise> 
	</xsl:choose>     
     </td>
</xsl:template>	

<xsl:template match="Value">   
	<td><p>fats:<xsl:value-of select="fats"/> <xsl:value-of select="fats/@units"/>	</p>
	<p>proteins:<xsl:value-of select="proteins"/> <xsl:value-of select="proteins/@units"/>	</p>
	<p>carbohydrates:<xsl:value-of select="carbohydrates"/> <xsl:value-of select="carbohydrates/@units"/>	</p>
    </td>
</xsl:template>	
</xsl:stylesheet>
