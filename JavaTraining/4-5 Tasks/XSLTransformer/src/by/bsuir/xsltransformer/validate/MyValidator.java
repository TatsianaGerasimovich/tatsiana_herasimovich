package by.bsuir.xsltransformer.validate;

import com.sun.xml.internal.ws.developer.ValidationErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * @author Tatiana
 * @version 1.00 23.04.2015.
 */
public class MyValidator {
    public static void main(String[] args) {
        try {
            SchemaFactory sf = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            File f = new File("src\\by\\bsuir\\xsltransformer\\resources\\candies.xsd");
            Schema sc = sf.newSchema(f);
            Validator v = sc.newValidator();
            v.setErrorHandler(new ValidationErrorHandler() {
                @Override
                public void warning(SAXParseException exception) throws SAXException {
                    System.out.println(exception.toString());
                }

                @Override
                public void error(SAXParseException exception) throws SAXException {
                    System.out.println(exception.toString());
                }

                @Override
                public void fatalError(SAXParseException exception) throws SAXException {
                    System.out.println(exception.toString());
                }
            });
            v.validate(new StreamSource("src\\by\\bsuir\\xsltransformer\\resources\\candies.xml"));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
