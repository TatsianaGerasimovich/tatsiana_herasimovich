package by.bsuir.xsltransformer.transformer;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * @author Tatiana
 * @version 1.00 19.04.2015.
 */
public class XslTransformer {
    public static void main(String[] args) {
        try {
            TransformerFactory factory= TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource("src\\by\\bsuir\\xsltransformer\\resources\\candies.xsl"));
            transformer.transform(new StreamSource("src\\by\\bsuir\\xsltransformer\\resources\\candiesTransformer.xml"),
                    new StreamResult("src\\by\\bsuir\\xsltransformer\\resources\\candies.html"));
            System.out.println("Done");
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
