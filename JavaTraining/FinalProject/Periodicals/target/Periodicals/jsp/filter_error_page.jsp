<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 31.05.2015
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html>
<head>
    <style>
        p {
            margin-left: 150px;
            margin-top: 200px;
            font-size: 35px;
            color: black;
        }
    </style>
    <title></title>
</head>
<body>
<p><fmt:message key="error_filter.message"/>
    <script>
        setTimeout(function () {
            document.location.replace("/Periodicals/index.jsp");
        }, 4000);
    </script>
</body>
</html>

