<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 04.06.2015
  Time: 15:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Users</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="jsp/css/bootstrap.min.css">
    <link rel="stylesheet" href="jsp/css/normalize.min.css">
    <link rel="stylesheet" href="jsp/css/font-awesome.min.css">
    <link rel="stylesheet" href="jsp/css/animate.css">
    <link rel="stylesheet" href="jsp/css/templatemo_misc.css">
    <link rel="stylesheet" href="jsp/css/templatemo_style.css">
    <script src="jsp/js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="jsp/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="jsp/js/jquery.easing-1.3.js"></script>
    <script src="jsp/js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(function() {
                $(window).scroll(function() {
                    if($(this).scrollTop() != 0) {
                        $('#toTop').fadeIn();
                    } else {
                        $('#toTop').fadeOut();
                    }
                });

                $('#toTop').click(function() {
                    $('body,html').animate({scrollTop:0},800);
                });
            });
        });
        function blockButtons() {
            $('.btn').each(function () {
                $(this).attr('disabled', 'disabled');
            })
        }
        function deleteUser(user_id) {
            blockButtons();
            var user = {id:  user_id};
            var request = new Object();
            request.data = JSON.stringify(user);
            request.command = 'REMOVE_USER';
            $.ajax({
                url: "Controller",
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(request),
                contentType: 'application/json',
                mimeType: 'application/json',

                success: function (data) {
                    if (data == -1) {
                        window.location.replace('Controller?command=users_page')
                    }
                    else {
                        window.location.replace('Controller?command=users_page')
                    }
                },
                error: function (data, status, er) {
                    alert("error: " + data + " status: " + status + " er:" + er);
                }
            });
        }
        function makeAdmin(login) {
            blockButtons();
            var userAdmin = {login: login};
            var request = new Object();
            request.command = 'make_admin';
            request.data = JSON.stringify(userAdmin);
            $.ajax({
                url: "Controller",
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(request),
                contentType: 'application/json',
                mimeType: 'application/json',

                success: function (data) {
                    if (data == -1) {
                        window.location.replace('Controller?command=users_page')
                    }
                    else {
                        window.location.replace('Controller?command=users_page')
                    }
                },
                error: function (data, status, er) {
                    alert("error: " + data + " status: " + status + " er:" + er);
                    window.location.replace('Controller?command=users_page')
                }
            });

        }
    </script>
</head>
<body>
<%@include file="header.jsp" %>

<div id="log" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title"><fmt:message key="admin.users"/></h1>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
        <div class="row">

            <div class="col-md-12 col-sm-12">

                    <p class="has-error">${errorMsg}</p>
                    <table class="table " id="orderList">

                        <c:forEach items="${users}" var="user1" varStatus="status">
                            <tr id="${user1.id}">
                                <td width="20px">${status.index+1}</td>
                                <td width="50px"><img src="${user1.userPhoto}" class="inOrder"></td>
                                <td width="70px">${user1.userName}</td>
                                <td width="70px">${user1.userEmail}</td>
                                <td width="20px">${user1.userRole}</td>
                                <c:if test="${user1.id != user.id}">
                                    <td width="70px">
                                        <button class="btn btn-danger" onclick="deleteUser(${user1.id})">
                                            <fmt:message key="orders.btn.remove"/>
                                        </button>
                                    </td>
                                </c:if>
                                <c:if test="${!(user1.userRole eq 'admin')}">
                                    <td width="70px">
                                        <button class="btn btn-default" onclick="makeAdmin('${user1.login}')">
                                            <fmt:message key="users.make_admin"/>
                                        </button>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </table>

            </div>
            <!-- /.col-md-12 -->

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /#product-promotion -->


<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                    <span>
                    	Copyright &copy; 2015 Gerasimovich Tatiana
                    </span>

            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">
                <ul class="social">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                    <li><a href="#" class="fa fa-rss"></a></li>
                </ul>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.site-footer -->


<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>

<script src="jsp/js/bootstrap.js"></script>
<script src="jsp/js/plugins.js"></script>
<script src="jsp/js/main.js"></script>

</body>
</html>