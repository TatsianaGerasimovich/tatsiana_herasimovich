<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 28.05.2015
  Time: 0:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Sign in</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="jsp/css/bootstrap.min.css">
    <link rel="stylesheet" href="jsp/css/normalize.min.css">
    <link rel="stylesheet" href="jsp/css/font-awesome.min.css">
    <link rel="stylesheet" href="jsp/css/animate.css">
    <link rel="stylesheet" href="jsp/css/templatemo_misc.css">
    <link rel="stylesheet" href="jsp/css/templatemo_style.css">

    <script src="jsp/js/vendor/modernizr-2.6.2.min.js"></script>

</head>
<body>
<%@include file="header.jsp" %>

<div id="log" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title"> <fmt:message key="sign_in.title"/></h1>
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
        <div class="row">
            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo_f" items="${first_photos}">
                    <div class="item-small">
                        <img src="${photo_f.coverOfPeriodical}" alt="Product 1">
                    </div> <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div> <!-- /.col-md-2 -->

            <div class="col-md-8 col-sm-6">

                <div class="item-large">

                    <div class="text-center">
                        <p class="text-danger"><c:if test="${errorLoginPassMessage != null}">${errorLoginPassMessage}</c:if></p>

                        <fieldset class="col-md-12" >
                            <input type="text" name="login"  placeholder="<fmt:message key="profile.login"/>" style="width: 50%" form="form1"/>
                        </fieldset>
                        <fieldset class="col-md-12" >
                            <input id="password" type="password" name="password"  placeholder="<fmt:message key="profile.password"/>" style="width: 50%" form="form1"/>
                        </fieldset>
                        <fieldset class="col-md-12" >
                            <input type="hidden" name="command" value="sign_in" form="form1"/>
                        </fieldset>

                            <fieldset class="col-md-12">
                                <input type="submit" name="send" value="<fmt:message key="navbar.sign_in"/>" id="submit" class="button" form="form1"/>
                            </fieldset>
                        <form method="post" action="Controller"  class="text-center" id="form1">
                        </form>
                    </div> <!-- /.contact-form -->


                </div> <!-- /.item-large -->

            </div> <!-- /.col-md-8 -->

            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo" items="${last_photos}">
                    <div class="item-small">
                        <img src="${photo.coverOfPeriodical}" alt="Product 1">
                    </div> <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div> <!-- /.col-md-2 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /#product-promotion -->


<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                    <span>
                    	Copyright &copy; 2015 Gerasimovich Tatiana
                    </span>

            </div> <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">
                <ul class="social">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                    <li><a href="#" class="fa fa-rss"></a></li>
                </ul>
            </div> <!-- /.col-md-6 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.site-footer -->


<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>

<script src="jsp/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="jsp/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="jsp/js/jquery.easing-1.3.js"></script>
<script src="jsp/js/bootstrap.js"></script>
<script src="jsp/js/plugins.js"></script>
<script src="jsp/js/main.js"></script>
<script type="jsp/text/javascript">

    $(document).ready(function() {
        $(function() {
            $(window).scroll(function() {
                if($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });

            $('#toTop').click(function() {
                $('body,html').animate({scrollTop:0},800);
            });
        });
    });
</script>
</body>
</html>
