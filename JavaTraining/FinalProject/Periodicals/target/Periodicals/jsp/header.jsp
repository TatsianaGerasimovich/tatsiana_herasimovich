<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 26.05.2015
  Time: 18:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="front">
<div class="site-header">
<div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-6">
            <div id="templatemo_logo">
                <h1><a href="Controller?command=periodical_list">Periodicals</a></h1>
            </div>
            <!-- /.logo -->
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-8 col-sm-6 col-xs-6">
            <a href="index.jsp" class="toggle-menu"><i class="fa fa-bars"></i></a>
            <script type="text/javascript">
            function changeLanguage(locale) {
            var request = new Object();
            request.command = 'change_language';
            request.data = locale;
            $.ajax({
            url: "Controller",
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(request),
            contentType: 'application/json',
            mimeType: 'application/json',

            success: function (data) {
            location.reload();

            },
            error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er:" + er);
            }
            });

            }
            </script>
            <div class="main-menu">
                <ul>
                    <c:if test="${user.userRole eq 'guest'}">
                        <li><a href="Controller?command=periodical_list"><fmt:message
                                key="navbar.home"/></a></li>
                        <li><a href="Controller?command=periodical_list"><fmt:message
                                key="navbar.periodicals"/></a></li>
                        <li class="dropdown active"><a href="#log"><fmt:message
                                key="navbar.log_in"/></a>
                            <ul class="dropdown-menu">
                                <li><a href="Controller?command=sign_in_page"><fmt:message
                                        key="navbar.sign_in"/></a></li>
                                <li><a href="Controller?command=sign_up_page"><fmt:message
                                        key="navbar.sign_up"/></a></li>
                            </ul>
                        </li>
                        <li class="dropdown active"><a href="#contact"><fmt:message
                                key="navbar.language"/></a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:changeLanguage('en_US')"><fmt:message
                                        key="navbar.language.english"/></a></li>
                                <li><a href="javascript:changeLanguage('ru_RU')"><fmt:message
                                        key="navbar.language.russian"/></a></li>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${user.userRole eq 'user'}">
                        <li><a href="Controller?command=periodical_list"><fmt:message
                            key="navbar.home"/></a></li>
                        <li><a href="Controller?command=periodical_list"><fmt:message
                                key="navbar.periodicals"/></a></li>
                        <li class="dropdown active"><a href="#log"><fmt:message
                                key="navbar.log_out"/></a>
                            <ul class="dropdown-menu">
                                <li><a href="Controller?command=log_out">${user.userName}</a></li>
                            </ul>
                        </li>
                        <li class="dropdown active"><a href="#contact"><fmt:message
                                key="navbar.language"/></a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:changeLanguage('en_US')"><fmt:message
                                        key="navbar.language.english"/></a></li>
                                <li><a href="javascript:changeLanguage('ru_RU')"><fmt:message
                                        key="navbar.language.russian"/></a></li>
                            </ul>
                        </li>
                    </c:if>
                    <c:if test="${user.userRole eq 'admin'}">
                        <li><a href="Controller?command=periodical_list"><fmt:message
                                key="navbar.home"/></a></li>
                        <li class="dropdown active"><a href="Controller?command=periodical_list"><fmt:message
                                key="navbar.periodicals"/></a>
                            <ul class="dropdown-menu">
                                <li><a href="Controller?command=new_periodical_page"><fmt:message
                                        key="navbar.add_periodical"/></a></li>
                                <li><a href="Controller?command=periodical_list_for_delete"><fmt:message
                                        key="navbar.delete_periodical"/></a></li>
                            </ul>
                        </li>
                        <li class="dropdown active"><a href="#log"><fmt:message
                                key="navbar.log_out"/></a>
                            <ul class="dropdown-menu">
                                <li><a href="Controller?command=log_out">${user.userName}</a></li>
                            </ul>
                        </li>
                        <li class="dropdown active"><a href="#contact"><fmt:message
                                key="navbar.language"/></a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:changeLanguage('en_US')"><fmt:message
                                        key="navbar.language.english"/></a></li>
                                <li><a href="javascript:changeLanguage('ru_RU')"><fmt:message
                                        key="navbar.language.russian"/></a></li>
                            </ul>
                        </li>
                    </c:if>
                </ul>
            </div>
            <!-- /.main-menu -->
        </div>
        <!-- /.col-md-8 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="responsive">
                <div class="main-menu">
                    <ul>
                        <c:if test="${user.userRole eq 'guest'}">
                            <li><a href="Controller?command=periodical_list"><fmt:message
                                    key="navbar.home"/></a></li>
                            <li><a href="Controller?command=periodical_list"><fmt:message
                                    key="navbar.periodicals"/></a></li>
                            <li class="dropdown active"><a href="#log"><fmt:message
                                    key="navbar.log_in"/></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Controller?command=sign_in_page"><fmt:message
                                            key="navbar.sign_in"/></a></li>
                                    <li><a href="Controller?command=sign_up_page"><fmt:message
                                            key="navbar.sign_up"/></a></li>
                                </ul>
                            </li>
                            <li class="dropdown active"><a href="#contact"><fmt:message
                                    key="navbar.language"/></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:changeLanguage('en_US')"><fmt:message
                                            key="navbar.language.english"/></a></li>
                                    <li><a href="javascript:changeLanguage('ru_RU')"><fmt:message
                                            key="navbar.language.russian"/></a></li>
                                </ul>
                            </li>
                        </c:if>
                        <c:if test="${user.userRole eq 'user'}">
                            <li><a href="Controller?command=periodical_list"><fmt:message
                                    key="navbar.home"/></a></li>
                            <li><a href="Controller?command=periodical_list"><fmt:message
                                    key="navbar.periodicals"/></a></li>
                            <li class="dropdown active"><a href="#log"><fmt:message
                                    key="navbar.log_out"/></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Controller?command=log_out">${user.userName}</a></li>
                                </ul>
                            </li>
                            <li class="dropdown active"><a href="#contact"><fmt:message
                                    key="navbar.language"/></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:changeLanguage('en_US')"><fmt:message
                                            key="navbar.language.english"/></a></li>
                                    <li><a href="javascript:changeLanguage('ru_RU')"><fmt:message
                                            key="navbar.language.russian"/></a></li>
                                </ul>
                            </li>
                        </c:if>
                        <c:if test="${user.userRole eq 'admin'}">
                            <li><a href="Controller?command=periodical_list"><fmt:message
                                    key="navbar.home"/></a></li>
                            <li class="dropdown active"><a href="Controller?command=periodical_list"><fmt:message
                                    key="navbar.periodicals"/></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Controller?command=new_periodical_page"><fmt:message
                                            key="navbar.add_periodical"/></a></li>
                                    <li><a href="Controller?command=periodical_list_for_delete"><fmt:message
                                            key="navbar.delete_periodical"/></a></li>
                                </ul>
                            </li>
                            <li class="dropdown active"><a href="#log"><fmt:message
                                    key="navbar.log_out"/></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Controller?command=log_out">${user.userName}</a></li>
                                </ul>
                            </li>
                            <li class="dropdown active"><a href="#contact"><fmt:message
                                    key="navbar.language"/></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:changeLanguage('en_US')"><fmt:message
                                            key="navbar.language.english"/></a></li>
                                    <li><a href="javascript:changeLanguage('ru_RU')"><fmt:message
                                            key="navbar.language.russian"/></a></li>
                                </ul>
                            </li>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- /.container -->
<c:if test="${user.userRole eq 'user'}">

    <script>
        function submitForm(formId) {
            document.getElementById(formId).submit();
        }
    </script>
    <div class="col-md-1">
        <div class="service-Profile" onclick="submitForm('Profile')">
            <form method="post" action="Controller" id="Profile">
                <input type="hidden" name="command" value="view_profile">
                <span onclick="submitForm('Profile')" class="service-icon third"></span>

                <h3 onclick="submitForm('Profile')"><fmt:message
                        key="navbar.profile"/></h3>
            </form>
        </div>
        <!-- /.service-Profile -->

        <div class="service-Profile" onclick="submitForm('Cart')">
            <form method="post" action="Controller" id="Cart">
                <input type="hidden" name="command" value="view_cart">
                <span onclick="submitForm('Cart')" class="service-icon second"></span>

                <h3 onclick="submitForm('Cart')"><fmt:message
                        key="navbar.cart"/> <b id="periodicalsInOrder">(${user.cart.size})</b>
                </h3>
            </form>
        </div>
        <!-- /.service-Profile -->
    </div>

</c:if>

<c:if test="${user.userRole eq 'admin'}">
    <script>
        function submitForm(formId) {
            document.getElementById(formId).submit();
        }
    </script>
    <div class="col-md-1">
        <div class="service-Profile" onclick="submitForm('Users')">
            <form method="post" action="Controller" id="Users">
                <input type="hidden" name="command" value="users_page">
                <span onclick="submitForm('Users')" class="service-icon third"></span>

                <h3 onclick="submitForm('Users')"><fmt:message
                        key="navbar.users"/></h3>
            </form>
        </div>
        <!-- /.service-Profile -->

        <div class="service-Profile" onclick="submitForm('Orders')">
            <form method="post" action="Controller" id="Orders">
                <input type="hidden" name="command" value="view_all_orders">
                <span onclick="submitForm('Orders')" class="service-icon fourth"></span>

                <h3 onclick="submitForm('Orders')"> <fmt:message
                        key="navbar.orders"/>
                </h3>
            </form>
        </div>
        <!-- /.service-Profile -->
    </div>

</c:if>

</div>
<!-- /.site-header -->
</div>
<!-- /#front -->