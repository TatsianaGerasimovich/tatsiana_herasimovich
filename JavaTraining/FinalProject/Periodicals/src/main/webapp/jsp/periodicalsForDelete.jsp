<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 07.06.2015
  Time: 20:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Periodicals</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="jsp/css/bootstrap.min.css">
    <link rel="stylesheet" href="jsp/css/normalize.min.css">
    <link rel="stylesheet" href="jsp/css/font-awesome.min.css">
    <link rel="stylesheet" href="jsp/css/animate.css">
    <link rel="stylesheet" href="jsp/css/templatemo_misc.css">
    <link rel="stylesheet" href="jsp/css/templatemo_style.css">
    <script src="jsp/js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="jsp/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="jsp/js/jquery.easing-1.3.js"></script>
    <script src="jsp/js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(function() {
                $(window).scroll(function() {
                    if($(this).scrollTop() != 0) {
                        $('#toTop').fadeIn();
                    } else {
                        $('#toTop').fadeOut();
                    }
                });

                $('#toTop').click(function() {
                    $('body,html').animate({scrollTop:0},800);
                });
            });
        });
        function blockButtons() {
            $('.btn').each(function () {
                $(this).attr('disabled', 'disabled');
            })
        }

        function removePeriodical(id) {
            blockButtons();
            var periodical = {id: id};
            var request = new Object();
            request.command = 'remove_periodical';
            request.data = JSON.stringify(periodical);
            $.ajax({
                url: "Controller",
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(request),
                contentType: 'application/json',
                mimeType: 'application/json',

                success: function (data) {
                    window.location.replace('Controller?command=periodical_list_for_delete')
                },
                error: function (data, status, er) {
                    alert("error: " + data + " status: " + status + " er:" + er);
                    window.location.replace('Controller?command=periodical_list_for_delete')
                }
            });
        }

    </script>
</head>
<body>
<%@include file="header.jsp" %>

<div id="log" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title"><fmt:message key="orders.title"/></h1>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
        <div class="row">

            <div class="col-md-12 col-sm-12">

                <form method="post" action="Controller">
                    <table class="table " id="periodicalList">
                        <thead>
                        <th>№</th>
                        <th>Periodical</th>
                        <th>Name</th>
                        </thead>
                        <c:forEach items="${periodicals}" var="periodical"  varStatus="status">
                            <tr id="${periodical.id}">
                                <td width="40px" >${status.index+1}</td>
                                <td width="50px"><img src="${periodical.coverOfPeriodical}" class="inOrder"></td>
                                <td width="200px" >${periodical.periodicalName}</td>
                                <td width="30px">
                                    <button class="btn btn-danger " type="button" onclick="removePeriodical('${periodical.id}')">
                                        <fmt:message key="orders.btn.remove"/>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
               </form>
            </div>
            <!-- /.col-md-12 -->

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /#product-promotion -->


<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                    <span>
                    	Copyright &copy; 2015 Gerasimovich Tatiana
                    </span>

            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">
                <ul class="social">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                    <li><a href="#" class="fa fa-rss"></a></li>
                </ul>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.site-footer -->


<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>

<script src="jsp/js/bootstrap.js"></script>
<script src="jsp/js/plugins.js"></script>
<script src="jsp/js/main.js"></script>
</body>
</html>
