<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 02.06.2015
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Comments</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="jsp/css/bootstrap.min.css">
    <link rel="stylesheet" href="jsp/css/normalize.min.css">
    <link rel="stylesheet" href="jsp/css/font-awesome.min.css">
    <link rel="stylesheet" href="jsp/css/animate.css">
    <link rel="stylesheet" href="jsp/css/templatemo_misc.css">
    <link rel="stylesheet" href="jsp/css/templatemo_style.css">
    <script src="jsp/js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="jsp/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="jsp/js/jquery.easing-1.3.js"></script>
    <script src="jsp/js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">
        function cancelChanging() {

            $('#commentList').html(tableState);

            //removing delete buttons
            $('.btn-danger ').remove();

            //removing save button
            $('#SaveCommentBtn').remove();
            $('#CancelBtn').remove();

            //making buttons disabel
            if (!(($('#commentList tr').size()) == 1)) {
                $('#changeCommentBtn').show();
                document.getElementById('changeCommentBtn').removeAttribute('disabled');
            }
            else{
                $('#changeCommentBtn').show();
                document.getElementById('changeCommentBtn').setAttribute('disabled', 'disabled');
            }
        }

        var tableState;

        function removeTr(id) {
            var del = $('#' + id);
            del.remove();
            if ((($('#commentList tr').size()) == 1)) {
                document.getElementById('makeOrderBtn').setAttribute('disabled', 'disabled');
            }
        }

        function saveOrder() {

            var rows = $('#commentList tbody tr');
            var commentMass = [];
            var id;
            for (var i = 0; i < rows.length; i++) {
                id = $(rows[i]).attr('id');
                commentMass.push({id: id});
            }

            var request = new Object();
            request.data = JSON.stringify(commentMass);
            request.command = 'update_comments';

            $.ajax({
                url: "Controller",
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(request),
                contentType: 'application/json',
                mimeType: 'application/json',

                success: function (data) {
                    if (data.id == -1) {
                        location.reload();
                    }
                    else {
                        $('#errorMess').attr('style', 'visibility: visible;')
                    }
                },
                error: function (data, status, er) {
                    $('#errorMess').attr('style', 'visibility: visible;')
                    alert("error: " + data + " status: " + status + " er:" + er);
                }
            });
        }

        function changeOrders() {

            tableState = $('#commentList').html();
            //adding delete buttons

            var rows = $('#commentList tbody tr');
            for (var i = 0; i < rows.length; i++) {
                var last = $(rows[i]).find('td:last');
                var trId = $(last).closest('tr').attr('id');
                var deleteBtn = '<button class="btn btn-danger " type="button" onclick="removeTr(' + trId + ')"><fmt:message key="orders.btn.remove"/></button>';
                $(last).after(deleteBtn);
            }
            //adding save button
            var btn = '<br><button class="btn btn-success " type="button" id="SaveCommentBtn" onclick="saveOrder()"><fmt:message key="cart.save"/></button>';
            var btn2 = '<button class="btn btn-danger " type="button" id="CancelBtn" onclick="cancelChanging()"><fmt:message key="cart.cancel"/></button>';
            $('#changeCommentBtn').after(btn);
            $('#SaveCommentBtn').after(btn2);

            document.getElementById('changeCommentBtn').setAttribute('disabled', 'disabled');
            $('#changeCommentBtn').hide();
        }

    </script>
</head>
<body>
<%@include file="header.jsp" %>

<div id="log" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title"><fmt:message key="user.comments.title"/></h1>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo_f" items="${first_photos}">
                    <div class="item-small">
                        <img src="${photo_f.coverOfPeriodical}" alt="Product 1">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->

            <div class="col-md-8 col-sm-6">

                <div class="item-large">

                    <form method="post" action="Controller">
                        <table class="table " id="commentList">
                            <thead>
                            <th>№</th>
                            <th><fmt:message key="comments.description"/></th>
                            <th><fmt:message key="orders.periodical"/></th>
                            </thead>
                            <c:set value="${periodicals}" var="periodicals"></c:set>
                            <c:forEach items="${comments}" var="comment" varStatus="status">
                                <tr id="${comment.id}">
                                    <td width="40px">${status.index+1}</td>
                                    <td width="600px"> ${comment.reviewDescription}</td>
                                    <td width="20px">
                                            ${periodicals[status.index].periodicalName}
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                        <br>
                        <button class="btn btn-default " type="button" id="changeCommentBtn"
                                <c:if test="${comments.size() == 0}">disabled="disabled" </c:if> onclick="changeOrders()">
                            <fmt:message key="comments.btn.change"/>
                        </button>
                        <br>

                        <p id="errorMes" style="visibility: hidden;">
                            <fmt:message key="comments.error"/></p>
                    </form>
                </div>
                <!-- /.item-large -->

            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo" items="${last_photos}">
                    <div class="item-small">
                        <img src="${photo.coverOfPeriodical}" alt="Product 1">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /#product-promotion -->


<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                    <span>
                    	Copyright &copy; 2015 Gerasimovich Tatiana
                    </span>

            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">
                <ul class="social">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                    <li><a href="#" class="fa fa-rss"></a></li>
                </ul>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.site-footer -->


<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>

<script src="jsp/js/bootstrap.js"></script>
<script src="jsp/js/plugins.js"></script>
<script src="jsp/js/main.js"></script>
<script type="jsp/text/javascript">

    $(document).ready(function() {
        $(function() {
            $(window).scroll(function() {
                if($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });

            $('#toTop').click(function() {
                $('body,html').animate({scrollTop:0},800);
            });
        });
    });

</script>
</body>
</html>