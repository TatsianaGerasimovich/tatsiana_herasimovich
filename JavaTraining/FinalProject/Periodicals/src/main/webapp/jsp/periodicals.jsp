<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 23.05.2015
  Time: 21:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="/WEB-INF/mytaglib.tld" prefix="mytag" %>

<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Welcome</title>
    <meta name="description" content="">

    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="jsp/css/bootstrap.min.css">
    <link rel="stylesheet" href="jsp/css/normalize.min.css">
    <link rel="stylesheet" href="jsp/css/font-awesome.min.css">
    <link rel="stylesheet" href="jsp/css/animate.css">
    <link rel="stylesheet" href="jsp/css/templatemo_misc.css">
    <link rel="stylesheet" href="jsp/css/templatemo_style.css">

    <script src="jsp/js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">
        function addToCart(id) {
            var periodical = {
                periodicalId: id
            }
            var data = {
                periodical: periodical,
                number: 1
            }
            // get inputs
            var request = new Object();
            request.data = JSON.stringify(data);
            request.command = 'add_to_cart';

            $.ajax({
                url: "Controller",
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(request),
                contentType: 'application/json',
                mimeType: 'application/json',

                success: function (data) {
                    if (data == -1) {
                        alert('You have already add this book to order. Go to order to change number');
                     //  $('#errorMessage').text('You have already add this book to order. Go to order to change number');
                    }
                    else {
                        $('#periodicalsInOrder').text(data);
                    }
                },
                error: function (data, status, er) {
                    alert("error: " + data + " status: " + status + " er:" + er);
                }
            });
        }

    </script>
</head>
<body>
<%@include file="header.jsp" %>
<mytag:carousel/>

<div id="products" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title"><fmt:message key="periodicals.title"/></h1>

                <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                        <fmt:message key="navbar.categoriesBtn"/> <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <c:forEach var="categoryItem" items="${categories}">
                            <li>
                                <a href="Controller?command=PERIODICAL_LIST_BY_CATEGORY&categoryId=${categoryItem.categoryId}">${categoryItem.categoryName}</a>
                            </li>
                        </c:forEach>
                        <li>
                            <a href="Controller?command=PERIODICAL_LIST_BY_CATEGORY&categoryId=0"><fmt:message key="periodicals.category.all"/></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
        <div class="row">

            <c:forEach var="periodicalItem" items="${periodicals}">
                <div class="col-md-3 col-sm-6">
                    <div class="product-item">
                        <c:if test="${user.userRole eq 'guest'}">
                        <a class="thumbnail"
                           href="Controller?command=show_periodical&periodicalId=${periodicalItem.periodicalId}">
                            </c:if>
                            <div class="item-thumb">
                                <c:if test="${user.userRole eq 'user'}">
                                    <div class="overlay">
                                        <div class="overlay-inner">
                                            <a href="#products"  onclick="addToCart(${periodicalItem.periodicalId})" class="view-detail"><fmt:message key="periodical.add_to_cart"/></a>
                                        </div>
                                    </div>
                                    <!-- /.overlay -->
                                </c:if>
                                <img src="${periodicalItem.coverOfPeriodical}" alt="">
                            </div>
                            <!-- /.item-thumb -->
                            <c:if test="${user.userRole eq 'guest'}">
                        </a>
                        </c:if>
                        <h3>
                            <a class="thumbnail"
                               href="Controller?command=SHOW_PERIODICAL&periodicalId=${periodicalItem.periodicalId}">
                                    ${periodicalItem.periodicalName}
                            </a>
                        </h3>
                            <span>Price: <em class="text-muted">$${periodicalItem.price*1.2}</em> - <em
                                    class="price">$${periodicalItem.price}</em></span>
                    </div>
                    <!-- /.product-item -->

                </div>
                <!-- /.col-md-3 -->
            </c:forEach>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /#products -->

<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                    <span>
                    	Copyright &copy; 2015 Gerasimovich Tatiana
                    </span>

            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">
                <ul class="social">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                </ul>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.site-footer -->

<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>


<script src="jsp/js/vendor/jquery-1.10.1.min.js"></script>
<script>window.jQuery || document.write('<script src="jsp/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="jsp/js/jquery.easing-1.3.js"></script>
<script src="jsp/js/bootstrap.js"></script>
<script src="jsp/js/plugins.js"></script>
<script src="jsp/js/main.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });

            $('#toTop').click(function () {
                $('body,html').animate({scrollTop: 0}, 800);
            });
        });
    });
</script>
</body>
</html>
