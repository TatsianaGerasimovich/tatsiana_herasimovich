<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 02.06.2015
  Time: 14:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Profile</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="jsp/css/bootstrap.min.css">
    <link rel="stylesheet" href="jsp/css/normalize.min.css">
    <link rel="stylesheet" href="jsp/css/font-awesome.min.css">
    <link rel="stylesheet" href="jsp/css/animate.css">
    <link rel="stylesheet" href="jsp/css/templatemo_misc.css">
    <link rel="stylesheet" href="jsp/css/templatemo_style.css">
    <script src="jsp/js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="jsp/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="jsp/js/jquery.easing-1.3.js"></script>
    <script src="jsp/js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript">

        function cancelChanging() {

            $('#profile').html(tableState);
            //removing save button
            $('#SaveProfileBtn').remove();
            $('#CancelBtn').remove();
            $('#changeOrderBtn').show();
            document.getElementById('changeOrderBtn').removeAttribute("disabled");
        }

        var tableState;

        function saveProfile() {

            var profile;
            var userName;
            userName = $('#1').val();
            var Login;
            Login = $('#2').val();
            var userCountry;
            userCountry = $('#3').val();
            var userCity;
            userCity = $('#8').val();
            var userAddress;
            userAddress = $('#4').val();
            var userPostcode;
            userPostcode = $('#5').val();
            var userEmail;
            userEmail = $('#6').val();
            var userPassword;
            userPassword = $('#7').val();
            profile = {userName: userName, login: Login, userCountry: userCountry,
                userCity: userCity, userAddress: userAddress, userPostcode: userPostcode,
                userEmail: userEmail, userPassword: userPassword};
            var request = new Object();
            request.data = JSON.stringify(profile);
            request.command = 'update_profile';

            $.ajax({
                url: "Controller",
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(request),
                contentType: 'application/json',
                mimeType: 'application/json',

                success: function (data) {
                    if (data.id == 1) {
                        location.reload();
                    }
                    else if (data.id == -1) {
                        alert("This login is not available");
                    }
                    else {
                        $('#errorMess').attr('style', 'visibility: visible;')
                    }
                },
                error: function (data, status, er) {
                    $('#errorMess').attr('style', 'visibility: visible;')
                    alert("error: " + data + " status: " + status + " er:" + er);
                }
            });
        }

        function changeProfile() {

            tableState = $('#profile').html();

            //adding save button
            var btn = '<br><button class="btn btn-success " type="button" id="SaveProfileBtn" onclick="saveProfile()"><fmt:message key="cart.save"/></button>';
            var btn2 = '<button class="btn btn-danger " type="button" id="CancelBtn" onclick="cancelChanging()"><fmt:message key="cart.cancel"/></button>';
            $('#changeOrderBtn').after(btn);
            $('#SaveProfileBtn').after(btn2);

            //adding input for Name
            var name = $('#Name');
            var nameText = $(name).text();
            var inputName = '<div class="form-group"><input name="Name" id="1"  type="text" required class="form-control" value="' + nameText + '"><div/>';
            $(name).html(inputName)

            //adding input for Login
            var login = $('#Login');
            var loginText = $(login).text();
            var inputLogin = '<div class="form-group"><input name="Login" id="2" type="text" required class="form-control" value="' + loginText + '"><div/>';
            $(login).html(inputLogin)

            //adding input for Country
            var country = $('#Country');
            var countryText = $(country).text();
            var inputCountry = '<div class="form-group"><input name="Country" id="3" type="text" required class="form-control" value="' + countryText + '"><div/>';
            $(country).html(inputCountry)

            //adding input for City
            var city = $('#City');
            var cityText = $(city).text();
            var inputCity = '<div class="form-group"><input name="City" id="8" type="text" required class="form-control" value="' + cityText + '"><div/>';
            $(city).html(inputCity)

            //adding input for Address
            var address = $('#Address');
            var addressText = $(address).text();
            var inputAddress = '<div class="form-group"><input name="Address" id="4" type="text" required class="form-control" value="' + addressText + '"><div/>';
            $(address).html(inputAddress)

            //adding input for Postcode
            var postcode = $('#Postcode');
            var postcodeText = $(postcode).text();
            var inputPostcode = '<div class="form-group"><input name="Postcode" id="5" type="text" required class="form-control" value="' + postcodeText + '"><div/>';
            $(postcode).html(inputPostcode)

            //adding input for Email
            var email = $('#Email');
            var emailText = $(email).text();
            var inputEmail = '<div class="form-group"><input name="Email" id="6" type="text" required  class="form-control" value="' + emailText + '" ><div/>';
            $(email).html(inputEmail)

            //adding input for Password
            var password = $('#Password');
            var passwordText = $(password).text();
            var inputPassword = '<div class="form-group"><input name="Password" id="7" type="text" required  class="form-control" value="' + passwordText + '"><div/>';
            $(password).html(inputPassword)

            document.getElementById('changeOrderBtn').setAttribute('disabled', 'disabled');
            $('#changeOrderBtn').hide();
        }

    </script>
</head>
<body>
<%@include file="header.jsp" %>

<div id="log" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="section-title">My Profile</h1>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo_f" items="${first_photos}">
                    <div class="item-small">
                        <img src="${photo_f.coverOfPeriodical}" alt="Periodical">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->

            <div class="col-md-8 col-sm-6">

                <div class="item-large">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="${user.userPhoto}" alt="My photo">

                            <form action="Controller" id="ListOrders" method="post">
                                <input type="hidden" name="command" value="order_list">
                                <br>
                                <button class="btn btn-success " type="submit" id="ViewAllOrders">
                                    <fmt:message key="profile.view_orders"/>
                                </button>
                            </form>
                            <br>

                            <form action="Controller" id="ListComments" method="post">
                                <input type="hidden" name="command" value="comment_list">
                                <button class="btn btn-success " type="submit" id="ViewAllComments">
                                    <fmt:message key="profile.view_comments"/>
                                </button>
                            </form>
                        </div>
                        <div class="col-md-9">
                            <form method="post" action="Controller">
                                <table class="table " id="profile">
                                    <tr>
                                        <td width="40px"> <fmt:message key="profile.name"/></td>
                                        <td width="100px" id="Name">${user.userName}</td>
                                    </tr>
                                    <tr>
                                        <td width="40px"><fmt:message key="profile.login"/></td>
                                        <td width="100px" id="Login">${user.login}</td>
                                    </tr>
                                    <tr>
                                        <td width="40px"><fmt:message key="profile.country"/></td>
                                        <td width="100px" id="Country">${user.userCountry}</td>
                                    </tr>
                                    <tr>
                                        <td width="40px"><fmt:message key="profile.city"/></td>
                                        <td width="100px" id="City">${user.userCity}</td>
                                    </tr>
                                    <tr>
                                        <td width="40px"><fmt:message key="profile.address"/></td>
                                        <td width="100px" id="Address">${user.userAddress}</td>
                                    </tr>
                                    <tr>
                                        <td width="40px"><fmt:message key="profile.postcode"/></td>
                                        <td width="100px" id="Postcode">${user.userPostcode}</td>
                                    </tr>
                                    <tr>
                                        <td width="40px"><fmt:message key="profile.email"/></td>
                                        <td width="100px" id="Email">${user.userEmail}</td>
                                    </tr>
                                    <tr>
                                        <td width="40px"><fmt:message key="profile.password"/></td>
                                        <td width="100px" id="Password">${user.userPassword}</td>
                                    </tr>
                                </table>
                                <br>
                                <button class="btn btn-default " type="button" id="changeOrderBtn"
                                        <c:if test="${orders.size() == 0}">disabled="disabled" </c:if>
                                        onclick="changeProfile()">
                                    <fmt:message key="cart.change"/>
                                </button>
                                <br>

                                <p id="errorMes" style="visibility: hidden;">
                                    <fmt:message key="profile.error"/></p>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.item-large -->

            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-2 col-sm-3">
                <c:forEach var="photo" items="${last_photos}">
                    <div class="item-small">
                        <img src="${photo.coverOfPeriodical}" alt="Product 1">
                    </div>
                    <!-- /.item-small -->
                    <br/>
                </c:forEach>
            </div>
            <!-- /.col-md-2 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /#product-promotion -->


<div class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                  <span>
                    	Copyright &copy; 2015 Gerasimovich Tatiana
                    </span>
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6 col-sm-6">
                <ul class="social">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                    <li><a href="#" class="fa fa-rss"></a></li>
                </ul>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.site-footer -->


<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>

<script src="jsp/js/bootstrap.js"></script>
<script src="jsp/js/plugins.js"></script>
<script src="jsp/js/main.js"></script>
<script type="jsp/text/javascript">

    $(document).ready(function() {
        $(function() {
            $(window).scroll(function() {
                if($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });

            $('#toTop').click(function() {
                $('body,html').animate({scrollTop:0},800);
            });
        });
    });




</script>
</body>
</html>
