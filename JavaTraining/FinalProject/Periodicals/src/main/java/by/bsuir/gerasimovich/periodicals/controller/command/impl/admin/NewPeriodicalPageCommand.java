package by.bsuir.gerasimovich.periodicals.controller.command.impl.admin;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.service.AdminService;
import by.bsuir.gerasimovich.periodicals.service.ServiceResponse;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * the transition to the page of creating a new periodical
 *
 * @author Tatiana
 * @version 1.00 01.06.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class NewPeriodicalPageCommand implements ICommand {
    /**
     * Constants to set request parameters
     */
    private static final String CATEGORIES = "categories";

    /**
     * Override method that call special logic of
     * transition to the page of  creating a new periodical
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        try {
            ServiceResponse serviceResponse = AdminService.getInstance().categoryList();
            request.setAttribute(CATEGORIES, serviceResponse.get(ServiceResponse.CATEGORIES));
            page = PageHelper.getInstance().getProperty(PageHelper.NEW_PERIODICAL_PAGE);

        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        return page;
    }
}
