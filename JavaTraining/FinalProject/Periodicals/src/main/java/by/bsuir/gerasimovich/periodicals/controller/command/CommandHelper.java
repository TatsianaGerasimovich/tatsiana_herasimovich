package by.bsuir.gerasimovich.periodicals.controller.command;


import by.bsuir.gerasimovich.periodicals.controller.command.impl.admin.*;
import by.bsuir.gerasimovich.periodicals.controller.command.impl.guest.*;
import by.bsuir.gerasimovich.periodicals.controller.command.impl.user.*;

import java.util.EnumMap;
import java.util.Map;

/**
 * One of the management classes.
 * That implements the command pattern, pattern factory and pattern Singleton.
 * Contains a list of possible commands.
 * Calls one of the possible list of commands and depending on the parameter passed to the request
 *
 * @author Tatiana
 * @version 1.00 23.05.2015.
 */
public class CommandHelper {
    /**
     * Main unique
     */
    private static CommandHelper requestHelper;
    /**
     * Map that store main commands
     */
    private Map<CommandName, ICommand> commands = new EnumMap<>(CommandName.class);

    /**
     * Private constructor, which initialize possible
     * commands/ Bind request parameter with suitable command
     */
    private CommandHelper() {
        commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());
        commands.put(CommandName.PERIODICAL_LIST, new PeriodicalsListCommand());
        commands.put(CommandName.PERIODICAL_LIST_BY_CATEGORY, new PeriodicalListByCategoryCommand());
        commands.put(CommandName.SHOW_PERIODICAL, new ShowPeriodicalCommand());
        commands.put(CommandName.SIGN_IN_PAGE, new SignInPageCommand());
        commands.put(CommandName.SIGN_UP_PAGE, new SignUpPageCommand());
        commands.put(CommandName.SIGN_IN, new SignInCommand());
        commands.put(CommandName.SIGN_UP, new SignUpCommand());
        commands.put(CommandName.LOG_OUT, new LogOutCommand());
        commands.put(CommandName.ADD_TO_CART, new AddToCartCommand());
        commands.put(CommandName.VIEW_CART, new ViewCartCommand());
        commands.put(CommandName.CREATE_ORDER, new CreateOrderCommand());
        commands.put(CommandName.ORDER_LIST, new OrderListCommand());
        commands.put(CommandName.UPDATE_CART, new UpdateCartCommand());
        commands.put(CommandName.CREATE_COMMENT, new CreateCommentCommand());
        commands.put(CommandName.UPDATE_ORDERS, new UpdateOrdersCommand());
        commands.put(CommandName.COMMENT_LIST, new CommentListCommand());
        commands.put(CommandName.UPDATE_COMMENTS, new UpdateCommentsCommand());
        commands.put(CommandName.VIEW_PROFILE, new ViewProfileCommand());
        commands.put(CommandName.UPDATE_PROFILE, new UpdateProfileCommand());
        commands.put(CommandName.USERS_PAGE, new UserListCommand());
        commands.put(CommandName.REMOVE_USER, new RemoveUserCommand());
        commands.put(CommandName.VIEW_ALL_ORDERS, new ViewAllOrdersCommand());
        commands.put(CommandName.MAKE_ADMIN, new MakeAdminCommand());
        commands.put(CommandName.PAID_ORDER, new PaidOrderCommand());
        commands.put(CommandName.REMOVE_ORDER, new RemoveOrderCommand());
        commands.put(CommandName.PERIODICAL_LIST_FOR_DELETE, new PeriodicalListForDeleteCommand());
        commands.put(CommandName.REMOVE_PERIODICAL, new RemovePeriodicalCommand());
        commands.put(CommandName.NEW_PERIODICAL_PAGE, new NewPeriodicalPageCommand());
        commands.put(CommandName.NEW_PERIODICAL, new CreatePeriodicalCommand());
        commands.put(CommandName.CHANGE_LANGUAGE, new ChangeLocalCommand());
    }

    /**
     * Method that returns unique CommandHelper instance
     * Standard realization of singleton pattern
     *
     * @return CommandHelper instance
     */
    public static CommandHelper getInstance() {
        if (requestHelper == null)
            requestHelper = new CommandHelper();
        return requestHelper;
    }

    /**
     * Method take from request parameter and get controller the command
     * from the <map>commands</map> with a String key which he matches
     * @param commandName
     * @return on if command in <map>commands</map>
     */
    public ICommand getCommand(String commandName) {
        CommandName action;
        try {
            action = CommandName.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException ex) {
            action = null;
        }
        ICommand command;
        if (action == null) {
            command = commands.get(CommandName.NO_SUCH_COMMAND);
        } else {
            command = commands.get(action);
        }
        return command;
    }
}
