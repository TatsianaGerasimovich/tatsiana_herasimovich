package by.bsuir.gerasimovich.periodicals.controller.command.impl.admin;

import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Orders;
import by.bsuir.gerasimovich.periodicals.service.AdminService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * deleting a specific order
 *
 * @author Tatiana
 * @version 1.00 01.06.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class RemoveOrderCommand implements ICommand {
    /**
     * Constants to set request parameters
     */
    private static final String JSON = "json";

    /**
     * Override method that call special logic of
     * deleting a specific order from general list
     *
     * @param request
     * @param response
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Orders order = mapper.readValue(json, Orders.class);
            AdminService.getInstance().removeOrder(order);
            mapper.writeValue(response.getOutputStream(), -1);
        } catch (IOException | ServiceException ex) {
            throw new CommandException(ex);
        }
        return null;
    }
}

