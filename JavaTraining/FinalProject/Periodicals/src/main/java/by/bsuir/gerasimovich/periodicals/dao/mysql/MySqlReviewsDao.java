package by.bsuir.gerasimovich.periodicals.dao.mysql;

import by.bsuir.gerasimovich.periodicals.dao.AbstractJDBCDao;
import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlDaoException;
import by.bsuir.gerasimovich.periodicals.entity.Reviews;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public class MySqlReviewsDao extends AbstractJDBCDao<Reviews> {

    private static MySqlReviewsDao instance;
    private final String SELECT = "SELECT reviewId, reviewDescription, userId, periodicalId, statusRemove   FROM reviews WHERE statusRemove= false;";
    private final String SELECT_BY_PK = "SELECT reviewId, reviewDescription, userId, periodicalId, statusRemove   FROM reviews WHERE statusRemove= false AND reviewId=?;";
    private final String SELECT_BY_USER = "SELECT reviewId, reviewDescription, userId, periodicalId, statusRemove FROM reviews WHERE statusRemove= false AND userId = ?;";
    private final String INSERT = "INSERT INTO reviews ( reviewDescription, userId, periodicalId, statusRemove) \n"
            + "VALUES ( ?, ?, ?, ?);";
    private final String UPDATE = "UPDATE reviews SET reviewDescription = ?, userId = ?, periodicalId = ?, statusRemove =?   WHERE reviewId= ?;";
    private final String UPDATE_FOR_DELETE = "UPDATE reviews SET statusRemove = 1  WHERE reviewId= ?;";
    private final String DELETE = "DELETE FROM reviews WHERE reviewId = ?;";
    private final String SELECT_BY_PERIODICAL = "SELECT reviewId, reviewDescription, userId, periodicalId, statusRemove  FROM reviews WHERE statusRemove= false AND periodicalId = ?;";

    private MySqlReviewsDao() throws DAOException {
        super();

    }

    public static MySqlReviewsDao getInstance() throws DAOException {
        MySqlReviewsDao localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlReviewsDao.class) {
                localInstance=instance;
                if (localInstance == null) {
                    instance = localInstance= new MySqlReviewsDao();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getSelectByPkQuery() {
        return SELECT_BY_PK;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {

        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    protected List<Reviews> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<Reviews> result = new LinkedList<Reviews>();
        try {
            while (rs.next()) {

                Reviews review = new Reviews();
                review.setId(rs.getInt("reviewId"));
                review.setReviewDescription(rs.getString("reviewDescription"));
                review.setUserId(rs.getInt("userId"));
                review.setPeriodicalId(rs.getInt("periodicalId"));
                review.setStatusRemove(rs.getBoolean("statusRemove"));
                result.add(review);
            }
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
        return result;
    }
    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Reviews object) throws DAOException {
        try {
            statement.setString(1, object.getReviewDescription());
            statement.setInt(2, object.getUserId());
            statement.setInt(3, object.getPeriodicalId());
            statement.setBoolean(4, object.getStatusRemove());

        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Reviews object) throws DAOException {
        try {
            statement.setString(1, object.getReviewDescription());
            statement.setInt(2, object.getUserId());
            statement.setInt(3, object.getPeriodicalId());
            statement.setBoolean(4, object.getStatusRemove());
            statement.setInt(5, object.getId());
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    public List<Reviews> getAllByPeriodical(int periodicalId) throws DAOException {
        List<Reviews> list;
        ResultSet rs = null;

        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_PERIODICAL)) {
            statement.setInt(1, periodicalId);
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                putConnection();
            } catch (SQLException e) {
                throw new DAOException(e);
            }

        }
        return list;
    }

    public List<Reviews> getAllByUser(int clientId) throws DAOException {
        List<Reviews> list;
        ResultSet rs = null;

        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_USER)) {
            statement.setInt(1, clientId);
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                putConnection();
            } catch (SQLException e) {
                throw new DAOException(e);
            }

        }
        return list;
    }

    public void updateForDelete(int reviewId) throws DAOException {
        try (PreparedStatement statement = getConnection().prepareStatement(UPDATE_FOR_DELETE)) {
            statement.setInt(1, reviewId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On update modify more then 1 record: " + count);
            }
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            putConnection();

        }
    }
}