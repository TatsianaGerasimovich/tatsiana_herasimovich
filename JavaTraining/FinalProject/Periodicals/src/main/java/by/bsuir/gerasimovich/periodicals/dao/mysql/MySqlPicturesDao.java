package by.bsuir.gerasimovich.periodicals.dao.mysql;

import by.bsuir.gerasimovich.periodicals.dao.AbstractJDBCDao;
import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlDaoException;
import by.bsuir.gerasimovich.periodicals.entity.Pictures;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public class MySqlPicturesDao extends AbstractJDBCDao<Pictures> {

    private static MySqlPicturesDao instance;
    private final String SELECT = "SELECT pictureId, picturePath, periodicalId, statusRemove, isMain   FROM pictures WHERE statusRemove= false;";
    private final String SELECT_BY_PK = "SELECT pictureId, picturePath, periodicalId, statusRemove, isMain   FROM pictures WHERE statusRemove= false AND pictureId=?;";
    private final String INSERT = "INSERT INTO pictures (pictureId, picturePath, periodicalId, statusRemove, isMain) \n"
            + "VALUES (?, ?, ?, ?, ?);";
    private final String UPDATE = "UPDATE pictures SET picturePath = ?, periodicalId = ?, statusRemove =?, isMain = ?   WHERE pictureId= ?;";
    private final String DELETE = "DELETE FROM pictures WHERE pictureId = ?;";
    private final String SELECT_BY_PERIODICAL = "SELECT pictureId, picturePath, periodicalId, statusRemove, isMain   FROM pictures WHERE statusRemove= false AND periodicalId = ?;";

    private MySqlPicturesDao() throws DAOException {
        super();

    }

    public static MySqlPicturesDao getInstance() throws DAOException {
        MySqlPicturesDao localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlPicturesDao.class) {
                localInstance=instance;
                if (localInstance == null) {
                    instance = localInstance= new MySqlPicturesDao();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getSelectByPkQuery() {
        return SELECT_BY_PK;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {

        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    protected List<Pictures> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<Pictures> result = new LinkedList<Pictures>();
        try {
            while (rs.next()) {

                Pictures picture = new Pictures();
                picture.setId(rs.getInt("pictureId"));
                picture.setPicturePath(rs.getString("picturePath"));
                picture.setPeriodicalId(rs.getInt("periodicalId"));
                picture.setStatusRemove(rs.getBoolean("statusRemove"));
                picture.setIsMain(rs.getBoolean("isMain"));
                result.add(picture);
            }
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Pictures object) throws DAOException {
        try {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getPicturePath());
            statement.setInt(4, object.getPeriodicalId());
            statement.setBoolean(5, object.getStatusRemove());
            statement.setBoolean(5, object.getIsMain());

        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Pictures object) throws DAOException {
        try {
            statement.setString(1, object.getPicturePath());
            statement.setInt(2, object.getPeriodicalId());
            statement.setBoolean(3, object.getStatusRemove());
            statement.setBoolean(4, object.getIsMain());
            statement.setInt(5, object.getId());
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    public List<Pictures> getAllByPeriodical(int periodicalId) throws DAOException {
        List<Pictures> list;
        ResultSet rs = null;

        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_PERIODICAL)) {
            statement.setInt(1, periodicalId);
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                putConnection();
            } catch (SQLException e) {
                throw new DAOException(e);
            }

        }
        return list;
    }
}