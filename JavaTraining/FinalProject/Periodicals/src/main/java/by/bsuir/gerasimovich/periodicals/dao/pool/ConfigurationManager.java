package by.bsuir.gerasimovich.periodicals.dao.pool;

import by.bsuir.gerasimovich.periodicals.dao.pool.exception.ParseException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class ConfigurationManager {
    /**
     * main constants, which correspond to the CarouselTag-name
     * of <file>config.xml</file>
     */
    //private static final String XML_FILE_NAME = "src/main/resources/config.xml";
    private static final String XML_FILE_NAME = "D:\\Work\\idea\\EPAM\\Periodicals\\src\\main\\resources\\config.xml";
    private static final String DATABASE_CONNECTION= "database-connection";
    public static final String DATABASE_DRIVER_NAME= "database-driver-name";
    public static final String DATABASE_URL= "database-url";
    public static final String DATABASE_LOGIN= "database-login";
    public static final String DATABASE_PASSWORD = "database-password";
    public static final String DATABASE_MAX_CONNECTION_VALUE= "max-value";
    public static final String DATABASE_MIN_CONNECTION_VALUE= "min-value";

    /**
     * Collection in which store parse config information
     */
    private static Map<String, String> configInformation         = new HashMap<String, String>();

    /**
     * Method that parse <file>config.xml</file>
     * and save all information in collection
     * <collection>configInformation</collection>
     */
    public void initConnectionProperties() throws ParseException {

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        org.w3c.dom.Document document = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(new File(XML_FILE_NAME));

        } catch (ParserConfigurationException e) {
            throw new ParseException(e);
        } catch (SAXException e) {
            throw new ParseException(e);
        } catch (IOException e) {
            throw new ParseException(e);
        }

        NodeList nodeList = document.getElementsByTagName(DATABASE_CONNECTION);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element element = (Element) nodeList.item(i);
            configInformation.put(DATABASE_DRIVER_NAME, element.getElementsByTagName(DATABASE_DRIVER_NAME).item(0).getTextContent());
            configInformation.put(DATABASE_URL, element.getElementsByTagName(DATABASE_URL).item(0).getTextContent());
            configInformation.put(DATABASE_LOGIN, element.getElementsByTagName(DATABASE_LOGIN).item(0).getTextContent());
            configInformation.put(DATABASE_PASSWORD, element.getElementsByTagName(DATABASE_PASSWORD).item(0).getTextContent());
            configInformation.put(DATABASE_MAX_CONNECTION_VALUE, element.getElementsByTagName(DATABASE_MAX_CONNECTION_VALUE).item(0).getTextContent());
            configInformation.put(DATABASE_MIN_CONNECTION_VALUE, element.getElementsByTagName(DATABASE_MIN_CONNECTION_VALUE).item(0).getTextContent());
        }
    }

    /**
     * Get method that return collection with config information
     * @return Map<String, String>
     */
    public static Map<String, String> getConfigInformation() {
        return configInformation;
    }
}


