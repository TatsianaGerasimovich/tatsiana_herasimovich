package by.bsuir.gerasimovich.periodicals.controller.filter;

import by.bsuir.gerasimovich.periodicals.entity.Users;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * @author Tatiana
 * @version 1.00 02.06.2015.
 */
public class MultipartFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(MultipartFilter.class);
    private static final String COMMAND = "command";
    private static final String SIGN_UP = "sign_up";
    private static final String NEW_PERIODICAL = "new_periodical";
    private static final String TEMPDIR = "javax.servlet.context.tempdir";


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        boolean isMultipart = ServletFileUpload.isMultipartContent(httpRequest);
        if (isMultipart) {

            // Создаём класс фабрику
            DiskFileItemFactory factory = new DiskFileItemFactory();

            // Максимальный буфера данных в байтах,
            // при его привышении данные начнут записываться на диск во временную директорию
            // устанавливаем один мегабайт
            factory.setSizeThreshold(1024*1024);
            HttpSession session = httpRequest.getSession();
            // устанавливаем временную директорию
            File tempDir = (File)session.getServletContext().getAttribute(TEMPDIR);
            factory.setRepository(tempDir);

            //Создаём сам загрузчик
            ServletFileUpload upload = new ServletFileUpload(factory);

            //максимальный размер данных который разрешено загружать в байтах
            //по умолчанию -1, без ограничений. Устанавливаем 10 мегабайт.
        //    upload.setSizeMax(1024 * 1024 * 10);

            try {
                List items = upload.parseRequest(httpRequest);
                Iterator iter = items.iterator();

                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();

                    if (item.isFormField()) {
                        //если принимаемая часть данных является полем формы
                        processFormField(item, request);
                    } else {
                        //в противном случае рассматриваем как файл
                        processUploadedFile(item, request);
                    }
                }
            } catch (Exception e) {
                LOG.error(e);
            }

        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

    private void processUploadedFile(FileItem item,ServletRequest request) throws Exception {
        File uploadetFile = null;
        Random random = new Random();
        if(item.getSize()==0){
            request.setAttribute(item.getFieldName(), Users.NO_PHOTO);
        }
        else {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession();
            String path=null;
            //выбираем файлу имя пока не найдём свободное
            do {
                String nameFile=random.nextInt() + item.getName();
                String command = (String) request.getAttribute(COMMAND);
                if(command.equals(SIGN_UP)) {
                    path = session.getServletContext().getRealPath("/jsp/images/userPhoto/" + nameFile);
                    uploadetFile = new File(path);
                    path = "jsp/images/userPhoto/" + nameFile;
                }
                else if(command.equals(NEW_PERIODICAL)){
                    path = session.getServletContext().getRealPath("/jsp/images/products/" + nameFile);
                    uploadetFile = new File(path);
                    path = "jsp/images/products/" + nameFile;
                }
            } while (uploadetFile.exists());

            //создаём файл
            uploadetFile.createNewFile();
            //записываем в него данные
            item.write(uploadetFile);
            request.setAttribute(item.getFieldName(), path);
        }
    }

    private void processFormField(FileItem item,ServletRequest request) {
        request.setAttribute(item.getFieldName(), item.getString());
    }

}

