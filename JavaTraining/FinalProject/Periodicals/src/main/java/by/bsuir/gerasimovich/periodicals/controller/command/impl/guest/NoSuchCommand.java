package by.bsuir.gerasimovich.periodicals.controller.command.impl.guest;


import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * transition to the error page when entering an incorrect command
 *
 * @author Tatiana
 * @version 1.00 23.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class NoSuchCommand implements ICommand {

    /**
     * Override method that call special logic of
     * transition to the error page when entering an incorrect command
     *
     * @param request
     * @param response
     * @return String(forward page)
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return PageHelper.getInstance().getProperty(PageHelper.ERROR_PAGE);
    }
}
