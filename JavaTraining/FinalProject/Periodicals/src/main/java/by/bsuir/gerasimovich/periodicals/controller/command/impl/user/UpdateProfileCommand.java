package by.bsuir.gerasimovich.periodicals.controller.command.impl.user;

import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.UserService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * saving the changes of profile
 *
 * @author Tatiana
 * @version 1.00 30.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class UpdateProfileCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String USER = "user";
    private static final String JSON = "json";

    /**
     * Override method that call special logic of
     * saving the changes of profile
     *
     * @param request
     * @param response
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        ObjectMapper mapper = new ObjectMapper();
        String json = (String) request.getAttribute(JSON);

        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute(USER);

        try {
            Users newProfile = mapper.readValue(json, new TypeReference<Users>() {
            });
            UpdatingProfile up = new UpdatingProfile();
            up.setId(UserService.getInstance().updateProfile(newProfile, user));
            mapper.writeValue(response.getOutputStream(), up);

        } catch (IOException | ServiceException ex) {
            throw new CommandException(ex);
        }

        return null;
    }

    /**
     * Static class is designed to send an ajax response
     */
    static class UpdatingProfile {
        private int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

    }
}
