package by.bsuir.gerasimovich.periodicals.dao.mysql;

import by.bsuir.gerasimovich.periodicals.dao.AbstractJDBCDao;
import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlDaoException;
import by.bsuir.gerasimovich.periodicals.entity.Users;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public class MySqlUsersDao extends AbstractJDBCDao<Users> {

    private static MySqlUsersDao instance;
    private final String SELECT = "SELECT userId, userEmail, userAddress, userCity, userPostcode, userCountry, userPhoto, login, userPassword, userRole, userName   FROM users WHERE statusRemove= false;";
    private final String SELECT_BY_PK = "SELECT userId, userEmail, userAddress, userCity, userPostcode, userCountry, userPhoto, login, userPassword, userRole, userName   FROM users WHERE statusRemove= false AND userId = ?;";
    private final String SELECT_BY_LOGIN = "SELECT userId, userEmail, userAddress, userCity, userPostcode, userCountry, userPhoto, login, userPassword, userRole, userName   FROM users WHERE statusRemove= false AND login = ?;";
    private final String INSERT = "INSERT INTO users ( userEmail, userAddress, userCity, userPostcode, userCountry, userPhoto, login, userPassword, userRole, userName) \n"
            + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private final String UPDATE = "UPDATE users SET userEmail = ?, userAddress = ?, userCity = ?, userPostcode =?, userCountry = ?, userPhoto = ?, login = ?, userPassword = ?, userRole =?, userName = ?   WHERE userId= ?;";
    private final String UPDATE_FOR_DELETE = "UPDATE users SET statusRemove = 1  WHERE userId= ?;";
    private final String DELETE = "DELETE FROM users WHERE userId = ?;";

    private MySqlUsersDao() throws DAOException {
        super();

    }

    public static MySqlUsersDao getInstance() throws DAOException {
        MySqlUsersDao localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlUsersDao.class) {
                localInstance=instance;
                if (localInstance == null) {
                    instance = localInstance= new MySqlUsersDao();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getSelectByPkQuery() {
        return SELECT_BY_PK;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {

        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    protected List<Users> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<Users> result = new LinkedList<Users>();
        try {
            while (rs.next()) {

                Users user = new Users();
                user.setId(rs.getInt("userId"));
                user.setUserEmail(rs.getString("userEmail"));
                user.setUserAddress(rs.getString("userAddress"));
                user.setUserCity(rs.getString("userCity"));
                user.setUserPostcode(rs.getDouble("userPostcode"));
                user.setUserCountry(rs.getString("userCountry"));
                user.setUserPhoto(rs.getString("userPhoto"));
                user.setLogin(rs.getString("login"));
                user.setUserPassword(rs.getString("userPassword"));
                user.setUserRole(rs.getString("userRole"));
                user.setUserName(rs.getString("userName"));
                result.add(user);
            }
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
        return result;
    }
    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Users object) throws DAOException {
        try {

            statement.setString(1, object.getUserEmail());
            statement.setString(2, object.getUserAddress());
            statement.setString(3, object.getUserCity());
            statement.setDouble(4, object.getUserPostcode());
            statement.setString(5, object.getUserCountry());
            statement.setString(6, object.getUserPhoto());
            statement.setString(7, object.getLogin());
            statement.setString(8, object.getUserPassword());
            statement.setString(9, object.getUserRole());
            statement.setString(10, object.getUserName());

        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Users object) throws DAOException {
        try {
            statement.setString(1, object.getUserEmail());
            statement.setString(2, object.getUserAddress());
            statement.setString(3, object.getUserCity());
            statement.setDouble(4, object.getUserPostcode());
            statement.setString(5, object.getUserCountry());
            statement.setString(6, object.getUserPhoto());
            statement.setString(7, object.getLogin());
            statement.setString(8, object.getUserPassword());
            statement.setString(9, object.getUserRole());
            statement.setString(10, object.getUserName());
            statement.setInt(11, object.getId());
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    public List<Users> getAllByLogin(String login) throws DAOException {
        List<Users> list;
        ResultSet rs = null;

        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_LOGIN)) {
            statement.setString(1, login);
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                putConnection();
            } catch (SQLException e) {
                throw new DAOException(e);
            }

        }
        return list;
    }
    public void updateForDelete(int userId) throws DAOException {
        try (PreparedStatement statement = getConnection().prepareStatement(UPDATE_FOR_DELETE)) {
            statement.setInt(1, userId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On update modify more then 1 record: " + count);
            }
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            putConnection();

        }
    }
}