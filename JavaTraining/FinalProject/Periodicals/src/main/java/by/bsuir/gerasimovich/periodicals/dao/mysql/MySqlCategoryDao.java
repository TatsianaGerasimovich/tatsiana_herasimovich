package by.bsuir.gerasimovich.periodicals.dao.mysql;

import by.bsuir.gerasimovich.periodicals.dao.AbstractJDBCDao;
import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlDaoException;
import by.bsuir.gerasimovich.periodicals.entity.Category;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class MySqlCategoryDao extends AbstractJDBCDao<Category> {

    private static MySqlCategoryDao instance;
    private final String SELECT = "SELECT categoryId, categoryName FROM category";
    private final String SELECT_BY_PK = "SELECT categoryId, categoryName FROM category WHERE categoryId = ?";
    private final String INSERT = "INSERT INTO category (categoryId, categoryName) \n"
            + "VALUES (?, ?);";
    private final String UPDATE = "UPDATE category SET categoryName = ?  WHERE categoryId= ?;";
    private final String DELETE = "DELETE FROM category WHERE categoryId = ?;";

    private MySqlCategoryDao() throws DAOException {
        super();

    }

    public static MySqlCategoryDao getInstance() throws DAOException {
        MySqlCategoryDao localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlCategoryDao.class) {
                localInstance=instance;
                if (localInstance == null) {
                    instance = localInstance= new MySqlCategoryDao();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getSelectByPkQuery() {
        return SELECT_BY_PK;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {

        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    protected List<Category> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<Category> result = new LinkedList<Category>();
        try {
            while (rs.next()) {
                Category category = new Category();
                category.setId(rs.getInt("categoryId"));
                category.setCategoryName(rs.getString("categoryName"));
                result.add(category);
            }
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
        return result;
    }
    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Category object) throws DAOException {
        try {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getCategoryName());

        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Category object) throws DAOException {
        try {
            statement.setString(1, object.getCategoryName());
            statement.setInt(2, object.getId());
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }
}
