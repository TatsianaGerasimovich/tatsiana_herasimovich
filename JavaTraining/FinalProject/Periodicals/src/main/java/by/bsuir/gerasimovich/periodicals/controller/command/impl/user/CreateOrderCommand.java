package by.bsuir.gerasimovich.periodicals.controller.command.impl.user;

import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Orders;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.UserService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * creation a new order
 *
 * @author Tatiana
 * @version 1.00 29.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class CreateOrderCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String JSON = "json";
    private static final String USER = "user";

    /**
     * Override method that call special logic of
     * creation a new order
     *
     * @param request
     * @param response
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        ObjectMapper mapper = new ObjectMapper();
        String json = (String) request.getAttribute(JSON);
        Orders newOrder = null;
        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute(USER);


        try {
            newOrder = mapper.readValue(json, Orders.class);
            if (newOrder != null && newOrder.getPeriodicals().size() > 0) {
                newOrder.setClientId(user.getId());
                UserService.getInstance().createOrder(newOrder, user);
                AjaxResponse ajaxResponse = new AjaxResponse();
                ajaxResponse.setCode(1);
                mapper.writeValue(response.getOutputStream(), ajaxResponse);
            }
        } catch (ServiceException | IOException ex) {
            throw new CommandException(ex);
        }
        return null;
    }
    /**
     * Static class is designed to send an ajax response
     */
    static class AjaxResponse {
        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
