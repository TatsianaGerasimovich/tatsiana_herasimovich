package by.bsuir.gerasimovich.periodicals.dao;

import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.pool.ConnectionPool;
import by.bsuir.gerasimovich.periodicals.dao.pool.exception.ConnectionPoolException;
import by.bsuir.gerasimovich.periodicals.entity.GeneralEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * abstract class provides a base implementation CRUD operations using JDBC.
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public abstract class AbstractJDBCDao<T extends GeneralEntity> implements GenericDao<T> {

    private Connection connection;


    /**
     * Returns sql query to retrieve all records. SELECT * FROM [Table]
     */
    public abstract String getSelectQuery();

    /**
     * INSERT INTO [Table] ([column, column, ...]) VALUES (?, ?, ...);
     */
    public abstract String getCreateQuery();

    /**
     * UPDATE [Table] SET [column = ?, column = ?, ...] WHERE id = ?;
     */
    public abstract String getUpdateQuery();

    /**
     * DELETE FROM [Table] WHERE id= ?;
     */
    public abstract String getDeleteQuery();

    public abstract String getSelectByPkQuery();

    /**
     * ResultSet parses and returns a list appropriate object
     * content ResultSet.
     */
    protected abstract List<T> parseResultSet(ResultSet rs) throws DAOException;

    protected abstract void prepareStatementForInsert(PreparedStatement statement, T object) throws DAOException;

    protected abstract void prepareStatementForUpdate(PreparedStatement statement, T object) throws DAOException;

    @Override
    public int create(T object) throws DAOException {
        String sql = getCreateQuery();
        int key=0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (ConnectionPoolException e) {
            throw new DAOException(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            prepareStatementForInsert(statement, object);
            int count = statement.executeUpdate();
            ResultSet rs=statement.getGeneratedKeys();
            while (rs.next()) {
                key=rs.getInt(1);
             }
            if (count != 1) {
                throw new DAOException("On persist modify more then 1 record: " + count);
            }
            if (statement != null)
                statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            if (connection != null)
                try {
                    ConnectionPool.getInstance().closeConnection(connection);
                } catch (ConnectionPoolException e) {
                    throw new DAOException(e);
                }
        }
        return key;
    }

    @Override
    public T getByPK(Integer key) throws DAOException {
        List<T> list;
        String sql = getSelectByPkQuery();
        ResultSet rs = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (ConnectionPoolException e) {
            throw new DAOException(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, key);
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
            if (connection != null)
                try {
                    ConnectionPool.getInstance().closeConnection(connection);
                } catch (ConnectionPoolException e) {
                    throw new DAOException(e);
                }

        }
        if (list == null || list.size() == 0) {
            throw new DAOException("Record with PK = " + key + " not found.");
        }
        if (list.size() > 1) {
            throw new DAOException("Received more than one record.");
        }
        return list.iterator().next();
    }

    @Override
    public void update(T object) throws DAOException {
        String sql = getUpdateQuery();
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (ConnectionPoolException e) {
            throw new DAOException(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(sql);) {
            prepareStatementForUpdate(statement, object);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On update modify more then 1 record: " + count);
            }
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            if (connection != null)
                try {
                    ConnectionPool.getInstance().closeConnection(connection);
                } catch (ConnectionPoolException e) {
                    throw new DAOException(e);
                }

        }
    }

    @Override
    public void delete(T object) throws DAOException {
        String sql = getDeleteQuery();
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (ConnectionPoolException e) {
            throw new DAOException(e);
        }
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            try {
                statement.setObject(1, object.getId());
            } catch (SQLException e) {
                throw new DAOException(e);
            }
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On delete modify more then 1 record: " + count);
            }
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            if (connection != null)
                try {
                    ConnectionPool.getInstance().closeConnection(connection);
                } catch (ConnectionPoolException e) {
                    throw new DAOException(e);
                }
        }
    }

    @Override
    public List<T> getAll() throws DAOException {
        List<T> list;
        ResultSet rs = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (ConnectionPoolException e) {
            throw new DAOException(e);
        }
        String sql = getSelectQuery();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (connection != null)
                    ConnectionPool.getInstance().closeConnection(connection);
            } catch (SQLException e) {
                throw new DAOException(e);
            } catch (ConnectionPoolException e) {
                throw new DAOException(e);
            }

        }
        return list;
    }

    public Connection getConnection() throws DAOException {
        try {
            connection = ConnectionPool.getInstance().getConnection();
        } catch (ConnectionPoolException e) {
            throw new DAOException(e);
        }
        return connection;
    }

    public void putConnection() throws DAOException {
        if (connection != null)
            try {
                ConnectionPool.getInstance().closeConnection(connection);
            } catch (ConnectionPoolException e) {
                throw new DAOException(e);
            }

    }
}
