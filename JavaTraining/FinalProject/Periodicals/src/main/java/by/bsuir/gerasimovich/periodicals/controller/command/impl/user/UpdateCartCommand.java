package by.bsuir.gerasimovich.periodicals.controller.command.impl.user;

import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.PeriodicalInf;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.UserService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * saving the changes of cart
 *
 * @author Tatiana
 * @version 1.00 28.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class UpdateCartCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String USER = "user";
    private static final String JSON = "json";

    /**
     * Override method that call special logic of
     * saving the changes of cart
     *
     * @param request
     * @param response
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        ObjectMapper mapper = new ObjectMapper();
        String json = (String) request.getAttribute(JSON);

        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute(USER);

        try {
            List<PeriodicalInf> data = mapper.readValue(json, new TypeReference<List<PeriodicalInf>>() {
            });
            PeriodicalInf result = UserService.getInstance().updateCart(data, user);
            if (result == null) {
                mapper.writeValue(response.getOutputStream(), -1);
            } else {
                mapper.writeValue(response.getOutputStream(), result);
            }

        } catch (IOException | ServiceException ex) {
            throw new CommandException(ex);
        }
        return null;
    }
}
