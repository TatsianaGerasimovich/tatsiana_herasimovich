package by.bsuir.gerasimovich.periodicals.dao.mysql;

import by.bsuir.gerasimovich.periodicals.dao.GenericDao;
import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.factory.AbstractDAOFactory;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlDaoException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlFactoryException;
import by.bsuir.gerasimovich.periodicals.entity.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class MySqlDaoFactory extends AbstractDAOFactory {
    public interface DaoCreator {
        public GenericDao create() throws MySqlFactoryException;
    }

    private Map<Class, DaoCreator> creators;
    private static MySqlDaoFactory instance;

    public static MySqlDaoFactory getInstance() {
        MySqlDaoFactory localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlDaoFactory.class) {
                localInstance=instance;
                if (localInstance == null) {
                    instance = localInstance= new MySqlDaoFactory();
                }
            }
        }
        return localInstance;
    }
    @Override
    public GenericDao getDao(Class entityClass) throws MySqlFactoryException {
        DaoCreator creator = creators.get(entityClass);
        if (creator == null) {
            throw new MySqlFactoryException("Dao object for " + entityClass + " not found.");
        }
        return creator.create();
    }

    private MySqlDaoFactory() {
        creators = new HashMap<Class,  DaoCreator>();
        creators.put(Category.class, () -> {
            try {
                return  MySqlCategoryDao.getInstance();
            }  catch (DAOException e) {
                throw new MySqlFactoryException(e);
            }
        });
        creators.put(Orders.class, new DaoCreator() {
            @Override
            public GenericDao create() throws MySqlFactoryException {
                try {
                    return MySqlOrdersDao.getInstance();
                } catch (DAOException e) {
                    throw new MySqlFactoryException(e);
                }
            }
        });
        creators.put(Periodicals.class, new DaoCreator() {
            @Override
            public GenericDao create()throws MySqlFactoryException {
                try {
                    return  MySqlPeriodicalsDao.getInstance();
                } catch (DAOException e) {
                    throw new MySqlFactoryException(e);
                }
            }
        });
        creators.put(Pictures.class, new DaoCreator() {
            @Override
            public GenericDao create() throws MySqlFactoryException {
                try {
                    return  MySqlPicturesDao.getInstance();
                } catch (DAOException e) {
                    throw new MySqlFactoryException(e);
                }
            }
        });
        creators.put(Reviews.class, new DaoCreator() {
            @Override
            public GenericDao create() throws MySqlFactoryException{
                try {
                    return  MySqlReviewsDao.getInstance();
                } catch (DAOException e) {
                    throw new MySqlFactoryException(e);
                }
            }
        });
        creators.put(Users.class, new DaoCreator() {
            @Override
            public GenericDao create() throws MySqlFactoryException {
                try {
                    return MySqlUsersDao.getInstance();
                } catch (DAOException e) {
                    throw new MySqlFactoryException(e);
                }
            }
        });
        creators.put(OrderPeriodicals.class, new DaoCreator() {
            @Override
            public GenericDao create()throws MySqlFactoryException {
                try {
                    return MySqlOrderPeriodicalsDao.getInstance();
                } catch (DAOException e) {
                    throw new MySqlFactoryException(e);
                }
            }
        });
    }
}
