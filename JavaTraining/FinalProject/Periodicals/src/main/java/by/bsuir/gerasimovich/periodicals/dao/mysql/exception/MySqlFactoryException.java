package by.bsuir.gerasimovich.periodicals.dao.mysql.exception;

import by.bsuir.gerasimovich.periodicals.dao.factory.exception.AbstractFactoryException;

/**
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class MySqlFactoryException extends AbstractFactoryException{
    /**
     * default constructor
     */
    public MySqlFactoryException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public MySqlFactoryException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public MySqlFactoryException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public MySqlFactoryException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public MySqlFactoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

