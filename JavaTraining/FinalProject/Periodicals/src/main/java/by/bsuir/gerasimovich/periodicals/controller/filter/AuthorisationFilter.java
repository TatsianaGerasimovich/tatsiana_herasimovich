package by.bsuir.gerasimovich.periodicals.controller.filter;


import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.CommandName;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
/**
 * @author Tatiana
 * @version 1.00 29.05.2015.
 */
public class AuthorisationFilter implements Filter {

    public static final String COMMAND_NAME = "command";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session =
                ((HttpServletRequest) request).getSession(true);
        String userStatus = ((Users) session.getAttribute("user")).getUserRole();

        CommandName commandName = getCommandName((HttpServletRequest) request);
        String URI = ((HttpServletRequest) request).getRequestURI();

        boolean authorized = checkAccess(userStatus, commandName);

        if (authorized) {
            chain.doFilter(request, response);
        } else {
            ((HttpServletResponse) response).sendRedirect(PageHelper.
                    getInstance().getProperty(PageHelper.FILTER_ERROR_PAGE));
        }
    }

    @Override
    public void destroy() {

    }

    private boolean checkAccess(String status, CommandName command) {
        UserStatus userStatus = UserStatus.valueOf(status.toUpperCase());
        boolean access = false;
        switch (userStatus) {
            case GUEST: {
                switch (command) {
                    case PERIODICAL_LIST:
                        access = true;
                        break;
                    case PERIODICAL_LIST_BY_CATEGORY:
                        access = true;
                        break;
                    case SHOW_PERIODICAL:
                        access = true;
                        break;
                    case SIGN_IN_PAGE:
                        access = true;
                        break;
                    case SIGN_UP_PAGE:
                        access = true;
                        break;
                    case SIGN_IN:
                        access = true;
                        break;
                    case SIGN_UP:
                        access = true;
                        break;
                    case CHANGE_LANGUAGE:
                        access = true;
                        break;
                }
                break;
            }
            case USER:
                switch (command) {
                    case PERIODICAL_LIST:
                        access = true;
                        break;
                    case PERIODICAL_LIST_BY_CATEGORY:
                        access = true;
                        break;
                    case SHOW_PERIODICAL:
                        access = true;
                        break;
                    case ADD_TO_CART:
                        access = true;
                        break;
                    case VIEW_CART:
                        access = true;
                        break;
                    case ORDER_LIST:
                        access = true;
                        break;
                    case CREATE_ORDER:
                        access = true;
                        break;
                    case UPDATE_CART:
                        access = true;
                        break;
                    case UPDATE_ORDERS:
                        access = true;
                        break;
                    case LOG_OUT:
                        access = true;
                        break;
                    case CREATE_COMMENT:
                        access = true;
                        break;
                    case COMMENT_LIST:
                        access = true;
                        break;
                    case UPDATE_COMMENTS:
                        access = true;
                        break;
                    case VIEW_PROFILE:
                        access = true;
                        break;
                    case UPDATE_PROFILE:
                        access = true;
                        break;
                    case CHANGE_LANGUAGE:
                        access = true;
                        break;

                }
                break;
            case ADMIN:
                switch (command) {
                    case PERIODICAL_LIST:
                        access = true;
                        break;
                    case PERIODICAL_LIST_BY_CATEGORY:
                        access = true;
                        break;
                    case SHOW_PERIODICAL:
                        access = true;
                        break;
                    case LOG_OUT:
                        access = true;
                        break;
                    case USERS_PAGE:
                        access = true;
                        break;
                    case VIEW_ALL_ORDERS:
                        access = true;
                        break;
                    case REMOVE_USER:
                        access = true;
                        break;
                    case MAKE_ADMIN:
                        access = true;
                        break;
                    case PAID_ORDER:
                        access = true;
                        break;
                    case REMOVE_ORDER:
                        access = true;
                        break;
                    case PERIODICAL_LIST_FOR_DELETE:
                        access = true;
                        break;
                    case REMOVE_PERIODICAL:
                        access = true;
                        break;
                    case NEW_PERIODICAL_PAGE:
                        access = true;
                        break;
                    case NEW_PERIODICAL:
                        access = true;
                        break;
                    case CHANGE_LANGUAGE:
                        access = true;
                        break;
                }
        }
        return access;

    }

    private CommandName getCommandName(HttpServletRequest request) {
        String commandName;
        if (isAjax(request)) {
            commandName = (String) request.getAttribute(COMMAND_NAME);
        }
        else if (isMultipart(request)){
            commandName = (String) request.getAttribute(COMMAND_NAME);
        }
        else {
            commandName = request.getParameter(COMMAND_NAME);
        }
        CommandName action;
        try {
            action = CommandName.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException ex) {
            action = CommandName.NO_SUCH_COMMAND;
        }
        return action;

    }

    private boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }
    private boolean isMultipart(HttpServletRequest request) {
        return ServletFileUpload.isMultipartContent(request);
    }
    private enum UserStatus {
        GUEST, USER, ADMIN
    }
}
