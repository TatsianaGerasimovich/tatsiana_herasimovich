package by.bsuir.gerasimovich.periodicals.controller.command.exception;

import by.bsuir.gerasimovich.periodicals.exeption.PeriodicalException;

/**
 * Exception class created specifically to describe the exceptional
 * situation arises in Command layer
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class CommandException extends PeriodicalException {
    /**
     * constructor parameters
     *
     * @param message
     */
    public CommandException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     *
     * @param message
     * @param cause
     */

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     *
     * @param cause
     */
    public CommandException(Throwable cause) {
        super(cause);
    }
}
