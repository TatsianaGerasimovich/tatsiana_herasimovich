package by.bsuir.gerasimovich.periodicals.dao.pool.exception;

import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;

/**
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class ParseException extends DAOException {
    /**
     * default constructor
     */
    public ParseException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public ParseException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public ParseException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public ParseException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public ParseException (String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
