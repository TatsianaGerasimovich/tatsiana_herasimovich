package by.bsuir.gerasimovich.periodicals.controller.command.impl.user;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Orders;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.UserService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * transition to the page with all orders of certain user
 *
 * @author Tatiana
 * @version 1.00 28.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class OrderListCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String USER = "user";
    /**
     * Constants to set request parameters
     */
    private static final String ORDERS = "orders";

    /**
     * Override method that call special logic of
     * transition to the page with all orders of certain user
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute(USER);
        Integer userId = user.getId();

        try {
            List<Orders> orders = UserService.getInstance().getOrders(userId);
            request.setAttribute(ORDERS, orders);
            page = PageHelper.getInstance().getProperty(PageHelper.ORDERS_PAGE);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }

        return page;

    }
}