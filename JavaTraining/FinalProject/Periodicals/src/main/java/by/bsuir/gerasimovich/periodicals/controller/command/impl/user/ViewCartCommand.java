package by.bsuir.gerasimovich.periodicals.controller.command.impl.user;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Periodicals;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.UserService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * transition to the page of cart
 *
 * @author Tatiana
 * @version 1.00 29.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class ViewCartCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String USER = "user";
    /**
     * Constants to set request parameters
     */
    private static final String RESULT = "resultMap";

    /**
     * Override method that call special logic of
     * transition to the page of cart
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute(USER);
        try {
            Map<Periodicals, Integer> resultMap = UserService.getInstance().viewCart(user);
            request.setAttribute(RESULT, resultMap);
            return PageHelper.getInstance().getProperty(PageHelper.CART_PAGE);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }

    }
}
