package by.bsuir.gerasimovich.periodicals.dao.factory;

import by.bsuir.gerasimovich.periodicals.dao.GenericDao;
import by.bsuir.gerasimovich.periodicals.dao.factory.exception.AbstractFactoryException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.MySqlDaoFactory;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlFactoryException;

/**
 * Class generates a certain class factory dao.
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
abstract public class AbstractDAOFactory {
    /**
     * Method returns a certain type of factory dao
     * @param whichFactory type of DAO
     * @return factory objects to work with a particular database
     */
    public static AbstractDAOFactory getDAOFactory(
            String whichFactory) throws AbstractFactoryException {
        TypeOfDAO type = TypeOfDAO.getEnum(whichFactory.toLowerCase());
        switch (type) {
            case MYSQL:
                return MySqlDaoFactory.getInstance();
            default:
               throw new AbstractFactoryException("Factory for this type is not found");
        }
    }

    /**
     *The method returns the specific entity Tao
     * @param entityClass
     * @return
     * @throws MySqlFactoryException
     */
   abstract public GenericDao getDao(Class entityClass)
            throws MySqlFactoryException;
    /**
     *Enum for all types of supported dao
     */
    public enum TypeOfDAO {
        MYSQL("mysql");

        public String getValue() {
            return value;
        }

        public static TypeOfDAO getEnum(String value) {
            TypeOfDAO result = null;
            for (TypeOfDAO en : TypeOfDAO.values()) {
                if (en.getValue().equals(value)) {
                    result = en;
                }
            }
            return result;
        }

        private String value;

        private TypeOfDAO(String value) {
            this.value = value;
        }
    }
}
