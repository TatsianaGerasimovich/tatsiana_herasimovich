package by.bsuir.gerasimovich.periodicals.service;

import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.factory.AbstractDAOFactory;
import by.bsuir.gerasimovich.periodicals.dao.factory.exception.AbstractFactoryException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.*;
import by.bsuir.gerasimovich.periodicals.entity.*;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public class UserService {
    private static final Logger LOG = Logger.getLogger(UserService.class);
    private static volatile UserService instance;

    private UserService() {
    }

    public static UserService getInstance() {
        UserService localInstance = instance;
        if (localInstance == null) {
            synchronized (UserService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new UserService();
                }
            }
        }
        return localInstance;
    }

    public Users logOut() {
        Users newUser = new Users();
        newUser.setUserRole(Users.GUEST);
        return newUser;
    }

    public PeriodicalCart addToCart(Users user, PeriodicalInf periodicalInf) {
        PeriodicalCart cart = user.getCart();
        int periodicalId = periodicalInf.getPeriodical().getId();
        if (cart.getNumber(periodicalId) == null) {
            cart.addPeriodical(periodicalId, periodicalInf.getNumber());
            return cart;
        } else {
            return null;
        }
    }

    public Map<Periodicals, Integer> viewCart(Users user) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            PeriodicalCart cart = user.getCart();

            Integer ids[] = cart.getIds();
            if (ids.length > 0) {
                List<Periodicals> periodicalsList = new ArrayList<>();
                Map<Periodicals, Integer> resultMap = new HashMap<Periodicals, Integer>();
                for (int i = 0; i < ids.length; i++) {
                    periodicalsList.add(mySqlPeriodicalsDao.getByPK(ids[i]));
                }

                for (Periodicals periodical : periodicalsList) {
                    int id = periodical.getId();
                    resultMap.put(periodical, cart.getNumber(id));
                }
                return resultMap;
            }
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return null;
    }

    public void createOrder(Orders order, Users user) throws ServiceException {

        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlOrdersDao mySqlOrdersDao = (MySqlOrdersDao) factory.getDao(Orders.class);
            MySqlOrderPeriodicalsDao mySqlOrderPeriodicalsDao = (MySqlOrderPeriodicalsDao) factory.getDao(OrderPeriodicals.class);
            int orderId;
            order.setStatusRemove(false);
            order.setStatus(0);
            order.setDateOfOrder(new java.sql.Date(new java.util.Date().getTime()));
            orderId = mySqlOrdersDao.create(order);
            for (PeriodicalInf item : order.getPeriodicals()) {
                OrderPeriodicals orderPeriodical = new OrderPeriodicals();
                orderPeriodical.setNumber(item.getNumber());
                orderPeriodical.setOrderId(orderId);
                orderPeriodical.setPeriodicalId(item.getPeriodical().getPeriodicalId());
                mySqlOrderPeriodicalsDao.create(orderPeriodical);
            }
            user.setCart(new PeriodicalCart());

        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    public List<Orders> getOrders(int userId) throws ServiceException {
        List<Orders> orders;
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlOrdersDao mySqlOrdersDao = (MySqlOrdersDao) factory.getDao(Orders.class);
            MySqlOrderPeriodicalsDao mySqlOrderPeriodicalsDao = (MySqlOrderPeriodicalsDao) factory.getDao(OrderPeriodicals.class);
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            orders = mySqlOrdersDao.getAllByUser(userId);
            for (Orders item : orders) {
                List<OrderPeriodicals> orderPeriodicalses = mySqlOrderPeriodicalsDao.getAllByOrder(item.getOrderId());
                List<PeriodicalInf> periodicalInfList = new ArrayList<>();
                for (OrderPeriodicals obj : orderPeriodicalses) {
                    periodicalInfList.add(new PeriodicalInf(mySqlPeriodicalsDao.getByPK(obj.getPeriodicalId()), obj.getNumber()));
                }
                item.setPeriodicals(periodicalInfList);
            }
            return orders;
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public PeriodicalInf updateCart(List<PeriodicalInf> data, Users user) throws ServiceException {

        if (data.size() <= 0) {
            user.setCart(new PeriodicalCart());
            return null;
        } else {
            Map<Integer, Integer> dataMap = new HashMap<Integer, Integer>();
            for (PeriodicalInf periodicalInf : data) {
                dataMap.put(periodicalInf.getPeriodical().getId(), periodicalInf.getNumber());
            }
            user.setCart(new PeriodicalCart(dataMap));
            return null;
        }
    }

    public ServiceResponse createComment(int periodicalId, String comment, Users user) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            MySqlReviewsDao mySqlReviewsDao = (MySqlReviewsDao) factory.getDao(Reviews.class);
            MySqlPicturesDao mySqlPicturesDao = (MySqlPicturesDao) factory.getDao(Pictures.class);
            MySqlUsersDao mySqlUsersDao = (MySqlUsersDao) factory.getDao(Users.class);

            Reviews review = new Reviews(comment, user.getUserId(), periodicalId);
            mySqlReviewsDao.create(review);

            List<Reviews> reviewsList = mySqlReviewsDao.getAllByPeriodical(periodicalId);
            List<Pictures> picturesList = mySqlPicturesDao.getAllByPeriodical(periodicalId);
            String mainPicture = null;
            Iterator itr = picturesList.iterator();
            List<Users> users = new ArrayList<>();
            List<Pictures> firstPhoto = new ArrayList<>();
            int half = picturesList.size() / 2;
            boolean flag=false;
            for (int i = 0; i < half; ) {
                Pictures element = (Pictures) itr.next();
                if (element.getIsMain()) {
                    mainPicture = element.getPicturePath();
                    flag=true;
                } else {
                    firstPhoto.add(element);
                    i++;
                }

            }
            int half2 = picturesList.size() - half;
            if(flag)
                half2-=1;
            List<Pictures> lastPhoto = new ArrayList<>();
            for (int i = 0; i < half2; i++) {
                Pictures element = (Pictures) itr.next();
                if (element.getIsMain()) {
                    mainPicture = element.getPicturePath();

                } else {
                    lastPhoto.add(element);

                }
            }

            for (Reviews obj : reviewsList) {
                users.add(mySqlUsersDao.getByPK(obj.getUserId()));
            }
            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.PERIODICAL, mySqlPeriodicalsDao.getByPK(periodicalId));
            response.put(ServiceResponse.REVIEWS, reviewsList);
            response.put(ServiceResponse.FIRST_PHOTOS, firstPhoto);
            response.put(ServiceResponse.LAST_PHOTOS, lastPhoto);
            response.put(ServiceResponse.MAIN_PICTURE, mainPicture);
            response.put(ServiceResponse.USERS, users);
            return response;

        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void updateOrders(List<Orders> newOrders, int userId) throws ServiceException {
        List<Orders> orders;
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlOrdersDao mySqlOrdersDao = (MySqlOrdersDao) factory.getDao(Orders.class);
            orders = mySqlOrdersDao.getAllByUser(userId);

            List<Integer> ids = new ArrayList<>();
            for (Orders item : newOrders) {
                ids.add(item.getOrderId());
            }

            for (Orders item : orders) {
                if (!ids.contains(item.getOrderId())) {
                    mySqlOrdersDao.updateForDelete(item.getOrderId());
                }
            }
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public ServiceResponse getComments(int userId) throws ServiceException {
        List<Reviews> reviewses;
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlReviewsDao mySqlReviewsDao = (MySqlReviewsDao) factory.getDao(Reviews.class);
            MySqlPeriodicalsDao mySqlPeriodicalsDao = (MySqlPeriodicalsDao) factory.getDao(Periodicals.class);
            reviewses = mySqlReviewsDao.getAllByUser(userId);
            List<Periodicals> periodicals = new ArrayList<>();

            for (Reviews item : reviewses) {
                periodicals.add(mySqlPeriodicalsDao.getByPK(item.getPeriodicalId()));
            }
            ServiceResponse response = new ServiceResponse();
            response.put(ServiceResponse.PERIODICALS, periodicals);
            response.put(ServiceResponse.REVIEWS, reviewses);
            return response;
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void updateComments(List<Reviews> newComments, int userId) throws ServiceException {
        List<Reviews> reviewses;
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlReviewsDao mySqlReviewsDao = (MySqlReviewsDao) factory.getDao(Reviews.class);
            reviewses = mySqlReviewsDao.getAllByUser(userId);

            List<Integer> ids = new ArrayList<>();
            for (Reviews item : newComments) {
                ids.add(item.getReviewId());
            }

            for (Reviews item : reviewses) {
                if (!ids.contains(item.getReviewId())) {
                    mySqlReviewsDao.updateForDelete(item.getReviewId());
                }
            }
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public int updateProfile(Users newProfile, Users user) throws ServiceException {
        try {
            MySqlDaoFactory factory = (MySqlDaoFactory) AbstractDAOFactory.getDAOFactory("mysql");
            MySqlUsersDao mySqlUsersDao = (MySqlUsersDao) factory.getDao(Users.class);
            List<Users> users = mySqlUsersDao.getAll();

            List<String> logins = new ArrayList<>();
            for (Users item : users) {
                if(!item.getLogin().equals(user.getLogin()))
                logins.add(item.getLogin());
            }
            if (logins.contains(newProfile.getLogin())) {
                return -1;
            }
            user.setUserName(newProfile.getUserName());
            user.setLogin(newProfile.getLogin());
            user.setUserCountry(newProfile.getUserCountry());
            user.setUserCity(newProfile.getUserCity());
            user.setUserAddress(newProfile.getUserAddress());
            user.setUserPostcode(newProfile.getUserPostcode());
            user.setUserEmail(newProfile.getUserEmail());
            user.setUserPassword(newProfile.getUserPassword());
            mySqlUsersDao.update(user);
            return 1;
        } catch (AbstractFactoryException e) {
            throw new ServiceException("unable to get factory", e);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

}
