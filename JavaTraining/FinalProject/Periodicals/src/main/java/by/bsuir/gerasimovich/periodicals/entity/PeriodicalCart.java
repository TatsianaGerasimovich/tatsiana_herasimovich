package by.bsuir.gerasimovich.periodicals.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Class contains information about the user's cart
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class PeriodicalCart {
    Map<Integer, Integer> cartMap;

    public PeriodicalCart () {
        cartMap = new HashMap<>();
    }

    public PeriodicalCart (Map<Integer, Integer> cartMap) {
        this.cartMap = cartMap;
    }

    public void addPeriodical(Integer id, Integer number) {
        cartMap.put(id, number);
    }

    public int getSize() {
        return cartMap.size();
    }

    public Map<Integer, Integer> getCartMap() {
        return cartMap;
    }

    public void removeBook(int id) {
        cartMap.remove(id);
    }

    public Integer getNumber(Integer id) {
        return cartMap.get(id);
    }

    public Integer[] getIds() {
        Integer[] arr = new Integer[getSize()];
        return cartMap.keySet().toArray(arr);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof PeriodicalCart)) return false;

        PeriodicalCart that = (PeriodicalCart) o;

        if (cartMap != null ? !cartMap.equals(that.cartMap) : that.cartMap != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return cartMap != null ? cartMap.hashCode() : 0;
    }
}
