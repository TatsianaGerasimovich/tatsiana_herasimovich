package by.bsuir.gerasimovich.periodicals.entity;

/**
 * Class-Entity store the information about <entity>Pictures</entity>
 * Part of the class hierarchy of entities
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.entity.GeneralEntity
 */
public class Pictures implements GeneralEntity{
    private int pictureId;
    private String picturePath;
    private int periodicalId;
    private Boolean statusRemove;
    private Boolean isMain;

    public Integer getId() {
        return pictureId;
    }

    public void setId(int pictureId) {
        this.pictureId = pictureId;
    }

    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public int getPeriodicalId() {
        return periodicalId;
    }

    public void setPeriodicalId(int periodicalId) {
        this.periodicalId = periodicalId;
    }

    public Boolean getStatusRemove() {
        return statusRemove;
    }

    public void setStatusRemove(Boolean statusRemove) {
        this.statusRemove = statusRemove;
    }

    public Boolean getIsMain() {
        return isMain;
    }

    public void setIsMain(Boolean isMain) {
        this.isMain = isMain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof Pictures)) return false;

        Pictures pictures = (Pictures) o;

        if (periodicalId != pictures.periodicalId) return false;
        if (pictureId != pictures.pictureId) return false;
        if (isMain != null ? !isMain.equals(pictures.isMain) : pictures.isMain != null) return false;
        if (picturePath != null ? !picturePath.equals(pictures.picturePath) : pictures.picturePath != null)
            return false;
        if (statusRemove != null ? !statusRemove.equals(pictures.statusRemove) : pictures.statusRemove != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = pictureId;
        result = 31 * result + (picturePath != null ? picturePath.hashCode() : 0);
        result = 31 * result + periodicalId;
        result = 31 * result + (statusRemove != null ? statusRemove.hashCode() : 0);
        result = 31 * result + (isMain != null ? isMain.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Pictures{" +
                "pictureId=" + pictureId +
                ", picturePath='" + picturePath + '\'' +
                ", periodicalId=" + periodicalId +
                ", statusRemove=" + statusRemove +
                ", isMain=" + isMain +
                '}';
    }
}
