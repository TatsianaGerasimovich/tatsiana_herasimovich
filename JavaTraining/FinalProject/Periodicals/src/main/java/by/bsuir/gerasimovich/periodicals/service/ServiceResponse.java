package by.bsuir.gerasimovich.periodicals.service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Tatiana
 * @version 1.00 23.05.2015.
 */
public class ServiceResponse {
    private Map<String, Object> objects;


    public ServiceResponse() {
        objects = new HashMap<>();
    }

    public Object get(String key) {
        return objects.get(key);
    }

    public void put(String key, Object object) {
        objects.put(key, object);
    }

    public static final String PERIODICALS = "periodicals";
    public static final String CATEGORIES = "categories";
    public static final String PERIODICAL = "periodical";
    public static final String REVIEWS = "reviews";
    public static final String FIRST_PHOTOS = "first_photos";
    public static final String LAST_PHOTOS = "last_photos";
    public static final String MAIN_PICTURE = "main_picture";
    public static final String USERS = "users";
    public static final String ORDERS = "orders";
}
