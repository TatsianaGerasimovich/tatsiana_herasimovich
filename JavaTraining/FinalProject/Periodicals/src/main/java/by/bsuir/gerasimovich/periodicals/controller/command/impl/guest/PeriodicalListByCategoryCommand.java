package by.bsuir.gerasimovich.periodicals.controller.command.impl.guest;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.service.GuestService;
import by.bsuir.gerasimovich.periodicals.service.ServiceResponse;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * transition to the page with periodicals of certain category
 *
 * @author Tatiana
 * @version 1.00 25.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class PeriodicalListByCategoryCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    public static final String CATEGORY_ID = "categoryId";
    /**
     * Constants to set request parameters
     */
    private static final String PERIODICALS = "periodicals";
    private static final String CATEGORIES = "categories";

    /**
     * Override method that call special logic of
     * transition to the page with periodicals of certain category
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        String page = null;
        try {
            int categoryId = Integer.parseInt(request.getParameter(CATEGORY_ID));
            ServiceResponse serviceResponse = GuestService.getInstance().periodicalListByCategory(categoryId);
            request.setAttribute(PERIODICALS, serviceResponse.get(ServiceResponse.PERIODICALS));
            request.setAttribute(CATEGORIES, serviceResponse.get(ServiceResponse.CATEGORIES));
            page = PageHelper.getInstance().getProperty(PageHelper.PERIODICALS_LIST_PAGE);
        } catch (NumberFormatException ex) {
            throw new CommandException(ex);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        return page;
    }
}
