package by.bsuir.gerasimovich.periodicals.controller.command.impl.guest;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.service.GuestService;
import by.bsuir.gerasimovich.periodicals.service.ServiceResponse;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * transition to the page with information about certain periodical
 *
 * @author Tatiana
 * @version 1.00 23.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class ShowPeriodicalCommand implements ICommand {
    /**
     * Constants to set request parameters
     */
    public static final String PERIODICAL = "periodical";
    public static final String REVIEWS = "reviews";
    public static final String FIRST_PHOTOS = "first_photos";
    public static final String LAST_PHOTOS = "last_photos";
    public static final String MAIN_PICTURE = "main_picture";
    public static final String USERS = "users";
    /**
     * Constants to get request parameters
     */
    public static final String PERIODICAL_ID = "periodicalId";
    /**
     * Override method that call special logic of
     * transition to the page with information about certain periodical
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        try {
            int periodicalId = Integer.parseInt(request.getParameter(PERIODICAL_ID));
            ServiceResponse serviceResponse = GuestService.getInstance().showPeriodical(periodicalId);
            request.setAttribute(PERIODICAL, serviceResponse.get(ServiceResponse.PERIODICAL));
            request.setAttribute(REVIEWS, serviceResponse.get(ServiceResponse.REVIEWS));
            request.setAttribute(FIRST_PHOTOS, serviceResponse.get(ServiceResponse.FIRST_PHOTOS));
            request.setAttribute(LAST_PHOTOS, serviceResponse.get(ServiceResponse.LAST_PHOTOS));
            request.setAttribute(MAIN_PICTURE, serviceResponse.get(ServiceResponse.MAIN_PICTURE));
            request.setAttribute(USERS, serviceResponse.get(ServiceResponse.USERS));
            page = PageHelper.getInstance().getProperty(PageHelper.PERIODICAL_PAGE);
        } catch (NumberFormatException ex) {
            throw new CommandException(ex);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        return page;
    }
}