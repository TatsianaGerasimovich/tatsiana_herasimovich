package by.bsuir.gerasimovich.periodicals.controller;


import by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public final class Controller extends HttpServlet implements Servlet {


    public static final String COMMAND_NAME = "command";
    private static final Logger LOG = Logger.getLogger(Controller.class);
    private static final String AJAX_REQUEST = "XMLHttpRequest";
    private static final String REQUEST = "X-Requested-With";
    private CommandHelper commandHelper = CommandHelper.getInstance();

    public Controller() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String commandName;
        boolean isAjax = isAjax(request);
        if (isAjax) {
            commandName = (String) request.getAttribute(COMMAND_NAME);
        } else if (isMultipart(request)) {
            commandName = (String) request.getAttribute(COMMAND_NAME);
        } else {
            commandName = request.getParameter(COMMAND_NAME);
        }
        ICommand command = commandHelper.getCommand(commandName);
        String page = null;
        try {
            page = command.execute(request, response);
            if (isAjax) {
                return;
            }
            RequestDispatcher rd = getServletContext().getRequestDispatcher(page);
            rd.forward(request, response);
        } catch (ServletException | IOException | CommandException ex) {
            LOG.error(ex);
            RequestDispatcher rd = getServletContext()
                    .getRequestDispatcher(PageHelper.getInstance()
                            .getProperty(PageHelper.ERROR_PAGE));
            try {
                rd.forward(request, response);
            } catch (ServletException | IOException exept) {
                LOG.error(exept);
            }
        }
    }

    private boolean isAjax(HttpServletRequest request) {
        return AJAX_REQUEST.equals(request.getHeader(REQUEST));
    }

    private boolean isMultipart(HttpServletRequest request) {
        return ServletFileUpload.isMultipartContent(request);
    }
}
