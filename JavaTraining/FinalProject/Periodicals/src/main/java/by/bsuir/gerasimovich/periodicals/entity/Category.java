package by.bsuir.gerasimovich.periodicals.entity;

/**
 * Class-Entity store the information about <entity>Category</entity>
 * Part of the class hierarchy of entities
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.entity.GeneralEntity
 */
public class Category implements GeneralEntity {
    private int categoryId;
    private String categoryName;

    public Integer getId() {
        return categoryId;
    }

    public void setId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof Category)) return false;

        Category category = (Category) o;

        if (categoryId != category.categoryId) return false;
        if (!categoryName.equals(category.categoryName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = categoryId;
        result = 31 * result + categoryName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
