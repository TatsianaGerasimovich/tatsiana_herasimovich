package by.bsuir.gerasimovich.periodicals.dao.mysql.exception;

import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;

/**
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class MySqlDaoException extends DAOException {
    /**
     * default constructor
     */
    public MySqlDaoException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public MySqlDaoException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public MySqlDaoException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public MySqlDaoException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public MySqlDaoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}