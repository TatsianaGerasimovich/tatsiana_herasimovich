package by.bsuir.gerasimovich.periodicals.dao.mysql;

import by.bsuir.gerasimovich.periodicals.dao.AbstractJDBCDao;
import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlDaoException;
import by.bsuir.gerasimovich.periodicals.entity.OrderPeriodicals;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public class MySqlOrderPeriodicalsDao extends AbstractJDBCDao<OrderPeriodicals> {

    private static MySqlOrderPeriodicalsDao instance;
    private final String SELECT = "SELECT orderId, periodicalId, numberPeriodical  FROM orderPeriodicals;";
    private final String SELECT_BY_ORDER = "SELECT orderId, periodicalId, numberPeriodical FROM orderPeriodicals WHERE orderId = ?;";
    private final String SELECT_BY_PERIODICAL = "SELECT orderId, periodicalId, numberPeriodical FROM orderPeriodicals WHERE periodicalId = ?;";
    private final String INSERT = "INSERT INTO orderPeriodicals (orderId, periodicalId, numberPeriodical) \n"
            + "VALUES (?, ?, ?);";
    private final String UPDATE = "UPDATE orderPeriodicals SET numberPeriodical = ? WHERE orderId = ? AND periodicalId = ?;";
    private final String DELETE = "DELETE FROM orderPeriodicals WHERE orderId = ? AND periodicalId = ?;";

    private MySqlOrderPeriodicalsDao() throws DAOException {
        super();

    }

    public static MySqlOrderPeriodicalsDao getInstance() throws DAOException {
        MySqlOrderPeriodicalsDao localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlOrderPeriodicalsDao.class) {
                localInstance=instance;
                if (localInstance == null) {
                    instance = localInstance= new MySqlOrderPeriodicalsDao();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getSelectByPkQuery() {
        return null;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {

        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    protected List<OrderPeriodicals> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<OrderPeriodicals> result = new LinkedList<OrderPeriodicals>();
        try {
            while (rs.next()) {

                OrderPeriodicals orderPeriodical = new OrderPeriodicals();
                orderPeriodical.setOrderId(rs.getInt("orderId"));
                orderPeriodical.setPeriodicalId(rs.getInt("periodicalId"));
                orderPeriodical.setNumber(rs.getInt("numberPeriodical"));
                result.add(orderPeriodical);
            }
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
        return result;
    }


    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, OrderPeriodicals object) throws DAOException {
        try {
            statement.setInt(1, object.getOrderId());
            statement.setInt(2, object.getPeriodicalId());
            statement.setInt(3, object.getNumber());

        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, OrderPeriodicals object) throws DAOException {
        try {
            statement.setInt(1, object.getNumber());
            statement.setInt(2, object.getOrderId());
            statement.setInt(3, object.getPeriodicalId());

        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    @Override
    public void delete(OrderPeriodicals object) throws DAOException {
        String sql = getDeleteQuery();
        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
            try {
                statement.setInt(1, object.getOrderId());
                statement.setInt(2, object.getPeriodicalId());

            } catch (SQLException e) {
                throw new DAOException(e);
            }
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On delete modify more then 1 record: " + count);
            }
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            putConnection();
        }
    }

    public List<OrderPeriodicals> getAllByOrder(int orderId) throws DAOException {
        List<OrderPeriodicals> list;
        ResultSet rs = null;
        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_ORDER)) {
            statement.setInt(1, orderId);
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                putConnection();
            } catch (SQLException e) {
                throw new DAOException(e);
            }

        }
        return list;
    }

    public List<OrderPeriodicals> getAllByPeriodical(int periodicalId) throws DAOException {
        List<OrderPeriodicals> list;
        ResultSet rs = null;
        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_PERIODICAL)) {
            statement.setInt(1, periodicalId);
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                putConnection();
            } catch (SQLException e) {
                throw new DAOException(e);
            }

        }
        return list;
    }
}