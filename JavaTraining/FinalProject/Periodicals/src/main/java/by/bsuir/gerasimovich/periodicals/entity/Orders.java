package by.bsuir.gerasimovich.periodicals.entity;


import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Class-Entity store the information about <entity>Orders</entity>
 * Part of the class hierarchy of entities
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.entity.GeneralEntity
 */
public class Orders implements GeneralEntity{
    private int orderId;
    private Date dateOfOrder;
    private int clientId;
    private int status;
    private Boolean statusRemove;
    private List<PeriodicalInf> periodicals = new ArrayList<>();

    public Integer getId() {
        return orderId;
    }

    public void setId(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Date getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(Date dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Boolean getStatusRemove() {
        return statusRemove;
    }

    public void setStatusRemove(Boolean statusRemove) {
        this.statusRemove = statusRemove;
    }

    public List<PeriodicalInf> getPeriodicals() {
        return periodicals;
    }

    public void setPeriodicals(List<PeriodicalInf> periodicals) {
        this.periodicals = periodicals;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof Orders)) return false;

        Orders orders = (Orders) o;

        if (clientId != orders.clientId) return false;
        if (orderId != orders.orderId) return false;
        if (status != orders.status) return false;
        if (dateOfOrder != null ? !dateOfOrder.equals(orders.dateOfOrder) : orders.dateOfOrder != null) return false;
        if (periodicals != null ? !periodicals.equals(orders.periodicals) : orders.periodicals != null) return false;
        if (statusRemove != null ? !statusRemove.equals(orders.statusRemove) : orders.statusRemove != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderId;
        result = 31 * result + (dateOfOrder != null ? dateOfOrder.hashCode() : 0);
        result = 31 * result + clientId;
        result = 31 * result + status;
        result = 31 * result + (statusRemove != null ? statusRemove.hashCode() : 0);
        result = 31 * result + (periodicals != null ? periodicals.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "orderId=" + orderId +
                ", dateOfOrder=" + dateOfOrder +
                ", clientId=" + clientId +
                ", status=" + status +
                ", statusRemove=" + statusRemove +
                ", periodicals=" + periodicals +
                '}';
    }
}
