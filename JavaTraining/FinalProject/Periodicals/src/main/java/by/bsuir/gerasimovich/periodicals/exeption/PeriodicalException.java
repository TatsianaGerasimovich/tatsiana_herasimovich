package by.bsuir.gerasimovich.periodicals.exeption;

/**
 * @author Tatiana
 * @version 1.00 07.06.2015.
 */
public class PeriodicalException extends Exception {
    /**
     * default constructor
     */
    public PeriodicalException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public PeriodicalException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public PeriodicalException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public PeriodicalException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public PeriodicalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
