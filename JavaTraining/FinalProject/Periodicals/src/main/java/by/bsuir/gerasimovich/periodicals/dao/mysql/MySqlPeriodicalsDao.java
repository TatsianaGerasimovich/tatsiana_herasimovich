package by.bsuir.gerasimovich.periodicals.dao.mysql;

import by.bsuir.gerasimovich.periodicals.dao.AbstractJDBCDao;
import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.mysql.exception.MySqlDaoException;
import by.bsuir.gerasimovich.periodicals.entity.Periodicals;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 18.05.2015.
 */
public class MySqlPeriodicalsDao extends AbstractJDBCDao<Periodicals> {

    private static MySqlPeriodicalsDao instance;
    private final String SELECT = "SELECT periodicalId, periodicalName, periodicalDescription, price, coverOfPeriodical, categoryId, statusRemove   FROM periodicals WHERE statusRemove= false;";
    private final String SELECT_BY_PK = "SELECT periodicalId, periodicalName, periodicalDescription, price, coverOfPeriodical, categoryId, statusRemove   FROM periodicals WHERE statusRemove= false AND periodicalId = ?;";
    private final String INSERT = "INSERT INTO periodicals ( periodicalName, periodicalDescription, price, coverOfPeriodical, categoryId, statusRemove) \n"
            + "VALUES ( ?, ?, ?, ?, ?, ?);";
    private final String UPDATE = "UPDATE periodicals SET periodicalName = ?, periodicalDescription = ?, price = ?, coverOfPeriodical =?, categoryId = ?, statusRemove = ?   WHERE periodicalId= ?;";
    private final String UPDATE_FOR_DELETE = "UPDATE periodicals SET statusRemove = 1  WHERE periodicalId= ?;";
    private final String DELETE = "DELETE FROM periodicals WHERE periodicalId = ?;";
    private final String SELECT_BY_CATEGORY = "SELECT periodicalId, periodicalName, periodicalDescription, price, coverOfPeriodical, categoryId, statusRemove   FROM periodicals WHERE statusRemove= false AND categoryId = ?;";

    private MySqlPeriodicalsDao() throws DAOException {
        super();

    }

    public static MySqlPeriodicalsDao getInstance() throws DAOException {
        MySqlPeriodicalsDao localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlPeriodicalsDao.class) {
                localInstance=instance;
                if (localInstance == null) {
                    instance = localInstance= new MySqlPeriodicalsDao();
                }
            }
        }
        return localInstance;
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getSelectByPkQuery() {
        return SELECT_BY_PK;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {

        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    protected List<Periodicals> parseResultSet(ResultSet rs) throws DAOException {
        LinkedList<Periodicals> result = new LinkedList<Periodicals>();
        try {
            while (rs.next()) {

                Periodicals periodical = new Periodicals();
                periodical.setId(rs.getInt("periodicalId"));
                periodical.setPeriodicalName(rs.getString("periodicalName"));
                periodical.setPeriodicalDescription(rs.getString("periodicalDescription"));
                periodical.setPrice(rs.getDouble("price"));
                periodical.setCoverOfPeriodical(rs.getString("coverOfPeriodical"));
                periodical.setCategoryId(rs.getInt("categoryId"));
                periodical.setStatusRemove(rs.getBoolean("statusRemove"));
                result.add(periodical);
            }
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Periodicals object) throws DAOException {
        try {
            statement.setString(1, object.getPeriodicalName());
            statement.setString(2, object.getPeriodicalDescription());
            statement.setDouble(3, object.getPrice());
            statement.setString(4, object.getCoverOfPeriodical());
            statement.setInt(5, object.getCategoryId());
            statement.setBoolean(6, object.getStatusRemove());

        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Periodicals object) throws DAOException {
        try {
            statement.setString(1, object.getPeriodicalName());
            statement.setString(2, object.getPeriodicalDescription());
            statement.setDouble(3, object.getPrice());
            statement.setString(4, object.getCoverOfPeriodical());
            statement.setInt(5, object.getCategoryId());
            statement.setBoolean(6, object.getStatusRemove());
            statement.setInt(7, object.getId());
        } catch (SQLException e) {
            throw new MySqlDaoException(e);
        }
    }

    public List<Periodicals> getAllByCategory(int categoryId) throws DAOException {
        List<Periodicals> list;
        ResultSet rs = null;

        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_CATEGORY)) {
            statement.setInt(1, categoryId);
            rs = statement.executeQuery();
            list = parseResultSet(rs);
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                putConnection();
            } catch (SQLException e) {
                throw new DAOException(e);
            }

        }
        return list;
    }

    public void updateForDelete(int periodicalId) throws DAOException {
        try (PreparedStatement statement = getConnection().prepareStatement(UPDATE_FOR_DELETE)) {
            statement.setInt(1, periodicalId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException("On update modify more then 1 record: " + count);
            }
            statement.close();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            putConnection();

        }
    }
}