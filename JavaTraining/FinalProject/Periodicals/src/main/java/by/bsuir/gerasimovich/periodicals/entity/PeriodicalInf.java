package by.bsuir.gerasimovich.periodicals.entity;

/**
 * Class contains information about number of ordered periodical
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class PeriodicalInf {
    private Periodicals periodical;
    private int number;

    public PeriodicalInf(Periodicals periodical,int number) {
        this.periodical = periodical;
        this.number=number;
    }

    public PeriodicalInf() {
        periodical = new Periodicals();
        number=0;
    }

    public Periodicals getPeriodical() {
        return periodical;
    }

    public void setPeriodical(Periodicals periodical) {
        this.periodical = periodical;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof PeriodicalInf)) return false;

        PeriodicalInf that = (PeriodicalInf) o;

        if (number != that.number) return false;
        if (periodical != null ? !periodical.equals(that.periodical) : that.periodical != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = periodical != null ? periodical.hashCode() : 0;
        result = 31 * result + number;
        return result;
    }
}
