package by.bsuir.gerasimovich.periodicals.controller.command.impl.admin;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.service.AdminService;
import by.bsuir.gerasimovich.periodicals.service.ServiceResponse;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * the transition to the page with a list of all orders
 *
 * @author Tatiana
 * @version 1.00 31.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class ViewAllOrdersCommand implements ICommand {
    /**
     * Constants to set request parameters
     */
    private static final String ORDERS = "orders";
    private static final String USERS = "users";

    /**
     * Override method that call special logic of
     * transition to the page with a list of all orders
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        try {
            ServiceResponse serviceResponse
                    = AdminService.getInstance().getAllOrders();
            request.setAttribute(ORDERS, serviceResponse.get(ServiceResponse.ORDERS));
            request.setAttribute(USERS, serviceResponse.get(ServiceResponse.USERS));
            page = PageHelper.getInstance().getProperty(PageHelper.ALL_ORDERS_PAGE);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        return page;
    }
}
