package by.bsuir.gerasimovich.periodicals.controller.command.impl.guest;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.GuestService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * logging in
 *
 * @author Tatiana
 * @version 1.00 28.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class SignInCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    /**
     * Constants to set request parameters
     */
    private static final String USER = "user";
    private static final String ERROR_MESSAGE = "errorLoginPassMessage";

    /**
     * Override method that call special logic of
     * logging in
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        PageHelper pageHelper = PageHelper.getInstance();

        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        HttpSession session = request.getSession();

        try {
            Users user = GuestService.getInstance().signIn(login, password);
            if (user == null) {
                page = pageHelper.getProperty(PageHelper.SIGN_IN_PAGE);
                request.setAttribute(ERROR_MESSAGE, "Неправильный логин или пароль.");
            } else {
                session.setAttribute(USER, user);
                page = pageHelper.getProperty(PageHelper.MAIN_PAGE);
            }
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }

        return page;
    }
}