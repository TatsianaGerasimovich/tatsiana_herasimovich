package by.bsuir.gerasimovich.periodicals.controller.command.impl.admin;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Periodicals;
import by.bsuir.gerasimovich.periodicals.service.AdminService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command
 * to create a new periodical
 *
 * @author Tatiana
 * @version 1.00 01.06.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class CreatePeriodicalCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String PRICE = "price";
    private static final String CATEGORY = "category";
    private static final String PHOTO = "photo";

    /**
     * Override method that call special logic to create a new periodical
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        PageHelper pageHelper = PageHelper.getInstance();

        String name = (String) request.getAttribute(NAME);
        String description = (String) request.getAttribute(DESCRIPTION);
        double price = Double.parseDouble((String) request.getAttribute(PRICE));
        int categoryId = Integer.parseInt((String) request.getAttribute(CATEGORY));
        String photo = (String) request.getAttribute(PHOTO);
        try {
            Periodicals periodical = AdminService.getInstance().createPeriodical(name, description, price, categoryId, photo);
            page = pageHelper.getProperty(PageHelper.MAIN_PAGE);
        } catch (NumberFormatException ex) {
            throw new CommandException(ex);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }

        return page;
    }
}