package by.bsuir.gerasimovich.periodicals.controller.command.impl.guest;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.service.GuestService;
import by.bsuir.gerasimovich.periodicals.service.ServiceResponse;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * transition to the page of logging in
 *
 * @author Tatiana
 * @version 1.00 28.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class SignInPageCommand implements ICommand {
    /**
     * Constants to set request parameters
     */
    public static final String FIRST_PHOTOS = "first_photos";
    public static final String LAST_PHOTOS = "last_photos";

    /**
     * Override method that call special logic of
     * transition to the page of logging in
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        try {

            ServiceResponse serviceResponse = GuestService.getInstance().signInPage();
            request.setAttribute(FIRST_PHOTOS, serviceResponse.get(ServiceResponse.FIRST_PHOTOS));
            request.setAttribute(LAST_PHOTOS, serviceResponse.get(ServiceResponse.LAST_PHOTOS));

            page = PageHelper.getInstance().getProperty(PageHelper.SIGN_IN_PAGE);

        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }
        return page;
    }
}