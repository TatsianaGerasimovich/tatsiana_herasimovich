package by.bsuir.gerasimovich.periodicals.controller.command.impl.user;

import by.bsuir.gerasimovich.periodicals.controller.PageHelper;
import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.ServiceResponse;
import by.bsuir.gerasimovich.periodicals.service.UserService;
import by.bsuir.gerasimovich.periodicals.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * transition to the page with all comments of certain user
 *
 * @author Tatiana
 * @version 1.00 28.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class CommentListCommand implements ICommand {
    public static final String PERIODICALS = "periodicals";
    /**
     * Constants to get request parameters
     */
    private static final String USER = "user";
    /**
     * Constants to set request parameters
     */
    private static final String COMMENTS = "comments";

    /**
     * Override method that call special logic of
     * transition to the page with all comments of certain user
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        HttpSession session = request.getSession();
        Users user = (Users) session.getAttribute(USER);
        Integer userId = user.getId();

        try {
            ServiceResponse serviceResponse = UserService.getInstance().getComments(userId);
            request.setAttribute(PERIODICALS, serviceResponse.get(ServiceResponse.PERIODICALS));
            request.setAttribute(COMMENTS, serviceResponse.get(ServiceResponse.REVIEWS));
            page = PageHelper.getInstance().getProperty(PageHelper.COMMENTS_PAGE);
        } catch (ServiceException ex) {
            throw new CommandException(ex);
        }

        return page;

    }
}