package by.bsuir.gerasimovich.periodicals.controller.command.impl.user;

import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import by.bsuir.gerasimovich.periodicals.entity.PeriodicalCart;
import by.bsuir.gerasimovich.periodicals.entity.PeriodicalInf;
import by.bsuir.gerasimovich.periodicals.entity.Users;
import by.bsuir.gerasimovich.periodicals.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * adding a new periodical to the cart
 *
 * @author Tatiana
 * @version 1.00 28.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class AddToCartCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String JSON = "json";
    private static final String USER = "user";
    private static final int ERROR = -1;

    /**
     * Override method that call special logic of
     * adding a new periodical to the cart
     *
     * @param request
     * @param response
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        ObjectMapper mapper = new ObjectMapper();
        String json = (String) request.getAttribute(JSON);
        try {
            PeriodicalInf data = mapper.readValue(json, PeriodicalInf.class);
            HttpSession session = request.getSession();
            Users user = (Users) session.getAttribute(USER);

            PeriodicalCart cart = UserService.getInstance().addToCart(user, data);
            if (cart != null) {
                mapper.writeValue(response.getOutputStream(), cart.getSize());
            } else {
                mapper.writeValue(response.getOutputStream(), ERROR);
            }
        } catch (IOException ex) {
            throw new CommandException(ex);
        }
        return null;

    }
}
