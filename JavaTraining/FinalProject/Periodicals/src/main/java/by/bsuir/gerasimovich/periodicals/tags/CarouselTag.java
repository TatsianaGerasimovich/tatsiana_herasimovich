package by.bsuir.gerasimovich.periodicals.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * @author Tatiana
 * @version 1.00 05.06.2015.
 */
public class CarouselTag extends TagSupport {

    @Override
    public int doStartTag() throws JspException {
        try {
            String to = "<div class=\"site-slider\">\n" +
                    "    <ul class=\"bxslider\">\n" +
                    "        <li>\n" +
                    "            <img src=\"jsp/images/slider/slide1.jpg\" alt=\"slider image 1\">\n" +
                    "\n" +
                    "            <div class=\"container\">\n" +
                    "                <div class=\"row\">\n" +
                    "                    <div class=\"col-md-12 text-right\">\n" +
                    "                        <div class=\"slider-caption\">\n" +
                    "                            <h2>See the world</h2>\n" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </li>\n" +
                    "        <li>\n" +
                    "            <img src=\"jsp/images/slider/slide2.jpg\" alt=\"slider image 2\">\n" +
                    "\n" +
                    "            <div class=\"container caption-wrapper\">\n" +
                    "                <div class=\"slider-caption\">\n" +
                    "                    <h2>Take precaution against social evils</h2>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </li>\n" +
                    "        <li>\n" +
                    "            <img src=\"jsp/images/slider/slide3.jpg\" alt=\"slider image 3\">\n" +
                    "\n" +
                    "            <div class=\"container\">\n" +
                    "                <div class=\"row\">\n" +
                    "                    <div class=\"col-md-12 text-right\">\n" +
                    "                        <div class=\"slider-caption\">\n" +
                    "                            <h2>Receive a variety of news every day</h2>\n" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </li>\n" +
                    "        <li>\n" +
                    "            <img src=\"jsp/images/slider/slide4.jpg\" alt=\"slider image 4\">\n" +
                    "\n" +
                    "            <div class=\"container\">\n" +
                    "                <div class=\"row\">\n" +
                    "                    <div class=\"col-md-12 text-right\">\n" +
                    "                        <div class=\"slider-caption\">\n" +
                    "                            <h2>Take pleasure</h2>\n" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </li>\n" +
                    "        <li>\n" +
                    "            <img src=\"jsp/images/slider/slide5.jpg\" alt=\"slider image 5\">\n" +
                    "\n" +
                    "            <div class=\"container\">\n" +
                    "                <div class=\"row\">\n" +
                    "                    <div class=\"col-md-12 text-right\">\n" +
                    "                        <div class=\"slider-caption\">\n" +
                    "                            <h2>Contribute a great deal to the development of your knowledge</h2>\n" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </li>\n" +
                    "    </ul>\n" +
                    "    <!-- /.bxslider -->\n" +
                    "    <div class=\"bx-thumbnail-wrapper\">\n" +
                    "        <div class=\"container\">\n" +
                    "            <div class=\"row\">\n" +
                    "                <div class=\"col-md-12\">\n" +
                    "                    <div id=\"bx-pager\">\n" +
                    "                        <a data-slide-index=\"0\" href=\"\"><img src=\"jsp/images/slider/thumb1.jpg\" alt=\"image 1\"/></a>\n" +
                    "                        <a data-slide-index=\"1\" href=\"\"><img src=\"jsp/images/slider/thumb2.jpg\" alt=\"image 2\"/></a>\n" +
                    "                        <a data-slide-index=\"2\" href=\"\"><img src=\"jsp/images/slider/thumb3.jpg\" alt=\"image 3\"/></a>\n" +
                    "                        <a data-slide-index=\"3\" href=\"\"><img src=\"jsp/images/slider/thumb4.jpg\" alt=\"image 4\"/></a>\n" +
                    "                        <a data-slide-index=\"4\" href=\"\"><img src=\"jsp/images/slider/thumb5.jpg\" alt=\"image 5\"/></a>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>\n" +
                    "        </div>\n" +
                    "    </div>\n" +
                    "</div>\n" +
                    "<!-- /.site-slider -->";
            pageContext.getOut().write(to);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}