package by.bsuir.gerasimovich.periodicals.controller.command.impl.guest;

import by.bsuir.gerasimovich.periodicals.controller.command.ICommand;
import by.bsuir.gerasimovich.periodicals.controller.command.exception.CommandException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class is a member of the pattern command and implements the interface <code>ICommand</code>
 * Class is designed to execute the command of
 * changing the locale of the whole project
 *
 * @author Tatiana
 * @version 1.00 02.06.2015.
 * @see by.bsuir.gerasimovich.periodicals.controller.command.CommandHelper
 * @see by.bsuir.gerasimovich.periodicals.controller.command.ICommand
 */
public class ChangeLocalCommand implements ICommand {
    /**
     * Constants to set and get request parameters
     */
    private static final String LOCALE = "locale";
    private static final String JSON = "json";

    /**
     * Override method that call special logic of
     * changing the locale of the whole project
     *
     * @param request
     * @param response
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(true);
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            session.setAttribute(LOCALE, json);
            mapper.writeValue(response.getOutputStream(), -1);
        } catch (IOException ex) {
            throw new CommandException(ex);
        }
        return null;
    }
}
