package by.bsuir.gerasimovich.periodicals.entity;

/**
 * Class-Entity store the information about <entity>Users</entity>
 * Part of the class hierarchy of entities
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.entity.GeneralEntity
 */
public class Users implements GeneralEntity{
    public static final String GUEST = "guest";
    public static final String USER = "user";
    public static final String ADMIN = "admin";
    public static final String NO_PHOTO ="jsp/images/userPhoto/No_avatar.jpg";
    private int userId;
    private String userEmail;
    private String userAddress;
    private String userCity;
    private double userPostcode;
    private String userCountry;
    private String userPhoto;
    private String login;
    private String userPassword;
    private String userRole;
    private String userName;
    private Boolean statusRemove;

    private PeriodicalCart cart;

    public Users (){
        userRole = "guest";
        userPhoto=NO_PHOTO;
        statusRemove=false;
    }
    public Integer getId() {
        return userId;
    }

    public Users(int userId,String userEmail, String userAddress, String userCity,
                 double userPostcode,String userCountry, String userPhoto,
                 String login,String userPassword,String userRole, String userName ) {
        this.userId = userId;
        this.userEmail = userEmail;
        this.userAddress = userAddress;
        this.userCity = userCity;
        this.userPostcode = userPostcode;
        this.userCountry = userCountry;
        this.userPhoto = userPhoto;
        this.login = login;
        this.userPassword = userPassword;
        this.userRole = userRole;
        this.userName = userName;
        this.statusRemove=false;
    }

    public void setId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public double getUserPostcode() {
        return userPostcode;
    }

    public void setUserPostcode(double userPostcode) {
        this.userPostcode = userPostcode;
    }

    public String getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public PeriodicalCart getCart() {
        return cart;
    }

    public void setCart(PeriodicalCart cart) {
        this.cart = cart;
    }

    public Boolean getStatusRemove() {
        return statusRemove;
    }

    public void setStatusRemove(Boolean statusRemove) {
        this.statusRemove = statusRemove;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof Users)) return false;

        Users users = (Users) o;

        if (userId != users.userId) return false;
        if (Double.compare(users.userPostcode, userPostcode) != 0) return false;
        if (cart != null ? !cart.equals(users.cart) : users.cart != null) return false;
        if (login != null ? !login.equals(users.login) : users.login != null) return false;
        if (statusRemove != null ? !statusRemove.equals(users.statusRemove) : users.statusRemove != null) return false;
        if (userAddress != null ? !userAddress.equals(users.userAddress) : users.userAddress != null) return false;
        if (userCity != null ? !userCity.equals(users.userCity) : users.userCity != null) return false;
        if (userCountry != null ? !userCountry.equals(users.userCountry) : users.userCountry != null) return false;
        if (userEmail != null ? !userEmail.equals(users.userEmail) : users.userEmail != null) return false;
        if (userName != null ? !userName.equals(users.userName) : users.userName != null) return false;
        if (userPassword != null ? !userPassword.equals(users.userPassword) : users.userPassword != null) return false;
        if (userPhoto != null ? !userPhoto.equals(users.userPhoto) : users.userPhoto != null) return false;
        if (userRole != null ? !userRole.equals(users.userRole) : users.userRole != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = userId;
        result = 31 * result + (userEmail != null ? userEmail.hashCode() : 0);
        result = 31 * result + (userAddress != null ? userAddress.hashCode() : 0);
        result = 31 * result + (userCity != null ? userCity.hashCode() : 0);
        temp = Double.doubleToLongBits(userPostcode);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (userCountry != null ? userCountry.hashCode() : 0);
        result = 31 * result + (userPhoto != null ? userPhoto.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (userPassword != null ? userPassword.hashCode() : 0);
        result = 31 * result + (userRole != null ? userRole.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (statusRemove != null ? statusRemove.hashCode() : 0);
        result = 31 * result + (cart != null ? cart.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Users{" +
                "userId=" + userId +
                ", userEmail='" + userEmail + '\'' +
                ", userAddress='" + userAddress + '\'' +
                ", userCity='" + userCity + '\'' +
                ", userPostcode=" + userPostcode +
                ", userCountry='" + userCountry + '\'' +
                ", userPhoto='" + userPhoto + '\'' +
                ", login='" + login + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userRole='" + userRole + '\'' +
                ", userName='" + userName + '\'' +
                ", statusRemove=" + statusRemove +
                ", cart=" + cart +
                '}';
    }
}
