package by.bsuir.gerasimovich.periodicals.controller.listener;

import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.pool.ConfigurationManager;
import by.bsuir.gerasimovich.periodicals.dao.pool.ConnectionPool;
import by.bsuir.gerasimovich.periodicals.dao.pool.exception.ConnectionPoolException;
import by.bsuir.gerasimovich.periodicals.dao.pool.exception.ParseException;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @author Tatiana
 * @version 1.00 24.05.2015.
 */
@WebListener
public class SimpleContextListener implements ServletContextListener {


    public static final Logger LOG = Logger.getLogger(SimpleContextListener.class);

    public void contextInitialized(ServletContextEvent ev) {
        try {
            new ConfigurationManager().initConnectionProperties();
            ConnectionPool.getInstance().initPoolData();

        } catch (DAOException e) {
            LOG.fatal("error in initialization connection pool", e);
        }
    }

    public void contextDestroyed(ServletContextEvent ev) {
        try {
            ConnectionPool.getInstance().dispose();
        } catch (ConnectionPoolException ex) {
            LOG.error("error iin disposing connection pool", ex);
        }
    }
}