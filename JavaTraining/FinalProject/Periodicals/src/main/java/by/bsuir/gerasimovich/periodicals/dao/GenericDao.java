package by.bsuir.gerasimovich.periodicals.dao;

import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;
import by.bsuir.gerasimovich.periodicals.dao.pool.ConnectionPool;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public interface GenericDao <T> {
    /**
     * Method creates a new entry, the corresponding object
     */
    public int create(T object) throws DAOException;

    /**
     * Method gets the appropriate record with a primary key or a null key
     */
    public T getByPK(Integer key) throws DAOException;

    /**
     * Method saves the state of the object in the database
     */
    public void update(T object) throws DAOException;

    /**
     * Removes a record of the object from the database
     */
    public void delete(T object) throws DAOException;

    /**
     * Returns a list of all appropriate records in the database
     */
    public List<T> getAll() throws DAOException;
}
