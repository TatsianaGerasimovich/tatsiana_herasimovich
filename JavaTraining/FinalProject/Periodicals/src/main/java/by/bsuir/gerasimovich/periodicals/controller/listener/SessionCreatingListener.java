package by.bsuir.gerasimovich.periodicals.controller.listener;

import by.bsuir.gerasimovich.periodicals.entity.Users;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
/**
 * @author Tatiana
 * @version 1.00 24.05.2015.
 */
@WebListener
public class SessionCreatingListener implements HttpSessionListener {

    private static final String USER = "user";
    private static final String LOCALE = "locale";

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        synchronized (session) {
            session.setAttribute(USER, new Users());
            session.setAttribute(LOCALE, "en_US");
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

    }

}
