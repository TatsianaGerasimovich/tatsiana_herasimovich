package by.bsuir.gerasimovich.periodicals.entity;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Class-Entity store the information about <entity>Reviews</entity>
 * Part of the class hierarchy of entities
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 * @see by.bsuir.gerasimovich.periodicals.entity.GeneralEntity
 */
public class Reviews implements GeneralEntity{
    private int reviewId;
    private String reviewDescription;
    private int userId;
    private int periodicalId;
    private Boolean statusRemove;

    public Reviews() {
    }

    public Reviews(String reviewDescription, int userId, int periodicalId) {
        this.reviewDescription = reviewDescription;
        this.userId = userId;
        this.periodicalId = periodicalId;
        this.statusRemove = false;
    }

    public Integer getId() {
        return reviewId;
    }

    public void setId(int reviewId) {
        this.reviewId = reviewId;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public String getReviewDescription() {
        return reviewDescription;
    }

    public void setReviewDescription(String reviewDescription) {
        this.reviewDescription = reviewDescription;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPeriodicalId() {
        return periodicalId;
    }

    public void setPeriodicalId(int periodicalId) {
        this.periodicalId = periodicalId;
    }

    public Boolean getStatusRemove() {
        return statusRemove;
    }

    public void setStatusRemove(Boolean statusRemove) {
        this.statusRemove = statusRemove;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof Reviews)) return false;

        Reviews reviews = (Reviews) o;

        if (periodicalId != reviews.periodicalId) return false;
        if (reviewId != reviews.reviewId) return false;
        if (userId != reviews.userId) return false;
        if (reviewDescription != null ? !reviewDescription.equals(reviews.reviewDescription) : reviews.reviewDescription != null)
            return false;
        if (statusRemove != null ? !statusRemove.equals(reviews.statusRemove) : reviews.statusRemove != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = reviewId;
        result = 31 * result + (reviewDescription != null ? reviewDescription.hashCode() : 0);
        result = 31 * result + userId;
        result = 31 * result + periodicalId;
        result = 31 * result + (statusRemove != null ? statusRemove.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Reviews{" +
                "reviewId=" + reviewId +
                ", reviewDescription='" + reviewDescription + '\'' +
                ", userId=" + userId +
                ", periodicalId=" + periodicalId +
                ", statusRemove=" + statusRemove +
                '}';
    }
}

