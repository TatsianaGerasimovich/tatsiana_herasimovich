package by.bsuir.gerasimovich.periodicals.dao.pool.exception;

import by.bsuir.gerasimovich.periodicals.dao.exception.DAOException;

/**
 * Exception class created specifically to describe the exceptional
 * situation arises in ConnectionPool
 *
 * @author Tatiana
 * @version 1.00 17.05.2015.
 */
public class  ConnectionPoolException extends DAOException {
    /**
     * default constructor
     */
    public ConnectionPoolException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public ConnectionPoolException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public ConnectionPoolException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public ConnectionPoolException (String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
