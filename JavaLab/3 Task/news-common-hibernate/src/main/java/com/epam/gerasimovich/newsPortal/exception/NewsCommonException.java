package com.epam.gerasimovich.newsPortal.exception;

/**
 * @author Tatiana
 * @version 1.00 30.09.2015.
 */
public class NewsCommonException extends Exception {
    /**
     * default constructor
     */
    public NewsCommonException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public NewsCommonException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public NewsCommonException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public NewsCommonException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public NewsCommonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

