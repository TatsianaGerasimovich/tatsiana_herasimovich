package com.epam.gerasimovich.newsPortal.dao.hibernate;
;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Author;

/**
 * Interface provides a base implementation CRUD operations using JDBC for Author entity.
 *
 * Created by Tatiana on 02.10.2015.
 */
public interface AuthorDao extends GenericDao<Author> {

    /*Long create(Author object) throws DAOCommonException;

    List<Author> getAll() throws DAOCommonException;

    void update(Author object) throws DAOCommonException;

    void delete(List<Author> objects) throws DAOCommonException;

    Author getByPK(Long key) throws DAOCommonException;*/

}
