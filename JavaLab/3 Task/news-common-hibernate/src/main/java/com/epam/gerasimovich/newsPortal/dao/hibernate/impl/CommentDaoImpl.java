package com.epam.gerasimovich.newsPortal.dao.hibernate.impl;

import com.epam.gerasimovich.newsPortal.dao.hibernate.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.News;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 1/3/2016
 */
@Transactional
public class CommentDaoImpl implements CommentDao {

    private SessionFactory sessionFactory;

    public CommentDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void update(Comments object) throws DAOCommonException {
        Transaction tx = null;
        try {
   //         sessionFactory.openSession();
            Session ss = sessionFactory.getCurrentSession();
            tx = ss.beginTransaction();
            sessionFactory.getCurrentSession().update(object);
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public Long create(Comments object) throws DAOCommonException {
        Transaction tx = null;
        try {
    //        sessionFactory.openSession();
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
           // session.save(object);
            session.persist(object);
            tx.commit();
            return object.getCommentId();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }

    }


    public List<Comments> getAll() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }


    public Comments getByPK(Long key) throws DAOCommonException {

        //   Comments comment = (Comments)session.load(Comments.class, key);
        Comments comment = (Comments) sessionFactory.openSession().get(Comments.class, key);
            Hibernate.initialize(comment.getNews());
        return comment;
    }



    public void delete(List<Comments> objects) throws DAOCommonException {
        Transaction tx = null;
        try {
     //       sessionFactory.openSession();
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            for (Comments comment : objects) {
                if (null != comment) {
                    session.delete(comment);
                }
            }
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public List<Comments> getCommentsByNews(News news) throws DAOCommonException {
        Transaction tx = null;
        try {
     //       sessionFactory.openSession();
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Comments.class);
            List<Comments> comments = criteria.add(Restrictions.eq("news", news)).list();
            for (Comments comment : comments) {
                comment.setNews(news);
            }
            tx.commit();
            return comments;
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public void deleteByNews(List<News> news) throws DAOCommonException {
        Transaction tx = null;
        try {
     //       sessionFactory.openSession();
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            String hql = "delete from Comments  where news.newsId= :news";
            for (News newsItem : news) {
                if (null != newsItem) {
                    session.createQuery(hql).setLong("news", newsItem.getNewsId()).executeUpdate();
                }
            }
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }


    }
}
