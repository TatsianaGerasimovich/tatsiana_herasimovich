package com.epam.gerasimovich.newsPortal.dao.hibernate;

import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;

import java.util.List;

/**
 * Interface provides a base implementation CRUD operations using JDBC for Tag entity.
 * <p/>
 * Created by Tatiana on 02.10.2015.
 */
public interface TagDao extends GenericDao<Tag> {

   /* Long create(Tag object) throws DAOCommonException;

    List<Tag> getAll() throws DAOCommonException;

    void update(Tag object) throws DAOCommonException;

    void delete(List<Tag> objects) throws DAOCommonException;*/

}

