package com.epam.gerasimovich.newsPortal.dao.hibernate.impl;

import com.epam.gerasimovich.newsPortal.dao.hibernate.AuthorDao;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;
import org.hibernate.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by Tatiana on 02.10.2015.
 */
@Transactional
public class AuthorDaoImpl implements AuthorDao {
    private SessionFactory sessionFactory;

    public AuthorDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public Long create(Author object) throws DAOCommonException {
        Transaction tx = null;
        try {
     //       sessionFactory.openSession();
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            // session.save(object);
            session.persist(object);
            tx.commit();
            return object.getAuthorId();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }
   public List<Author> getAll() throws DAOCommonException {
       Transaction tx = null;
       try {
   //        sessionFactory.openSession();
           Session session = sessionFactory.getCurrentSession();
           tx = session.beginTransaction();
           List<Author> authors=session.createCriteria(Author.class).list();
           tx.commit();
           return authors;
       } catch (HibernateException e) {
           tx.rollback();
           throw new DAOCommonException(e);
       }
    }
   public void update(Author object) throws DAOCommonException{
       Transaction tx = null;
       try {
    //       sessionFactory.openSession();
           Session ss = sessionFactory.getCurrentSession();
           tx = ss.beginTransaction();
           sessionFactory.getCurrentSession().update(object);
           tx.commit();
       } catch (HibernateException e) {
           tx.rollback();
           throw new DAOCommonException(e);
       }
    }
   public void delete(List<Author> objects) throws DAOCommonException{
       Transaction tx = null;
       try {
   //        sessionFactory.openSession();
           Session session = sessionFactory.getCurrentSession();
           tx = session.beginTransaction();
           for (Author author : objects) {
               if (null != author) {
                   session.delete(author);
               }
           }
           tx.commit();
       } catch (HibernateException e) {
           tx.rollback();
           throw new DAOCommonException(e);
       }
    }

   public Author getByPK(Long key) throws DAOCommonException{
       Author author = (Author) sessionFactory.openSession().get(Author.class, key);
      /*  Author author = (Author) sessionFactory.openSession().load(Author.class, key);
       Hibernate.initialize(author);*/
       return author;
    }

}