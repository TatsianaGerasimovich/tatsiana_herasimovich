package com.epam.gerasimovich.newsPortal.dao.hibernate.entity;



import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Class-Entity store the information about <entity>Comments</entity>
 *
 * @author Tatiana
 * @version 1.00 30.09.2015.
 */
@Entity
@Table(name="COMMENTS")
public class Comments implements Serializable {
    /**
     * fields of comments entity
     */
    @Id
    @Column(name = "COMMENT_ID",nullable = false,unique = true)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "COMMENTS_SEQ")
    @SequenceGenerator(name="COMMENTS_SEQ", sequenceName="COMMENTS_SEQ")
    private Long commentId;
    @Column(name = "COMMENT_TEXT", length = 100)
    private String commentText;
    @Column(name = "CREATION_DATE")
    //@Temporal(value=TemporalType.TIMESTAMP)
    private Timestamp creationDate;
    //many to one
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NEWS_ID")
    private News news;

    /**
     * Default constructor
     */
    public Comments() {
    }

    /**
     * getter for comment id field
     * @return value of comment id field
     */

    public Long getCommentId() {
        return commentId;
    }

    /**
     * setter for comment id field
     * @param commentId
     */

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    /**
     * getter for news field
     * @return value of news field
     */
    public News getNews() {
        return news;
    }
    /**
     * setter for news field
     * @param news
     */
    public void setNews(News news) {
        this.news = news;
    }

    /**
     * getter for comment text field
     * @return value of comment text field
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * setter for comment text field
     * @param commentText
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * getter for creation date field
     * @return value of creation date field
     */

    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * setter for creation date field
     * @param creationDate
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comments comments = (Comments) o;

        if (commentId != null ? !commentId.equals(comments.commentId) : comments.commentId != null) return false;
        if (commentText != null ? !commentText.equals(comments.commentText) : comments.commentText != null)
            return false;
        if (creationDate != null ? !creationDate.equals(comments.creationDate) : comments.creationDate != null)
            return false;
        return !(news != null ? !news.equals(comments.news) : comments.news != null);

    }

    @Override
    public int hashCode() {
        int result = commentId != null ? commentId.hashCode() : 0;
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (news != null ? news.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comments{" +
                "commentId=" + commentId +
                ", commentText='" + commentText + '\'' +
                ", creationDate=" + creationDate +
                ", news=" + news +
                '}';
    }
}
