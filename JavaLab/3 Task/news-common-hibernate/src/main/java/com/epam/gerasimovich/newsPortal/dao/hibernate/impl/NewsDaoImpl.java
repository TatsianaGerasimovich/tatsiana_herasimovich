package com.epam.gerasimovich.newsPortal.dao.hibernate.impl;


import com.epam.gerasimovich.newsPortal.dao.hibernate.NewsDao;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.News;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.hibernate.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;
import org.hibernate.*;
import org.hibernate.criterion.Projections;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.StringType;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 04.10.2015.
 */
public class NewsDaoImpl implements NewsDao {
    private final String QURERY_SEARCH_CRITERIA_AUTHOR = "NA.AUTHOR_ID = ?\n";
    private final String QURERY_SEARCH_CRITERIA_AND = " AND ";
    private final String QURERY_SEARCH_CRITERIA_TAG = "NT.TAG_ID = ?\n";
    private final String QURERY_SEARCH_CRITERIA_TAG_OR = "OR NT.TAG_ID = ?\n";
    private final String QUERY_SELECT_ALL_BY_PK_BEGIN = "SELECT\n" +
            "  NEWS_ID,\n" +
            "  TITLE,\n" +
            "  SHORT_TEXT,\n" +
            "  FULL_TEXT,\n" +
            "  CREATION_DATE,\n" +
            "  MODIFICATION_DATE,\n" +
            "  AUTHOR_ID,\n" +
            "  AUTHOR_NAME,\n" +
            "  NEXT_ID,\n" +
            "  PRE_ID\n" +
            "FROM (\n" +
            "  SELECT\n" +
            "    NEWS_ID,\n" +
            "    TITLE,\n" +
            "    SHORT_TEXT,\n" +
            "    FULL_TEXT,\n" +
            "    CREATION_DATE,\n" +
            "    MODIFICATION_DATE,\n" +
            "    AUTHOR_ID,\n" +
            "    AUTHOR_NAME,\n" +
            "    POPULARITY,\n" +
            "    lead(NEWS_ID)\n" +
            "    OVER (ORDER BY POPULARITY DESC, CREATION_DATE DESC)\n" +
            "      AS NEXT_ID,\n" +
            "    lag(NEWS_ID)\n" +
            "    OVER (ORDER BY POPULARITY DESC, CREATION_DATE DESC)\n" +
            "      AS PRE_ID\n" +
            "  FROM (\n" +
            "    SELECT DISTINCT\n" +
            "      N.NEWS_ID,\n" +
            "      N.TITLE,\n" +
            "      N.SHORT_TEXT,\n" +
            "      N.FULL_TEXT,\n" +
            "      N.CREATION_DATE,\n" +
            "      N.MODIFICATION_DATE,\n" +
            "      A.AUTHOR_ID,\n" +
            "      A.AUTHOR_NAME,\n" +
            "      (SELECT COUNT(*)\n" +
            "       FROM Comments C\n" +
            "       WHERE C.NEWS_ID = N.NEWS_ID) AS POPULARITY\n" +
            "    FROM News N\n" +
            "     LEFT JOIN News_Tag NT ON N.NEWS_ID = NT.NEWS_ID\n" +
            "      JOIN News_Author NA ON N.NEWS_ID = NA.NEWS_ID\n" +
            "      JOIN AUTHOR A ON NA.AUTHOR_ID = A.AUTHOR_ID\n";
    private final String QURERY_WHERE = "WHERE ";
    private final String QUERY_SELECT_ALL_BY_PK_END =
            "ORDER BY POPULARITY DESC, CREATION_DATE DESC" +
                    "  )\n" +
                    ")\n" +
                    "WHERE NEWS_ID = ?";
    private SessionFactory sessionFactory;
    private String QUERY_COUNT_NEWS = "SELECT COUNT(*) AS QUANTITY\n" +
            "                          FROM (\n" +
            "SELECT  NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,\n" +
            "  CREATION_DATE,MODIFICATION_DATE,\n" +
            "  A.AUTHOR_ID,A.AUTHOR_NAME,\n" +
            "  POPULARITY\n" +
            "FROM (SELECT N.NEWS_ID,N.TITLE,N.SHORT_TEXT,N.FULL_TEXT,N.CREATION_DATE,N.MODIFICATION_DATE,\n" +
            "        COUNT(C.COMMENT_ID) AS POPULARITY\n" +
            "      FROM NEWS N\n" +
            "        LEFT JOIN COMMENTS C ON N.NEWS_ID = C.NEWS_ID\n" +
            "      GROUP BY N.NEWS_ID,N.TITLE,N.SHORT_TEXT,N.FULL_TEXT,N.CREATION_DATE,N.MODIFICATION_DATE\n" +
            "      ORDER BY POPULARITY DESC, CREATION_DATE DESC) N\n" +
            "  INNER JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID\n" +
            "  INNER JOIN TAG T ON T.TAG_ID = NT.TAG_ID\n" +
            "  INNER JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID\n" +
            "  INNER JOIN AUTHOR A ON A.AUTHOR_ID = NA.AUTHOR_ID\n" +
            "WHERE ";
    private String QUERY_COUNT_END =
            "GROUP BY NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,POPULARITY,A.AUTHOR_ID,A.AUTHOR_NAME\n" +
                    "ORDER BY POPULARITY DESC, CREATION_DATE DESC) ";
    private final String COLUMN_TITLE = "TITLE";
    private final String COLUMN_NEWS_ID = "NEWS_ID";
    private final String COLUMN_SHORT_TEXT = "SHORT_TEXT";
    private final String COLUMN_FULL_TEXT = "FULL_TEXT";
    private final String COLUMN_CREATION_DATE = "CREATION_DATE";
    private final String COLUMN_MODIFICATION_DATE = "MODIFICATION_DATE";
    private final String COLUMN_AUTHOR_ID = "AUTHOR_ID";
    private final String COLUMN_AUTHOR_NAME = "AUTHOR_NAME";
    private final String COLUMN_POPULARITY = "POPULARITY";
    private final String COLUMN_NEXT_ID = "NEXT_ID";
    private final String COLUMN_PRE_ID = "PRE_ID";
    /*  public void bindAuthor(News news, Author author) throws DAOCommonException {
          Transaction transaction = null;
      //    sessionFactory.openSession();
          Session session = sessionFactory.getCurrentSession();
          try {
              transaction = session.beginTransaction();
              news=getByPK(news.getNewsId());
              Set<Author> authors=news.getAuthors();
              authors.add(author);
              session.save(news);
              transaction.commit();
          } catch (Exception e) {
              transaction.rollback();
              throw new DAOCommonException(e);
          }
      }

      public void bindTags(News news, List<Tag> tags) throws DAOCommonException {
          Transaction transaction = null;
    //      sessionFactory.openSession();
          Session session = sessionFactory.getCurrentSession();
          try {
              transaction = session.beginTransaction();
              for (Tag tag : tags) {
                  news.getTags().add(tag);
              }
              session.save(news);
              transaction.commit();
          } catch (Exception e) {
              transaction.rollback();
              throw new DAOCommonException(e);
          } finally {
              session.close();
          }
      }*/
    private String QUERY_SELECT_ALL_INFORMATION_BEGIN =
            "SELECT NWR.* FROM(\n" +
                    "                  SELECT  NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,\n" +
                    "                    CREATION_DATE,MODIFICATION_DATE,\n" +
                    "                    A.AUTHOR_ID,A.AUTHOR_NAME,\n" +
                    "                    POPULARITY,\n" +
                    "                    ROW_NUMBER() OVER (ORDER BY POPULARITY DESC ) AS rn\n" +
                    "                  FROM (SELECT N.NEWS_ID,N.TITLE,N.SHORT_TEXT,N.FULL_TEXT,N.CREATION_DATE,N.MODIFICATION_DATE,\n" +
                    "                          COUNT(C.COMMENT_ID) AS POPULARITY\n" +
                    "                        FROM NEWS N\n" +
                    "                          LEFT JOIN COMMENTS C ON N.NEWS_ID = C.NEWS_ID\n" +
                    "                        GROUP BY N.NEWS_ID,N.TITLE,N.SHORT_TEXT,N.FULL_TEXT,N.CREATION_DATE,N.MODIFICATION_DATE\n" +
                    "                        ORDER BY POPULARITY DESC, CREATION_DATE DESC) N\n" +
                    "                    INNER JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID\n" +
                    "                    INNER JOIN TAG T ON T.TAG_ID = NT.TAG_ID\n" +
                    "                    INNER JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID\n" +
                    "                    INNER JOIN AUTHOR A ON A.AUTHOR_ID = NA.AUTHOR_ID ";
    private String QUERY_SELECT_ALL_INFORMATION_END =
            "GROUP BY NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,POPULARITY,A.AUTHOR_ID,A.AUTHOR_NAME\n" +
                    "                  ORDER BY POPULARITY DESC, CREATION_DATE DESC) NWR\n" +
                    "WHERE rn>=? AND rn<=?";

    public NewsDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public Long create(News object) throws DAOCommonException {
        Transaction tx = null;
        try {
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            // session.save(object);
            session.persist(object);
            tx.commit();
            return object.getNewsId();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public void update(News object) throws DAOCommonException {
        Transaction tx = null;
        try {
            Session ss = sessionFactory.getCurrentSession();
            tx = ss.beginTransaction();
            sessionFactory.getCurrentSession().update(object);
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public void delete(List<News> objects) throws DAOCommonException {
        Transaction tx = null;
        try {
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            for (News news : objects) {
                if (null != news) {
                    session.delete(news);
                }
            }
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public List<News> getAll() throws DAOCommonException {
        Transaction tx = null;
        try {
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            List<News> news = session.createCriteria(News.class).list();
            tx.commit();
            return news;
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public News getByPK(Long key) throws DAOCommonException {
        News news = (News) sessionFactory.openSession().load(News.class, key);
      /* Author author = (Author) sessionFactory.openSession().get(Author.class, key);
       Hibernate.initialize(author);*/
        return news;
    }

    public void unbindAuthors(List<News> news) throws DAOCommonException {

        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            for (News newsItem : news) {
                newsItem.getAuthors().clear();
                session.save(newsItem);
            }
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw new DAOCommonException(e);
        } finally {
            session.close();
        }
    }

    public void unbindTags(List<News> news) throws DAOCommonException {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            for (News newsItem : news) {
                newsItem.getTags().clear();
                session.save(newsItem);
            }
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw new DAOCommonException(e);
        }
    }

    public List<Tag> getTagsByNewsId(Long newsId) throws DAOCommonException {
        News news = (News) sessionFactory.openSession().load(News.class, newsId);
        return new ArrayList<Tag>(news.getTags());
    }

    public Author getAuthorByNewsID(Long newsId) throws DAOCommonException {
        News news = (News) sessionFactory.openSession().load(News.class, newsId);
        return new ArrayList<Author>(news.getAuthors()).get(0);
    }

    public long countNews(NewsSearchCriteria criteria) throws DAOCommonException {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = null;
        Long count;
        if ((criteria == null) || (criteria.getAuthor() == null && (criteria.getTagList() == null || criteria.getTagList().size() == 0))) {
            try {
                transaction = session.beginTransaction();
                count = Long.parseLong(session.createCriteria(News.class).setProjection(Projections.rowCount()).uniqueResult().toString());
                transaction.commit();
            } catch (Exception e) {
                transaction.rollback();
                throw new DAOCommonException(e);
            }
            return count;
        } else {
            StringBuffer sb = new StringBuffer("");
            sb.append(QUERY_COUNT_NEWS);
            if (criteria.getAuthor() != null) {
                sb.append(QURERY_SEARCH_CRITERIA_AUTHOR);
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    sb.append(QURERY_SEARCH_CRITERIA_AND);
                }
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (int i = 0; i < criteria.getTagList().size(); i++) {
                    if (i == 0) {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG);
                    } else {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG_OR);
                    }
                }
            }
            sb.append(QUERY_COUNT_END);
            try {
                transaction = session.beginTransaction();
                SQLQuery query = session.createSQLQuery(sb.toString());
                int numberFlag = 0;
                if (criteria.getAuthor() != null) {
                    query.setLong(numberFlag++, criteria.getAuthor().getAuthorId());
                }
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    for (Tag obj : criteria.getTagList()) {
                        query.setLong(numberFlag++, obj.getTagId());
                    }
                }
                count = Long.parseLong(query.uniqueResult().toString());
                transaction.commit();
            } catch (Exception e) {
                transaction.rollback();
                throw new DAOCommonException(e);
            }
            return count;
        }
    }

    public ViewNews getAllByPK(Long id, NewsSearchCriteria criteria) throws DAOCommonException {
        StringBuffer sb = new StringBuffer("");
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = null;
        sb.append(QUERY_SELECT_ALL_BY_PK_BEGIN);
        ViewNews returnNews = null;
        if ((criteria == null) || (criteria.getAuthor() == null && (criteria.getTagList() == null || criteria.getTagList().size() == 0))) {
            sb.append(QUERY_SELECT_ALL_BY_PK_END);
            try {
                transaction = session.beginTransaction();
                SQLQuery query = session.createSQLQuery(sb.toString())
                        .addScalar(COLUMN_NEWS_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_TITLE, StringType.INSTANCE)
                        .addScalar(COLUMN_SHORT_TEXT, StringType.INSTANCE)
                        .addScalar(COLUMN_FULL_TEXT, StringType.INSTANCE)
                        .addScalar(COLUMN_CREATION_DATE, StandardBasicTypes.TIMESTAMP)
                        .addScalar(COLUMN_MODIFICATION_DATE, StandardBasicTypes.DATE)
                        .addScalar(COLUMN_AUTHOR_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_AUTHOR_NAME, StringType.INSTANCE)
                        .addScalar(COLUMN_NEXT_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_PRE_ID, StandardBasicTypes.LONG);
                List<Object[]> empData = query.setLong(0, id).list();
                returnNews = parseData(empData);
                transaction.commit();
            } catch (Exception e) {
                transaction.rollback();
                throw new DAOCommonException(e);
            }
            return returnNews;
        } else {
            sb.append(QURERY_WHERE);
            if (criteria.getAuthor() != null) {
                sb.append(QURERY_SEARCH_CRITERIA_AUTHOR);
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    sb.append(QUERY_SELECT_ALL_BY_PK_END);
                }
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (int i = 0; i < criteria.getTagList().size(); i++) {
                    if (i == 0) {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG);
                    } else {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG_OR);
                    }
                }
            }
            sb.append(QUERY_SELECT_ALL_BY_PK_END);

            try {
                transaction = session.beginTransaction();
                SQLQuery query = session.createSQLQuery(sb.toString())
                        .addScalar(COLUMN_NEWS_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_TITLE, StringType.INSTANCE)
                        .addScalar(COLUMN_SHORT_TEXT, StringType.INSTANCE)
                        .addScalar(COLUMN_FULL_TEXT, StringType.INSTANCE)
                        .addScalar(COLUMN_CREATION_DATE, StandardBasicTypes.TIMESTAMP)
                        .addScalar(COLUMN_MODIFICATION_DATE, StandardBasicTypes.DATE)
                        .addScalar(COLUMN_AUTHOR_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_AUTHOR_NAME, StringType.INSTANCE)
                        .addScalar(COLUMN_NEXT_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_PRE_ID, StandardBasicTypes.LONG);
                int numberFlag = 0;
                if (criteria.getAuthor() != null) {
                    query.setLong(numberFlag++, criteria.getAuthor().getAuthorId());
                }
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    for (Tag obj : criteria.getTagList()) {
                        query.setLong(numberFlag++, obj.getTagId());
                    }
                }
                List<Object[]> empData = query.setLong(numberFlag, id).list();
                returnNews = parseData(empData);
                transaction.commit();
            } catch (Exception e) {
                transaction.rollback();
                throw new DAOCommonException(e);
            }
            return returnNews;
        }
    }

    private ViewNews parseData(List<Object[]> empData) throws DAOCommonException {
        ViewNews returnNews = null;
        for (Object[] row : empData) {
            returnNews = new ViewNews();
            News news = new News();
            Author author = new Author();

            news.setNewsId(Long.parseLong(row[0].toString()));
            news.setTitle(row[1].toString());
            news.setShortText(row[2].toString());
            news.setFullText(row[3].toString());
            news.setCreationDate(Timestamp.valueOf(row[4].toString()));
            news.setModificationDate(Date.valueOf(row[5].toString()));
            author.setAuthorId(Long.parseLong(row[6].toString()));
            author.setAuthorName(row[7].toString());
            if (row[8] != null) {
                returnNews.setNextNewsId(Long.parseLong(row[8].toString()));
            } else {
                returnNews.setNextNewsId(0L);
            }
            if (row[9] != null) {
                returnNews.setPreNewsId(Long.parseLong(row[9].toString()));
            } else {
                returnNews.setPreNewsId(0L);
            }
            returnNews.setTagList(getTagsByNewsId(news.getNewsId()));
            returnNews.setNews(news);
            returnNews.setAuthor(author);
        }
        return returnNews;
    }

    private List<ListNews> parseDataForSearch(List<Object[]> empData) throws DAOCommonException {
        List<ListNews> list = new ArrayList<ListNews>();

        for (Object[] row : empData) {
            News news = new News();
            Author author = new Author();
            ListNews newsForList = new ListNews();

            news.setNewsId(Long.parseLong(row[0].toString()));
            news.setTitle(row[1].toString());
            news.setShortText(row[2].toString());
            news.setFullText(row[3].toString());
            news.setCreationDate(Timestamp.valueOf(row[4].toString()));
            news.setModificationDate(Date.valueOf(row[5].toString()));
            author.setAuthorId(Long.parseLong(row[6].toString()));
            author.setAuthorName(row[7].toString());
            long countOfComments = 0L;
            if (row[8] != null)
                countOfComments = Long.parseLong(row[8].toString());

            newsForList.setNews(news);
            newsForList.setAuthor(author);
            newsForList.setCountOfComments(countOfComments);
            newsForList.setTagList(getTagsByNewsId(news.getNewsId()));
            list.add(newsForList);
        }

        return list;
    }

    public List<ListNews> searchAllInformation(NewsSearchCriteria criteria, Integer firstRow, Integer rowsPerPage) throws DAOCommonException {
        StringBuffer sb = new StringBuffer("");
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = null;
        sb.append(QUERY_SELECT_ALL_INFORMATION_BEGIN);
        List<ListNews> returnNews = null;
        if ((criteria == null) || (criteria.getAuthor() == null && (criteria.getTagList() == null || criteria.getTagList().size() == 0))) {
            sb.append(QUERY_SELECT_ALL_INFORMATION_END);

            try {
                transaction = session.beginTransaction();
                SQLQuery query = session.createSQLQuery(sb.toString())
                        .addScalar(COLUMN_NEWS_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_TITLE, StringType.INSTANCE)
                        .addScalar(COLUMN_SHORT_TEXT, StringType.INSTANCE)
                        .addScalar(COLUMN_FULL_TEXT, StringType.INSTANCE)
                        .addScalar(COLUMN_CREATION_DATE, StandardBasicTypes.TIMESTAMP)
                        .addScalar(COLUMN_MODIFICATION_DATE, StandardBasicTypes.DATE)
                        .addScalar(COLUMN_AUTHOR_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_AUTHOR_NAME, StringType.INSTANCE)
                        .addScalar(COLUMN_POPULARITY, StandardBasicTypes.LONG);
                List<Object[]> empData = query.setLong(0, firstRow)
                        .setLong(1, firstRow + rowsPerPage).list();
                returnNews = parseDataForSearch(empData);
                transaction.commit();
            } catch (Exception e) {
                transaction.rollback();
                throw new DAOCommonException(e);
            }
            return returnNews;
        } else {
            sb.append(QURERY_WHERE);
            if (criteria.getAuthor() != null) {
                sb.append(QURERY_SEARCH_CRITERIA_AUTHOR);
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    sb.append(QURERY_SEARCH_CRITERIA_AND);
                }
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (int i = 0; i < criteria.getTagList().size(); i++) {
                    if (i == 0) {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG);
                    } else {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG_OR);
                    }
                }
            }
            sb.append(QUERY_SELECT_ALL_INFORMATION_END);
            try {
                transaction = session.beginTransaction();
                SQLQuery query = session.createSQLQuery(sb.toString())
                        .addScalar(COLUMN_NEWS_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_TITLE, StringType.INSTANCE)
                        .addScalar(COLUMN_SHORT_TEXT, StringType.INSTANCE)
                        .addScalar(COLUMN_FULL_TEXT, StringType.INSTANCE)
                        .addScalar(COLUMN_CREATION_DATE, StandardBasicTypes.TIMESTAMP)
                        .addScalar(COLUMN_MODIFICATION_DATE, StandardBasicTypes.DATE)
                        .addScalar(COLUMN_AUTHOR_ID, StandardBasicTypes.LONG)
                        .addScalar(COLUMN_AUTHOR_NAME, StringType.INSTANCE)
                        .addScalar(COLUMN_POPULARITY, StandardBasicTypes.LONG);
                int numberFlag = 0;
                if (criteria.getAuthor() != null) {
                    query.setLong(numberFlag++, criteria.getAuthor().getAuthorId());
                }
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    for (Tag obj : criteria.getTagList()) {
                        query.setLong(numberFlag++, obj.getTagId());
                    }
                }
                List<Object[]> empData = query.setLong(numberFlag++, firstRow)
                        .setLong(numberFlag++, firstRow + rowsPerPage).list();
                returnNews = parseDataForSearch(empData);
                transaction.commit();
            } catch (Exception e) {
                transaction.rollback();
                throw new DAOCommonException(e);
            }
        }
        return returnNews;
    }

}