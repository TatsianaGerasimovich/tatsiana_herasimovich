package com.epam.gerasimovich.newsPortal.dao.hibernate;

import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.News;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 1/5/2016
 */
public class Test {


    public void method() throws DAOCommonException {
        ApplicationContext context=new ClassPathXmlApplicationContext("spring-module.xml");
        CommentDao commentsDAO= (CommentDao) context.getBean("commentsDAO");
        Comments comment = new Comments();
       comment.setCommentText("one");
        comment.setNews(new News(1L));
        Date date = new Date();
       long time = date.getTime();
        comment.setCreationDate(new Timestamp(time));
        System.out.println("Create: " + commentsDAO.create(comment));
       /* System.out.println("Get all");
        for(Comments obj : commentsDAO.getAll()){
            System.out.println(obj);
        }*/
        comment.setCommentId(105550L);//!
        comment.setCommentText("two");
      //  commentsDAO.update(comment);
    //    System.out.println("Update:" + comment);
      //  System.out.println("Get:");
     //   for(Comments obj : commentsDAO.getAll()){
     /*   try {
            System.out.println(commentsDAO.getByPK(105550L).getNews().getNewsId());
        } catch (DAOCommonException e) {
            e.printStackTrace();
        }*/
        //}
       /* List<Comments> comments=new ArrayList<Comments>();
        comments.add(comment);
        System.out.println("Delete:");
        commentsDAO.delete(comments);*/
        News news=new News();
        news.setNewsId(18L);
       /* for(Comments obj : commentsDAO.getCommentsByNews(news)){
            System.out.println(obj);
        }*/
        List<News> newsList =new ArrayList<News>();
        newsList.add(news);
       // commentsDAO.deleteByNews(newsList);
       /* try {
            Comments commentGet= commentsDAO.getByPK(18L);
            System.out.println(commentGet.getCommentText());
        } catch (DAOCommonException e) {
            e.printStackTrace();
        }*/
    }
    public static void main(String[] args) {
        Test test=new Test();
        try {
            test.method();
        } catch (DAOCommonException e) {
            e.printStackTrace();
        }
    }
}
