package com.epam.gerasimovich.newsPortal.dao.hibernate.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Class-Entity store the information about <entity>Author</entity>
 *
 * @author Tatiana
 * @version 1.00 30.09.2015.
 */
@Entity
@Table(name="AUTHOR")
public class Author implements Serializable {

    /**
     * fields of author entity
     */
    @Id
    @Column(name = "AUTHOR_ID",nullable = false,unique = true)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "AUTHORS_SEQ")
    @SequenceGenerator(name="AUTHORS_SEQ", sequenceName="AUTHORS_SEQ")
    private Long authorId;
    @Column(name = "AUTHOR_NAME", length = 20)
    private String authorName;
    @Column(name = "EXPIRED")
    private Timestamp expired;

   /* @ManyToMany(fetch = FetchType.LAZY, mappedBy = "authors",cascade = CascadeType.ALL,
            targetEntity = News.class)
   // @OnDelete(action = OnDeleteAction.CASCADE)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)*/
   @ManyToMany(fetch = FetchType.LAZY, targetEntity = News.class, cascade = CascadeType.ALL)
   @JoinTable(name = "NEWS_AUTHOR", joinColumns = {
           @JoinColumn(name = "AUTHOR_ID", nullable = false, updatable = false)},
           inverseJoinColumns = {@JoinColumn(name = "NEWS_ID",
                   nullable = false, updatable = false)})
   @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private Set<News> news = new HashSet<News>(0);
    /**
     * Constructor parameters
     */
    public Author(Long authorId, String authorName, Timestamp expired) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.expired = expired;
    }

    /**
     * Default constructor
     */
    public Author() {
    }

    /**
     * getter for author id field
     *
     * @return value of author id field
     */
    public Long getAuthorId() {
        return authorId;
    }

    /**
     * setter for author id field
     *
     * @param authorId
     */
    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    /**
     * getter for author name field
     *
     * @return value of author name field
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     * setter for author name field
     *
     * @param authorName
     */

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    /**
     * getter for expired field
     *
     * @return value of expired field
     */
    public Timestamp getExpired() {
        return expired;
    }

    /**
     * setter for expired field
     *
     * @param expired
     */
    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (authorId != null ? !authorId.equals(author.authorId) : author.authorId != null) return false;
        if (authorName != null ? !authorName.equals(author.authorName) : author.authorName != null) return false;
        if (expired != null ? !expired.equals(author.expired) : author.expired != null) return false;
        return !(news != null ? !news.equals(author.news) : author.news != null);

    }

    @Override
    public int hashCode() {
        int result = authorId != null ? authorId.hashCode() : 0;
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        result = 31 * result + (news != null ? news.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                ", expired=" + expired +
                '}';
    }
}
