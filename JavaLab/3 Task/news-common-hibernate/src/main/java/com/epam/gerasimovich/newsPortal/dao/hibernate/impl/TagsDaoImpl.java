package com.epam.gerasimovich.newsPortal.dao.hibernate.impl;


import com.epam.gerasimovich.newsPortal.dao.hibernate.TagDao;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.News;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
@Transactional
public class TagsDaoImpl implements TagDao {
    private SessionFactory sessionFactory;

    public TagsDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public Long create(Tag object) throws DAOCommonException {
        Transaction tx = null;
        try {
         //   sessionFactory.openSession();
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            // session.save(object);
            session.persist(object);
            tx.commit();
            return object.getTagId();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public List<Tag> getAll() throws DAOCommonException {
        Transaction tx = null;
        try {
       //     sessionFactory.openSession();
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            List<Tag> tags = session.createCriteria(Tag.class).list();
            tx.commit();
            return tags;
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public void update(Tag object) throws DAOCommonException {
        Transaction tx = null;
        try {
      //      sessionFactory.openSession();
            Session ss = sessionFactory.getCurrentSession();
            tx = ss.beginTransaction();
            sessionFactory.getCurrentSession().update(object);
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public void delete(List<Tag> objects) throws DAOCommonException {
        Transaction tx = null;
        try {
      //      sessionFactory.openSession();
            Session session = sessionFactory.getCurrentSession();
            tx = session.beginTransaction();
            for (Tag tag : objects) {
                if (null != tag) {
                    session.delete(tag);
                }
            }
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            throw new DAOCommonException(e);
        }
    }

    public Tag getByPK(Long key) throws DAOCommonException {
       Tag tag = (Tag) sessionFactory.openSession().get(Tag.class, key);
      /* Tag tag = (Tag) sessionFactory.openSession().load(Tag.class, key);
       Hibernate.initialize(author);*/
        return tag;
    }

}
