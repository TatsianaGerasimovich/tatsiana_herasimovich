package com.epam.gerasimovich.newsPortal.dao.hibernate;



import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.News;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Interface provides a base implementation CRUD operations using JDBC for Comment entity.
 * <p/>
 * Created by Tatiana on 02.10.2015.
 */
@Transactional
public interface CommentDao extends GenericDao<Comments> {
    /**
     * Long create(Comments object) throws DAOCommonException;
     * <p/>
     * List<Comments> getAll() throws DAOCommonException;
     * <p/>
     * void update(Comments object) throws DAOCommonException;
     * <p/>
     * void delete(List<Comments> objects) throws DAOCommonException;
     */




    /**
     * Method gets a list of all appropriate records in the database which have certain news id
     *
     * @param news
     * @return list of all appropriate records in the database
     * @throws DAOCommonException
     */
    List<Comments> getCommentsByNews(News news) throws DAOCommonException;

    /**
     * Method deletes a list of all appropriate records in the database which have certain news id
     *
     * @param news
     * @throws DAOCommonException
     */
    void deleteByNews(List<News> news) throws DAOCommonException;
}
