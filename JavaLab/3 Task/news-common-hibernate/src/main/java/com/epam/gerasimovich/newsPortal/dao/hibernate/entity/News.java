package com.epam.gerasimovich.newsPortal.dao.hibernate.entity;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Class-Entity store the information about <entity>News</entity>
 *
 * @author Tatiana
 * @version 1.00 30.09.2015.
 */
@Entity
@Table(name = "NEWS")
public class News implements Serializable {
    //TODO каскадное удаление tag, author
    /**
     * fields of comments entity
     */
    @Id
    @Column(name = "NEWS_ID", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_SEQ")
    @SequenceGenerator(name = "NEWS_SEQ", sequenceName = "NEWS_SEQ")
    private Long newsId;
    @Column(name = "TITLE", length = 30)
    private String title;
    @Column(name = "SHORT_TEXT", length = 100)
    private String shortText;
    @Column(name = "FULL_TEXT", length = 2000)
    private String fullText;
    @Column(name = "CREATION_DATE")
    //@Temporal(value=TemporalType.TIMESTAMP)
    private Timestamp creationDate;
    @Column(name = "MODIFICATION_DATE")
    //@Temporal(value=TemporalType.DATE)
    private Date modificationDate;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "news")
    private Set<Comments> comments = new HashSet<Comments>(
            0);
    @ManyToMany(fetch = FetchType.LAZY, targetEntity = Author.class, cascade = CascadeType.ALL)
    @JoinTable(name = "NEWS_AUTHOR", joinColumns = {
            @JoinColumn(name = "NEWS_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "AUTHOR_ID",
                    nullable = false, updatable = false)})
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private Set<Author> authors = new HashSet<Author>(0);

    @ManyToMany(fetch = FetchType.LAZY, targetEntity = Tag.class, cascade = CascadeType.ALL)
    @JoinTable(name = "NEWS_TAG", joinColumns = {
            @JoinColumn(name = "NEWS_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "TAG_ID",
                    nullable = false, updatable = false)})
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private Set<Tag> tags = new HashSet<Tag>(0);

    /**
     * Default constructor
     */
    public News() {
    }

    public News(Long newsId) {
        this.newsId = newsId;
    }

    /**
     * Constructor parameters
     *
     * @param newsId
     * @param title
     * @param shortText
     * @param fullText
     * @param creationDate
     * @param modificationDate
     */
    public News(Long newsId, String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate) {
        this.modificationDate = modificationDate;
        this.creationDate = creationDate;
        this.fullText = fullText;
        this.shortText = shortText;
        this.title = title;
        this.newsId = newsId;
    }

    public News(Long newsId, String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate, Set<Comments> comments) {
        this.modificationDate = modificationDate;
        this.creationDate = creationDate;
        this.fullText = fullText;
        this.shortText = shortText;
        this.title = title;
        this.newsId = newsId;
        this.comments = comments;
    }

    /**
     * getter for news id field
     *
     * @return value of news id field
     */
    public Long getNewsId() {
        return newsId;
    }

    /**
     * setter for news id field
     *
     * @param newsId
     */
    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    /**
     * getter for title field
     *
     * @return value of title field
     */
    public String getTitle() {
        return title;
    }

    /**
     * setter for title field
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * getter for short text field
     *
     * @return value of short text field
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * setter for short text field
     *
     * @param shortText
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * getter for full text field
     *
     * @return value of full text field
     */
    public String getFullText() {
        return fullText;
    }

    /**
     * setter for full text field
     *
     * @param fullText
     */
    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    /**
     * getter for creation date field
     *
     * @return value of creation date field
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * setter for creation date field
     *
     * @param creationDate
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * getter for modification date field
     *
     * @return value of creation date field
     */
    public Date getModificationDate() {
        return modificationDate;
    }

    /**
     * setter for modification date field
     *
     * @param modificationDate
     */
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Set<Comments> getComments() {
        return comments;
    }

    public void setComments(Set<Comments> comments) {
        this.comments = comments;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (newsId != news.newsId) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) return false;
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) return false;
        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
        return !(modificationDate != null ? !modificationDate.toString().equals(news.modificationDate.toString()) : news.modificationDate != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "News{" +
                "newsId=" + newsId +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                '}';
    }
}
