package com.epam.gerasimovich.newsPortal.dao.hibernate;

import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;

import java.util.List;

/**
 * Interface provides a base implementation CRUD operations using JDBC.
 *
 * @author Tatiana
 * @version 1.00 28.09.2015.
 */

public interface GenericDao<T> {
    /**
     * Method creates a new entry, the corresponding object
     */
    public Long create(T object) throws DAOCommonException;

    /**
     * Method gets the appropriate record with a primary key or a null key
     */

    public T getByPK(Long key) throws DAOCommonException;

    /**
     * Method saves the state of the object in the database
     */
    public void update(T object) throws DAOCommonException;

    /**
     * Removes a record of the object from the database
     */
    public void delete(List<T> objects) throws DAOCommonException;

    /**
     * Returns a list of all appropriate records in the database
     */
    public List<T> getAll() throws DAOCommonException;

}
