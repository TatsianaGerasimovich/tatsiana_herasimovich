package newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.hibernate.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/5/2015�
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-module-test.xml"})
@DatabaseSetup("classpath:inputData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class CommentsDaoTest {
    private final static Logger LOG = Logger.getLogger(CommentsDaoTest.class);
   /* ApplicationContext context=new ClassPathXmlApplicationContext("spring-module-test.xml");
    CommentDao commentsDao= (CommentDao) context.getBean("commentsDAO");*/
    @Autowired
    private CommentDao commentsDao;

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:commentsTest/updateExpectedDatabase.xml")
    public void updateTest() throws DAOCommonException {
            Comments c = new Comments();
            c.setCommentId(1L);
            c.setCommentText("badly!");
            c.setCreationDate(new Timestamp(1439806332000L));
            c.setNews(new News(101L));
            commentsDao.update(c);
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:commentsTest/deleteExpectedDatabase.xml")
    public void deleteTest() throws DAOCommonException {
        Comments comment = new Comments();
        comment.setCommentId(1L);
        List<Comments> list = new ArrayList<Comments>();
        list.add(comment);
        commentsDao.delete(list);
    }
    @Test
    public void getByNewsTest() throws DAOCommonException {
        List<Comments> comments = null;
       comments = commentsDao.getCommentsByNews(new News(102L));
        Assert.assertNotNull(comments);
        Assert.assertEquals(3, comments.size());
    }

   /* @Test
    public void createTest() throws DAOCommonException {
        Comments comment = new Comments();
        Comments commentRes = null;
            comment.setNews(new News(101L));
            comment.setCommentText("I don't believe");
            comment.setCreationDate(new Timestamp(new Date().getTime() + 60000L));
            long resId = commentsDao.create(comment);
            comment.setCommentId(resId);
            commentRes = commentsDao.getByPK(resId);

        Assert.assertEquals(comment, commentRes);
    }*/

    @Test
    public void getByNewsIdTest() throws DAOCommonException {
        List<Comments> resList = null;

            resList=commentsDao.getCommentsByNews(new News(101L));

        Assert.assertNotNull(resList);
        Assert.assertEquals(2, resList.size());
    }
    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:commentsTest/deleteByNewsIdExpectedDatabase.xml")
    public void deleteByNewsTest() throws DAOCommonException {
        News news = new News();
        news.setNewsId(102L);
        List<News> list = new ArrayList<News>();
        list.add(news);
        commentsDao.deleteByNews(list);
    }

}