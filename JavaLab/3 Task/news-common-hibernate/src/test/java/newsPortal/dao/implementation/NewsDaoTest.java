package newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.hibernate.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.hibernate.NewsDao;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.News;
import com.epam.gerasimovich.newsPortal.dao.hibernate.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.hibernate.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.hibernate.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/5/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-module-test.xml"})
@DatabaseSetup("classpath:inputData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class NewsDaoTest {
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private CommentDao commentsDao;

    @Test
    public void getAllTest() throws DAOCommonException {
        List<News> resList = null;
        resList = newsDao.getAll();

        Assert.assertNotNull(resList);
        Assert.assertEquals(2, resList.size());
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/updateExpectedDatabase.xml")
    public void updateTest() throws DAOCommonException {
        News news = new News();
        news.setNewsId(102L);
        news.setTitle("berries");
        news.setShortText("About berries");
        news.setFullText("berries are helpful");
        news.setCreationDate(new Timestamp(1439802732000L));
        news.setModificationDate(new java.sql.Date(1442437200000L));
        newsDao.update(news);
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/deleteExpectedDatabase.xml")
    public void deleteTest() throws DAOCommonException {
        News news = new News();
        news.setNewsId(101L);
        List<News> list = new ArrayList<News>();
        list.add(news);
        commentsDao.deleteByNews(list);
        newsDao.delete(list);
    }

    @Test
    public void insertTest() throws DAOCommonException {
        News news = new News();
        news.setTitle("Baries");
        news.setShortText("About baries");
        news.setFullText("Baries are helpful");
        news.setCreationDate(new Timestamp(new Date().getTime() + 60000L));
        news.setModificationDate(new java.sql.Date(new Date().getTime() + 360000L));
        News resNews = null;
        long resId = newsDao.create(news);
        news.setNewsId(resId);
        resNews = newsDao.getByPK(resId);

        Assert.assertEquals(news.toString(), resNews.toString());
    }

    @Test
    public void getByIdTest() throws DAOCommonException {
        ViewNews news = new ViewNews();
        news.setNews(new News(101L, "Vegetables", "About vegetables",
                "vegetables are helpful", Timestamp.valueOf("2015-07-25 14:17:25.0"),
                java.sql.Date.valueOf( "2015-08-17" )));
        news.setNextNewsId(0L);
        news.setPreNewsId(102L);
        news.setAuthor(new Author(1L, "Tanya", null));
        List <Tag> tags=new ArrayList<Tag>();
        tags.add(new Tag(101L, "vegetables"));
        tags.add(new Tag(103L, "helpful"));
        news.setTagList(tags);
        ViewNews resNews=null;
            resNews = newsDao.getAllByPK(101L,null);

        Assert.assertEquals(news, resNews);
    }

    @Test
    public void getAuthorByNewsIdTest() throws DAOCommonException {
        Author author = new Author();
        Author resAuthor = null;
        author.setAuthorId(1L);
        author.setAuthorName("Tanya");
        author.setExpired(new Timestamp(1441868400000L));
        resAuthor = newsDao.getAuthorByNewsID(101L);

        Assert.assertEquals(author.toString(), resAuthor.toString());
    }

    @Test
    public void getTagsByNewsIdTest() throws DAOCommonException {
        Tag tag1 = new Tag();
        tag1.setTagId(101L);
        tag1.setTagName("vegetables");
        Tag tag2 = new Tag();
        tag2.setTagId(103L);
        tag2.setTagName("helpful");
        List<Tag> list = new ArrayList<Tag>();
        list.add(tag1);
        list.add(tag2);
        List<Tag> resList = null;

        resList = newsDao.getTagsByNewsId(101L);

        Assert.assertEquals(list.size(), resList.size());
    }


    @Test
    public void searchAllInfotmationTest() throws DAOCommonException {
        Author author = new Author();
        author.setAuthorId(1L);
        Tag tag = new Tag();
        tag.setTagId(103L);
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(tag);
        NewsSearchCriteria criteria = new NewsSearchCriteria();
        criteria.setAuthor(author);
        criteria.setTagList(tags);
        List<ListNews> resNews = null;

            resNews = newsDao.searchAllInformation(criteria, 1, 2);

        Assert.assertEquals(1, resNews.size());
    }

    @Test
    public void countNewsTest() throws DAOCommonException {
        long quantity = 0;
        Author author = new Author();
        author.setAuthorId(1L);
        Tag tag = new Tag();
        tag.setTagId(103L);
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(tag);
        NewsSearchCriteria criteria = new NewsSearchCriteria();
        criteria.setAuthor(author);
        criteria.setTagList(tags);
        quantity = newsDao.countNews(criteria);
        Assert.assertEquals(quantity, 1);
    }

    @Test
    public void countNewsWithoutCriteriaTest() throws DAOCommonException {
        long quantity = 0;
        NewsSearchCriteria criteria = new NewsSearchCriteria();
        quantity = newsDao.countNews(criteria);
        Assert.assertEquals(quantity, 2);
    }

    /*@Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/contactAuthorExpectedDatabase.xml")
    public void setAuthorTest() throws DAOCommonException {
            News news = new News();
            news.setNewsId(101L);
            Author author = new Author();
            author.setAuthorId(3L);
            newsDao.bindAuthor(news, author);
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/contactTagsExpectedDatabase.xml")
    public void contactTagsTest() throws DAOCommonException {
            News news = new News();
            news.setNewsId(101L);
            Tag tag = new Tag();
            tag.setTagId(101L);
            List<Tag> tagList = new ArrayList<Tag>();
            tagList.add(tag);
            newsDao.bindTags(news, tagList);

    }*/
}