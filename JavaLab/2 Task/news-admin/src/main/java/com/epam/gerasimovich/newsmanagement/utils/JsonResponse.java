package com.epam.gerasimovich.newsmanagement.utils;

/**
 * @author Tatiana
 * @version 1.00 11/20/2015
 */
public class JsonResponse {


    private Object result = null;
    private String msgError=null;

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }
}
