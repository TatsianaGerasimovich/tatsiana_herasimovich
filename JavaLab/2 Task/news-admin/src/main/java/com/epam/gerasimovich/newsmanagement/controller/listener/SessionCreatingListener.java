package com.epam.gerasimovich.newsmanagement.controller.listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author Tatiana
 * @version 1.00 11/5/2015
 */
public class SessionCreatingListener implements HttpSessionListener {

    private static final String CURRENT_PAGE = "currentPage";
    private static final String SEARCH_CRITERIA = "searchCriteria";
    private static final String LOCALE = "locale";

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        synchronized (session) {
            session.setAttribute(CURRENT_PAGE, 1);
            session.setAttribute(SEARCH_CRITERIA, null);
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) throws UnsupportedOperationException{
        throw new UnsupportedOperationException();
    }

}
