package com.epam.gerasimovich.newsmanagement.controller.validator;

import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author Tatiana
 * @version 1.00 11/18/2015
 */
@Component
public class CommentValidator implements Validator {
    public boolean supports(Class<?> clazz) {
        return Comments.class.equals(clazz);
    }

    public void validate(Object target, Errors errors) {
        Comments comment = (Comments) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "commentText", "empty.comment");
       if(comment.getCommentText()!=null && comment.getCommentText().length()>100){
            errors.rejectValue("commentText","error.comment.size");
        }
    }
}
