package com.epam.gerasimovich.newsmanagement.controller;

import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsmanagement.utils.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Tatiana
 * @version 1.00 11/19/2015
 */
@Controller
@RequestMapping("/edit-tags")
public class EditTagsController {
    private static final String TAGS_LIST = "tagList";
    private static final String NEW_TAG = "newTag";

    @Autowired
    private IMainService newsMainService;
    @Autowired
    private Validator tagValidator;
    @Autowired
    private MessageSource messageSource;

    @InitBinder("tag")
    private void initTagBinder(WebDataBinder binder){binder.setValidator(tagValidator);}

    @InitBinder("newTag")
    private void initNewTagBinder(WebDataBinder binder){binder.setValidator(tagValidator);}

    @RequestMapping()
    public String getRootTagsPage() throws ServiceCommonException {
        return "redirect:/edit-tags/allTags";
    }

    @RequestMapping(value = "/allTags",method = RequestMethod.GET)
    public String getEditPage(Model model) throws ServiceCommonException {
        List<Tag> tagList = newsMainService.getAllTags();
        model.addAttribute(TAGS_LIST,tagList);
        if(!model.containsAttribute(NEW_TAG)) {
            model.addAttribute(NEW_TAG,new Tag());
        }
        return "edit-tags";
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse updateTag(@RequestBody @Validated final Tag tag) throws ServiceCommonException {
        JsonResponse res = new JsonResponse();
        newsMainService.updateTag(tag);
        res.setResult(tag.getTagName());
        return res;
    }
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse deleteTag(@RequestBody final Tag tag) throws ServiceCommonException{
        JsonResponse res = new JsonResponse();
        List <Tag> tags=new ArrayList<>();
        tags.add(tag);
        newsMainService.deleteTag(tags);
        res.setResult(newsMainService.getAllTags());
        return res;
    }

    @RequestMapping(value = "/allTags",method = RequestMethod.POST,produces = "text/plain;charset=UTF-8")
    public String addTag(@ModelAttribute(value = NEW_TAG)@Validated final Tag tag,
                         final BindingResult br,
                         RedirectAttributes attr) throws ServiceCommonException{
        if(br.hasErrors()){
            attr.addFlashAttribute("org.springframework.validation.BindingResult.newTag",br);
            attr.addFlashAttribute(NEW_TAG,tag);
            return "redirect:/edit-tags/allTags";
        }
        newsMainService.createTag(tag);
        return "redirect:/edit-tags/allTags";
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public JsonResponse handleException(MethodArgumentNotValidException ex,Locale locale) {
        JsonResponse res = new JsonResponse();
        String message=null;
        for (Object object : ex.getBindingResult().getAllErrors()) {
            if (object instanceof FieldError) {
                FieldError fieldError = (FieldError) object;
                message = messageSource.getMessage(fieldError, locale);
            }
        }
        res.setMsgError(message);
        return res;
    }


}


