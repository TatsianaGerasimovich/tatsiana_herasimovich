package com.epam.gerasimovich.newsmanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Locale;

/**
 * @author Tatiana
 * @version 1.00 12/1/2015
 */
@Controller

public class LoginController {
    private static final String MESSAGE = "message";
    private static final String MESSAGE_FIELD = "message";
    private static final String ACCESS = "access.denied";
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/authentication/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/authentication/accessdenied", method = RequestMethod.GET)
    public String loginerror(Model model,Locale locale) {
        String message= messageSource.getMessage(ACCESS,null, locale);
        model.addAttribute(MESSAGE, message);
        return "login";
    }

    @RequestMapping(value = "/logout")
    public String logout() {
        return "login";
    }




}