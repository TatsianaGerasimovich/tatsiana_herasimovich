package com.epam.gerasimovich.newsmanagement.controller.validator;

import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author Tatiana
 * @version 1.00 11/22/2015
 */
@Component
public class TagValidator implements Validator
{
    public boolean supports(Class<?> clazz) {
        return Tag.class.isAssignableFrom(clazz);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tagName", "empty.tag");
        Tag tag = (Tag) target;
        if(tag.getTagName().length()>30){
            errors.rejectValue("tagName","error.tag.size");
        }
    }
}
