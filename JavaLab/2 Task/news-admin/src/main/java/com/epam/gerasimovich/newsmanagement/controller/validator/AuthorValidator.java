package com.epam.gerasimovich.newsmanagement.controller.validator;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author Tatiana
 * @version 1.00 11/22/2015
 */
@Component
public class AuthorValidator implements Validator {

    public boolean supports(Class<?> clazz) {
        return Author.class.isAssignableFrom(clazz);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "authorName", "empty.author");
        Author author = (Author) target;
        if(author.getAuthorName().length()>30){
            errors.rejectValue("authorName","error.author.size");
        }
    }
}
