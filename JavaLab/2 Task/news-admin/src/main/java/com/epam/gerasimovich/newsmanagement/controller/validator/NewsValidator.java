package com.epam.gerasimovich.newsmanagement.controller.validator;

import com.epam.gerasimovich.newsPortal.valueObject.ModelNews;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author Tatiana
 * @version 1.00 11/26/2015
 */
@Component
public class NewsValidator implements Validator {
    public boolean supports(Class<?> clazz) {
        return ModelNews.class.isAssignableFrom(clazz);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.title", "empty.news.title");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"news.shortText","empty.news.shortText");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"news.fullText","empty.news.fullText");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"news.modificationDate","empty.news.date");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"authorId","empty.author");
        ModelNews news = (ModelNews) target;
        if(news.getNews().getTitle().length()>30){
            errors.rejectValue("news.title","error.news.title.size");
        }
        if(news.getNews().getShortText().length()>100){
            errors.rejectValue("news.shortText","error.news.shortText.size");
        }
        if(news.getNews().getFullText().length()>2000){
            errors.rejectValue("news.fullText","error.news.fullText.size");
        }
       /* if(news.getAuthorId()==null){
            errors.rejectValue("authorId","error.news.author.empty");
        }*/

    }
}
