package com.epam.gerasimovich.newsmanagement.controller;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsmanagement.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 11/14/2015
 */
@Controller
public class NewsListController {

    private static final String AUTHORS = "authors";
    private static final String TAG_LIST = "tagList";
    private static final String NEWS_LIST = "newsList";
    private static final String SEARCH_CRITERIA = "searchCriteria";
    private static final String AUTHOR_SEARCH = "authorSearch";
    private static final String TAGS_SEARCH = "tagsSearch";
    private static final String TOTAL_PAGES = "totalPages";
    private static final String FIRST_PAGE = "firstPage";
    private static final String CURRENT_PAGE = "currentPage";
    private final Integer rowsPerPage = 3;

    @Autowired
    private IMainService newsMainService;

    @RequestMapping({"/", "/index",})
    public String index() {

        return "redirect:/page/1";
    }

    @RequestMapping({"/page/{page}"})
    public String getNews(@PathVariable("page") Integer page,
                          Model model, HttpSession httpSession) throws ServiceCommonException {
        NewsSearchCriteria criteria = (NewsSearchCriteria) httpSession.getAttribute(SEARCH_CRITERIA);

        Author authorSearch = null;
        List<Tag> tagsSearch = null;
        if (criteria != null) {
            authorSearch = criteria.getAuthor();
            tagsSearch = criteria.getTagList();
        }
        long totalRows = newsMainService.countNews(authorSearch, tagsSearch);
        long totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
        Integer firstRow = (page - 1) * rowsPerPage;
        httpSession.setAttribute(CURRENT_PAGE, page);
        model.addAttribute(NEWS_LIST, newsMainService.search(authorSearch, tagsSearch, firstRow, rowsPerPage));
        model.addAttribute(TAG_LIST, newsMainService.getAllTags());
        List<Author> authors=newsMainService.getAllAuthor();
        Iterator<Author> it = authors.iterator();
        while (it.hasNext()) {
            Author author = it.next();
            if (author.getExpired()!=null) {
                it.remove();
            }
        }
        model.addAttribute(AUTHORS, authors);
        model.addAttribute(AUTHOR_SEARCH, authorSearch);
        model.addAttribute(TAGS_SEARCH, tagsSearch);
        model.addAttribute(TOTAL_PAGES, totalPages);
        model.addAttribute(FIRST_PAGE, 1);
        return "list";
    }

    @RequestMapping(value = {"/page"})
    public String filterNews(@RequestParam(value = "authorId", required = false) Long authorId,
                             @RequestParam(value = "tagId", required = false) Long[] tagIds,
                             HttpSession httpSession) throws ServiceCommonException {

        NewsSearchCriteria criteria = null;
        if (authorId != null || tagIds != null) {
            criteria = new NewsSearchCriteria();
            if (authorId != null && authorId != 0) {
                Author author = new Author();
                author.setAuthorId(authorId);
                criteria.setAuthor(author);
            }

            if (tagIds != null) {
                List<Tag> tagsSearch = new ArrayList<Tag>();
                for (Long tagId : tagIds) {
                    if (tagId != 0) {
                        Tag tag = new Tag();
                        tag.setTagId(tagId);
                        tagsSearch.add(tag);
                    }
                }
                criteria.setTagList(tagsSearch);
            }
        }
        httpSession.setAttribute(SEARCH_CRITERIA, criteria);
        return "redirect:/page/1";
    }

    @RequestMapping(value = {"/page/delete"}, method = RequestMethod.POST)
    public String deleteNews(@RequestParam(value = "newsId", required = false) Long[] newsIds,
                             HttpSession httpSession) throws ServiceCommonException {
        if(newsIds!=null) {
            List<News> newsList = new ArrayList<News>(newsIds.length);
            News news;
            for (Long newsId : newsIds) {
                news = new News();
                news.setNewsId(newsId);
                newsList.add(news);
            }
        newsMainService.delete(newsList);
        }
        NewsSearchCriteria criteria = (NewsSearchCriteria) httpSession.getAttribute(SEARCH_CRITERIA);
        return "redirect:/page" + ControllerUtils.constructUrl(criteria);
    }

}

