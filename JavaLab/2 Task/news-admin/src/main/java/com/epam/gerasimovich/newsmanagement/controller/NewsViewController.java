package com.epam.gerasimovich.newsmanagement.controller;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsmanagement.utils.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author Tatiana
 * @version 1.00 11/17/2015
 */
@Controller
@RequestMapping("/news")
public class NewsViewController {

    private static final String SEARCH_CRITERIA = "searchCriteria";
    private static final String NEWS = "news";

    @Autowired
    private IMainService newsMainService;
    @Autowired
    private MessageSource messageSource;

    @Autowired
    @Qualifier("commentValidator")
    private Validator commentValidator;

    @InitBinder("comments")
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(commentValidator);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showUser(@PathVariable("id") Integer newsId, Model model, HttpSession httpSession) throws ServiceCommonException {

        NewsSearchCriteria criteria = (NewsSearchCriteria) httpSession.getAttribute(SEARCH_CRITERIA);
        Author authorSearch = null;
        List<Tag> tagsSearch = null;
        if (criteria != null) {
            authorSearch = criteria.getAuthor();
            tagsSearch = criteria.getTagList();
        }
        model.addAttribute(NEWS, newsMainService.getAllNewsById(newsId, authorSearch, tagsSearch));

        return "single-news";
    }


    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse postComment(@Validated @RequestBody final Comments comment)
            throws ServiceCommonException {
        JsonResponse res = new JsonResponse();
        Date date = new Date();
        long time = date.getTime();
        comment.setCreationDate(new Timestamp(time));
        newsMainService.createComment(comment);
        res.setResult(newsMainService.getAllCommentsByNews(comment.getNewsId()));
        return res;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public List<Comments> removeComment(@RequestBody final Comments comment) throws ServiceCommonException {
        newsMainService.deleteComment(comment);
        return newsMainService.getAllCommentsByNews(comment.getNewsId());
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public JsonResponse handleException(MethodArgumentNotValidException ex,Locale locale) {
        JsonResponse res = new JsonResponse();
        String message=null;
        for (Object object : ex.getBindingResult().getAllErrors()) {
            if (object instanceof FieldError) {
                FieldError fieldError = (FieldError) object;
                message = messageSource.getMessage(fieldError, locale);
            }
        }
        res.setMsgError(message);
        return res;
    }
}
