package com.epam.gerasimovich.newsmanagement.controller;


import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsmanagement.utils.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Timestamp;
import java.util.*;

/**
 * @author Tatiana
 * @version 1.00 11/19/2015
 */
@Controller
@RequestMapping("/edit-authors")
public class EditAuthorsController {
    private static final String AUTHORS_LIST = "authorList";
    private static final String NEW_AUTHOR = "newAuthor";


    @Autowired
    private IMainService newsMainService;
    @Autowired
    private Validator authorValidator;
    @Autowired
    private MessageSource messageSource;

    @InitBinder("author")
    private void initAuthorBinder(WebDataBinder binder){binder.setValidator(authorValidator);}
    @InitBinder("newAuthor")
    private void initNewAuthorBinder(WebDataBinder binder){binder.setValidator(authorValidator);}

    @RequestMapping()
    public String getRootTagsPage() throws ServiceCommonException {
        return "redirect:/edit-authors/allAuthors";
    }

    @RequestMapping(value = "/allAuthors",method = RequestMethod.GET)
    public String getEditPage(Model model) throws ServiceCommonException {
        List<Author> authors=newsMainService.getAllAuthor();
        Iterator<Author> it = authors.iterator();
        while (it.hasNext()) {
            Author author = it.next();
            if (author.getExpired()!=null) {
                it.remove();
            }
        }
        model.addAttribute(AUTHORS_LIST ,authors);
        if(!model.containsAttribute(NEW_AUTHOR)) {
            model.addAttribute(NEW_AUTHOR,new Author());
        }
        return "edit-authors";
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse updateTag(@RequestBody @Validated final Author author) throws ServiceCommonException {
        JsonResponse res = new JsonResponse();
        newsMainService.updateAuthor(author);
        res.setResult(author.getAuthorName());
        return res;
    }

    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse deleteTag(@RequestBody final Author author) throws ServiceCommonException{
        JsonResponse res = new JsonResponse();
        List <Author> authors=new ArrayList<>();
        Date date=new Date();
        author.setExpired(new Timestamp(date.getTime()));
        authors.add(author);
        newsMainService.deleteAuthors(authors);

        List<Author> authorsM=newsMainService.getAllAuthor();
        Iterator<Author> it = authorsM.iterator();
        while (it.hasNext()) {
            Author authorD = it.next();
            if (authorD.getExpired()!=null) {
                it.remove();
            }
        }
        res.setResult(authorsM);
        return res;
    }

    @RequestMapping(value = "/allAuthors",method = RequestMethod.POST,produces = "text/plain;charset=UTF-8")
    public String addTag(@ModelAttribute(value = NEW_AUTHOR)@Validated final Author author,
                         final BindingResult br,
                         RedirectAttributes attr) throws ServiceCommonException{
        if(br.hasErrors()){
            attr.addFlashAttribute("org.springframework.validation.BindingResult.newAuthor",br);
            attr.addFlashAttribute(NEW_AUTHOR,author);
            return "redirect:/edit-authors/allAuthors";
        }
        newsMainService.createAuthor(author);
        return "redirect:/edit-authors/allAuthors";
    }
    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public JsonResponse handleException(MethodArgumentNotValidException ex,Locale locale) {
        JsonResponse res = new JsonResponse();
        String message=null;
        for (Object object : ex.getBindingResult().getAllErrors()) {
            if (object instanceof FieldError) {
                FieldError fieldError = (FieldError) object;
                message = messageSource.getMessage(fieldError, locale);
            }
        }
        res.setMsgError(message);
        return res;
    }
}
