package com.epam.gerasimovich.newsmanagement.utils;

import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;

/**
 * @author Tatiana
 * @version 1.00 11/17/2015
 */
public class ControllerUtils {
    public static String constructUrl(NewsSearchCriteria criteria){
        StringBuilder url = new StringBuilder();
        String delimiter = "";
        String startDelimeter= "?";
        if(criteria!=null) {
            url.append(startDelimeter);
            if (criteria.getAuthor() != null) {
                url.append(delimiter).append("authorId=").append(criteria.getAuthor().getAuthorId());
            }
            for (Tag tag : criteria.getTagList()) {
                delimiter = "&";
                url.append(delimiter).append("tagId=").append(tag.getTagId());
            }
        }

        return url.toString();
    }
}
