package com.epam.gerasimovich.newsmanagement.controller;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsPortal.valueObject.ModelNews;
import com.epam.gerasimovich.newsmanagement.controller.validator.NewsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Tatiana
 * @version 1.00 11/24/2015
 */
@Controller
public class EditNewsController {

    private static final String AUTHORS = "authors";
    private static final String TAG_LIST = "tagList";
    private static final String NEWS = "news";

    @Autowired
    private IMainService newsMainService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private NewsValidator newsValidator;

    @InitBinder("news")
    private void initTagBinder(WebDataBinder binder, Locale locale) {
        binder.setValidator(newsValidator);
        SimpleDateFormat dateFormat = new SimpleDateFormat(messageSource.getMessage("date.format", null, locale));
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
                dateFormat, true));
    }

    @RequestMapping(value = "/news/add-news", method = RequestMethod.GET)
    public String addNews(Model model) throws ServiceCommonException {
        java.util.Date utilDate = new java.util.Date();
        java.sql.Timestamp sq = new java.sql.Timestamp(utilDate.getTime());
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        News newsCommon = new News();
        newsCommon.setCreationDate(sq);
        newsCommon.setModificationDate(sqlDate);
        ModelNews news = new ModelNews();
        news.setNews(newsCommon);
        model.addAttribute(TAG_LIST, newsMainService.getAllTags());
        List<Author> authors = newsMainService.getAllAuthor();
        Iterator<Author> it = authors.iterator();
        while (it.hasNext()) {
            Author author = it.next();
            if (author.getExpired() != null) {
                it.remove();
            }
        }
        model.addAttribute(AUTHORS, authors);
        if (!model.containsAttribute(NEWS)) {
            model.addAttribute(NEWS, news);
        }
        return "edit-news";
    }

    @RequestMapping(value = "/edit-news/{id}", method = RequestMethod.GET)
    public String editNews(@PathVariable("id") Integer newsId,
                           Model model) throws ServiceCommonException {
        ModelNews news = new ModelNews();
        news.setNews(newsMainService.getAllNewsById(newsId, null, null).getNews());
        model.addAttribute(TAG_LIST, newsMainService.getAllTags());
        List<Author> authors = newsMainService.getAllAuthor();
        Iterator<Author> it = authors.iterator();
        while (it.hasNext()) {
            Author author = it.next();
            if (author.getExpired() != null) {
                it.remove();
            }
        }
        model.addAttribute(AUTHORS, authors);
        news.setAuthorId(newsMainService.getAuthorByNewsId(newsId).getAuthorId());
        List<Tag> tagList = newsMainService.getTagsByNewsId(newsId);
        Long[] tagIds = new Long[tagList.size()];
        int i = 0;
        for (Tag tag : tagList) {
            tagIds[i++] = tag.getTagId();
        }

        news.setTagIds(tagIds);
        if (!model.containsAttribute(NEWS)) {
            model.addAttribute(NEWS, news);
        }
        return "edit-news";
    }

    @RequestMapping(value = {"/edit-news/{id}", "/news/add-news"}, method = RequestMethod.POST)
    public String addNews(@ModelAttribute("news") @Validated final ModelNews news,
                          final BindingResult br, RedirectAttributes attr) throws ServiceCommonException {
        if (br.hasErrors()) {
            attr.addFlashAttribute("news", news);
            attr.addFlashAttribute("org.springframework.validation.BindingResult.news", br);
            if (news.getNews().getNewsId() == null) {
                return "redirect:/news/add-news";
            } else {
                return "redirect:/edit-news/" + news.getNews().getNewsId();
            }
        }

        List<Tag> tags = null;

        if (news.getTagIds() != null) {
            Tag tag;
            tags = new ArrayList<Tag>();
            for (Long tagId : news.getTagIds()) {
                tag = new Tag();
                tag.setTagId(tagId);
                tags.add(tag);
            }
        }
        Author author = new Author(news.getAuthorId(), null, null);
        if (news.getNews().getNewsId() == null) {
            Long newsId = newsMainService.create(news.getNews(), author, tags);
            news.getNews().setNewsId(newsId);
        } else {
            newsMainService.update(news.getNews(), author, tags);

        }
        return "redirect:/news/" + news.getNews().getNewsId();
    }
}
