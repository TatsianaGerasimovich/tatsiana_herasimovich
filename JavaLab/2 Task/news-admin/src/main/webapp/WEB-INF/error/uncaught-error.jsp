<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 11/1/2015
  Time: 9:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link href="../../resources/css/styleError.css" rel="stylesheet" type="text/css"  media="all" />

  <title>Exception</title>
</head>
<body>
<!--start-wrap--->
<div class="wrap">
  <!---start-header---->
  <div class="header">
    <div class="logo">
      <h1><a href="#">Ohh</a></h1>
    </div>
  </div>
  <!---End-header---->
  <!--start-content------>
  <div class="content">
    <p><span><label>O</label>hh error.....</span>You Requested resulted in an error</p>
    <a href="/NewsPortal/Admin/">Back To Home</a>
    <div class="copy-right">
      <p>Copyright &#169 Epam 2015 | All rights reserved</p>
    </div>
  </div>
  <!--End-Cotent------>
</div>
<!--End-wrap--->

  <script>
    setTimeout(function () {
      document.location.replace("/NewsPortal/Admin/");
    }, 4000);
  </script>
</body>
</html>

