<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="locale"%>

<div id="main">
     <div id="author_form">
        <sf:form modelAttribute="newTag" method="post" id="add">
            <div class="form_row">
                <p><sf:errors path="tagName" class="error"/></p>
                <label><strong><locale:message code="title.add"/><br><locale:message code="news.tag"/></strong></label>
                <sf:input path="tagName" type="text"/>

                <div class="button_author" onclick="submitForm('add')"><locale:message code="button.save"/></div>
            </div>
        </sf:form>
        <br>
        <div id="edit">
        <c:forEach var="tag" items="${tagList}">

                <div class="form_row">
                    <label><strong><locale:message code="news.tag"/></strong></label>
                    <div id="error${tag.tagId}" class="error"></div>
                    <input type="text" id="${tag.tagId}" value="<c:out value="${tag.tagName}"/>" disabled="disabled"/>
                    <div class="button_author" id="edit-author-btn${tag.tagId}" onclick="editTag(${tag.tagId}, this)"><locale:message code="news.edit"/></div>
                </div>

        </c:forEach>
        </div>
    </div>
</div>
