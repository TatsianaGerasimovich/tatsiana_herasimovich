<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 11/13/2015
  Time: 2:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="locale"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="left-sidebar">

  <div id="header_left">

    <div id="site_title">
      <h1><a href="<c:url value="/index"/>" target="_parent"><locale:message code="sidebar.news"/>
        <strong><locale:message code="sidebar.portal"/></strong><span><locale:message code="sidebar.administration"/></span></a>
      </h1>
    </div>
    <!-- end of site_title -->

  </div>
  <!-- end of left header -->
<c:if test="${pageContext.request.userPrincipal.name != null}">
  <div id="sidebar">
    <ul class="list">
      <li><a href=" <c:url value="/page/${currentPage}"/>" target="_parent"><locale:message code="menu.newslist"/></a></li>
        <li><a href="<c:url value="/news/add-news"/>" target="_parent"><locale:message code="menu.addnews"/></a></li>
        <li><a href="<c:url value="/edit-authors"/>" target="_parent"><locale:message code="menu.editauthors"/></a></li>
      <li><a href="<c:url value="/edit-tags"/>" target="_parent"><locale:message code="menu.edittags"/></a></li>
    </ul>
  </div>
  <!-- end of sidebar -->
</c:if>

</div>
<!-- end of left_column -->
