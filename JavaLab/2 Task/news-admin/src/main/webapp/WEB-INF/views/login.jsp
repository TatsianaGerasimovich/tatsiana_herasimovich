<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="main">
      <div class="login">
        <h2><locale:message code="login.title"/></h2>

        <c:if test="${not empty message}">
          <div class="error">${message}</div>
        </c:if>

   <%--     <form method="POST" action="<c:url value='/NewsPortal/Admin/j_spring_security_check' />">--%>
        <form method="POST" name="f" action="/NewsPortal/Admin/j_spring_security_check">
          <p><input type="text" name="j_username" value="" placeholder="<locale:message code="login"/>"></p>
          <p><input type="password" name="j_password" value="" placeholder="<locale:message code="password"/>"></p>
          <p class="submit"><input type="submit"  value="<locale:message code="button.login"/>"></p>
        </form>
      </div>
    </div>

