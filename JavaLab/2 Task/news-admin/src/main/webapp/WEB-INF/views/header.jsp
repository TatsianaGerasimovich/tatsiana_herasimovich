<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 11/11/2015
  Time: 2:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="locale"%>
<div id="logout">
  <c:url value="/j_spring_security_logout" var="logoutUrl" />

  <ul>
    <li><a href="?lang=ru">RU</a></li>
    <li><a href="?lang=en">EN</a></li>
    <c:if test="${pageContext.request.userPrincipal.name != null}">
 <li><span class="name"><locale:message code="author.hello"/>,<c:out value="${pageContext.request.userPrincipal.name}"/> </span> <a href="${logoutUrl}"><locale:message code="button.logout"/></a></li>
    </c:if>
  </ul>

</div>