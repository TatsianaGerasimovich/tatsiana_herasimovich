<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="locale"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="main">
    <c:set var="news" value="${news}"/>
    <c:choose>
        <c:when test="${empty news.preNewsId||news.preNewsId==0}">
            <div class="button_pre" style="display:none"><a
                    href="<c:url value="/news/${news.preNewsId}"/>"><locale:message code="button.prev"/></a></div>
        </c:when>
        <c:when test="${not empty news.preNewsId||news.preNewsId!=0}">
            <div class="button_pre"><a href="<c:url value="/news/${news.preNewsId}"/>"><locale:message code="button.prev"/></a>
            </div>
        </c:when>
    </c:choose>
    <c:choose>
        <c:when test="${empty news.nextNewsId || news.nextNewsId==0}">
            <div class="button_next" style="display:none"><a
                    href="<c:url value="/news/${news.nextNewsId}"/>"><locale:message code="button.next"/></a></div>
        </c:when>
        <c:when test="${not empty news.nextNewsId || news.nextNewsId!=0}">
            <div class="button_next"><a href="<c:url value="/news/${news.nextNewsId}"/>"><locale:message code="button.next"/></a></div>
        </c:when>
    </c:choose>

    <div class="post_section">
        <span class="comment">${fn:length(news.commentList)}</span>

        <h2><c:out value="${news.news.title}"/></h2>

        <fmt:formatDate type="date" value="${news.news.creationDate}"/> |<locale:message code="news.author"/></strong><c:out value="${news.author.authorName}"/>  | <strong><locale:message code="news.tags"/></strong>
        <c:forEach var="tag" items="${news.tagList}">
            <a href="#"><c:out value="${tag.tagName}"/> </a>
        </c:forEach>
        <p><c:out value="${news.news.fullText}"/></p>

    </div>
    <div class="comment_tab">
        <locale:message code="view.comments"/>
    </div>
    <div id="comment_section">
        <ol class="comments first_level">

            <c:forEach var="comment" items="${news.commentList}">
                <li>
                    <div class="comment_box commentbox">
                        <div class="comment_text">
                            <div class="delete" onclick="deleteComment(${comment.commentId},${news.news.newsId})"><a href="javascript:void(0)" onclick="deleteComment(${comment.commentId},${news.news.newsId})"><img src="../resources/images/cross.png" alt="Delete"/></a></div>
                            <div class="comment_author"><span class="date"><fmt:formatDate type="date"
                                                                                           value="${comment.creationDate}"/></span>
                                        <span class="time"><fmt:formatDate type="time"
                                                                           value="${comment.creationDate}"/></span>
                            </div>
                            <p><c:out value="${comment.commentText}"/></p>
                            <%--<p><c:out value="${fn:escapeXml(comment.commentText)}"/></p>--%>
                        </div>
                    </div>

                </li>
            </c:forEach>

        </ol>
    </div>

    <div id="comment_form">
        <h3><locale:message code="button.postComment"/></h3>

        <div class="form">
            <div class="form_row">
                <label><strong><locale:message code="view.comment"/></strong></label>
                <br/>
                <div id="error" class="error"></div>
                <textarea id="comment" name="comment" rows="" value="" cols=""></textarea>
            </div>
            <input type="button" value="<locale:message code="button.postComment"/>"
                   onclick="postComment(${news.news.newsId})" class="submit_btn"/>
        </div>
    </div>
</div>

