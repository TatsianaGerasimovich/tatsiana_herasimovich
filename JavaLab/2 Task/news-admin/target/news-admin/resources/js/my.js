var state;
var contexPath = "/NewsPortal/Admin";
function editTag(id, btn) {
    state = $('#' + id).val();

    //adding buttons
    var updateBtn = '<div class="button_author" onclick="updateTag(' + id + ')" id="update">Update</div>';
    var deleteBtn = '<div class="button_author" onclick="deleteTag(' + id + ')" id="delete">Delete</div>';
    var cancelBtn = '<div class="button_author" onclick="cancelChanging(' + id + ')" id="cancel">Cancel</div>';
    $(".button_author").hide();
    $('#' + id).after(cancelBtn);
    $('#' + id).after(deleteBtn);
    $('#' + id).after(updateBtn);
    $(btn).hide();
    document.getElementById(id).removeAttribute('disabled');

}

function editAuthor(id, btn) {
    state = $('#' + id).val();

    //adding buttons
    var updateBtn = '<div class="button_author" onclick="updateAuthor(' + id + ')" id="update">Update</div>';
    var deleteBtn = '<div class="button_author" onclick="deleteAuthor(' + id + ')" id="delete">Delete</div>';
    var cancelBtn = '<div class="button_author" onclick="cancelChanging(' + id + ')" id="cancel">Cancel</div>';
    $(".button_author").hide();
    $('#' + id).after(cancelBtn);
    $('#' + id).after(deleteBtn);
    $('#' + id).after(updateBtn);
    $(btn).hide();
    document.getElementById(id).removeAttribute('disabled');

}

function cancelChanging(id) {
    //
    $('#' + id).val(state);
    //
    $('#update').remove();
    $('#delete').remove();
    $('#cancel').remove();
    $('#edit-author-btn' + id).show();
    $('.button_author').show();
    document.getElementById(id).setAttribute('disabled', 'disabled');

}
function updateTag(id) {
    update(id, "/edit-tags/update");

}
function updateAuthor(id) {
    update(id, "/edit-authors/update");

}
function update(id, url) {
    var data;
    var text = $("#" + id).val();
    if (url == "/edit-tags/update") {
        data = {tagId: id, tagName: text};
    } else {
        data = {authorId: id, authorName: text};
    }

    $.ajax({
        url: contexPath + url,
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        mimeType: 'application/json; charset=utf-8',

        success: function (data) {
            $('#' + id).val(data.result);
            //
            $('#update').remove();
            $('#delete').remove();
            $('#cancel').remove();
            $('#edit-author-btn' + id).show();
            $('.button_author').show();
            document.getElementById(id).setAttribute('disabled', 'disabled');
            var htmlText = " <div id=\"error" + id + "\" class=\"error\"></div>";
            $("#error" + id).replaceWith(htmlText);

        },
        error: function (data, status, er) {
            var exceptionVO = jQuery.parseJSON(data.responseText);
            var htmlText = " <div id=\"error" + id + "\" class=\"error\">" + exceptionVO.msgError +
                "</div>";
            $("#error" + id).replaceWith(htmlText);
        }
    });
}

function deleteTag(id) {
    deleteEn(id, "/edit-tags/delete");
}
function deleteAuthor(id) {
    deleteEn(id, "/edit-authors/delete");
}

function deleteEn(id,url) {
    var data;
    if(url=="/edit-tags/delete"){
        data={tagId: id};
    } else{
        data={authorId: id};
    }

    $.ajax({
        url: contexPath+url,
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        mimeType: 'application/json; charset=utf-8',

        success: function (data) {

            var htmlText = "<div id=\"edit\">";
            jQuery.each(data.result, function (i, val) {
                var id;
                var name;
                htmlText = htmlText +"<div class=\"form_row\">";
                if(url=="/edit-tags/delete"){
                    htmlText = htmlText +"<label><strong>Tag:</strong></label>";
                    id=val.tagId;
                    name=val.tagName;
                } else{
                    htmlText = htmlText +"<label><strong>Author:</strong></label>";
                    id=val.authorId;
                    name=val.authorName;
                }
                htmlText = htmlText +
                    " <div id=\"error"+id+"\" class=\"error\"></div>" +
                    "<input type=\"text\" id=\""+id+"\" value=\""+name.escape()+"\" disabled=\"disabled\"/>" +
                    "<div class=\"button_author\" id=\"edit-author-btn"+id+"\" onclick=\"editTag("+id+", this)\">Edit</div>"+
                "</div>";
            });
            htmlText = htmlText + "</div>";
            $("#edit").replaceWith(htmlText);
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er:" + er);
        }
    });
}

var display = 'block';

function showTagList() {
    var tags = $('#checkboxes');
    var display = tags.css('display');
    if (display === 'none')
        display = 'block';
    else
        display = 'none';
    tags.css('display', display);
}

var defaultText = 'Select tags...';

function setText() {
    var tagList = [];
    var checkBoxes = $('#checkboxes');
    var tags = checkBoxes.find('label');
    $.each(tags, function (key, value) {
        var input = $(this).find('input[type=checkbox]');
        var el = $(this);
        if (input.prop('checked') === true) {
            tagList.push(el.text().trim());
        }
    });
    var tagsSelector = checkBoxes.parent().find('option');
    var defText = defaultText;
    if (tagList.length > 0) {
        if (tagList.length < 3) {
            defText = tagList.join();
        } else {
            var sel = ' selected';
            if (defText.charAt(0) == 'В')
                sel = ' выбрано';
            defText = tagList.length + sel;
        }
    }
    tagsSelector.text(defText);
}

function setTags() {
    var tagList = $('#checkboxes').find('input[type=checkbox]');
    $.each(tagList, function (k, v) {
        var el = $(this);
        $.each(selectedTags, function (key2, value2) {
            if (parseInt(el.val()) === value2) {
                el.prop('checked', true);
                //el.prop('name','tagList['+ key2 +'].id');
            }
        });
    })
}
$(document).ready(function () {
    var tagBoxes = $('#checkboxes').find('label');
    $.each(tagBoxes, function (key, value) {
        $(this).attr('onclick', 'setText()');
    });
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() != 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });

        $('#toTop').click(function () {
            $('body,html').animate({scrollTop: 0}, 800);
        });
    });
    setText();
});
var selectedTags = [];
function submitForm(formId) {
    document.getElementById(formId).submit();
}
function resetForm(formId) {
    var checkBoxes = $('#checkboxes');
    var tags = checkBoxes.find('label');
    $.each(tags, function (key, value) {
        var input = $(this).find('input[type=checkbox]');
        if (input.prop('checked') === true) {
            input.prop('checked', false);
        }
    });
    $("#selectAuthor :selected").prop('selected', false);
    document.getElementById(formId).submit();
}


function pagination(page) {

    document.location.href = page;
}
String.prototype.escape = function() {
    var tagsToReplace = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;'
    };
    return this.replace(/[&<>]/g, function(tag) {
        return tagsToReplace[tag] || tag;
    });
};
function postComment(newsId) {
    var commentText = $("#comment").val();
    var comment;
    if (commentText.length > 0) {
        comment = {commentText: commentText, newsId: newsId};
    } else {
        comment = {newsId: newsId};
    }
    $.ajax({
        url: contexPath + "/news/comment",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(comment),
        contentType: 'application/json; charset=utf-8',
        mimeType: 'application/json; charset=utf-8',

        success: function (data) {
            var htmlText = "<div id=\"comment_section\">" +
                "<ol class=\"comments first_level\">";
            jQuery.each(data.result, function (i, val) {
                var date = new Date();
                date.setTime(val.creationDate);
                var month = date.getMonth() + 1;
                var day = date.getDate();
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var seconds = date.getSeconds();
                month = (month < 10 ? "0" : "") + month;
                day = (day < 10 ? "0" : "") + day;
                htmlText = htmlText +
                    " <li>" +
                    "<div class=\"comment_box commentbox\">" +
                    "<div class=\"comment_text\">" +
                    "<div class=\"delete\" onclick=\"deleteComment(" + val.commentId + "," + val.newsId + ")\"><a href=\"javascript:void(0)\" onclick=\"deleteComment(" + val.commentId + "," + val.newsId + ")\"><img src=\"../resources/images/cross.png\" alt=\"Delete\"/></a></div>" +
                    "<div class=\"comment_author\">" +
                    "<span class=\"date\">" +
                    day + "." + month + "." + date.getUTCFullYear() +
                    "</span>" +
                    "<span class=\"time\">" +
                    hours + ":" + minutes + ":" + seconds +
                    "</span></div>" +
                    "<p>" + val.commentText.escape() + "</p>" +
                    " </div>" +
                    "</div>" +
                    " </li>";
            });
            htmlText = htmlText + "</ol>" + "</div>";

            $("#comment_section").replaceWith(htmlText);
            var htmlError = " <div id=\"error\" class=\"error\"></div>";
            $("#error").replaceWith(htmlError);
            $("#comment").val("");
        },
        error: function (data, status, er) {
            var exceptionVO = jQuery.parseJSON(data.responseText);
            var htmlText = " <div id=\"error\" class=\"error\">" + exceptionVO.msgError +
                "</div>";
            $("#error").replaceWith(htmlText);
            $("#comment").val("");
        }
    });

}
function deleteComment(commentId, newsId) {
    var comment = {commentId: commentId, newsId: newsId};

    $.ajax({
        url: contexPath + "/news/delete",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(comment),
        contentType: 'application/json; charset=utf-8',
        mimeType: 'application/json; charset=utf-8',

        success: function (data) {
            var htmlText = "<div id=\"comment_section\">" +
                "<ol class=\"comments first_level\">";
            jQuery.each(data, function (i, val) {
                var date = new Date();
                date.setTime(val.creationDate);
                var month = date.getMonth() + 1;
                var day = date.getDate();
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var seconds = date.getSeconds();
                month = (month < 10 ? "0" : "") + month;
                day = (day < 10 ? "0" : "") + day;
                htmlText = htmlText +
                    " <li>" +
                    "<div class=\"comment_box commentbox\">" +
                    "<div class=\"comment_text\">" +
                    "<div class=\"delete\" onclick=\"deleteComment(" + val.commentId + "," + val.newsId + ")\"><a href=\"javascript:void(0)\" onclick=\"deleteComment(" + val.commentId + "," + val.newsId + ")\"><img src=\"../resources/images/cross.png\" alt=\"Delete\"/></a></div>" +
                    "<div class=\"comment_author\">" +
                    "<span class=\"date\">" +
                    day + "." + month + "." + date.getUTCFullYear() +
                    "</span>" +
                    "<span class=\"time\">" +
                    hours + ":" + minutes + ":" + seconds +
                    "</span></div>" +
                    "<p>" + val.commentText.escape() + "</p>" +
                    " </div>" +
                    "</div>" +
                    " </li>";
            });
            htmlText = htmlText + "</ol>" + "</div>";

            $("#comment_section").replaceWith(htmlText);
            var htmlError = " <div id=\"error\" class=\"error\"></div>";
            $("#error").replaceWith(htmlError);
            $("#comment").val("");
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er:" + er);
        }
    });

}