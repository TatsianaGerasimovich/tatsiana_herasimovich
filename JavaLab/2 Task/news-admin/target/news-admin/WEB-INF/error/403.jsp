<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 12/1/2015
  Time: 11:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

  <link href="../../resources/css/styleError.css" rel="stylesheet" type="text/css"  media="all" />
</head>
<body>
<h1>HTTP Status 403 - Access is denied</h1>
<script>
  setTimeout(function () {
    document.location.replace("/NewsPortal/Admin/");
  }, 4000);
</script>
    <h2>You do not have permission to access this page!</h2>

</body>
</html>