<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="locale"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="main">
    <div class="filter">

        <select id="selectAuthor" class="select" name="authorId" form="Search">
            <option value="0"><locale:message code="filter.any"/></option>
            <c:forEach var="author" items="${authors}">
                <c:choose>
                    <c:when test="${author.authorId != authorSearch.authorId}">
                        <option value="${author.authorId}">${author.authorName}</option>
                    </c:when>
                    <c:when test="${author.authorId == authorSearch.authorId}">
                        <option value="${author.authorId}" selected="selected">${author.authorName}</option>
                    </c:when>
                </c:choose>
            </c:forEach>
        </select>

        <div class="multiselect" style="height: 24px">
            <div class="select-box" style="height: 24px" onclick="showTagList()">
                <select name="tag">
                    <option><locale:message code="select.title"/></option>
                </select>

                <div id="checkboxes" class="tag-list">
                    <input type="checkbox" name="tagId"
                           value="0" style="display:none" checked="checked" form="Search">

                    <c:forEach var="tag" items="${tagList}">

                        <c:set var="contains" value="false"/>
                        <c:forEach var="tagSearch" items="${tagsSearch}">
                            <c:if test="${tag.tagId == tagSearch.tagId}">
                                <c:set var="contains" value="true"/>
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains eq 'true'}">
                                <label><input type="checkbox" name="tagId"
                                              value="${tag.tagId}" checked="checked" form="Search"><c:out
                                        value="${tag.tagName}"/></label>
                            </c:when>
                            <c:when test="${contains eq 'false'}">
                                <label><input type="checkbox" form="Search" name="tagId"
                                              value="${tag.tagId}"><c:out value="${tag.tagName}"/></label>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                </div>
            </div>
        </div>


        <div class="button_filter" onclick="resetForm('Search')"><a  href="javascript:void(0)" onclick="resetForm('Search')"><locale:message code="reset"/></a></div>

        <form id="Search" method="POST" action="./">
        </form>

        <div class="button_filter" onclick="submitForm('Search')"><a href="javascript:void(0)" onclick="submitForm('Search')"><locale:message code="filter.button"/></a></div>
    </div>
    <div id="content">
        <form method="POST" id="Delete" action="delete">
            <c:choose>
                <c:when test="${not empty newsList}">

                    <c:forEach var="news" items="${newsList}">
                        <div class="post_section">

                    <span class="comment">
                        <a href="<c:url value="/news/${news.news.newsId}"/>"><c:out value="${news.countOfComments}"/> </a></span>

                            <h2><a href="<c:url value="/news/${news.news.newsId}"/>"><c:out value="${news.news.title}"/> </a>
                            </h2>
                            <fmt:formatDate type="date" value="${news.news.creationDate}"/> |
                            <strong><locale:message code="news.author"/></strong> <c:out value="${news.author.authorName}"/> | <strong><locale:message code="news.tags"/></strong>
                            <c:forEach var="tag" items="${news.tagList}">
                                <a href="#"><c:out value="${tag.tagName}"/> </a>
                            </c:forEach>

                            <p><c:out value="${news.news.shortText}"/></p>

                            <div class="delete"><input type="checkbox" name="newsId" value="${news.news.newsId}"/></div>
                            <div class="delete"><a href="<c:url value="/edit-news/${news.news.newsId}"/>"><locale:message code="news.edit"/></a></div>
                            <a href="<c:url value="/news/${news.news.newsId}"/>"><locale:message code="news.read"/></a>
                        </div>
                    </c:forEach>

                    <div class="button_filter" onclick="submitForm('Delete')"><a href="javascript:void(0)" onclick="submitForm('Delete')"><locale:message code="news.delete"/></a></div>
                </c:when>
                <c:when test="${empty newsList}">
                    <div class="post_section">
                        <h2><a href="#"><locale:message code="filter.result"/></a></h2>
                    </div>
                </c:when>
            </c:choose>
        </form>
        <%-- The paging buttons --%>
        <c:set var="firstPage" value="${firstPage}"/>
        <c:choose>
            <c:when test="${currentPage <= 1}">
                <button type="button" disabled><locale:message code="button.first"/></button>
                <button type="button" disabled><locale:message code="button.prev"/></button>
            </c:when>
            <c:when test="${currentPage > 1}">
                <button onclick="location.href='<c:url value="/page/${firstPage}"/>'"><locale:message code="button.first"/> (${firstPage})</button>
                <button onclick="location.href='<c:url value="/page/${currentPage-1}"/>'"><locale:message code="button.prev"/> (${currentPage-1})</button>
            </c:when>
        </c:choose>
        <c:choose>
            <c:when test="${currentPage >= totalPages}">
                <button disabled><locale:message code="button.next"/>${currentPage+1}</button>
                <button disabled>Last${totalPages}</button>
            </c:when>
            <c:when test="${currentPage < totalPages}">
                <button onclick="location.href='<c:url value="/page/${currentPage+1}"/>'"><locale:message code="button.next"/>(${currentPage+1})</button>
                <button onclick="location.href='<c:url value="/page/${totalPages}"/>'"><locale:message code="button.last"/> (${totalPages})</button>
            </c:when>
        </c:choose>

        <c:out value="${currentPage}"/>/<c:out value="${totalPages}"/>

        <%-- The paging links --%>
        <select onchange="pagination(this.options[this.selectedIndex].value)">
            <c:forEach begin="${firstPage}" end="${totalPages}" var="page">
                <c:choose>
                    <c:when test="${page == currentPage}">
                        <option value="${page}" selected="selected"><c:out value="${page}"/></option>
                    </c:when>
                    <c:when test="${page!= currentPage}">
                        <option value="${page}">${page} </option>
                    </c:when>
                </c:choose>
            </c:forEach>
        </select>
    </div>

</div>