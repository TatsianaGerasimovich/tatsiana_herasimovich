<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 11/2/2015
  Time: 9:53 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>News portal</title>
    <link href="../resources/css/style.css" rel="stylesheet" type="text/css"/>
    <!-- JavaScripts-->
    <script type="text/javascript" src="../resources/js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="../resources/js/my.js"></script>
</head>
<body>
<div id="wrapper">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="sidebar"/>
    <div id="right_column">
        <tiles:insertAttribute name="content"/>
    </div>
    <!-- Footer Page -->
    <tiles:insertAttribute name="footer"/>
</div>
<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>
</body>
</html>
