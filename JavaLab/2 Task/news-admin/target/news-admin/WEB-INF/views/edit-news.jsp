<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="locale"%>

<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 11/20/2015
  Time: 9:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="main">

  <div id="author_form">
     <sf:form modelAttribute="news" id="edit">
        <div class="form">
         <table>

             <tr>
                 <td>
                     <label><strong>Title:</strong></label></td>
                 <td>
                     <p> <sf:errors path="news.title" class="error"/></p>
                     <sf:input path="news.title" type="text"/>
                     </td>
             </tr>

             <tr>

                 <td><label><strong>Date:</strong></label></td>

                 <td>
                   <p> <sf:errors path="news.modificationDate" class="error"/></p>
                     <c:choose>
                         <c:when test="${empty news.news.newsId}">

                             <fmt:formatDate value="${news.news.modificationDate}" var="dateString" pattern="yyyy-MM-dd" />

                             <sf:input path="news.modificationDate" value = "${dateString}" size="30" type="date" readonly="true"/>
                         </c:when>
                         <c:when test="${not empty news.news.newsId}">

                            <fmt:formatDate value="${news.news.modificationDate}" var="dateString" pattern="yyyy-MM-dd" />

                             <sf:input path="news.modificationDate" value = "${dateString}" size="30" type="date"/>
                         </c:when>
                     </c:choose>

                     <fmt:formatDate value="${news.news.creationDate}" var="creationDateString" pattern="yyyy-MM-dd hh:mm:ss" />

                     <sf:hidden path="news.creationDate" value = "${creationDateString}"/>
                     </td>
             </tr>
             <tr>
                 <td>
                     <label><strong>Brief:</strong></label></td>

                 <td>
                     <p><sf:errors path="news.shortText" class="error"/></p>
                     <sf:textarea path="news.shortText" rows="4" cols="50" />
                     </td>
             </tr>
             <tr>
                 <td>
                     <label><strong>Content:</strong></label></td>

                 <td>
                     <p><sf:errors path="news.fullText" class="error"/></p>
                     <sf:textarea path="news.fullText" rows="14" cols="50" cssClass="form-control"/>
                     </td>
             </tr>

         </table>
        </div>


      <div class="filter">
        <p><sf:errors path="authorId" class="error"/></p>

        <select id="selectAuthor" class="select" name="authorId">
          <c:forEach var="author" items="${authors}">
            <c:choose>
              <c:when test="${author.authorId != news.authorId}">
                <option value="${author.authorId}">${author.authorName}</option>
              </c:when>
              <c:when test="${author.authorId == news.authorId}">
                <option value="${author.authorId}" selected="selected">${author.authorName}</option>
              </c:when>
            </c:choose>
          </c:forEach>
        </select>

        <div class="multiselect" style="height: 24px">
          <div class="select-box" style="height: 24px" onclick="showTagList()">
            <select name="tag">
              <option>Select tags...</option>
            </select>

            <div id="checkboxes" class="tag-list">

              <c:forEach var="tag" items="${tagList}">

                <c:set var="contains" value="false"/>
                <c:forEach var="tagSearch" items="${news.tagIds}">
                  <c:if test="${tag.tagId == tagSearch}">
                    <c:set var="contains" value="true"/>
                  </c:if>
                </c:forEach>

                <c:choose>
                  <c:when test="${contains eq 'true'}">
                    <label><input type="checkbox" name="tagIds"
                                  value="${tag.tagId}" checked="checked"><c:out
                            value="${tag.tagName}"/></label>
                  </c:when>
                  <c:when test="${contains eq 'false'}">
                    <label><input type="checkbox"  name="tagIds"
                                  value="${tag.tagId}"><c:out value="${tag.tagName}"/></label>
                  </c:when>
                </c:choose>
              </c:forEach>
            </div>
          </div>
        </div>
</div>
      <sf:hidden path="news.newsId"/>

             <input type="button" value="Save"
              onclick="submitForm('edit')" class="submit_btn"/>

     </sf:form>
  </div>
</div>
