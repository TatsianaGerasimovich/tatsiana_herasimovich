<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="locale"%>

<div id="main">

    <div id="author_form">
        <sf:form modelAttribute="newAuthor" id="add">
            <div class="form_row">
                <p><sf:errors path="authorName" class="error"/></p>
                <label><strong><locale:message code="title.add"/><br><locale:message code="news.author"/></strong></label>
                <sf:input path="authorName" type="text"/>
                <div class="button_author" onclick="submitForm('add')"><locale:message code="button.save"/></div>
            </div>
        </sf:form>
        <br>
        <div id="edit">
        <c:forEach var="author" items="${authorList}">

                <div class="form_row">
                    <label><strong><locale:message code="news.author"/></strong></label>
                    <div id="error${author.authorId}" class="error"></div>
                    <input type="text" id="${author.authorId}" value="<c:out value="${author.authorName}"/>" disabled="disabled"/>

                    <div class="button_author" id="edit-author-btn${author.authorId}" onclick="editAuthor(${author.authorId}, this)"><locale:message code="news.edit"/></div>
                </div>
        </c:forEach>
        </div>
    </div>
</div>
