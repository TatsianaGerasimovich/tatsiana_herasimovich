<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 11/1/2015
  Time: 8:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>404</title>
  <link href="resources/css/styleError.css" rel="stylesheet" type="text/css"  media="all" />
  <script>
    setTimeout(function () {
      document.location.replace("/NewsPortal/Client/index.jsp");
    }, 4000);
  </script>
</head>
<body>
<!--start-wrap--->
<div class="wrap">
  <!---start-header---->
  <div class="header">
    <div class="logo">
      <h1><a href="#">Ohh</a></h1>
    </div>
  </div>
  <!---End-header---->
  <!--start-content------>
  <div class="content">
    <img src="resources/images/error-img.png" title="error" />
    <p><span><label>O</label>hh.....</span>You Requested the page that is no longer There.</p>
    <a href="/Client/index.jsp">Back To Home</a>
    <div class="copy-right">
      <p>Copyright &#169 Epam 2015 | All rights reserved</p>
    </div>
  </div>
  <!--End-Cotent------>
</div>
<!--End-wrap--->
</body>
</html>