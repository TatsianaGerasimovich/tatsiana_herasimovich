var state;
function editAuthor(id, btn) {
    state = $('#' + id).val();

    //adding buttons
    var updateBtn = '<div class="button_author" id="update">Update</div>';
    var deleteBtn = '<div class="button_author" id="delete">Delete</div>';
    var cancelBtn = '<div class="button_author" onclick="cancelChanging(' + id + ')" id="cancel">Cancel</div>';
    $(".button_author").hide();
    $('#' + id).after(cancelBtn);
    $('#' + id).after(deleteBtn);
    $('#' + id).after(updateBtn);
    $(btn).hide();
    document.getElementById(id).removeAttribute('disabled');

}

function cancelChanging(id) {
    //
    $('#' + id).val(state);
    //
    $('#update').remove();
    $('#delete').remove();
    $('#cancel').remove();
    $('#edit-author-btn' + id).show();
    $('.button_author').show();
    document.getElementById(id).setAttribute('disabled', 'disabled');

}

var display = 'block';

function showTagList() {
    var tags = $('#checkboxes');
    var display = tags.css('display');
    if (display === 'none')
        display = 'block';
    else
        display = 'none';
    tags.css('display', display);
}

$(document).ready(function () {
    var tagBoxes = $('#checkboxes').find('label');
    $.each(tagBoxes, function (key, value) {
        $(this).attr('onclick', 'setText()');
    });
    $(function() {
        $(window).scroll(function() {
            if($(this).scrollTop() != 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });

        $('#toTop').click(function() {
            $('body,html').animate({scrollTop:0},800);
        });
    });
    setText();
});

var defaultText = 'Select tags...';

function setText() {
    var tagList = [];
    var checkBoxes = $('#checkboxes');
    var tags = checkBoxes.find('label');
    $.each(tags, function (key, value) {
        var input = $(this).find('input[type=checkbox]');
        var el = $(this);
        if (input.prop('checked') === true) {
            tagList.push(el.text().trim());
        }
    });
    var tagsSelector = checkBoxes.parent().find('option');
    var defText = defaultText;
    if (tagList.length > 0) {
        if (tagList.length < 3) {
            defText = tagList.join();
        } else {
            var sel = ' selected';
            if (defText.charAt(0) == 'В')
                sel = ' выбрано';
            defText = tagList.length + sel;
        }
    }
    tagsSelector.text(defText);
}

var selectedTags = [];

function setTags() {
    var tagList = $('#checkboxes').find('input[type=checkbox]');
    $.each(tagList, function (k, v) {
        var el = $(this);
        $.each(selectedTags, function (key2, value2) {
            if (parseInt(el.val()) === value2) {
                el.prop('checked', true);
                //el.prop('name','tagList['+ key2 +'].id');
            }
        });
    })
}

function submitForm(formId) {
    document.getElementById(formId).submit();
}

function pagination(id) {
    var currentPage = {value: id};
    var request = new Object();
    request.command = 'GET_NEWS_PAGINATION';
    request.data = JSON.stringify(currentPage);
    $.ajax({
        url: "Client",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            var val;
            var htmlText = " <div id=\"content\">";
            jQuery.each(data.newsForList, function (i, val) {
                var date = new Date();
                date.setTime(val.news.creationDate);
                var month = date.getMonth() + 1;
                var day = date.getDate();
                month = (month < 10 ? "0" : "") + month;
                day = (day < 10 ? "0" : "") + day;
                htmlText = htmlText +
                    "<div class=\"post_section\">" +
                    "<span class=\"comment\">" +
                    "<a href=\"Client?command=SHOW_NEWS&newsId=" + val.news.newsId + "\">" + val.countOfComments + "</a></span>" +
                    "<h2><a href=\"Client?command=SHOW_NEWS&newsId=" + val.news.newsId + "\">" + val.news.title + "</a></h2>" + day + "." + month + "." + date.getUTCFullYear();
                if (data.locale == 'en_US') {
                    htmlText = htmlText + " | <strong>Author:</strong> " + val.author.authorName + "| <strong>Tags:</strong>";
                } else {
                    htmlText = htmlText + " | <strong>Автор:</strong> " + val.author.authorName + "| <strong>Теги:</strong>";
                }
                jQuery.each(val.tagList, function (j, tag) {
                    htmlText = htmlText + "<a href=\"#\">" +
                        tag.tagName + " </a>";
                });
                if (data.locale == 'en_US') {
                    htmlText = htmlText + "<p>" + val.news.shortText + "</p>" +
                        "<a href=\"Client?command=SHOW_NEWS&newsId=" + val.news.newsId + "\">Continue reading...</a>" +
                        "</div>";
                } else {
                    htmlText = htmlText + "<p>" + val.news.shortText + "</p>" +
                        "<a href=\"Client?command=SHOW_NEWS&newsId=" + val.news.newsId + "\">Читать далее...</a>" +
                        "</div>";
                }

            });
            var prev = data.currentPage - 1;
            if (data.currentPage <= 1) {
                if (data.locale == 'en_US') {
                    htmlText = htmlText + " <button type=\"button\" disabled>First</button>" +
                        " <button type=\"button\" disabled>Prev</button>";
                } else {
                    htmlText = htmlText + " <button type=\"button\" disabled>Первая</button>" +
                        " <button type=\"button\" disabled>Пред.</button>";
                }
            } else {
                if (data.locale == 'en_US') {
                    htmlText = htmlText + " <button onclick=\"pagination(" + data.firstPage + ")\">First (" + data.firstPage + ")</button>" +
                        " <button onclick=\"pagination(" + prev + ")\")>Prev " + prev + "</button>";
                } else {
                    htmlText = htmlText + " <button onclick=\"pagination(" + data.firstPage + ")\">Первая (" + data.firstPage + ")</button>" +
                        " <button onclick=\"pagination(" + prev + ")\")>Пред. " + prev + "</button>";
                }
            }
            var next = data.currentPage + 1;
            if (data.currentPage >= data.totalPages) {
                if (data.locale == 'en_US') {
                    htmlText = htmlText + " <button type=\"button\" disabled>Next</button>" +
                        " <button type=\"button\" disabled>Last</button>";
                } else {
                    htmlText = htmlText + " <button type=\"button\" disabled>След.</button>" +
                        " <button type=\"button\" disabled>Последняя</button>";
                }
            } else {
                if (data.locale == 'en_US') {
                    htmlText = htmlText + " <button onclick=\"pagination(" + next + ")\">Next (" + next + ")</button>" +
                        " <button onclick=\"pagination(" + data.totalPages + ")\")>Last (" + data.totalPages + ")</button>";
                } else {
                    htmlText = htmlText + " <button onclick=\"pagination(" + next + ")\">След. (" + next + ")</button>" +
                        " <button onclick=\"pagination(" + data.totalPages + ")\")>Последняя (" + data.totalPages + ")</button>";
                }
            }
            htmlText = htmlText +
                " " + data.currentPage + "/" + data.totalPages +
                " <select onchange=\"pagination(this.options[this.selectedIndex].value)\">";
            var firstPage = data.firstPage;
            var totalPages = data.totalPages;
            for (var i = firstPage; i <= totalPages; i++) {
                if (i == data.currentPage) {
                    htmlText = htmlText +
                        "<option value=\"" + i + "\" selected=\"selected\">" + i + "</option>";
                } else {
                    htmlText = htmlText +
                        "<option value=\"" + i + "\">" + i + "</option>";
                }
            }
            htmlText = htmlText +
                "</select>" +
                "</div>";


            $("#content").replaceWith(htmlText);
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er:" + er);
        }
    });

}
function postComment(newsId) {
    var commentText = $("#comment").val();
    var comment = {commentText: commentText, newsId: newsId};
    var request = new Object();
    request.command = 'POST_COMMENT';
    request.data = JSON.stringify(comment);
    $.ajax({
        url: "Client",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json; charset=utf-8',
        mimeType: 'application/json; charset=utf-8',

        success: function (data) {
            var htmlText = "<div id=\"comment_section\">" +
                "<ol class=\"comments first_level\">";
            jQuery.each(data, function (i, val) {
                var date = new Date();
                date.setTime(val.creationDate);
                var month = date.getMonth() + 1;
                var day = date.getDate();
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var seconds = date.getSeconds();
                month = (month < 10 ? "0" : "") + month;
                day = (day < 10 ? "0" : "") + day;
                htmlText = htmlText +
                    " <li>" +
                    "<div class=\"comment_box commentbox\">" +
                    "<div class=\"comment_text\">" +
                    "<div class=\"comment_author\">" +
                    "<span class=\"date\">" +
                    day + "." + month + "." + date.getUTCFullYear() +
                    "</span>" +
                    "<span class=\"time\">" +
                    hours + ":" + minutes + ":" + seconds +
                    "</span></div>" +
                    "<p>" + val.commentText + "</p>" +
                    " </div>" +
                    "</div>" +
                    " </li>";
            });
            htmlText = htmlText + "</ol>" + "</div>";

            $("#comment_section").replaceWith(htmlText);
            $("#comment").val("");
        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er:" + er);
        }
    });

}
function changeLanguage(locale) {
    var request = new Object();
    request.command = 'CHANGE_LANGUAGE';
    request.data = locale;
    $.ajax({
        url: "Client",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(request),
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            location.reload();

        },
        error: function (data, status, er) {
            alert("error: " + data + " status: " + status + " er:" + er);
        }
    });

}