<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 11/3/2015
  Time: 10:23 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>News portal</title>
    <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    <!-- JavaScripts-->
    <script type="text/javascript" src="resources/js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="resources/js/my.js"></script>
</head>
<body>

<div id="wrapper">


    <div id="logout">

        <ul>
            <li><a href="Client?command=GET_ALL_NEWS_COMMAND"><fmt:message key="button.back"/></a></li>
            <li><a href="javascript:changeLanguage('en_US')">EN</a></li>
            <li><a href="javascript:changeLanguage('ru_RU')">РУС</a></li>
        </ul>

    </div>
    <!-- end of logout -->

    <%@include file="sidebar.jsp" %>

    <div id="right_column">

        <div id="main">
            <c:set var="news" value="${news}"/>
            <c:choose>
                <c:when test="${news.preNewsId==0}">
                    <div class="button_pre" style="display:none"><a
                            href="Client?command=SHOW_NEWS&newsId=${news.preNewsId}"><fmt:message key="button.prev"/></a></div>
                </c:when>
                <c:when test="${news.preNewsId!=0}">
                    <div class="button_pre"><a href="Client?command=SHOW_NEWS&newsId=${news.preNewsId}"><fmt:message key="button.prev"/></a>
                    </div>
                </c:when>
            </c:choose>
            <c:choose>
                <c:when test="${news.nextNewsId==0}">
                    <div class="button_next" style="display:none"><a
                            href="Client?command=SHOW_NEWS&newsId=${news.nextNewsId}"><fmt:message key="button.next"/></a></div>
                </c:when>
                <c:when test="${news.nextNewsId!=0}">
                    <div class="button_next"><a href="Client?command=SHOW_NEWS&newsId=${news.nextNewsId}"><fmt:message key="button.next"/></a></div>
                </c:when>
            </c:choose>

            <div class="post_section">
                <span class="comment">${fn:length(news.commentList)}</span>

                <h2>${news.news.title}</h2>

                <fmt:formatDate type="date" value="${news.news.creationDate}"/> | <strong><fmt:message key="news.author"/></strong> ${news.author.authorName} | <strong><fmt:message key="news.tags"/></strong>
                <c:forEach var="tag" items="${news.tagList}">
                    <a href="#">${tag.tagName} </a>
                </c:forEach>
                <p>${news.news.fullText}</p>

            </div>
            <div class="comment_tab">
                <fmt:message key="view.comments"/>
            </div>
            <div id="comment_section">
                <ol class="comments first_level">

                    <c:forEach var="comment" items="${news.commentList}">
                        <li>
                            <div class="comment_box commentbox">
                                <div class="comment_text">
                                    <div class="comment_author"><span class="date"><fmt:formatDate type="date"
                                                                                                   value="${comment.creationDate}"/></span>
                                        <span class="time"><fmt:formatDate type="time"
                                                                           value="${comment.creationDate}"/></span>
                                    </div>
                                    <p>${comment.commentText}</p>
                                </div>
                            </div>

                        </li>
                    </c:forEach>

                </ol>
            </div>

            <div id="comment_form">
                <h3><fmt:message key="view.postComment"/></h3>

                <div class="form">
                    <div class="form_row">
                        <label><strong><fmt:message key="view.comment"/></strong></label>
                        <br/>
                        <textarea id="comment" name="comment" rows="" cols=""></textarea>
                    </div>
                    <input type="button" value="<fmt:message key="button.postComment"/>"
                           onclick="postComment(${news.news.newsId})" class="submit_btn"/>
                </div>
            </div>
        </div>

    </div>
    <!-- end of right column, main -->
    <%@include file="footer.jsp" %>
    <!-- end of footer -->
</div>
<!-- end of warpper -->
<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>
</body>
</html>
