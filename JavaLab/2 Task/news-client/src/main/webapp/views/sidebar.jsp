<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 11/11/2015
  Time: 9:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div id="left-sidebar">

  <div id="header_left">

    <div id="site_title">
      <h1><a href="Client?command=GET_ALL_NEWS_COMMAND" target="_parent">
        <fmt:message key="sidebar.news"/> <strong><fmt:message key="sidebar.portal"/></strong><span><fmt:message key="sidebar.welcome"/></span></a>
      </h1>
    </div>
    <!-- end of site_title -->

  </div>
  <!-- end of left header -->

</div>
<!-- end of left_column -->
</body>
</html>
