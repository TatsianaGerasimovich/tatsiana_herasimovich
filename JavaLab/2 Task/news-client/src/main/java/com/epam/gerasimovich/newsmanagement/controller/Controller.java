package com.epam.gerasimovich.newsmanagement.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import com.epam.gerasimovich.newsmanagement.controller.command.CommandHelper;
import com.epam.gerasimovich.newsmanagement.controller.command.ICommand;
import com.epam.gerasimovich.newsmanagement.controller.exception.CommandException;
import org.apache.log4j.Logger;

/**
 * @author Tatiana
 * @version 1.00 10/28/2015
 */
public class Controller extends HttpServlet {
    private static final String COMMAND_NAME = "command";
    private static final String AJAX_REQUEST = "XMLHttpRequest";
    private static final String REQUEST = "X-Requested-With";
    private static final Logger LOG = Logger.getLogger(Controller.class);
    private CommandHelper commandHelper = (CommandHelper) CommandHelper.getContext().getBean("commandHelper");

    public Controller() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String commandName;
        boolean isAjax = isAjax(request);
        if (isAjax)
            commandName = (String) request.getAttribute(COMMAND_NAME);
        else
            commandName = request.getParameter(COMMAND_NAME);

        ICommand command = commandHelper.getCommand(commandName);
        String page = null;
        try {
            page = command.execute(request, response);
            if (isAjax) {
                return;
            }
            RequestDispatcher rd = getServletContext().getRequestDispatcher(page);
            rd.forward(request, response);
        } catch (ServletException | IOException | CommandException ex) {
            LOG.error(ex);
        }
    }

    private boolean isAjax(HttpServletRequest request) {
        return AJAX_REQUEST.equals(request.getHeader(REQUEST));
    }
}
