package com.epam.gerasimovich.newsmanagement.controller.command.impl;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsmanagement.controller.command.ICommand;
import com.epam.gerasimovich.newsmanagement.controller.exception.CommandException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 11/5/2015
 */
public class GetNewsPaginationAjaxCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String JSON = "json";
    private static final String LOCALE = "locale";
    private static final String CURRENT_PAGE = "currentPage";
    private static final String AUTHOR_SEARCH = "authorSearch";
    private static final String TAGS_SEARCH = "tagsSearch";

    private final Integer rowsPerPage = 3;
    private IMainService newsMainService;

    public GetNewsPaginationAjaxCommand(IMainService newsMainService) {
        this.newsMainService = newsMainService;
    }

    /**
     * Override method that call special logic of
     * change the status of the order on the paid
     *
     * @param request
     * @param response
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            HttpSession session = request.getSession();
            Page currentPage = mapper.readValue(json, Page.class);
            session.setAttribute(CURRENT_PAGE, currentPage.getValue());
            Author authorSearch = (Author) session.getAttribute(AUTHOR_SEARCH);
            List<Tag> tagsSearch = (List<Tag>) session.getAttribute(TAGS_SEARCH);
            AjaxResponse ajaxResponse = new AjaxResponse();
            ajaxResponse.setCurrentPage(currentPage.getValue());
            Integer firstRow = (currentPage.getValue() - 1) * rowsPerPage;

            List<ListNews> newsForList=newsMainService.search(authorSearch, tagsSearch, firstRow, rowsPerPage);
            ajaxResponse.setNewsForList(newsForList);
            ajaxResponse.setLocale((String) session.getAttribute(LOCALE));

            long totalRows = newsMainService.countNews(authorSearch, tagsSearch);
            ajaxResponse.setFirstPage(1);

            long totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
            ajaxResponse.setTotalPages(totalPages);
            mapper.writeValue(response.getOutputStream(), ajaxResponse);
        } catch (IOException | ServiceCommonException ex) {
            throw new CommandException(ex);
        }
        return null;

    }

    static class AjaxResponse {
        private List<ListNews> newsForList;
        private Integer firstPage;
        private Integer currentPage;
        private Long totalPages;
        private String locale;
        private String message;

        public List<ListNews> getNewsForList() {
            return newsForList;
        }

        public void setNewsForList(List<ListNews> newsForList) {
            this.newsForList = newsForList;
        }

        public Integer getFirstPage() {
            return firstPage;
        }

        public void setFirstPage(Integer firstPage) {
            this.firstPage = firstPage;
        }

        public Long getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(Long totalPages) {
            this.totalPages = totalPages;
        }

        public Integer getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(Integer currentPage) {
            this.currentPage = currentPage;
        }

        public String getLocale() {
            return locale;
        }

        public void setLocale(String locale) {
            this.locale = locale;
        }
    }

    static class Page {
        private Integer value;

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }
    }
}
