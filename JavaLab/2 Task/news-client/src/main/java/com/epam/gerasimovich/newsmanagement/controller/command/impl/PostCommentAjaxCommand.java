package com.epam.gerasimovich.newsmanagement.controller.command.impl;

import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsmanagement.controller.command.ICommand;
import com.epam.gerasimovich.newsmanagement.controller.exception.CommandException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Tatiana
 * @version 1.00 11/9/2015
 */
public class PostCommentAjaxCommand implements ICommand {
    /**
     * Constants to get request parameters
     */
    private static final String JSON = "json";

    private IMainService newsMainService;

    public PostCommentAjaxCommand(IMainService newsMainService){
        this.newsMainService=newsMainService;
    }

    /**
     * Override method that call special logic of
     * change the status of the order on the paid
     *
     * @param request
     * @param response
     * @return null
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Comments comment =  mapper.readValue(json, Comments.class);
            Date date = new Date();
            long time = date.getTime();
            comment.setCreationDate(new Timestamp(time));
            newsMainService.createComment(comment);
            mapper.writeValue(response.getOutputStream(), newsMainService.getAllCommentsByNews(comment.getNewsId()));
        } catch (IOException | ServiceCommonException ex) {
            throw new CommandException(ex);
        }
        return null;

    }
}
