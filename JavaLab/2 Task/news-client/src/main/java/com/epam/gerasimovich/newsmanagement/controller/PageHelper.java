package com.epam.gerasimovich.newsmanagement.controller;

import java.util.ResourceBundle;
import java.util.logging.LogManager;

/**
 * @author Tatiana
 * @version 1.00 10/28/2015
 */
public class PageHelper {
    public static final String ERROR_PAGE = "ERROR_PAGE";
    public static final String NEWS_LIST = "NEWS_LIST";
    public static final String NEWS = "NEWS";
    private static final String BUNDLE_NAME = "pages";
    private ResourceBundle resourceBundle;


    private PageHelper() {
        resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    private static PageHelper instance;

    public static PageHelper getInstance() {
        PageHelper localInstance = instance;
        if (localInstance == null) {
            synchronized (PageHelper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new PageHelper();
                }
            }
        }
        return localInstance;
    }

    public String getProperty(String key) {
        return (String) resourceBundle.getObject(key);
    }
}
