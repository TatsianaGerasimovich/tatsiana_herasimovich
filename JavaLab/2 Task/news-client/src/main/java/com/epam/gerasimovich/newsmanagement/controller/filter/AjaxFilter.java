package com.epam.gerasimovich.newsmanagement.controller.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * @author Tatiana
 * @version 1.00 29.05.2015.
 */
public class AjaxFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(AjaxFilter.class);
    private static final String COMMAND = "command";
    private static final String JSON = "json";
    private static final String AJAX_REQUEST = "XMLHttpRequest";
    private static final String REQUEST = "X-Requested-With";



    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String commandName = null;
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        if (isAjax(httpRequest)) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8));
                String json = "";
                if (br != null) {
                    json = br.readLine();
                }
                ObjectMapper mapper = new ObjectMapper();
                AjaxRequest ajaxRequest = mapper.readValue(json, AjaxRequest.class);
                commandName = ajaxRequest.getCommand();
                request.setAttribute(JSON, ajaxRequest.getData());
                request.setAttribute(COMMAND, commandName);
            } catch (IOException ex) {
                LOG.error(ex);
            }
        }
        chain.doFilter(request, response);
    }

    public void destroy() {

    }

    private boolean isAjax(HttpServletRequest request) {
        return AJAX_REQUEST.equals(request.getHeader(REQUEST));
    }

    static public class AjaxRequest {

        private String command;

        private String data;

        public String getCommand() {
            return command;
        }

        public void setCommand(String command) {
            this.command = command;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }
    }

}
