package com.epam.gerasimovich.newsmanagement.controller.command;

import com.epam.gerasimovich.newsmanagement.controller.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Interface is designed to implement the pattern command
 *
 * @author Tatiana
 * @version 1.00 10/28/2015
 */
public interface ICommand {
    /**
     * Abstract method which acts as a template for the methods
     * described in the other classes
     *
     * @param request
     * @param response
     * @return
     * @throws CommandException
     */
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException;
}
