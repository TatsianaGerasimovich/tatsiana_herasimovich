package com.epam.gerasimovich.newsmanagement.controller.command.impl;

import com.epam.gerasimovich.newsmanagement.controller.PageHelper;
import com.epam.gerasimovich.newsmanagement.controller.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Tatiana
 * @version 1.00 11/1/2015
 */
public class NoSuchCommand implements ICommand {
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return PageHelper.getInstance().getProperty(PageHelper.ERROR_PAGE);
    }
}
