package com.epam.gerasimovich.newsmanagement.controller.command;

import com.epam.gerasimovich.newsmanagement.controller.command.impl.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.EnumMap;
import java.util.Map;

/**
 * One of the management classes.
 * That implements the command pattern, pattern factory and pattern Singleton.
 * Contains a list of possible commands.
 * Calls one of the possible list of commands and depending on the parameter passed to the request
 *
 * @author Tatiana
 * @version 1.00 10/28/2015
 */
public class CommandHelper {

    private static final String SPRING_CONFIG_XML = "spring-config.xml";
    private static ApplicationContext context = null;

    private Map<CommandName, ICommand> commands = new EnumMap<>(CommandName.class);

    private NoSuchCommand noSuchCommand;
    private GetAllNewsCommand getAllNewsCommand;
    private GetNewsById getNewsById;
    private GetNewsPaginationAjaxCommand getNewsPaginationAjaxCommand;
    private SearchCommand searchCommand;
    private PostCommentAjaxCommand postCommentAjaxCommand;
    private ChangeLocalCommand changeLocalCommand;

    public static void init() {
        context = new ClassPathXmlApplicationContext(SPRING_CONFIG_XML);
    }


    public static ApplicationContext getContext() {
        return context;
    }

    public CommandHelper(NoSuchCommand noSuchCommand, GetAllNewsCommand getAllNewsCommand,
                         GetNewsById getNewsById,GetNewsPaginationAjaxCommand getNewsPaginationAjaxCommand,
                         SearchCommand searchCommand,PostCommentAjaxCommand postCommentAjaxCommand,
                         ChangeLocalCommand changeLocalCommand) {
        this.noSuchCommand = noSuchCommand;
        commands.put(CommandName.NO_SUCH_COMMAND, this.noSuchCommand);
        this.getAllNewsCommand = getAllNewsCommand;
        commands.put(CommandName.GET_ALL_NEWS_COMMAND, this.getAllNewsCommand);
        this.getNewsById=getNewsById;
        commands.put(CommandName.SHOW_NEWS, this.getNewsById);
        this.getNewsPaginationAjaxCommand=getNewsPaginationAjaxCommand;
        commands.put(CommandName.GET_NEWS_PAGINATION, this.getNewsPaginationAjaxCommand);
        this.searchCommand=searchCommand;
        commands.put(CommandName.SEARCH,this.searchCommand);
        this.postCommentAjaxCommand=postCommentAjaxCommand;
        commands.put(CommandName.POST_COMMENT,this.postCommentAjaxCommand);
        this.changeLocalCommand=changeLocalCommand;
        commands.put(CommandName.CHANGE_LANGUAGE,this.changeLocalCommand);
    }

    public ICommand getCommand(String commandName) {
        CommandName action;
        try {
            action = CommandName.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException ex) {
            action = null;
        }
        ICommand command;
        if (action == null) {
            command = commands.get(CommandName.NO_SUCH_COMMAND);
        } else {
            command = commands.get(action);
        }
        return command;
    }

}
