package com.epam.gerasimovich.newsmanagement.controller.command.impl;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsmanagement.controller.PageHelper;
import com.epam.gerasimovich.newsmanagement.controller.command.ICommand;
import com.epam.gerasimovich.newsmanagement.controller.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 11/3/2015
 */
public class GetNewsById implements ICommand {
    /**
     * Constants to set request parameters
     */
    private static final String NEWS = "news";
    private static final String NEWS_ID = "newsId";

    private static final String AUTHOR_SEARCH = "authorSearch";
    private static final String TAGS_SEARCH = "tagsSearch";

    private IMainService newsMainService;

    public GetNewsById(IMainService newsMainService){
        this.newsMainService=newsMainService;
    }


    /**
     * Override method that call special logic of
     * transition to the page all periodicals
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {
        String page = null;
        int newsId = Integer.parseInt(request.getParameter(NEWS_ID));
        try {
            HttpSession session = request.getSession();
            Author authorSearch = (Author) session.getAttribute(AUTHOR_SEARCH);
            List<Tag> tagsSearch = (List<Tag>) session.getAttribute(TAGS_SEARCH);
            request.setAttribute(NEWS, newsMainService.getAllNewsById(newsId,authorSearch,tagsSearch));
            page = PageHelper.getInstance().getProperty(PageHelper.NEWS);

        } catch (ServiceCommonException ex) {
            throw new CommandException(ex);
        }
        return page;
    }

}
