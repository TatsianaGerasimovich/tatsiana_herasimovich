package com.epam.gerasimovich.newsmanagement.controller.command.impl;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsmanagement.controller.PageHelper;
import com.epam.gerasimovich.newsmanagement.controller.command.ICommand;
import com.epam.gerasimovich.newsmanagement.controller.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 11/7/2015
 */
public class SearchCommand implements ICommand {
    private final Integer rowsPerPage = 3;
    private final Integer currentPage = 1;

    /**
     * Constants to set request parameters
     */
    private static final String AUTHORS = "authors";
    private static final String AUTHOR_ID = "authorId";
    private static final String TAG_LIST = "tagList";
    private static final String TAGS_ID = "tagId";
    private static final String NEWS_LIST = "newsList";
    private static final String AUTHOR_SEARCH = "authorSearch";
    private static final String TAGS_SEARCH = "tagsSearch";
    private static final String CURRENT_PAGE = "currentPage";
    private static final String TOTAL_PAGES = "totalPages";
    private static final String FIRST_PAGE = "firstPage";
    IMainService newsMainService;

    public SearchCommand(IMainService newsMainService) {
        this.newsMainService = newsMainService;
    }


    /**
     * Override method that call special logic of
     * transition to the page all periodicals
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */

    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        String page = null;
        try {
            HttpSession session = request.getSession();
            Integer firstRow = (currentPage - 1) * rowsPerPage;
            long authorId = Long.parseLong(request.getParameter(AUTHOR_ID));
            String[] tagsIds = request.getParameterValues(TAGS_ID);
            session.setAttribute(CURRENT_PAGE, 1);
            Author authorSearch = null;
            List<Tag> tagsSearch = null;
            if (authorId != 0) {
                authorSearch = new Author();
                authorSearch.setAuthorId(authorId);
            }
            if (tagsIds.length != 1) {
                tagsSearch = new ArrayList<>();
                for (int i = 0; i < tagsIds.length; i++) {
                    if (!(tagsIds[i].equals("0"))) {
                        Tag tag = new Tag();
                        tag.setTagId(Long.valueOf(tagsIds[i]));
                        tagsSearch.add(tag);
                    }
                }
            }
            session.setAttribute(AUTHOR_SEARCH, authorSearch);
            session.setAttribute(TAGS_SEARCH, tagsSearch);
            request.setAttribute(AUTHORS, newsMainService.getAllAuthors());
            request.setAttribute(TAG_LIST, newsMainService.getAllTags());
            request.setAttribute(NEWS_LIST, newsMainService.search(authorSearch, tagsSearch, firstRow, rowsPerPage));
            long totalRows = newsMainService.countNews(authorSearch, tagsSearch);
            request.setAttribute(FIRST_PAGE, 1);
            long totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
            request.setAttribute(TOTAL_PAGES, totalPages);

            page = PageHelper.getInstance().getProperty(PageHelper.NEWS_LIST);

        } catch (ServiceCommonException ex) {
            throw new CommandException(ex);
        }
        return page;
    }

}