package com.epam.gerasimovich.newsmanagement.controller.command;

/**
 * @author Tatiana
 * @version 1.00 10/28/2015
 */
public enum CommandName {
    NO_SUCH_COMMAND, GET_ALL_NEWS_COMMAND, SHOW_NEWS, GET_NEWS_PAGINATION,
    SEARCH,POST_COMMENT,CHANGE_LANGUAGE
}
