package com.epam.gerasimovich.newsmanagement.controller.command.impl;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsmanagement.controller.PageHelper;
import com.epam.gerasimovich.newsmanagement.controller.command.ICommand;
import com.epam.gerasimovich.newsmanagement.controller.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 11/1/2015
 */
public class GetAllNewsCommand implements ICommand {
    private final Integer rowsPerPage=3;

    /**
     * Constants to set request parameters
     */
    private static final String AUTHORS = "authors";
    private static final String TAG_LIST = "tagList";
    private static final String NEWS_LIST = "newsList";
    private static final String AUTHOR_SEARCH = "authorSearch";
    private static final String TAGS_SEARCH = "tagsSearch";
    private static final String CURRENT_PAGE = "currentPage";
    private static final String TOTAL_PAGES = "totalPages";
    private static final String FIRST_PAGE = "firstPage";
    private IMainService newsMainService;

    public GetAllNewsCommand(IMainService newsMainService){
        this.newsMainService=newsMainService;
    }


    /**
     * Override method that call special logic of
     * transition to the page all periodicals
     *
     * @param request
     * @param response
     * @return String(forward page)
     * @throws CommandException
     */

    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws CommandException {

        String page = null;
        try {
            HttpSession session = request.getSession();
            Integer currentPage = (Integer) session.getAttribute(CURRENT_PAGE);
            Integer firstRow=(currentPage- 1) * rowsPerPage;
            Author authorSearch = (Author) session.getAttribute(AUTHOR_SEARCH);
            List<Tag> tagsSearch = (List<Tag>) session.getAttribute(TAGS_SEARCH);
            request.setAttribute(AUTHORS, newsMainService.getAllAuthors());
            request.setAttribute(TAG_LIST, newsMainService.getAllTags());
            request.setAttribute(NEWS_LIST, newsMainService.search( authorSearch, tagsSearch, firstRow, rowsPerPage));
            long totalRows =newsMainService.countNews(authorSearch, tagsSearch);
            request.setAttribute(FIRST_PAGE, 1);

            long totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
            request.setAttribute(TOTAL_PAGES, totalPages);

            page = PageHelper.getInstance().getProperty(PageHelper.NEWS_LIST);

        } catch (ServiceCommonException ex) {
            throw new CommandException(ex);
        }
        return page;
    }

}
