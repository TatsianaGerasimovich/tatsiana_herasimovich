package com.epam.gerasimovich.newsmanagement.controller.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import com.epam.gerasimovich.newsmanagement.controller.command.CommandHelper;

/**
 * @author Tatiana
 * @version 1.00 11/1/2015
 */
@WebListener
public class ContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent ev) {
            CommandHelper.init();
    }

    public void contextDestroyed(ServletContextEvent ev) throws UnsupportedOperationException{
     throw new UnsupportedOperationException();
    }
}