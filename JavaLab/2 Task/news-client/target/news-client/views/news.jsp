﻿<%--
  Created by IntelliJ IDEA.
  User: Tatiana
  Date: 11/2/2015
  Time: 9:53 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="locale"
       value="${locale}"/>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="messages"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>News portal</title>
    <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    <!-- JavaScripts-->
    <script type="text/javascript" src="resources/js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="resources/js/my.js"></script>
</head>
<body>

<div id="wrapper">


    <div id="logout">
        <ul>
            <li><a href="javascript:changeLanguage('en_US')">EN</a></li>
            <li><a href="javascript:changeLanguage('ru_RU')">РУС</a></li>
        </ul>

    </div>
    <!-- end of logout -->

    <%@include file="sidebar.jsp" %>

    <div id="right_column">

        <div id="main">
            <div class="filter">

                <select class="select" name="authorId" form="Search">
                    <option value="0">Any author</option>
                    <c:forEach var="author" items="${authors}">
                        <c:choose>
                            <c:when test="${author.authorId != authorSearch.authorId}">
                                <option value="${author.authorId}">${author.authorName}</option>
                            </c:when>
                            <c:when test="${author.authorId == authorSearch.authorId}">
                                <option value="${author.authorId}" selected="selected">${author.authorName}</option>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                </select>

                <div class="multiselect" style="height: 24px">
                    <div class="select-box" style="height: 24px" onclick="showTagList()">
                        <select name="tag">
                            <option><fmt:message key="select.title"/></option>
                        </select>

                        <div id="checkboxes" class="tag-list">
                            <input type="checkbox" name="tagId"
                                   value="0" style="display:none" checked="checked" form="Search"></label>

                            <c:forEach var="tag" items="${tagList}">

                            <c:set var="contains" value="false"/>
                            <c:forEach var="tagSearch" items="${tagsSearch}">
                                <c:if test="${tag.tagId == tagSearch.tagId}">
                                    <c:set var="contains" value="true"/>
                                </c:if>
                            </c:forEach>

                            <c:choose>
                            <c:when test="${contains eq 'true'}">
                            <label><input type="checkbox" name="tagId"
                                          value="${tag.tagId}" checked="checked" form="Search"><c:out
                                    value="${tag.tagName}"/>
                                </c:when>
                                <c:when test="${contains eq 'false'}">
                                <label><input type="checkbox" form="Search" name="tagId"
                                              value="${tag.tagId}"><c:out value="${tag.tagName}"/></label>
                                </c:when>
                                </c:choose>
                                </c:forEach>
                        </div>
                    </div>
                </div>


                <div class="button_filter"><a href="Client?command=SEARCH&authorId=0&tagId=0"><fmt:message key="reset"/></a></div>
                <form method="post" action="Client" id="Search">
                    <input type="hidden" name="command" value="SEARCH">
                </form>
                <div class="button_filter" onclick="submitForm('Search')"><a href="#"><fmt:message key="filter.button"/></a></div>

            </div>
            <div id="content">
                <c:choose>
                    <c:when test="${not empty newsList}">

                        <c:forEach var="news" items="${newsList}">
                            <div class="post_section">

                    <span class="comment">
                        <a href="Client?command=SHOW_NEWS&newsId=${news.news.newsId}">${news.countOfComments}</a></span>

                                <h2><a href="Client?command=SHOW_NEWS&newsId=${news.news.newsId}">${news.news.title}</a>
                                </h2>
                                <fmt:formatDate type="date" value="${news.news.creationDate}"/> |
                                <strong><fmt:message key="news.author"/></strong> ${news.author.authorName} | <strong><fmt:message key="news.tags"/></strong>
                                <c:forEach var="tag" items="${news.tagList}">
                                    <a href="#">${tag.tagName} </a>
                                </c:forEach>

                                <p>${news.news.shortText}</p>
                                <a href="Client?command=SHOW_NEWS&newsId=${news.news.newsId}"><fmt:message key="news.view"/></a>
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:when test="${empty newsList}">
                        <div class="post_section">
                            <h2><a href="#"><fmt:message key="filter.result"/></a></h2>
                        </div>
                    </c:when>
                </c:choose>
                <%-- The paging buttons --%>
                <c:set var="firstPage" value="${firstPage}"/>
                <c:choose>
                    <c:when test="${currentPage <= 1}">
                        <button type="button" disabled><fmt:message key="button.first"/></button>
                        <button type="button" disabled><fmt:message key="button.prev"/></button>
                    </c:when>
                    <c:when test="${currentPage > 1}">
                        <button onclick="pagination(${firstPage})"><fmt:message key="button.first"/> (${firstPage})</button>
                        <button onclick="pagination(${currentPage-1})"><fmt:message key="button.prev"/> (${currentPage-1})</button>
                    </c:when>
                </c:choose>
                <c:choose>
                    <c:when test="${currentPage >= totalPages}">
                        <button disabled><fmt:message key="button.next"/> ${currentPage+1}</button>
                        <button disabled><fmt:message key="button.last"/> ${totalPages}</button>
                    </c:when>
                    <c:when test="${currentPage < totalPages}">
                        <button onclick="pagination(${currentPage+1})"><fmt:message key="button.next"/> (${currentPage+1})</button>
                        <button onclick="pagination(${totalPages})"><fmt:message key="button.last"/> (${totalPages})</button>
                    </c:when>
                </c:choose>

                <c:out value="${currentPage}"/>/<c:out value="${totalPages}"/>

                <%-- The paging links --%>
                <select onchange="pagination(this.options[this.selectedIndex].value)">
                    <c:forEach begin="${firstPage}" end="${totalPages}" var="page">
                        <c:choose>
                            <c:when test="${page == currentPage}">
                                <option value="${page}" selected="selected"><c:out value="${page}"/></option>
                            </c:when>
                            <c:when test="${page!= currentPage}">
                                <option value="${page}">${page} </option>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>

        </div>
        <!-- end of right column, main -->
    </div>
    <%@include file="footer.jsp" %>
    <!-- end of footer -->
</div>
<!-- end of warpper -->

</body>
</html>
