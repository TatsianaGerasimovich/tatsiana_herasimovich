package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.service.IServiceComments;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class CommentsService implements IServiceComments{

    private CommentDao commentsDao;

    public CommentsService(CommentDao commentsDao) {
        this.commentsDao = commentsDao;
    }

    public long create(Comments comment) throws ServiceCommonException {
        try {
            if (comment != null) {
                return commentsDao.create(comment);
            } else {
                throw new ServiceCommonException("Null comment");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public List<Comments> getAllComments() throws ServiceCommonException {
        try {
            return commentsDao.getAll();
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public List<Comments> getCommentsByNews(long newsId) throws ServiceCommonException {
        try {
            return commentsDao.getCommentsByNewsId(newsId);
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void update(Comments comment) throws ServiceCommonException {
        try {
            if (comment != null) {
                commentsDao.update(comment);
            } else {
                throw new ServiceCommonException("Null comment");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void delete(List<Comments> comments) throws ServiceCommonException {
        try {
            if (comments != null) {
                commentsDao.delete(comments);
            } else {
                throw new ServiceCommonException("Null comment");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void deleteByNews(List<News> news) throws ServiceCommonException {
        try {
            if (news != null) {
                commentsDao.deleteByNewsId(news);
            } else {
                throw new ServiceCommonException("Null comment");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }
}
