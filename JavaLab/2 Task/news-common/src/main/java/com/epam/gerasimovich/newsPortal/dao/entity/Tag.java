package com.epam.gerasimovich.newsPortal.dao.entity;

import java.io.Serializable;

/**
 * Class-Entity store the information about <entity>Tag</entity>
 *
 * @author Tatiana
 * @version 1.00 30.09.2015.
 */
public class Tag implements Serializable {
    /**
     * fields of comments entity
     */
    private Long tagId;
    private String tagName;

    /**
     * Constructor parameters
     * @param tagId
     * @param tagName
     */
    public Tag(long tagId, String tagName) {
        this.tagId = tagId;
        this.tagName = tagName;
    }

    /**
     * Default constructor
     */
    public Tag() {
    }

    /**
     * getter for tag id field
     * @return value of tag id field
     */
    public Long getTagId() {
        return tagId;
    }

    /**
     * setter for tag id field
     * @param tagId
     */
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    /**
     * getter for tag name field
     * @return value of tag name field
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * setter for tag name field
     * @param tagName
     */
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        if (tagId != tag.tagId) return false;
        return !(tagName != null ? !tagName.equals(tag.tagName) : tag.tagName != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (tagId ^ (tagId >>> 32));
        result = 31 * result + (tagName != null ? tagName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "tagId=" + tagId +
                ", tagName='" + tagName + '\'' +
                '}';
    }
}
