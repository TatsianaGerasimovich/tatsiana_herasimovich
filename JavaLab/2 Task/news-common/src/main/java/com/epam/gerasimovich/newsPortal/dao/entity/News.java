package com.epam.gerasimovich.newsPortal.dao.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Class-Entity store the information about <entity>News</entity>
 *
 * @author Tatiana
 * @version 1.00 30.09.2015.
 */
public class News implements Serializable {
    /**
     * fields of comments entity
     */
    private Long newsId;
    private String title;
    private String shortText;
    private String fullText;
    private Timestamp creationDate;
    private Date modificationDate;

    /**
     * Default constructor
     */
    public News() {
    }

    /**
     * Constructor parameters
     *
     * @param newsId
     * @param title
     * @param shortText
     * @param fullText
     * @param creationDate
     * @param modificationDate
     */
    public News(Long newsId, String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate) {
        this.modificationDate = modificationDate;
        this.creationDate = creationDate;
        this.fullText = fullText;
        this.shortText = shortText;
        this.title = title;
        this.newsId = newsId;
    }

    /**
     * getter for news id field
     *
     * @return value of news id field
     */
    public Long getNewsId() {
        return newsId;
    }

    /**
     * setter for news id field
     *
     * @param newsId
     */
    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    /**
     * getter for title field
     *
     * @return value of title field
     */
    public String getTitle() {
        return title;
    }

    /**
     * setter for title field
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * getter for short text field
     *
     * @return value of short text field
     */
    public String getShortText() {
        return shortText;
    }

    /**
     * setter for short text field
     *
     * @param shortText
     */
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    /**
     * getter for full text field
     *
     * @return value of full text field
     */
    public String getFullText() {
        return fullText;
    }

    /**
     * setter for full text field
     *
     * @param fullText
     */
    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    /**
     * getter for creation date field
     *
     * @return value of creation date field
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * setter for creation date field
     *
     * @param creationDate
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * getter for modification date field
     *
     * @return value of creation date field
     */
    public Date getModificationDate() {
        return modificationDate;
    }

    /**
     * setter for modification date field
     *
     * @param modificationDate
     */
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (newsId != news.newsId) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) return false;
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) return false;
        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
        return !(modificationDate != null ? !modificationDate.toString().equals(news.modificationDate.toString()) : news.modificationDate != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "News{" +
                "newsId=" + newsId +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                '}';
    }
}
