package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.NewsDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;
import com.epam.gerasimovich.newsPortal.service.IServiceNews;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class NewsService implements IServiceNews {

    private NewsDao newsDao;

    public NewsService(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    public long create(News news) throws ServiceCommonException {
        try {
            if (news != null) {
                return newsDao.create(news);
            } else {
                throw new ServiceCommonException("Null news");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void update(News news) throws ServiceCommonException {
        try {
            if (news != null) {
                newsDao.update(news);
            } else {
                throw new ServiceCommonException("Null news");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void delete(List<News> news) throws ServiceCommonException {
        try {
            if (news != null) {
                newsDao.delete(news);
            } else {
                throw new ServiceCommonException("Null news");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void unbindAuthors(List<News> news) throws ServiceCommonException {
        try {
            if (news != null) {
                newsDao.unbindAuthors(news);
            } else {
                throw new ServiceCommonException("Null news");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void unbindTags(List<News> news) throws ServiceCommonException {
        try {
            if (news != null) {
                newsDao.unbindTags(news);
            } else {
                throw new ServiceCommonException("Null news");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void contactAuthor(News news, Author author) throws ServiceCommonException {
        try {
            if (news != null && author != null) {
                newsDao.bindAuthor(news, author);
            } else {
                throw new ServiceCommonException("Null news or author");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public Author getAuthorByNewsId(long newsId) throws ServiceCommonException {
        try {
            return newsDao.getAuthorByNewsID(newsId);
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void contactTags(News news, List<Tag> tags) throws ServiceCommonException {
        try {
            if (news != null && tags != null) {
                newsDao.bindTags(news, tags);
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public List<Tag> getTagsByNewsId(long newsId) throws ServiceCommonException {
        try {
            return newsDao.getTagsByNewsId(newsId);
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public List<News> getAllNews() throws ServiceCommonException {
        try {
            return newsDao.getAll();
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public News getNewsById(long id) throws ServiceCommonException {
        News news;
        try {
            news = newsDao.getByPK(id);
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
        return news;
    }

    public ViewNews getAllByNewsPK(long newsId,NewsSearchCriteria criteria) throws ServiceCommonException {
        ViewNews news;
        try {
            news = newsDao.getAllByPK(newsId, criteria);
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
        return news;
    }

    public List<ListNews> searchAllInformation(NewsSearchCriteria searchCriteria,Integer firstRow, Integer rowsPerPage) throws ServiceCommonException {
        try {
            return newsDao.searchAllInformation(searchCriteria,firstRow,rowsPerPage);
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public long countNews(NewsSearchCriteria criteria) throws ServiceCommonException {
        try {
            return newsDao.countNews(criteria);
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }
}
