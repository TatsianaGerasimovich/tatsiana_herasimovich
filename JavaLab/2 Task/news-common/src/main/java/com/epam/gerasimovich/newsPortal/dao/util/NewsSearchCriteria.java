package com.epam.gerasimovich.newsPortal.dao.util;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;

import java.util.List;

/**
 * Created by Tatiana on 04.10.2015.
 */
public class NewsSearchCriteria {
    private Author author;
    private List<Tag> tagList;

    public NewsSearchCriteria() {
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsSearchCriteria that = (NewsSearchCriteria) o;

        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        return !(tagList != null ? !tagList.equals(that.tagList) : that.tagList != null);

    }

    @Override
    public int hashCode() {
        int result = author != null ? author.hashCode() : 0;
        result = 31 * result + (tagList != null ? tagList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsSearchCriteria{" +
                "author=" + author +
                ", tagList=" + tagList +
                '}';
    }
}
