package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public interface IServiceNews {

    long create(News news) throws ServiceCommonException;

    void update(News news) throws ServiceCommonException;

    void delete(List<News> news) throws ServiceCommonException;

    void unbindAuthors(List<News> news) throws ServiceCommonException;

    void unbindTags(List<News> news) throws ServiceCommonException;

    News getNewsById(long id) throws ServiceCommonException;

    ViewNews getAllByNewsPK(long newsId,NewsSearchCriteria criteria) throws ServiceCommonException;

    List<News> getAllNews() throws ServiceCommonException;

    void contactAuthor(News news, Author author) throws ServiceCommonException;

    Author getAuthorByNewsId(long newsId) throws ServiceCommonException;

    void contactTags(News news, List<Tag> tags) throws ServiceCommonException;

    List<Tag> getTagsByNewsId(long newsId) throws ServiceCommonException;

    List<ListNews> searchAllInformation(NewsSearchCriteria searchCriteria,Integer firstRow, Integer rowsPerPage) throws ServiceCommonException;

    long countNews(NewsSearchCriteria criteria) throws ServiceCommonException;
}
