package com.epam.gerasimovich.newsPortal.dao;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;

import java.util.List;

/**
 * Interface provides operations using JDBC for News entity.
 *
 * @author Tatiana
 * @version 1.00 02.10.2015
 */
public interface NewsDao extends GenericDao<News> {

    /*Long create(News object) throws DAOCommonException;

    void update(News object) throws DAOCommonException;

    void delete(List<News> objects) throws DAOCommonException;

    List<News> getAll() throws DAOCommonException;

    News getByPK(Long id) throws DAOCommonException;*/

    /**
     * Method deletes a news list of appropriate records in the table NEWS_AUTHOR
     *
     * @param news a news list of appropriate records
     * @throws DAOCommonException
     */
    void unbindAuthors(List<News> news) throws DAOCommonException;

    /**
     * Method deletes a news list of appropriate records in the table NEWS_TAG
     *
     * @param news
     * @throws DAOCommonException
     */
    void unbindTags(List<News> news) throws DAOCommonException;

    /**
     * Method creates a record with certain value of news id and author is in the table NEWS_AUTHOR
     *
     * @param news
     * @param author
     * @throws DAOCommonException
     */
    void bindAuthor(News news, Author author) throws DAOCommonException;

    /**
     * Method gets the appropriate author record with certain news id
     *
     * @param newsId
     * @return author object
     * @throws DAOCommonException
     */
    Author getAuthorByNewsID(Long newsId) throws DAOCommonException;

    /**
     * Method creates a record with certain value of news id and author is in the table NEWS_TAG
     *
     * @param news
     * @param tags
     * @throws DAOCommonException
     */
    void bindTags(News news, List<Tag> tags) throws DAOCommonException;

    /**
     * Method gets the appropriate list of tag records with certain news id
     *
     * @param newsId
     * @return list of tag records
     * @throws DAOCommonException
     */
    List<Tag> getTagsByNewsId(Long newsId) throws DAOCommonException;

    /**
     * Method gets the appropriate record with a primary key or a null key
     *
     * @param id
     * @param criteria
     * @return object of ViewNews
     * @throws DAOCommonException
     */
    ViewNews getAllByPK(Long id, NewsSearchCriteria criteria) throws DAOCommonException;

    /**
     * Method gets the list of all appropriate records
     *
     * @param criteria
     * @param firstRow
     * @param rowsPerPage
     * @return list of ListNews objects
     * @throws DAOCommonException
     */
    List<ListNews> searchAllInformation(NewsSearchCriteria criteria, Integer firstRow, Integer rowsPerPage) throws DAOCommonException;

    /**
     * Method gets number of appropriate records
     *
     * @param criteria
     * @return number of appropriate records
     * @throws DAOCommonException
     */
    long countNews(NewsSearchCriteria criteria) throws DAOCommonException;
}
