package com.epam.gerasimovich.newsPortal.valueObject;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/14/2015
 */
public class ViewNews {
    private News news;
    private Author author;
    private List<Tag> tagList;
    private List<Comments> commentList;
    private Long nextNewsId;
    private Long preNewsId;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Comments> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comments> commentList) {
        this.commentList = commentList;
    }

    public long getNextNewsId() {
        return nextNewsId;
    }

    public void setNextNewsId(Long nextNewsId) {
        this.nextNewsId = nextNewsId;
    }

    public long getPreNewsId() {
        return preNewsId;
    }

    public void setPreNewsId(Long preNewsId) {
        this.preNewsId = preNewsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ViewNews that = (ViewNews) o;

        if (nextNewsId != that.nextNewsId) return false;
        if (preNewsId != that.preNewsId) return false;
        if (news != null ? !news.equals(that.news) : that.news != null) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (tagList != null ? !tagList.equals(that.tagList) : that.tagList != null) return false;
        return !(commentList != null ? !commentList.equals(that.commentList) : that.commentList != null);

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (tagList != null ? tagList.hashCode() : 0);
        result = 31 * result + (commentList != null ? commentList.hashCode() : 0);
        result = 31 * result + (int) (nextNewsId ^ (nextNewsId >>> 32));
        result = 31 * result + (int) (preNewsId ^ (preNewsId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "ViewNews{" +
                "news=" + news +
                ", author=" + author +
                ", tagList=" + tagList +
                ", commentList=" + commentList +
                ", nextNewsId=" + nextNewsId +
                ", preNewsId=" + preNewsId +
                '}';
    }
}
