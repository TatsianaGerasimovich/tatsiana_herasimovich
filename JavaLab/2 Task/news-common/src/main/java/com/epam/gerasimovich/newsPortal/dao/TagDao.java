package com.epam.gerasimovich.newsPortal.dao;

import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;

import java.util.List;

/**
 * Interface provides a base implementation CRUD operations using JDBC for Tag entity.
 * <p/>
 * Created by Tatiana on 02.10.2015.
 */
public interface TagDao extends GenericDao<Tag> {

   /* Long create(Tag object) throws DAOCommonException;

    List<Tag> getAll() throws DAOCommonException;

    void update(Tag object) throws DAOCommonException;

    void delete(List<Tag> objects) throws DAOCommonException;*/

    /**
     * Method deletes a list of appropriate records in the table NEWS_TAG
     *
     * @param tags list of appropriate records
     * @throws DAOCommonException
     */
    void unbindNews(List<Tag> tags) throws DAOCommonException;

}

