package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public interface IServiceComments {
    long create(Comments comment) throws ServiceCommonException;

    List<Comments> getAllComments() throws ServiceCommonException;

    List<Comments> getCommentsByNews(long newsId) throws ServiceCommonException;

    void update(Comments comment) throws ServiceCommonException;

    void delete(List<Comments> comments) throws ServiceCommonException;

    void deleteByNews(List<News> news) throws ServiceCommonException;
}
