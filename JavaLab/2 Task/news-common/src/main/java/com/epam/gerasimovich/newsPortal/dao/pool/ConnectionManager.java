package com.epam.gerasimovich.newsPortal.dao.pool;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Class is designed to get and release connection with DB
 * <p/>
 * Created by Tatiana on 02.10.2015.
 */
public class ConnectionManager {
    private final static Logger logger = Logger.getLogger(ConnectionManager.class);
    private DataSource dataSource;

    /**
     * Constructor parameters for spring injection
     */
    public ConnectionManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Method for getting connection with DB
     *
     * @return connection
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException {
        return DataSourceUtils.getConnection(dataSource);
    }

    /**
     * Method for releasing resources
     *
     * @param rs result set
     * @param st statement
     * @param c  connection
     */
    public void releaseResources(ResultSet rs, Statement st, Connection c) {
        try {
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }
            releaseResources(st, c);
        } catch (SQLException e) {
            logger.error(e, e.getCause());
        }
    }

    /**
     * Method for releasing resources
     *
     * @param st statement
     * @param c  connection
     */
    public void releaseResources(Statement st, Connection c) {
        try {
            if (st != null && !st.isClosed()) {
                st.close();
            }
            if (c != null && !c.isClosed()) {
                DataSourceUtils.releaseConnection(c, dataSource);
            }
        } catch (SQLException e) {
            logger.error(e, e.getCause());
        }
    }
}
