package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;

import java.util.List;

/**
 * Interface provides the service layer for author entity
 *
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public interface IServiceAuthors {

    long createAuthor(Author author) throws ServiceCommonException;

    void updateAuthor(Author author) throws ServiceCommonException;

    void deleteAuthors(List<Author> authors) throws ServiceCommonException;

    List<Author> getAllAuthors() throws ServiceCommonException;
}
