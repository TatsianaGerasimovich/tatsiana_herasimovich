package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.AuthorDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.pool.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
public class JDBCAuthorsDao implements AuthorDao {
    private ConnectionManager manager;
    private final String QUERY_INSERT = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) " +
            "VALUES (AUTHORS_SEQ.nextval, ?)";
    private final String QUERY_UPDATE = "UPDATE AUTHOR SET AUTHOR_NAME = ? WHERE AUTHOR_ID = ?";
    private final String QUERY_SELECT_BY_PK = "SELECT AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
    private final String QUERY_DELETE_AUTHOR = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
    private final String QUERY_SELECT_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";
    private final String SORTING = " ORDER BY AUTHOR_ID";
    private final String COLUMN_AUTHOR_ID ="AUTHOR_ID";
    private final String COLUMN_AUTHOR_NAME="AUTHOR_NAME";
    private final String COLUMN_EXPIRED="EXPIRED";

    public JDBCAuthorsDao(ConnectionManager manager) {
        this.manager = manager;
    }

    public Long create(Author author) throws DAOCommonException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        Long id=null;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(QUERY_INSERT, new String[]{COLUMN_AUTHOR_ID});
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(preparedStatement, c);
        }
        return id;
    }

    public List<Author> getAll() throws DAOCommonException {
        Connection c = null;
        Statement st = null;
        List<Author> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(QUERY_SELECT_ALL_AUTHORS + SORTING);
            list = new ArrayList<Author>();
            while (rs.next()) {
                Author author = new Author();
                author.setAuthorId(rs.getLong(COLUMN_AUTHOR_ID));
                author.setAuthorName(rs.getString(COLUMN_AUTHOR_NAME));
                author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
                list.add(author);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    public Author getByPK (Long authorId) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_SELECT_BY_PK);
            ps.setLong(1, authorId);
            rs = ps.executeQuery();
            Author author = null;
            if (rs.next()) {
                author = new Author();
                author.setAuthorId(authorId);
                author.setAuthorName(rs.getString(COLUMN_AUTHOR_NAME));
                author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
            }
            return author;
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public void update(Author author) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_UPDATE);
            ps.setString(1, author.getAuthorName());
            ps.setLong(2, author.getAuthorId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public void delete(List<Author> authors) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_DELETE_AUTHOR);
            for (Author obj : authors) {
                ps.setTimestamp(1, obj.getExpired());
                ps.setLong(2, obj.getAuthorId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }
}