package com.epam.gerasimovich.newsPortal.valueObject;

import com.epam.gerasimovich.newsPortal.dao.entity.News;


import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 11/25/2015
 */
public class ModelNews {
    private News news;
    private  Long authorId;
    private Long[] tagIds;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Long[] getTagIds() {
        return tagIds;
    }

    public void setTagIds(Long[] tagIds) {
        this.tagIds = tagIds;
    }
}
