package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public interface IServiceTags {
    long createTag(Tag tag) throws ServiceCommonException;

    void updateTag(Tag tag) throws ServiceCommonException;

    List<Tag> getAllTags() throws ServiceCommonException;

    void deleteTag(List<Tag> tags) throws ServiceCommonException;

    void unbindNews(List<Tag> tags) throws ServiceCommonException;
}
