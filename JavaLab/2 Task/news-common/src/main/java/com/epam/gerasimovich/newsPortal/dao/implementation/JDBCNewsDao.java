package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.NewsDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.pool.ConnectionManager;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 04.10.2015.
 */
public class JDBCNewsDao implements NewsDao {
    private final String QUERY_INSERT = "INSERT INTO News (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) " +
            "VALUES (NEWS_SEQ.nextval, ?, ?, ?, ?, ?)";
    // + "VALUES (NEWS_SEQ.nextval, ?, ?, ?, TO_TIMESTAMP(?, 'DD-MM-YY HH24:MI'),?);";
    private final String QUERY_SELECT_BY_PK = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE" +
            " FROM News WHERE NEWS_ID = ?";
    private final String QUERY_SELECT_ALL_BY_PK_BEGIN = "SELECT\n" +
            "  NEWS_ID,\n" +
            "  TITLE,\n" +
            "  SHORT_TEXT,\n" +
            "  FULL_TEXT,\n" +
            "  CREATION_DATE,\n" +
            "  MODIFICATION_DATE,\n" +
            "  AUTHOR_ID,\n" +
            "  AUTHOR_NAME,\n" +
            "  NEXT_ID,\n" +
            "  PRE_ID\n" +
            "FROM (\n" +
            "  SELECT\n" +
            "    NEWS_ID,\n" +
            "    TITLE,\n" +
            "    SHORT_TEXT,\n" +
            "    FULL_TEXT,\n" +
            "    CREATION_DATE,\n" +
            "    MODIFICATION_DATE,\n" +
            "    AUTHOR_ID,\n" +
            "    AUTHOR_NAME,\n" +
            "    POPULARITY,\n" +
            "    lead(NEWS_ID)\n" +
            "    OVER (ORDER BY POPULARITY DESC, CREATION_DATE DESC)\n" +
            "      AS NEXT_ID,\n" +
            "    lag(NEWS_ID)\n" +
            "    OVER (ORDER BY POPULARITY DESC, CREATION_DATE DESC)\n" +
            "      AS PRE_ID\n" +
            "  FROM (\n" +
            "    SELECT DISTINCT\n" +
            "      N.NEWS_ID,\n" +
            "      N.TITLE,\n" +
            "      N.SHORT_TEXT,\n" +
            "      N.FULL_TEXT,\n" +
            "      N.CREATION_DATE,\n" +
            "      N.MODIFICATION_DATE,\n" +
            "      A.AUTHOR_ID,\n" +
            "      A.AUTHOR_NAME,\n" +
            "      (SELECT COUNT(*)\n" +
            "       FROM Comments C\n" +
            "       WHERE C.NEWS_ID = N.NEWS_ID) AS POPULARITY\n" +
            "    FROM News N\n" +
            "     LEFT JOIN News_Tag NT ON N.NEWS_ID = NT.NEWS_ID\n" +
            "      JOIN News_Author NA ON N.NEWS_ID = NA.NEWS_ID\n" +
            "      JOIN AUTHOR A ON NA.AUTHOR_ID = A.AUTHOR_ID\n";

    private final String QUERY_SELECT_ALL_BY_PK_END =
            "ORDER BY POPULARITY DESC, CREATION_DATE DESC"+
            "  )\n" +
            ")\n" +
            "WHERE NEWS_ID = ?";
    private String QUERY_BIND_AUTHOR = "INSERT INTO News_Author (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
    private String QUERY_BIND_TAG = "INSERT INTO News_Tag (NEWS_ID, TAG_ID) VALUES (?, ?)";
    private String QUERY_UPDATE_NEWS = "UPDATE News " +
            "SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, " +
            "CREATION_DATE = ?, MODIFICATION_DATE = ? " +
            "WHERE NEWS_ID = ?";
    private String QUERY_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
    private String QUERY_UNBIND_AUTHOR_BY_NEWS = "DELETE FROM News_Author WHERE NEWS_ID = ?";
    private String UNBIND_TAGS_BY_NEWS = "DELETE FROM News_Tag WHERE NEWS_ID = ?";
    private String QUERY_GET_AUTHOR_FOR_NEWS = "SELECT A.AUTHOR_ID, A.AUTHOR_NAME, A.EXPIRED FROM News_Author NA " +
            "JOIN AUTHOR A ON A.AUTHOR_ID = NA.AUTHOR_ID " +
            "WHERE NA.NEWS_ID = ? ORDER BY A.AUTHOR_ID";
    private String QUERY_GET_TAGS_FOR_NEWS = "SELECT T.TAG_ID, T.TAG_NAME FROM News_Tag NT " +
            "JOIN Tag T ON T.TAG_ID = NT.TAG_ID " +
            "WHERE NT.NEWS_ID = ? ORDER BY T.TAG_ID";
    private String QUERY_SELECT_ALL_NEWS = "SELECT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, " +
            "N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, " +
            "(SELECT COUNT(*) FROM Comments C " +
            "WHERE C.NEWS_ID = N.NEWS_ID) AS POPULARITY " +
            "FROM News N";
    private String QUERY_SELECT_ALL_INFORMATION_BEGIN =
            "SELECT NWR.* FROM(\n" +
                    "                  SELECT  NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,\n" +
                    "                    CREATION_DATE,MODIFICATION_DATE,\n" +
                    "                    A.AUTHOR_ID,A.AUTHOR_NAME,\n" +
                    "                    POPULARITY,\n" +
                    "                    ROW_NUMBER() OVER (ORDER BY POPULARITY DESC ) AS rn\n" +
                    "                  FROM (SELECT N.NEWS_ID,N.TITLE,N.SHORT_TEXT,N.FULL_TEXT,N.CREATION_DATE,N.MODIFICATION_DATE,\n" +
                    "                          COUNT(C.COMMENT_ID) AS POPULARITY\n" +
                    "                        FROM NEWS N\n" +
                    "                          LEFT JOIN COMMENTS C ON N.NEWS_ID = C.NEWS_ID\n" +
                    "                        GROUP BY N.NEWS_ID,N.TITLE,N.SHORT_TEXT,N.FULL_TEXT,N.CREATION_DATE,N.MODIFICATION_DATE\n" +
                    "                        ORDER BY POPULARITY DESC, CREATION_DATE DESC) N\n" +
                    "                    INNER JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID\n" +
                    "                    INNER JOIN TAG T ON T.TAG_ID = NT.TAG_ID\n" +
                    "                    INNER JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID\n" +
                    "                    INNER JOIN AUTHOR A ON A.AUTHOR_ID = NA.AUTHOR_ID ";
    private String QUERY_SELECT_ALL_INFORMATION_END =
            "GROUP BY NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,POPULARITY,A.AUTHOR_ID,A.AUTHOR_NAME\n" +
                    "                  ORDER BY POPULARITY DESC, CREATION_DATE DESC) NWR\n" +
                    "WHERE rn>=? AND rn<=?";
    private String SORTING = " ORDER BY POPULARITY DESC";
   /*private String QUERY_COUNT_NEWS = "SELECT COUNT(DISTINCT N.NEWS_ID) AS QUANTITY" +
            " FROM NEWS N " +
            "JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID " +
            "JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID ";*/
   private String QUERY_COUNT_NEWS = "SELECT COUNT(*) AS QUANTITY\n" +
            "                          FROM (\n" +
           "SELECT  NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,\n" +
           "  CREATION_DATE,MODIFICATION_DATE,\n" +
           "  A.AUTHOR_ID,A.AUTHOR_NAME,\n" +
           "  POPULARITY\n" +
           "FROM (SELECT N.NEWS_ID,N.TITLE,N.SHORT_TEXT,N.FULL_TEXT,N.CREATION_DATE,N.MODIFICATION_DATE,\n" +
           "        COUNT(C.COMMENT_ID) AS POPULARITY\n" +
           "      FROM NEWS N\n" +
           "        LEFT JOIN COMMENTS C ON N.NEWS_ID = C.NEWS_ID\n" +
           "      GROUP BY N.NEWS_ID,N.TITLE,N.SHORT_TEXT,N.FULL_TEXT,N.CREATION_DATE,N.MODIFICATION_DATE\n" +
           "      ORDER BY POPULARITY DESC, CREATION_DATE DESC) N\n" +
           "  INNER JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID\n" +
           "  INNER JOIN TAG T ON T.TAG_ID = NT.TAG_ID\n" +
           "  INNER JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID\n" +
           "  INNER JOIN AUTHOR A ON A.AUTHOR_ID = NA.AUTHOR_ID\n";
    private String QUERY_COUNT_END =
           "GROUP BY NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE,POPULARITY,A.AUTHOR_ID,A.AUTHOR_NAME\n" +
                    "ORDER BY POPULARITY DESC, CREATION_DATE DESC) ";
    private final String COLUMN_TITLE ="TITLE";
    private final String COLUMN_NEWS_ID="NEWS_ID";
    private final String COLUMN_SHORT_TEXT="SHORT_TEXT";
    private final String COLUMN_FULL_TEXT="FULL_TEXT";
    private final String COLUMN_CREATION_DATE="CREATION_DATE";
    private final String COLUMN_MODIFICATION_DATE="MODIFICATION_DATE";
    private final String COLUMN_AUTHOR_ID ="AUTHOR_ID";
    private final String COLUMN_AUTHOR_NAME="AUTHOR_NAME";
    private final String COLUMN_EXPIRED="EXPIRED";
    private final String COLUMN_TAG_ID ="TAG_ID";
    private final String COLUMN_TAG_NAME="TAG_NAME";
    private final String COLUMN_POPULARITY="POPULARITY";
    private final String COLUMN_NEXT_ID="NEXT_ID";
    private final String COLUMN_PRE_ID ="PRE_ID";
    private final String COLUMN_QUANTITY="QUANTITY";
    private final String QURERY_WHERE= "WHERE ";
    private final String QURERY_SEARCH_CRITERIA_AUTHOR= "NA.AUTHOR_ID = ?\n";
    private final String QURERY_SEARCH_CRITERIA_AND= " AND ";
    private final String QURERY_SEARCH_CRITERIA_TAG= "NT.TAG_ID = ?\n";
    private final String QURERY_SEARCH_CRITERIA_TAG_OR= "OR NT.TAG_ID = ?\n";

    private ConnectionManager manager;

    public JDBCNewsDao(ConnectionManager manager) {
        this.manager = manager;
    }

    public Long create(News news) throws DAOCommonException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        long id = 0;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(QUERY_INSERT, new String[]{COLUMN_NEWS_ID});
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setTimestamp(4, news.getCreationDate());
            preparedStatement.setDate(5, news.getModificationDate());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(preparedStatement, c);
        }
        return id;
    }

    public void update(News news) throws DAOCommonException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(QUERY_UPDATE_NEWS);
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setTimestamp(4, news.getCreationDate());
            preparedStatement.setDate(5, news.getModificationDate());
            preparedStatement.setLong(6, news.getNewsId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(preparedStatement, c);
        }
    }

    public void delete(List<News> newsList) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_DELETE_NEWS);
            for (News obj : newsList) {
                ps.setLong(1, obj.getNewsId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public void unbindAuthors(List<News> news) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_UNBIND_AUTHOR_BY_NEWS);
            for (News obj : news) {
                ps.setLong(1, obj.getNewsId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public void unbindTags(List<News> news) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UNBIND_TAGS_BY_NEWS);
            for (News obj : news) {
                ps.setLong(1, obj.getNewsId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public void bindAuthor(News news, Author author) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_UNBIND_AUTHOR_BY_NEWS);
            ps.setLong(1, news.getNewsId());
            ps.executeUpdate();
            ps = c.prepareStatement(QUERY_BIND_AUTHOR);
            ps.setLong(1, news.getNewsId());
            ps.setLong(2, author.getAuthorId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public void bindTags(News news, List<Tag> tags) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UNBIND_TAGS_BY_NEWS);
            ps.setLong(1, news.getNewsId());
            ps.executeUpdate();
            ps = c.prepareStatement(QUERY_BIND_TAG);
            for (Tag t : tags) {
                ps.setLong(1, news.getNewsId());
                ps.setLong(2, t.getTagId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public List<News> getAll() throws DAOCommonException {
        Connection c = null;
        Statement st = null;
        List<News> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(QUERY_SELECT_ALL_NEWS + SORTING);
            list = new ArrayList<News>();
            while (rs.next()) {
                News news = new News();
                news.setNewsId(rs.getLong(COLUMN_NEWS_ID));
                news.setTitle(rs.getString(COLUMN_TITLE));
                news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
                news.setFullText(rs.getString(COLUMN_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
                news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    public News getByPK(Long newsId) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_SELECT_BY_PK);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            News news = null;
            if (rs.next()) {
                news = new News();
                news.setNewsId(newsId);
                news.setTitle(rs.getString(COLUMN_TITLE));
                news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
                news.setFullText(rs.getString(COLUMN_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
                news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public ViewNews getAllByPK(Long newsId,NewsSearchCriteria criteria) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = getPreparedStatementForGetById(criteria, c,newsId);
            rs = ps.executeQuery();
            ViewNews returnNews = new ViewNews();
            if (rs.next()) {
                News news = new News();
                Author author = new Author();

                news.setNewsId(rs.getLong(COLUMN_NEWS_ID));
                news.setTitle(rs.getString(COLUMN_TITLE));
                news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
                news.setFullText(rs.getString(COLUMN_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
                news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
                author.setAuthorId(rs.getLong(COLUMN_AUTHOR_ID));
                author.setAuthorName(rs.getString(COLUMN_AUTHOR_NAME));
                returnNews.setNextNewsId(rs.getLong(COLUMN_NEXT_ID));
                returnNews.setPreNewsId(rs.getLong(COLUMN_PRE_ID));

                returnNews.setTagList(getTagsByNewsId(news.getNewsId()));
                returnNews.setNews(news);
                returnNews.setAuthor(author);
            }
            return returnNews;
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public Author getAuthorByNewsID(Long newsId) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Author author = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_GET_AUTHOR_FOR_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            while (rs.next()) {
                author = new Author();
                author.setAuthorId(rs.getLong(COLUMN_AUTHOR_ID));
                author.setAuthorName(rs.getString(COLUMN_AUTHOR_NAME));
                author.setExpired(rs.getTimestamp(COLUMN_EXPIRED));
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return author;
    }

    public List<Tag> getTagsByNewsId(Long newsId) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Tag> tagList = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_GET_TAGS_FOR_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            tagList = new ArrayList<Tag>();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setTagId(rs.getLong(COLUMN_TAG_ID));
                tag.setTagName(rs.getString(COLUMN_TAG_NAME));
                tagList.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return tagList;
    }

    public List<ListNews> searchAllInformation(NewsSearchCriteria criteria,Integer firstRow, Integer rowsPerPage) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<ListNews> list = null;
        try {
            c = manager.getConnection();
            ps = getPreparedStatementForSearchQuery(criteria, c, QUERY_SELECT_ALL_INFORMATION_BEGIN,firstRow, rowsPerPage);
            rs = ps.executeQuery();
            list = new ArrayList<ListNews>();
            while (rs.next()) {
                News news = new News();
                Author author = new Author();
                ListNews newsForList = new ListNews();

                news.setNewsId(rs.getLong(COLUMN_NEWS_ID));
                news.setTitle(rs.getString(COLUMN_TITLE));
                news.setShortText(rs.getString(COLUMN_SHORT_TEXT));
                news.setFullText(rs.getString(COLUMN_FULL_TEXT));
                news.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
                news.setModificationDate(rs.getDate(COLUMN_MODIFICATION_DATE));
                author.setAuthorId(rs.getLong(COLUMN_AUTHOR_ID));
                author.setAuthorName(rs.getString(COLUMN_AUTHOR_NAME));
                long countOfComments = rs.getLong(COLUMN_POPULARITY);

                newsForList.setNews(news);
                newsForList.setAuthor(author);
                newsForList.setCountOfComments(countOfComments);
                newsForList.setTagList(getTagsByNewsId(news.getNewsId()));
                list.add(newsForList);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return list;
    }

    public long countNews(NewsSearchCriteria criteria) throws DAOCommonException {
        Author author = null;
        List<Tag> tagList = null;
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        long count = 0;
        try {
            c = manager.getConnection();
            ps = getPreparedStatementForCount(criteria, c);
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getLong(COLUMN_QUANTITY);
            }

        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return count;
    }

    private PreparedStatement getPreparedStatementForSearchQuery(NewsSearchCriteria criteria,
                                                                 Connection c, String searchQuery,
                                                                 Integer firstRow, Integer rowsPerPage) throws SQLException {

        StringBuffer sb = new StringBuffer("");
        PreparedStatement ps = null;
        sb.append(searchQuery);
        if ((criteria == null) || (criteria.getAuthor() == null && (criteria.getTagList() == null || criteria.getTagList().size() == 0))) {
            sb.append(QUERY_SELECT_ALL_INFORMATION_END);
            ps = c.prepareStatement(sb.toString());
            ps.setInt(1, firstRow+1);
            ps.setInt(2, firstRow+rowsPerPage);
            return ps;
        } else {
            sb.append(QURERY_WHERE);
            if (criteria.getAuthor() != null) {
                sb.append(QURERY_SEARCH_CRITERIA_AUTHOR);
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    sb.append(QURERY_SEARCH_CRITERIA_AND);
                }
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (int i = 0; i < criteria.getTagList().size(); i++) {
                    if (i == 0) {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG);
                    } else {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG_OR);
                    }
                }
            }
            sb.append(QUERY_SELECT_ALL_INFORMATION_END);
            ps = c.prepareStatement(sb.toString());
            int numberFlag = 1;
            if (criteria.getAuthor() != null) {
                ps.setLong(numberFlag++, criteria.getAuthor().getAuthorId());
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (Tag obj : criteria.getTagList()) {
                    ps.setLong(numberFlag++, obj.getTagId());
                }
            }
            ps.setInt(numberFlag++, firstRow);
            ps.setInt(numberFlag++, firstRow+rowsPerPage);
            return ps;

        }
    }


    private PreparedStatement getPreparedStatementForGetById (NewsSearchCriteria criteria, Connection c,Long newsId) throws SQLException {

        StringBuffer sb = new StringBuffer("");
        PreparedStatement ps = null;
        sb.append(QUERY_SELECT_ALL_BY_PK_BEGIN);
        if ((criteria == null) || (criteria.getAuthor() == null && (criteria.getTagList() == null || criteria.getTagList().size() == 0))) {
            sb.append(QUERY_SELECT_ALL_BY_PK_END);
            ps=c.prepareStatement(sb.toString());
            ps.setLong(1, newsId);
            return ps;
        } else {
            sb.append(QURERY_WHERE);
            if (criteria.getAuthor() != null) {
                sb.append(QURERY_SEARCH_CRITERIA_AUTHOR);
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    sb.append(QUERY_SELECT_ALL_BY_PK_END);
                }
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (int i = 0; i < criteria.getTagList().size(); i++) {
                    if (i == 0) {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG);
                    } else {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG_OR);
                    }
                }
            }
            sb.append(QUERY_SELECT_ALL_BY_PK_END);
            ps = c.prepareStatement(sb.toString());
            int numberFlag = 1;
            if (criteria.getAuthor() != null) {
                ps.setLong(numberFlag++, criteria.getAuthor().getAuthorId());
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (Tag obj : criteria.getTagList()) {
                    ps.setLong(numberFlag++, obj.getTagId());
                }
            }
            ps.setLong(numberFlag, newsId);
            return ps;

        }
    }

    private PreparedStatement getPreparedStatementForCount (NewsSearchCriteria criteria, Connection c) throws SQLException {

        StringBuffer sb = new StringBuffer("");
        PreparedStatement ps = null;
        sb.append(QUERY_COUNT_NEWS);
        if ((criteria == null) || (criteria.getAuthor() == null && (criteria.getTagList() == null || criteria.getTagList().size() == 0))) {
            sb.append(QUERY_COUNT_END);
            return c.prepareStatement(sb.toString());
        } else {
            sb.append(QURERY_WHERE);
            if (criteria.getAuthor() != null) {
                sb.append(QURERY_SEARCH_CRITERIA_AUTHOR);
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    sb.append(QURERY_SEARCH_CRITERIA_AND);
                }
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (int i = 0; i < criteria.getTagList().size(); i++) {
                    if (i == 0) {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG);
                    } else {
                        sb.append(QURERY_SEARCH_CRITERIA_TAG_OR);
                    }
                }
            }
            sb.append(QUERY_COUNT_END);
            ps = c.prepareStatement(sb.toString());
            int numberFlag = 1;
            if (criteria.getAuthor() != null) {
                ps.setLong(numberFlag++, criteria.getAuthor().getAuthorId());
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (Tag obj : criteria.getTagList()) {
                    ps.setLong(numberFlag++, obj.getTagId());
                }
            }
            return ps;

        }
    }
}