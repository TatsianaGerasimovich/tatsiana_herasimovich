package com.epam.gerasimovich.newsPortal.dao.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Class-Entity store the information about <entity>Comments</entity>
 *
 * @author Tatiana
 * @version 1.00 30.09.2015.
 */
public class Comments implements Serializable {
    /**
     * fields of comments entity
     */
    private Long commentId;
    private Long newsId;
    private String commentText;
    private Timestamp creationDate;

    /**
     * Default constructor
     */
    public Comments() {
    }

    /**
     * getter for comment id field
     * @return value of comment id field
     */

    public Long getCommentId() {
        return commentId;
    }

    /**
     * setter for comment id field
     * @param commentId
     */

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    /**
     * getter for news id field
     * @return value of news id field
     */
    public Long getNewsId() {
        return newsId;
    }

    /**
     * setter for news id field
     * @param newsId
     */
    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    /**
     * getter for comment text field
     * @return value of comment text field
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * setter for comment text field
     * @param commentText
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * getter for creation date field
     * @return value of creation date field
     */

    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * setter for creation date field
     * @param creationDate
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comments comments = (Comments) o;

        if (commentId != comments.commentId) return false;
        if (newsId != comments.newsId) return false;
        if (commentText != null ? !commentText.equals(comments.commentText) : comments.commentText != null)
            return false;
        return !(creationDate != null ? !creationDate.equals(comments.creationDate) : comments.creationDate != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (commentId ^ (commentId >>> 32));
        result = 31 * result + (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comments{" +
                "commentId=" + commentId +
                ", newsId=" + newsId +
                ", commentText='" + commentText + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
