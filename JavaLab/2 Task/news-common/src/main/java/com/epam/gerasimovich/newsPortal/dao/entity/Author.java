package com.epam.gerasimovich.newsPortal.dao.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Class-Entity store the information about <entity>Author</entity>
 *
 * @author Tatiana
 * @version 1.00 30.09.2015.
 */
public class Author implements Serializable {
    /**
     * fields of author entity
     */
    private Long authorId;
    private String authorName;
    private Timestamp expired;

    /**
     * Constructor parameters
     */
    public Author(Long authorId, String authorName, Timestamp expired) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.expired = expired;
    }

    /**
     * Default constructor
     */
    public Author() {
    }

    /**
     * getter for author id field
     *
     * @return value of author id field
     */
    public Long getAuthorId() {
        return authorId;
    }

    /**
     * setter for author id field
     *
     * @param authorId
     */
    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    /**
     * getter for author name field
     *
     * @return value of author name field
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     * setter for author name field
     *
     * @param authorName
     */

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    /**
     * getter for expired field
     *
     * @return value of expired field
     */
    public Timestamp getExpired() {
        return expired;
    }

    /**
     * setter for expired field
     *
     * @param expired
     */
    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (authorId != author.authorId) return false;
        if (authorName != null ? !authorName.equals(author.authorName) : author.authorName != null) return false;
        return !(expired != null ? !expired.equals(author.expired) : author.expired != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (authorId ^ (authorId >>> 32));
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                ", expired=" + expired +
                '}';
    }
}
