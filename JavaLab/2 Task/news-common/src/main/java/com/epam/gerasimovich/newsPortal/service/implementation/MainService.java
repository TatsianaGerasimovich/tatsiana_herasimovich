package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class MainService implements IMainService {

    private AuthorsService authorsService;
    private CommentsService commentsService;
    private NewsService newsService;
    private TagsService tagsService;


    public MainService(AuthorsService authorsService, CommentsService commentsService,
                       NewsService newsService, TagsService tagsService) {
        this.authorsService = authorsService;
        this.commentsService = commentsService;
        this.newsService = newsService;
        this.tagsService = tagsService;
    }

    public Long create(News news, Author author, List<Tag> tags) throws ServiceCommonException {
        news.setNewsId(newsService.create(news));
        newsService.contactAuthor(news, author);
        newsService.contactTags(news, tags);
        return news.getNewsId();
    }

    public void update(News news, Author author, List<Tag> tags) throws ServiceCommonException {
        newsService.update(news);
        List<News> list = new ArrayList<News>();
        list.add(news);
        newsService.unbindTags(list);
        newsService.unbindAuthors(list);
        newsService.contactAuthor(news, author);
        newsService.contactTags(news, tags);
    }

    public void delete(List<News> news) throws ServiceCommonException {
        newsService.unbindAuthors(news);
        newsService.unbindTags(news);
        commentsService.deleteByNews(news);
        newsService.delete(news);
    }

    public List<ListNews> getAllNews(Integer firstRow, Integer rowsPerPage) throws ServiceCommonException {
        List<ListNews> modelList = newsService.searchAllInformation(null, firstRow, rowsPerPage);
        return modelList;
    }

    public long countNews(Author author, List<Tag> tags) throws ServiceCommonException {
        NewsSearchCriteria criteria = new NewsSearchCriteria();
        criteria.setAuthor(author);
        criteria.setTagList(tags);
        return newsService.countNews(criteria);
    }

    public ListNews getNewsById(long id) throws ServiceCommonException {
        ListNews newsModel = new ListNews();
        News news = newsService.getNewsById(id);
        newsModel.setNews(news);
        newsModel.setAuthor(newsService.getAuthorByNewsId(news.getNewsId()));
        newsModel.setTagList(newsService.getTagsByNewsId(news.getNewsId()));
        newsModel.setCountOfComments(commentsService.getCommentsByNews(news.getNewsId()).size());
        return newsModel;
    }

    public ViewNews getAllNewsById(long id, Author author, List<Tag> tags) throws ServiceCommonException {
        NewsSearchCriteria criteria = new NewsSearchCriteria();
        criteria.setAuthor(author);
        criteria.setTagList(tags);
        ViewNews news = newsService.getAllByNewsPK(id, criteria);
        news.setCommentList(commentsService.getCommentsByNews(id));
        return news;
    }

    public List<ListNews> search(Author author, List<Tag> tags, Integer firstRow, Integer rowsPerPage) throws ServiceCommonException {
        NewsSearchCriteria criteria = new NewsSearchCriteria();
        criteria.setAuthor(author);
        criteria.setTagList(tags);
        List<ListNews> newsModels = newsService.searchAllInformation(criteria, firstRow, rowsPerPage);
        return newsModels;
    }

    public List<ListNews> searchNews(NewsSearchCriteria criteria, Integer firstRow, Integer rowsPerPage) throws ServiceCommonException {
        List<ListNews> newsModels = newsService.searchAllInformation(criteria, firstRow, rowsPerPage);
        return newsModels;
    }

    public long createAuthor(Author author) throws ServiceCommonException {
        return authorsService.createAuthor(author);
    }

    public List<Author> getAllAuthors() throws ServiceCommonException {
        return authorsService.getAllAuthors();
    }

    public void updateAuthor(Author author) throws ServiceCommonException {
        authorsService.updateAuthor(author);
    }

    public void deleteAuthors(List<Author> authors) throws ServiceCommonException {
        authorsService.deleteAuthors(authors);
    }

    public long createTag(Tag tag) throws ServiceCommonException {
        return tagsService.createTag(tag);
    }

    public void updateTag(Tag tag) throws ServiceCommonException {
        tagsService.updateTag(tag);
    }

    public List<Tag> getAllTags() throws ServiceCommonException {
        return tagsService.getAllTags();
    }

    public void deleteTag(List<Tag> tags) throws ServiceCommonException {
        tagsService.deleteTag(tags);
    }

    public void unbindNews(List<Tag> tags) throws ServiceCommonException {
        tagsService.unbindNews(tags);
    }

    public long createComment(Comments comment) throws ServiceCommonException {
        return commentsService.create(comment);
    }

    public List<Comments> getAllCommentsByNews(long newsId) throws ServiceCommonException {
        return commentsService.getCommentsByNews(newsId);
    }

    public void deleteComment(Comments comment) throws ServiceCommonException {
        List<Comments> comments = new ArrayList<>();
        comments.add(comment);
        commentsService.delete(comments);
    }

    public List<Author> getAllAuthor() throws ServiceCommonException {
        return authorsService.getAllAuthors();
    }

    public Author getAuthorByNewsId(long newsId) throws ServiceCommonException {
        return newsService.getAuthorByNewsId(newsId);
    }
    public List<Tag> getTagsByNewsId(long newsId) throws ServiceCommonException {
            return newsService.getTagsByNewsId(newsId);
    }
}
