package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.AuthorDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.service.IServiceAuthors;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class AuthorsService implements IServiceAuthors {

    private AuthorDao authorsDao;

    public AuthorsService(AuthorDao authorsDao) {
        this.authorsDao = authorsDao;
    }

    public long createAuthor(Author author) throws ServiceCommonException {
        try {
            if (author != null) {
                return authorsDao.create(author);
            } else {
                throw new ServiceCommonException("Null author");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public List<Author> getAllAuthors() throws ServiceCommonException {
        try {
            return authorsDao.getAll();
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void updateAuthor(Author author) throws ServiceCommonException {
        try {
            if (author != null) {
                authorsDao.update(author);
            } else {
                throw new ServiceCommonException("Null author");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void deleteAuthors(List<Author> authors) throws ServiceCommonException {
        try {
            if (authors != null) {
                authorsDao.delete(authors);
            } else {
                throw new ServiceCommonException("Null list of authors");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }
}

