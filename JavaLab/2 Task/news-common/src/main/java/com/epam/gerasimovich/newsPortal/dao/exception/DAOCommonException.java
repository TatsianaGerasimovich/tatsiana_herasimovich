package com.epam.gerasimovich.newsPortal.dao.exception;

import com.epam.gerasimovich.newsPortal.exception.NewsCommonException;

/**
 * Exception class for layer dao
 * @author Tatiana
 * @version 1.00 29.09.2015.
 */
public class DAOCommonException extends NewsCommonException {
    /**
     * default constructor
     */
    public DAOCommonException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public DAOCommonException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public DAOCommonException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public DAOCommonException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public DAOCommonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}