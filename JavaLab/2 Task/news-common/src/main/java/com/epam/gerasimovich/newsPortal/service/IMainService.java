package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
public interface IMainService extends IServiceAuthors,IServiceTags{

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
    Long create(News news, Author author, List<Tag> tags) throws ServiceCommonException;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
    void update(News news, Author author, List<Tag> tags) throws ServiceCommonException;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
    void delete(List<News> news) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<ListNews> getAllNews(Integer firstRow, Integer rowsPerPage) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    ListNews getNewsById(long id) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    ViewNews getAllNewsById(long id,Author author, List<Tag> tags) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<ListNews> search(Author author, List<Tag> tags,Integer firstRow, Integer rowsPerPage) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<ListNews> searchNews(NewsSearchCriteria criteria,Integer firstRow, Integer rowsPerPage) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    long countNews(Author author, List<Tag> tags) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    long createComment(Comments comment) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    void deleteComment(Comments comment) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<Comments> getAllCommentsByNews(long newsId) throws ServiceCommonException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<Tag> getAllTags() throws ServiceCommonException;


    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<Author> getAllAuthor() throws ServiceCommonException;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
    void updateTag(Tag tag) throws ServiceCommonException;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
    void updateAuthor(Author author) throws ServiceCommonException;

    void deleteAuthors(List<Author> authors) throws ServiceCommonException;

    void deleteTag(List<Tag> tags) throws ServiceCommonException;

    long createTag(Tag tag) throws ServiceCommonException;

    long createAuthor(Author author) throws ServiceCommonException;

    Author getAuthorByNewsId(long newsId) throws ServiceCommonException;

    List<Tag> getTagsByNewsId(long newsId) throws ServiceCommonException;
    //when it is necessary to add to this main service methods from other services
    // This Service will execute request by dependency injection
}
