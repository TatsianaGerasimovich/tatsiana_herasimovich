package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.TagDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.pool.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
public class JDBCTagsDao implements TagDao {
    private ConnectionManager manager;
    private final String QUERY_INSERT = "INSERT INTO Tag (TAG_ID, TAG_NAME) " +
            "VALUES (TAGS_SEQ.nextval, ?)";
    private final String QUERY_UPDATE = "UPDATE Tag SET TAG_NAME = ? WHERE TAG_ID = ?";
    private final String QUERY_SELECT_BY_PK = "SELECT TAG_NAME FROM Tag WHERE TAG_ID = ?";
    private final String QUERY_DELETE_TAG = "DELETE FROM Tag WHERE TAG_ID = ?";
    private final String QUERY_SELECT_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM Tag";
    private final String SORTING = " ORDER BY TAG_ID";
    private final String QUERY_UNBIND_NEWS_BY_TAG = "DELETE FROM News_Tag WHERE TAG_ID = ?";
    private final String COLUMN_TAG_ID ="TAG_ID";
    private final String COLUMN_TAG_NAME="TAG_NAME";


    public JDBCTagsDao(ConnectionManager manager) {
        this.manager = manager;
    }

    public Long create(Tag tag) throws DAOCommonException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        long id = 0;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(QUERY_INSERT, new String[]{COLUMN_TAG_ID});
            preparedStatement.setString(1, tag.getTagName());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(preparedStatement, c);
        }
        return id;
    }

    public List<Tag> getAll() throws DAOCommonException {
        Connection c = null;
        Statement st = null;
        List<Tag> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(QUERY_SELECT_ALL_TAGS + SORTING);
            list = new ArrayList<Tag>();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setTagId(rs.getLong(COLUMN_TAG_ID));
                tag.setTagName(rs.getString(COLUMN_TAG_NAME));
                list.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    public Tag getByPK (Long tagId) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_SELECT_BY_PK);
            ps.setLong(1, tagId);
            rs = ps.executeQuery();
            Tag tag = null;
            if (rs.next()) {
                tag = new Tag();
                tag.setTagId(tagId);
                tag.setTagName(rs.getString(COLUMN_TAG_NAME));
            }
            return tag;
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public void update(Tag tag) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_UPDATE);
            ps.setString(1, tag.getTagName());
            ps.setLong(2, tag.getTagId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public void delete(List<Tag> tags) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_DELETE_TAG);
            for (Tag obj : tags) {
                ps.setLong(1, obj.getTagId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public void unbindNews(List<Tag> tags) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_UNBIND_NEWS_BY_TAG);
            for (Tag obj : tags) {
                ps.setLong(1, obj.getTagId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }
}
