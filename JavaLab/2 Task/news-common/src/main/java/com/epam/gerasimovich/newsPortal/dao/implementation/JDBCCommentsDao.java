package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.pool.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
public class JDBCCommentsDao implements CommentDao{
    private ConnectionManager manager;
    private final String QUERY_INSERT = "INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) " +
            "VALUES (COMMENTS_SEQ.nextval, ?,?,?)";
    // + "VALUES (COMMENTS_SEQ.nextval, ?, ?,TO_TIMESTAMP(?, 'DD-MM-YY HH24:MI'));";
    private final String QUERY_UPDATE = "UPDATE Comments SET COMMENT_TEXT = ? WHERE COMMENT_ID = ?";
    private final String QUERY_SELECT_BY_PK = "SELECT NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM Comments WHERE COMMENT_ID = ?";
    private final String QUERY_DELETE_COMMENT = "DELETE FROM Comments WHERE COMMENT_ID = ?";
    private final String QUERY_SELECT_ALL_COMMENTS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM Comments";
    private final String SORTING = " ORDER BY COMMENT_ID";
    private final String QUERY_DELETE_COMMENTS_BY_NEWS_ID = "DELETE FROM Comments WHERE NEWS_ID = ?";
    private final String QUERY_GET_COMMENTS_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE " +
            "FROM Comments WHERE NEWS_ID = ?";
    private final String COLUMN_COMMENT_ID ="COMMENT_ID";
    private final String COLUMN_NEWS_ID="NEWS_ID";
    private final String COLUMN_COMMENT_TEXT="COMMENT_TEXT";
    private final String COLUMN_CREATION_DATE="CREATION_DATE";


    public JDBCCommentsDao(ConnectionManager manager) {
        this.manager = manager;
    }

    public Long create(Comments comment) throws DAOCommonException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        Long id = null;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(QUERY_INSERT, new String[]{COLUMN_COMMENT_ID});
            preparedStatement.setLong(1, comment.getNewsId());
            preparedStatement.setString(2,  comment.getCommentText() );
            preparedStatement.setTimestamp(3, comment.getCreationDate());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(preparedStatement, c);
        }
        return id;
    }

    public List<Comments> getAll() throws DAOCommonException {
        Connection c = null;
        Statement st = null;
        List<Comments> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(QUERY_SELECT_ALL_COMMENTS + SORTING);
            list = new ArrayList<Comments>();
            while (rs.next()) {
                Comments comment = new Comments();
                comment.setCommentId(rs.getLong(COLUMN_COMMENT_ID));
                comment.setNewsId(rs.getLong(COLUMN_NEWS_ID));
                comment.setCommentText(rs.getString(COLUMN_COMMENT_TEXT));
                comment.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
                list.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    public Comments getByPK (Long commentId) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_SELECT_BY_PK);
            ps.setLong(1, commentId);
            rs = ps.executeQuery();
            Comments comment = null;
            if (rs.next()) {
                comment = new Comments();
                comment.setCommentId(commentId);
                comment.setNewsId(rs.getLong(COLUMN_NEWS_ID));
                comment.setCommentText(rs.getString(COLUMN_COMMENT_TEXT));
                comment.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));
            }
            return comment;
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public void update(Comments comment) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_UPDATE);
            ps.setString(1, comment.getCommentText());
            ps.setLong(2, comment.getCommentId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public void delete(List<Comments> comments) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_DELETE_COMMENT);
            for (Comments obj : comments) {
                ps.setLong(1, obj.getCommentId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public void deleteByNewsId(List<News> news) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_DELETE_COMMENTS_BY_NEWS_ID);
            for (News obj : news) {
                ps.setLong(1, obj.getNewsId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(ps, c);
        }
    }

    public List<Comments> getCommentsByNewsId(Long newsId) throws DAOCommonException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Comments> list = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(QUERY_GET_COMMENTS_BY_NEWS_ID + SORTING);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            list = new ArrayList<Comments>();
            while (rs.next()) {
                Comments comment = new Comments();
                comment.setCommentId(rs.getLong(COLUMN_COMMENT_ID));
                comment.setNewsId(rs.getLong(COLUMN_NEWS_ID));
                comment.setCommentText(rs.getString(COLUMN_COMMENT_TEXT));
                comment.setCreationDate(rs.getTimestamp(COLUMN_CREATION_DATE));

                list.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOCommonException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return list;
    }
}
