package com.epam.gerasimovich.newsPortal.service.exception;

import com.epam.gerasimovich.newsPortal.exception.NewsCommonException;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class ServiceCommonException extends NewsCommonException {
    /**
     * default constructor
     */
    public ServiceCommonException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public ServiceCommonException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public ServiceCommonException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public ServiceCommonException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public ServiceCommonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}