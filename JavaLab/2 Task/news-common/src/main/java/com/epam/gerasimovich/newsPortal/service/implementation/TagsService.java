package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.TagDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.service.IServiceTags;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class TagsService implements IServiceTags {

    private TagDao tagsDao;

    public TagsService(TagDao tagsDao) {
        this.tagsDao = tagsDao;
    }

    public long createTag(Tag tag) throws ServiceCommonException {
        try {
            if (tag != null) {
                return tagsDao.create(tag);
            } else {
                throw new ServiceCommonException("Null comment");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void updateTag(Tag tag) throws ServiceCommonException {
        try {
            if (tag != null) {
                tagsDao.update(tag);
            } else {
                throw new ServiceCommonException("Null comment");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public List<Tag> getAllTags() throws ServiceCommonException {
        try {
            return tagsDao.getAll();
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void deleteTag(List<Tag> tags) throws ServiceCommonException {
        try {
            if (tags != null) {
                tagsDao.delete(tags);
            } else {
                throw new ServiceCommonException("Null comment");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }

    public void unbindNews(List<Tag> tags) throws ServiceCommonException {
        try {
            if (tags != null) {
                tagsDao.unbindNews(tags);
            } else {
                throw new ServiceCommonException("Null comment");
            }
        } catch (DAOCommonException e) {
            throw new ServiceCommonException(e);
        }
    }
}