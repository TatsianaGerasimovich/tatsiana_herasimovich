package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/5/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-module-test.xml"})
@DatabaseSetup("classpath:inputData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class JDBCCommentsDaoTest {
    private final static Logger LOG = Logger.getLogger(JDBCCommentsDaoTest.class);
    @Autowired
    private CommentDao commentsDao;

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:commentsTest/updateExpectedDatabase.xml")
    public void updateTest() {
        try {
            Comments c = new Comments();
            c.setCommentId(1L);
            c.setCommentText("badly!");
            c.setCreationDate(new Timestamp(1439806332000L));
            c.setNewsId(1L);
            commentsDao.update(c);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:commentsTest/deleteExpectedDatabase.xml")
    public void deleteTest() {
        Comments comment = new Comments();
        comment.setCommentId(1L);
        List<Comments> list = new ArrayList<>();
        list.add(comment);
        try {
            commentsDao.delete(list);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }
    @Test
    public void getByNewsTest() {
        List<Comments> comments = null;
        try {
            comments = commentsDao.getCommentsByNewsId(102L);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertNotNull(comments);
        Assert.assertEquals(3, comments.size());
    }

    @Test
    public void createTest() {
        Comments comment = new Comments();
        Comments commentRes = null;
        try {
            comment.setNewsId(101L);
            comment.setCommentText("I don't believe");
            comment.setCreationDate(new Timestamp(new Date().getTime() + 60000L));
            long resId = commentsDao.create(comment);
            comment.setCommentId(resId);
            commentRes = commentsDao.getByPK(resId);

        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertEquals(comment, commentRes);
    }

    @Test
    public void getByNewsIdTest() {
        List<Comments> resList = null;
        try {
            resList=commentsDao.getCommentsByNewsId(101L);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertNotNull(resList);
        Assert.assertEquals(2, resList.size());
    }
    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:commentsTest/deleteByNewsIdExpectedDatabase.xml")
    public void deleteByNewsTest() {
        News news = new News();
        news.setNewsId(102L);
        List<News> list = new ArrayList<>();
        list.add(news);
        try {
            commentsDao.deleteByNewsId(list);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

}