package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.TagDao;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Tatiana
 * @version 1.00 10/7/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class TagsServiceTest {
    private final static Logger lOG = Logger.getLogger(TagsServiceTest.class);
    @Mock
    private Tag tag;
    @Mock
    private TagDao tagsDao;
    @Mock
    private List<Tag> tagsList;
    @Mock
    private List<News> newsList;

    private TagsService tagsService;

    @Before
    public void initialize() {
        tagsService=new TagsService(tagsDao);
    }

    @Test
    public void getAllAuthorsTest() {
        try {
            when(tagsDao.getAll()).thenReturn(tagsList);
            List<Tag> listService = tagsService.getAllTags();
            Assert.assertEquals(tagsList, listService);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void createTest() {
        try {
            long id = 1L;
            when(tagsDao.create(any(Tag.class))).thenReturn(id);
            long idService = tagsService.createTag(tag);
            Assert.assertEquals(id, idService);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void updateTest() {
        try {
            doNothing().when(tagsDao).update(any(Tag.class));
            tagsService.updateTag(tag);
            verify(tagsDao).update(tag);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(tagsDao).delete(anyList());
            tagsService.deleteTag(tagsList);
            verify(tagsDao).delete(tagsList);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }

    @Test
    public void unbindTest() {
        try {
            doNothing().when(tagsDao).delete(anyList());
            tagsService.unbindNews(tagsList);
            verify(tagsDao).unbindNews(tagsList);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
}