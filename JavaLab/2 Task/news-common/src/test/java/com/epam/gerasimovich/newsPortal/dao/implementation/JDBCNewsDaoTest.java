package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.NewsDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/5/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-module-test.xml"})
@DatabaseSetup("classpath:inputData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class JDBCNewsDaoTest {
    private final static Logger LOG = Logger.getLogger(JDBCCommentsDaoTest.class);
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private CommentDao commentsDao;

    @Test
    public void getAllTest() {
        List<News> resList = null;
        try {
            resList = newsDao.getAll();
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertNotNull(resList);
        Assert.assertEquals(2, resList.size());
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/updateExpectedDatabase.xml")
    public void updateTest() {
        News news = new News();
        news.setNewsId(102L);
        news.setTitle("berries");
        news.setShortText("About berries");
        news.setFullText("berries are helpful");
        news.setCreationDate(new Timestamp(1439802732000L));
        news.setModificationDate(new java.sql.Date(1442437200000L));
        try {
            newsDao.update(news);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/deleteExpectedDatabase.xml")
    public void deleteTest() {
        News news = new News();
        news.setNewsId(101L);
        List<News> list = new ArrayList<>();
        list.add(news);
        try {
            commentsDao.deleteByNewsId(list);
            newsDao.unbindTags(list);
            newsDao.unbindAuthors(list);
            newsDao.delete(list);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void insertTest() {
        News news = new News();
        news.setTitle("Baries");
        news.setShortText("About baries");
        news.setFullText("Baries are helpful");
        news.setCreationDate(new Timestamp(new Date().getTime() + 60000L));
        news.setModificationDate(new java.sql.Date(new Date().getTime() + 360000L));
        News resNews = null;
        try {
            long resId = newsDao.create(news);
            news.setNewsId(resId);
            resNews = newsDao.getByPK(resId);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertEquals(news, resNews);
    }

    @Test
    public void getByIdTest() {
        ViewNews news = new ViewNews();
        news.setNews(new News(101L, "Vegetables", "About vegetables",
                "vegetables are helpful", Timestamp.valueOf("2015-07-25 14:17:25.0"),
                java.sql.Date.valueOf( "2015-08-17" )));
        news.setNextNewsId(0L);
        news.setPreNewsId(102L);
        news.setAuthor(new Author(1L, "Tanya", null));
        List <Tag> tags=new ArrayList<Tag>();
        tags.add(new Tag(101L, "vegetables"));
        tags.add(new Tag(103L, "helpful"));
        news.setTagList(tags);
        ViewNews resNews=null;
        try {
            resNews = newsDao.getAllByPK(101L,null);

        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertEquals(news, resNews);
    }

    @Test
    public void getAuthorByNewsIdTest() {
        Author author = new Author();
        Author resAuthor = null;
        author.setAuthorId(1L);
        author.setAuthorName("Tanya");
        author.setExpired(new Timestamp(1441868400000L));
        try {
            resAuthor = newsDao.getAuthorByNewsID(101L);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertEquals(author, resAuthor);
    }

    @Test
    public void getTagsByNewsIdTest() {
        Tag tag1 = new Tag();
        tag1.setTagId(101L);
        tag1.setTagName("vegetables");
        Tag tag2 = new Tag();
        tag2.setTagId(103L);
        tag2.setTagName("helpful");
        List<Tag> list = new ArrayList<>();
        list.add(tag1);
        list.add(tag2);
        List<Tag> resList = null;
        try {
            resList = newsDao.getTagsByNewsId(101L);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertEquals(list, resList);
    }


    @Test
    public void searchAllInfotmationTest() {
        Author author = new Author();
        author.setAuthorId(1L);
        Tag tag = new Tag();
        tag.setTagId(103L);
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        NewsSearchCriteria criteria = new NewsSearchCriteria();
        criteria.setAuthor(author);
        criteria.setTagList(tags);
        List<ListNews> resNews = null;
        try {
            resNews = newsDao.searchAllInformation(criteria,1,2);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertEquals(1, resNews.size());
    }

    @Test
    public void countNewsTest() {
        long quantity = 0;
        Author author = new Author();
        author.setAuthorId(1L);
        Tag tag = new Tag();
        tag.setTagId(103L);
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        NewsSearchCriteria criteria = new NewsSearchCriteria();
        criteria.setAuthor(author);
        criteria.setTagList(tags);
        try {
            quantity = newsDao.countNews(criteria);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertEquals(quantity, 1);
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/unbindAuthorsExpectedDatabase.xml")
    public void unbindAuthorsTest() {
        News news = new News();
        news.setNewsId(101L);
        List<News> list = new ArrayList<>();
        list.add(news);
        try {
            newsDao.unbindAuthors(list);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/unbindTagsExpectedDatabase.xml")
    public void unbindTagsTest() {
        News news = new News();
        news.setNewsId(101L);
        List<News> list = new ArrayList<>();
        list.add(news);
        try {
            newsDao.unbindTags(list);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/contactAuthorExpectedDatabase.xml")
    public void setAuthorTest() {
        try {
            News news = new News();
            news.setNewsId(101L);
            Author author = new Author();
            author.setAuthorId(3L);
            newsDao.bindAuthor(news, author);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:newsTest/contactTagsExpectedDatabase.xml")
    public void contactTagsTest() {
        try {
            News news = new News();
            news.setNewsId(101L);
            Tag tag = new Tag();
            tag.setTagId(101L);
            List<Tag> tagList = new ArrayList<>();
            tagList.add(tag);
            newsDao.bindTags(news, tagList);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }
}