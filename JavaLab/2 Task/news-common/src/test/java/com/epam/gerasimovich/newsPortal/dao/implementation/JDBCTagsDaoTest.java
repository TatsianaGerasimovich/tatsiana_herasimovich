package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.TagDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/5/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-module-test.xml"})
@DatabaseSetup("classpath:inputData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class JDBCTagsDaoTest {
    private final static Logger LOG = Logger.getLogger(JDBCTagsDaoTest.class);
    @Autowired
    private TagDao tagsDao;

    @Test
    public void insertTest() {
        Tag tag = new Tag();
        tag.setTagName("berries");
        Tag resTag = null;
        try {
            long resId = tagsDao.create(tag);
            tag.setTagId(resId);
            resTag=tagsDao.getByPK(resId);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertEquals(tag, resTag);
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:tagsTest/updateExpectedDatabase.xml")
    public void updateTest() {
        Tag tag = new Tag();
        tag.setTagId(102L);
        tag.setTagName("berries");
        try {
            tagsDao.update(tag);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:authorTest/deleteExpectedDatabase.xml")
    public void deleteTest() {
        Tag tag = new Tag();
        tag.setTagId(103L);
        List<Tag> list = new ArrayList<>();
        list.add(tag);
        try {
            tagsDao.unbindNews(list);
            tagsDao.delete(list);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getAllTest() {
        List<Tag> resList = null;
        try {
            resList = tagsDao.getAll();
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
        Assert.assertNotNull(resList);
        Assert.assertEquals(3, resList.size());
    }

    @Test
    @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:tagsTest/unbindExpectedDatabase.xml")
    public void unbindNewsTest() {
        Tag tag = new Tag();
        tag.setTagId(103L);
        List<Tag> list = new ArrayList<>();
        list.add(tag);
        try {
            tagsDao.unbindNews(list);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }


}