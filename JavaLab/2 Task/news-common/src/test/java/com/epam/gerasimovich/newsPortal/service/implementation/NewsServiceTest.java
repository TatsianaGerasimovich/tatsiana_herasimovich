package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.NewsDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSearchCriteria;
import com.epam.gerasimovich.newsPortal.valueObject.ListNews;
import com.epam.gerasimovich.newsPortal.valueObject.ViewNews;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Tatiana
 * @version 1.00 10/7/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

    private final static Logger LOG = Logger.getLogger(NewsServiceTest.class);
    @Mock
    private News news;
    @Mock
    private NewsDao newsDao;
    @Mock
    private List<News> newsList;
    @Mock
    private Author author;
    @Mock
    private List<Tag> tagList;
    @Mock
    private NewsSearchCriteria criteria;
    @Mock
    private List<ListNews> listNewsAll;
    @Mock
    private ViewNews newsAll;

    private NewsService newsService;

    @Before
    public void initialize() {
        newsService = new NewsService(newsDao);
    }

    @Test
    public void createTest() {
        try {
            long id = 1L;
            when(newsDao.create(any(News.class))).thenReturn(id);
            long resId = newsService.create(news);
            Assert.assertEquals(id, resId);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void updateTest() {
        try {
            doNothing().when(newsDao).update(any(News.class));
            newsService.update(news);
            verify(newsDao).update(news);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(newsDao).delete(anyList());
            newsService.delete(newsList);
            verify(newsDao).delete(newsList);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void unbindAuthorsTest() {
        try {
            doNothing().when(newsDao).unbindAuthors(anyList());
            newsService.unbindAuthors(newsList);
            verify(newsDao).unbindAuthors(newsList);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }


    @Test
    public void unbindTagsTest() {
        try {
            doNothing().when(newsDao).unbindTags(anyList());
            newsService.unbindTags(newsList);
            verify(newsDao).unbindTags(newsList);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getById() {
        try {
            long id = 1L;
            when(newsDao.getByPK(anyLong())).thenReturn(news);
            News news = newsService.getNewsById(id);
            verify(newsDao).getByPK(id);
            Assert.assertEquals(this.news, news);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getAllById() {
        try {
            long id = 1L;
            when(newsDao.getAllByPK(anyLong(),any(NewsSearchCriteria.class))).thenReturn(newsAll);
            ViewNews news = newsService.getAllByNewsPK(id,criteria);
            verify(newsDao).getAllByPK(id,criteria);
            Assert.assertEquals(this.newsAll, news);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getAllTest() {
        try {
            when(newsDao.getAll()).thenReturn(newsList);
            List<News> newsList = newsService.getAllNews();
            Assert.assertEquals(this.newsList, newsList);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void contactAuthorTest() {
        try {
            doNothing().when(newsDao).bindAuthor(any(News.class), any(Author.class));
            newsService.contactAuthor(news, author);
            verify(newsDao).bindAuthor(news, author);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void contactTagsTest() {
        try {
            doNothing().when(newsDao).bindTags(any(News.class), anyList());
            newsService.contactTags(news, tagList);
            verify(newsDao).bindTags(news, tagList);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getAuthorTest() {
        try {
            long id = 1L;
            when(newsDao.getAuthorByNewsID(anyLong())).thenReturn(author);
            Author author = newsService.getAuthorByNewsId(id);
            Assert.assertEquals(this.author, author);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getTagsByNewsIdTest() {
        try {
            long id = 1L;
            when(newsDao.getTagsByNewsId(anyLong())).thenReturn(tagList);
            List<Tag> list = newsService.getTagsByNewsId(id);
            Assert.assertEquals(list, tagList);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }


    @Test
    public void searchAllInformationTest() {
        try {
            when(newsDao.searchAllInformation(any(NewsSearchCriteria.class),anyInt(),anyInt())).thenReturn(listNewsAll);
            List<ListNews> list = newsService.searchAllInformation(criteria,1,4);
            verify(newsDao).searchAllInformation(criteria,1,4);
            Assert.assertEquals(listNewsAll, list);
        } catch (DAOCommonException | ServiceCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void countTest() {
        try {
            long count = 25L;
            when(newsDao.countNews(any(NewsSearchCriteria.class))).thenReturn(count);
            long countRes = newsDao.countNews(criteria);
            verify(newsDao).countNews(criteria);
            Assert.assertEquals(count, countRes);
        } catch (DAOCommonException e) {
            LOG.error(e, e.getCause());
        }
    }

}