package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.AuthorDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Tatiana
 * @version 1.00 10/7/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorsServiceTest {
    private final static Logger lOG = Logger.getLogger(AuthorsServiceTest.class);

    @Mock
    private Author author;
    @Mock
    private AuthorDao authorDao;
    @Mock
    private List<Author> authorList;

    private AuthorsService authorsService;

    @Before
    public void initialize() {
        authorsService =new AuthorsService(authorDao);
    }

    @Test
    public void getAllAuthorsTest() {
        try {
            when(authorDao.getAll()).thenReturn(authorList);
            List<Author> listService = authorsService.getAllAuthors();
            Assert.assertEquals(authorList, listService);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void createTest() {
        try {
            long id = 1L;
            when(authorDao.create(any(Author.class))).thenReturn(id);
            long idService = authorsService.createAuthor(author);
            Assert.assertEquals(id, idService);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void updateTest() {
        try {
            doNothing().when(authorDao).update(any(Author.class));
            authorsService.updateAuthor(author);
            verify(authorDao).update(author);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(authorDao).delete(anyList());
            authorsService.deleteAuthors(authorList);
            verify(authorDao).delete(authorList);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
}