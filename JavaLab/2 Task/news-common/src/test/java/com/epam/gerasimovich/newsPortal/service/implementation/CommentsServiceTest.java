package com.epam.gerasimovich.newsPortal.service.implementation;
import com.epam.gerasimovich.newsPortal.dao.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOCommonException;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceCommonException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentsServiceTest {
    private final static Logger lOG = Logger.getLogger(CommentsServiceTest.class);
    @Mock
    private Comments comment;
    @Mock
    private CommentDao commentsDao;
    @Mock
    private List<Comments> commentList;
    @Mock
    private List<News> newsList;

    private CommentsService commentsService;

    @Before
    public void initialize() {
        commentsService=new CommentsService(commentsDao);
    }

    @Test
    public void getAllCommentsTest() {
        try {
            when(commentsDao.getAll()).thenReturn(commentList);
            List<Comments> listService = commentsService.getAllComments();
            Assert.assertEquals(commentList, listService);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void getCommentsByNewsTest() {
        try {
            when(commentsDao.getCommentsByNewsId(anyLong())).thenReturn(commentList);
            long id = 1L;
            List<Comments> listService = commentsService.getCommentsByNews(id);
            Assert.assertEquals(commentList, listService);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void createTest() {
        try {
            long id = 1L;
            when(commentsDao.create(any(Comments.class))).thenReturn(id);
            long idService = commentsService.create(comment);
            Assert.assertEquals(id, idService);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void updateTest() {
        try {
            doNothing().when(commentsDao).update(any(Comments.class));
            commentsService.update(comment);
            verify(commentsDao).update(comment);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(commentsDao).delete(anyList());
            commentsService.delete(commentList);
            verify(commentsDao).delete(commentList);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void deleteByNewsTest() {
        try {
            doNothing().when(commentsDao).deleteByNewsId(newsList);
            commentsService.deleteByNews(newsList);
            verify(commentsDao).deleteByNewsId(newsList);
        } catch (DAOCommonException | ServiceCommonException e) {
            lOG.error(e, e.getCause());
        }
    }
}