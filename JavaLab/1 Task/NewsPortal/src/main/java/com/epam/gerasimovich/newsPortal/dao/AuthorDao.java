package com.epam.gerasimovich.newsPortal.dao;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;

import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
public interface AuthorDao extends GenericDao<Author>{

    long create(Author object) throws DAOException;

    List<Author> getAll() throws  DAOException;

    void update(Author object) throws  DAOException;

    void delete(List<Author> objects) throws  DAOException;

    Author getByPK(long key) throws DAOException;

}
