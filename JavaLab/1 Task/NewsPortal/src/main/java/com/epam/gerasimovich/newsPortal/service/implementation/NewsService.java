package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.NewsDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSeachCriteria;
import com.epam.gerasimovich.newsPortal.model.NewsForList;
import com.epam.gerasimovich.newsPortal.model.NewsForView;
import com.epam.gerasimovich.newsPortal.service.IServiceNews;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class NewsService implements IServiceNews {

    private NewsDao newsDao;

    public NewsService(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    public long create(News news) throws ServiceException {
        try {
            if (news != null) {
                return newsDao.create(news);
            } else {
                throw new ServiceException("Null news");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void update(News news) throws ServiceException {
        try {
            if (news != null) {
                newsDao.update(news);
            } else {
                throw new ServiceException("Null news");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void delete(List<News> news) throws ServiceException {
        try {
            if (news != null) {
                newsDao.delete(news);
            } else {
                throw new ServiceException("Null news");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void unbindAuthors(List<News> news) throws ServiceException {
        try {
            if (news != null) {
                newsDao.unbindAuthors(news);
            } else {
                throw new ServiceException("Null news");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void unbindTags(List<News> news) throws ServiceException {
        try {
            if (news != null) {
                newsDao.unbindTags(news);
            } else {
                throw new ServiceException("Null news");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void contactAuthor(News news, Author author) throws ServiceException {
        try {
            if (news != null && author != null) {
                newsDao.contactAuthor(news, author);
            } else {
                throw new ServiceException("Null news or author");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Author getAuthorByNewsId(long newsId) throws ServiceException {
        try {
            return newsDao.getAuthorByNewsID(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void contactTags(News news, List<Tag> tags) throws ServiceException {
        try {
            if (news != null && tags != null) {
                newsDao.contactTags(news, tags);
            } else {
                throw new ServiceException("Null news or author");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Tag> getTagsByNewsId(long newsId) throws ServiceException {
        try {
            return newsDao.getTagsByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<News> getAllNews() throws ServiceException {
        try {
            return newsDao.getAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<NewsForList> getAllInformation() throws ServiceException {
        try {
            return newsDao.getAllInformation();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public News getNewsById(long id) throws ServiceException {
        News news;
        try {
            news = newsDao.getByPK(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return news;
    }

    public NewsForView getAllByNewsPK(long newsId) throws ServiceException {
        NewsForView news;
        try {
            news = newsDao.getAllByPK(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return news;
    }

    public List<News> searchNews(NewsSeachCriteria searchCriteria) throws ServiceException {
        try {
            return newsDao.search(searchCriteria);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<NewsForList> searchAllInformation(NewsSeachCriteria searchCriteria) throws ServiceException {
        try {
            return newsDao.searchAllInformation(searchCriteria);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public long countNews(NewsSeachCriteria criteria) throws ServiceException {
        try {
            return newsDao.countNews(criteria);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
