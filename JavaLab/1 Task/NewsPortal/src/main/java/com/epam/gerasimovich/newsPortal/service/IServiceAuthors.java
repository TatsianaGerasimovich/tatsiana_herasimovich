package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public interface IServiceAuthors {

    long create(Author author) throws ServiceException;

    void update(Author author) throws ServiceException;

    void delete(List<Author> authors) throws ServiceException;

    List<Author> getAll() throws ServiceException;
}
