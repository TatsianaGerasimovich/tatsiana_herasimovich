package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.TagDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.dao.pool.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
public class JDBCTagsDao implements TagDao {
    private ConnectionManager manager;
    private final String INSERT = "INSERT INTO Tag (TAG_ID, TAG_NAME) " +
            "VALUES (TAGS_SEQ.nextval, ?)";
    private final String UPDATE = "UPDATE Tag SET TAG_NAME = ? WHERE TAG_ID = ?";
    private final String SELECT_BY_PK = "SELECT TAG_NAME FROM Tag WHERE TAG_ID = ?";
    private final String DELETE_TAG = "DELETE FROM Tag WHERE TAG_ID = ?";
    private final String SELECT_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM Tag";
    private final String SORTING = " ORDER BY TAG_ID";
    private final String UNBIND_NEWS_BY_TAG = "DELETE FROM News_Tag WHERE TAG_ID = ?";



    public JDBCTagsDao(ConnectionManager manager) {
        this.manager = manager;
    }

    public long create(Tag object) throws DAOException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        long id = 0;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(INSERT, new String[]{"TAG_ID"});
            preparedStatement.setString(1, object.getTagName());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, preparedStatement, c);
        }
        return id;
    }

    public List<Tag> getAll() throws DAOException {
        Connection c = null;
        Statement st = null;
        List<Tag> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(SELECT_ALL_TAGS + SORTING);
            list = new ArrayList<Tag>();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setTagId(rs.getLong("TAG_ID"));
                tag.setTagName(rs.getString("TAG_NAME"));
                list.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    public Tag getByPK (long id) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(SELECT_BY_PK);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            Tag tag = null;
            if (rs.next()) {
                tag = new Tag();
                tag.setTagId(id);
                tag.setTagName(rs.getString("TAG_NAME"));
            }
            return tag;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public void update(Tag object) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UPDATE);
            ps.setString(1, object.getTagName());
            ps.setLong(2, object.getTagId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public void delete(List<Tag> objects) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_TAG);
            for (Tag obj : objects) {
                ps.setLong(1, obj.getTagId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public void unbindNews(List<Tag> tags) throws DAOException{
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UNBIND_NEWS_BY_TAG);
            for (Tag obj : tags) {
                ps.setLong(1, obj.getTagId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }
}
