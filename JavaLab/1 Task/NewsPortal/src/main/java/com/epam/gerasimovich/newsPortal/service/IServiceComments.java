package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public interface IServiceComments {
    long create(Comments comment) throws ServiceException;

    List<Comments> getAllComments() throws ServiceException;

    List<Comments> getCommentsByNews(long newsId) throws ServiceException;

    void update(Comments comment) throws ServiceException;

    void delete(List<Comments> comments) throws ServiceException;

    void deleteByNews(List<News> news) throws ServiceException;
}
