package com.epam.gerasimovich.newsPortal.service.exception;

import com.epam.gerasimovich.newsPortal.exception.NewsException;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class ServiceException extends NewsException {
    /**
     * default constructor
     */
    public ServiceException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public ServiceException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}