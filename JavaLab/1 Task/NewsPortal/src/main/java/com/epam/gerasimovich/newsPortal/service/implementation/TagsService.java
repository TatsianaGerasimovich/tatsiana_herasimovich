package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.TagDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.service.IServiceTags;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class TagsService implements IServiceTags {

    private TagDao tagsDao;

    public TagsService(TagDao tagsDao) {
        this.tagsDao = tagsDao;
    }

    public long create(Tag tag) throws ServiceException {
        try {
            if (tag != null) {
                return tagsDao.create(tag);
            } else {
                throw new ServiceException("Null comment");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void update(Tag tag) throws ServiceException {
        try {
            if (tag != null) {
                tagsDao.update(tag);
            } else {
                throw new ServiceException("Null comment");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Tag> getAll() throws ServiceException {
        try {
            return tagsDao.getAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void delete(List<Tag> tags) throws ServiceException {
        try {
            if (tags != null) {
                tagsDao.delete(tags);
            } else {
                throw new ServiceException("Null comment");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void unbindNews(List<Tag> tags) throws ServiceException {
        try {
            if (tags != null) {
                tagsDao.unbindNews(tags);
            } else {
                throw new ServiceException("Null comment");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}