package com.epam.gerasimovich.newsPortal.exception;

/**
 * @author Tatiana
 * @version 1.00 30.09.2015.
 */
public class NewsException extends Exception {
    /**
     * default constructor
     */
    public NewsException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public NewsException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public NewsException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public NewsException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public NewsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

