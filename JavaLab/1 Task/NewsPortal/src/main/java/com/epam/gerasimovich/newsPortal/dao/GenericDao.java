package com.epam.gerasimovich.newsPortal.dao;

import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 28.09.2015.
 */
public interface GenericDao<T> {
    /**
     * Method creates a new entry, the corresponding object
     */
    public long create(T object) throws DAOException;

    /**
     * Method gets the appropriate record with a primary key or a null key
     */
    public T getByPK(long key) throws DAOException;

    /**
     * Method saves the state of the object in the database
     */
    public void update(T object) throws DAOException;

    /**
     * Removes a record of the object from the database
     */
    public void delete(List<T> objects) throws DAOException;

    /**
     * Returns a list of all appropriate records in the database
     */
    public List<T> getAll() throws DAOException;

}
