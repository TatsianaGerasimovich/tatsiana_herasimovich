package com.epam.gerasimovich.newsPortal.dao;


import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;

import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
public interface CommentDao extends GenericDao<Comments>{
    long create(Comments object) throws DAOException;

    List<Comments> getAll() throws  DAOException;

    void update(Comments object) throws  DAOException;

    void delete(List<Comments> objects) throws  DAOException;

    List<Comments> getCommentsByNewsId(Long newsId) throws DAOException;

    void deleteByNewsId(List<News> news) throws DAOException;
}
