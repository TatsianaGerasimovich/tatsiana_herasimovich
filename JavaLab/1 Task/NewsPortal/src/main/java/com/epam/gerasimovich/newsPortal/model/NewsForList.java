package com.epam.gerasimovich.newsPortal.model;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class NewsForList {
    private News news;
    private Author author;
    private List<Tag> tagList;
    private long countOfComments;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public long getCountOfComments() {
        return countOfComments;
    }

    public void setCountOfComments(long countOfComments) {
        this.countOfComments = countOfComments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsForList that = (NewsForList) o;

        if (countOfComments != that.countOfComments) return false;
        if (news != null ? !news.equals(that.news) : that.news != null) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        return !(tagList != null ? !tagList.equals(that.tagList) : that.tagList != null);

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (tagList != null ? tagList.hashCode() : 0);
        result = 31 * result + (int) (countOfComments ^ (countOfComments >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "NewsForList{" +
                "news=" + news +
                ", author=" + author +
                ", tagList=" + tagList +
                ", countOfComments=" + countOfComments +
                '}';
    }
}

