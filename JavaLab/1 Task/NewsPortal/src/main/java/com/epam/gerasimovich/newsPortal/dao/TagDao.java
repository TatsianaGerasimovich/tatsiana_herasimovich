package com.epam.gerasimovich.newsPortal.dao;

import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;

import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
public interface TagDao extends GenericDao<Tag>{

    long create(Tag object) throws DAOException;

    List<Tag> getAll() throws  DAOException;

    void update(Tag object) throws  DAOException;

    void delete(List<Tag> objects) throws  DAOException;

    void unbindNews(List<Tag> tags) throws DAOException;

}
