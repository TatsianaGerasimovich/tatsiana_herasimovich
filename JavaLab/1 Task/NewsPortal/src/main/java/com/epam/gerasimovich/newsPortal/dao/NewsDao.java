package com.epam.gerasimovich.newsPortal.dao;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSeachCriteria;
import com.epam.gerasimovich.newsPortal.model.NewsForList;
import com.epam.gerasimovich.newsPortal.model.NewsForView;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 02.10.2015
 */
public interface NewsDao extends GenericDao<News> {

    long create(News object) throws DAOException;

    void update(News object) throws DAOException;

    void delete(List<News> objects) throws DAOException;

    void unbindAuthors(List<News> news) throws DAOException;

    void unbindTags(List<News> news) throws DAOException;

    void contactAuthor(News news, Author author) throws DAOException;

    Author getAuthorByNewsID(Long newsId) throws DAOException;

    void contactTags(News news, List<Tag> tags) throws DAOException;

    List<Tag> getTagsByNewsId(Long newsId) throws DAOException;

    List<News> getAll() throws DAOException;

    List<NewsForList> getAllInformation() throws DAOException;

    News getByPK(long id) throws DAOException;

    NewsForView getAllByPK(long id) throws DAOException;

    List<News> search(NewsSeachCriteria criteria) throws DAOException;

    List<NewsForList> searchAllInformation(NewsSeachCriteria criteria) throws DAOException;

    long countNews(NewsSeachCriteria criteria) throws DAOException;
}
