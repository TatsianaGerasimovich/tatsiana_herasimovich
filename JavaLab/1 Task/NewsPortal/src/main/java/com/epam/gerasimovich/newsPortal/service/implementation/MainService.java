package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSeachCriteria;
import com.epam.gerasimovich.newsPortal.model.NewsForList;
import com.epam.gerasimovich.newsPortal.model.NewsForView;
import com.epam.gerasimovich.newsPortal.service.IMainService;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class MainService implements IMainService {

    private AuthorsService authorsService;
    private CommentsService commentsService;
    private NewsService newsService;
    private TagsService tagsService;

    public MainService(AuthorsService authorsService, CommentsService commentsService,
                       NewsService newsService, TagsService tagsService) {
        this.authorsService = authorsService;
        this.commentsService = commentsService;
        this.newsService = newsService;
        this.tagsService = tagsService;
    }

    public void create(News news, Author author, List<Tag> tags) throws ServiceException {
        news.setNewsId(newsService.create(news));
        newsService.contactAuthor(news, author);
        newsService.contactTags(news, tags);
    }

    public void update(News news, Author author, List<Tag> tags) throws ServiceException {
        newsService.update(news);
        List<News> list = new ArrayList<News>();
        list.add(news);
        newsService.unbindTags(list);
        newsService.unbindAuthors(list);
        newsService.contactAuthor(news, author);
        newsService.contactTags(news, tags);
    }

    public void delete(List<News> news) throws ServiceException {
        newsService.unbindAuthors(news);
        newsService.unbindTags(news);
        commentsService.deleteByNews(news);
        newsService.delete(news);
    }

    public List<NewsForList> getAllNews() throws ServiceException {
        List<NewsForList> modelList = newsService.getAllInformation();
        new ArrayList<>();
        return modelList;
    }

    public NewsForList getNewsById(long id) throws ServiceException {
        NewsForList newsModel = new NewsForList();
        News news = newsService.getNewsById(id);
        newsModel.setNews(news);
        newsModel.setAuthor(newsService.getAuthorByNewsId(news.getNewsId()));
        newsModel.setTagList(newsService.getTagsByNewsId(news.getNewsId()));
        newsModel.setCountOfComments(commentsService.getCommentsByNews(news.getNewsId()).size());
        return newsModel;
    }

    public  NewsForView getAllNewsById(long id) throws ServiceException {
        NewsForView news=newsService.getAllByNewsPK(id);
        news.setCommentList(commentsService.getCommentsByNews(id));
       return news;
    }

    public List<NewsForList> search(Author author, List<Tag> tags) throws ServiceException {
        NewsSeachCriteria criteria = new NewsSeachCriteria();
        criteria.setAuthor(author);
        criteria.setTagList(tags);
        List<NewsForList> newsModels =newsService.searchAllInformation(criteria);
        return newsModels;
    }


}
