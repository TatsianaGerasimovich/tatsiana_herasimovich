package com.epam.gerasimovich.newsPortal.dao.exception;

import com.epam.gerasimovich.newsPortal.exception.NewsException;

/**
 * Exception class for layer dao
 * @author Tatiana
 * @version 1.00 29.09.2015.
 */
public class DAOException extends NewsException {
    /**
     * default constructor
     */
    public DAOException() {
    }

    /**
     * constructor parameters
     * @param message
     */
    public DAOException(String message) {
        super(message);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor parameters
     * @param cause
     */
    public DAOException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor parameters
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}