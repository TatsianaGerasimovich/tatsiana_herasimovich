package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.AuthorDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.service.IServiceAuthors;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class AuthorsService implements IServiceAuthors {

    private AuthorDao authorsDao;

    public AuthorsService(AuthorDao authorsDao) {
        this.authorsDao = authorsDao;
    }

    public long create(Author author) throws ServiceException {
        try {
            if (author != null) {
                return authorsDao.create(author);
            } else {
                throw new ServiceException("Null author");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Author> getAll() throws ServiceException {
        try {
            return authorsDao.getAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void update(Author author) throws ServiceException {
        try {
            if (author != null) {
                authorsDao.update(author);
            } else {
                throw new ServiceException("Null author");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void delete(List<Author> authors) throws ServiceException {
        try {
            if (authors != null) {
                authorsDao.delete(authors);
            } else {
                throw new ServiceException("Null list of authors");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}

