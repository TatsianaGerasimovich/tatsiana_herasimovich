package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.service.IServiceComments;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public class CommentsService implements IServiceComments{

    private CommentDao commentsDao;

    public CommentsService(CommentDao commentsDao) {
        this.commentsDao = commentsDao;
    }

    public long create(Comments comment) throws ServiceException {
        try {
            if (comment != null) {
                return commentsDao.create(comment);
            } else {
                throw new ServiceException("Null comment");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Comments> getAllComments() throws ServiceException {
        try {
            return commentsDao.getAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Comments> getCommentsByNews(long newsId) throws ServiceException {
        try {
            return commentsDao.getCommentsByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void update(Comments comment) throws ServiceException {
        try {
            if (comment != null) {
                commentsDao.update(comment);
            } else {
                throw new ServiceException("Null comment");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void delete(List<Comments> comments) throws ServiceException {
        try {
            if (comments != null) {
                commentsDao.delete(comments);
            } else {
                throw new ServiceException("Null comment");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void deleteByNews(List<News> news) throws ServiceException {
        try {
            if (news != null) {
                commentsDao.deleteByNewsId(news);
            } else {
                throw new ServiceException("Null comment");
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
