package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.dao.pool.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
public class JDBCCommentsDao implements CommentDao{
    private ConnectionManager manager;
    private final String INSERT = "INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) " +
            "VALUES (COMMENTS_SEQ.nextval, ?,?,?)";
    // + "VALUES (COMMENTS_SEQ.nextval, ?, ?,TO_TIMESTAMP(?, 'DD-MM-YY HH24:MI'));";
    private final String UPDATE = "UPDATE Comments SET COMMENT_TEXT = ? WHERE COMMENT_ID = ?";
    private final String SELECT_BY_PK = "SELECT NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM Comments WHERE COMMENT_ID = ?";
    private final String DELETE_COMMENT = "DELETE FROM Comments WHERE COMMENT_ID = ?";
    private final String SELECT_ALL_COMMENTS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM Comments";
    private final String SORTING = " ORDER BY COMMENT_ID";
    private final String DELETE_COMMENTS_BY_NEWS_ID = "DELETE FROM Comments WHERE NEWS_ID = ?";
    private final String GET_COMMENTS_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE " +
            "FROM Comments WHERE NEWS_ID = ?";

    public JDBCCommentsDao(ConnectionManager manager) {
        this.manager = manager;
    }

    public long create(Comments object) throws DAOException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        long id = 0;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(INSERT, new String[]{"COMMENT_ID"});
            preparedStatement.setLong(1, object.getNewsId());
            preparedStatement.setString(2,  object.getCommentText() );
            preparedStatement.setTimestamp(3, object.getCreationDate());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, preparedStatement, c);
        }
        return id;
    }

    public List<Comments> getAll() throws DAOException {
        Connection c = null;
        Statement st = null;
        List<Comments> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(SELECT_ALL_COMMENTS + SORTING);
            list = new ArrayList<Comments>();
            while (rs.next()) {
                Comments comment = new Comments();
                comment.setCommentId(rs.getLong("COMMENT_ID"));
                comment.setNewsId(rs.getLong("NEWS_ID"));
                comment.setCommentText(rs.getString("COMMENT_TEXT"));
                comment.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                list.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    public Comments getByPK (long id) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(SELECT_BY_PK);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            Comments comment = null;
            if (rs.next()) {
                comment = new Comments();
                comment.setCommentId(id);
                comment.setNewsId(rs.getLong("NEWS_ID"));
                comment.setCommentText(rs.getString("COMMENT_TEXT"));
                comment.setCreationDate(rs.getTimestamp("CREATION_DATE"));
            }
            return comment;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public void update(Comments object) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UPDATE);
            ps.setString(1, object.getCommentText());
            ps.setLong(2, object.getCommentId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public void delete(List<Comments> objects) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_COMMENT);
            for (Comments obj : objects) {
                ps.setLong(1, obj.getCommentId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public void deleteByNewsId(List<News> news) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_COMMENTS_BY_NEWS_ID);
            for (News obj : news) {
                ps.setLong(1, obj.getNewsId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public List<Comments> getCommentsByNewsId(Long newsId) throws DAOException{
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Comments> list = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(GET_COMMENTS_BY_NEWS_ID + SORTING);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            list = new ArrayList<Comments>();
            while (rs.next()) {
                Comments comment = new Comments();
                comment.setCommentId(rs.getLong("COMMENT_ID"));
                comment.setNewsId(rs.getLong("NEWS_ID"));
                comment.setCommentText(rs.getString("COMMENT_TEXT"));
                comment.setCreationDate(rs.getTimestamp("CREATION_DATE"));

                list.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return list;
    }
}
