package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.AuthorDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.dao.pool.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 02.10.2015.
 */
public class JDBCAuthorsDao implements AuthorDao {
    private ConnectionManager manager;
    private final String INSERT = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) " +
            "VALUES (AUTHORS_SEQ.nextval, ?)";
    private final String UPDATE = "UPDATE AUTHOR SET AUTHOR_NAME = ? WHERE AUTHOR_ID = ?";
    private final String SELECT_BY_PK = "SELECT AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
    private final String DELETE_AUTHOR = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
    private final String SELECT_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";
    private final String SORTING = " ORDER BY AUTHOR_ID";

    public JDBCAuthorsDao(ConnectionManager manager) {
        this.manager = manager;
    }

    public long create(Author object) throws DAOException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        long id = 0;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(INSERT, new String[]{"AUTHOR_ID"});
            preparedStatement.setString(1, object.getAuthorName());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, preparedStatement, c);
        }
        return id;
    }

    public List<Author> getAll() throws DAOException {
        Connection c = null;
        Statement st = null;
        List<Author> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(SELECT_ALL_AUTHORS + SORTING);
            list = new ArrayList<Author>();
            while (rs.next()) {
                Author author = new Author();
                author.setAuthorId(rs.getLong("AUTHOR_ID"));
                author.setAuthorName(rs.getString("AUTHOR_NAME"));
                author.setExpired(rs.getTimestamp("EXPIRED"));
                list.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    public Author getByPK (long id) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(SELECT_BY_PK);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            Author author = null;
            if (rs.next()) {
                author = new Author();
                author.setAuthorId(id);
                author.setAuthorName(rs.getString("AUTHOR_NAME"));
                author.setExpired(rs.getTimestamp("EXPIRED"));
            }
            return author;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public void update(Author object) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UPDATE);
            ps.setString(1, object.getAuthorName());
            ps.setLong(2, object.getAuthorId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public void delete(List<Author> objects) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_AUTHOR);
            for (Author obj : objects) {
                ps.setTimestamp(1, obj.getExpired());
                ps.setLong(2, obj.getAuthorId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }
}