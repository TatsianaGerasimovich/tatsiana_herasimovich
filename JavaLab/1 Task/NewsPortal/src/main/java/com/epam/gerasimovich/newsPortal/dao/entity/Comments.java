package com.epam.gerasimovich.newsPortal.dao.entity;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Tatiana on 30.09.2015.
 */
public class Comments implements Serializable {
    private long commentId;
    private long newsId;
    private String commentText;
    private Timestamp creationDate;
    public Comments() {
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public long getNewsId() {
        return newsId;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comments comments = (Comments) o;

        if (commentId != comments.commentId) return false;
        if (newsId != comments.newsId) return false;
        if (commentText != null ? !commentText.equals(comments.commentText) : comments.commentText != null)
            return false;
        return !(creationDate != null ? !creationDate.equals(comments.creationDate) : comments.creationDate != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (commentId ^ (commentId >>> 32));
        result = 31 * result + (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comments{" +
                "commentId=" + commentId +
                ", newsId=" + newsId +
                ", commentText='" + commentText + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
