package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.model.NewsForList;
import com.epam.gerasimovich.newsPortal.model.NewsForView;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
public interface IMainService {

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
    void create(News news, Author author, List<Tag> tags) throws ServiceException;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
    void update(News news, Author author, List<Tag> tags) throws ServiceException;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = {RuntimeException.class, Exception.class})
    void delete(List<News> news) throws ServiceException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<NewsForList> getAllNews() throws ServiceException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    NewsForList getNewsById(long id) throws ServiceException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    NewsForView getAllNewsById(long id) throws ServiceException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = {RuntimeException.class, Exception.class})
    List<NewsForList> search(Author author, List<Tag> tags) throws ServiceException;

    //when it is necessary to add to this main service methods from other services
    // This Service will execute request by dependency injection
}
