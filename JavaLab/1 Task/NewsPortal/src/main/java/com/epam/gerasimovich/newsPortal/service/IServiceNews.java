package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSeachCriteria;
import com.epam.gerasimovich.newsPortal.model.NewsForList;
import com.epam.gerasimovich.newsPortal.model.NewsForView;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public interface IServiceNews {

    long create(News news) throws ServiceException;

    void update(News news) throws ServiceException;

    void delete(List<News> news) throws ServiceException;

    void unbindAuthors(List<News> news) throws ServiceException;

    void unbindTags(List<News> news) throws ServiceException;

    News getNewsById(long id) throws ServiceException;

    NewsForView getAllByNewsPK(long newsId) throws ServiceException;

    List<News> getAllNews() throws ServiceException;

    List<NewsForList> getAllInformation() throws ServiceException;

    void contactAuthor(News news, Author author) throws ServiceException;

    Author getAuthorByNewsId(long newsId) throws ServiceException;

    void contactTags(News news, List<Tag> tags) throws ServiceException;

    List<Tag> getTagsByNewsId(long newsId) throws ServiceException;

    List<News> searchNews(NewsSeachCriteria searchCriteria) throws ServiceException;

    List<NewsForList> searchAllInformation(NewsSeachCriteria searchCriteria) throws ServiceException;

    long countNews(NewsSeachCriteria criteria) throws ServiceException;
}
