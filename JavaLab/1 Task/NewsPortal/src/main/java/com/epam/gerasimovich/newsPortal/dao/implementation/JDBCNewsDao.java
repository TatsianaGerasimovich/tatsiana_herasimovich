package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.CommentDao;
import com.epam.gerasimovich.newsPortal.dao.NewsDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.dao.pool.ConnectionManager;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSeachCriteria;
import com.epam.gerasimovich.newsPortal.model.NewsForList;
import com.epam.gerasimovich.newsPortal.model.NewsForView;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatiana on 04.10.2015.
 */
public class JDBCNewsDao implements NewsDao {
    private final String INSERT = "INSERT INTO News (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) " +
            "VALUES (NEWS_SEQ.nextval, ?, ?, ?, ?, ?)";
    // + "VALUES (NEWS_SEQ.nextval, ?, ?, ?, TO_TIMESTAMP(?, 'DD-MM-YY HH24:MI'),?);";
    private final String SELECT_BY_PK = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE" +
            " FROM News WHERE NEWS_ID = ?";
    private final String SELECT_ALL_BY_PK = "SELECT\n" +
            "  NEWS_ID,\n" +
            "  TITLE,\n" +
            "  SHORT_TEXT,\n" +
            "  FULL_TEXT,\n" +
            "  CREATION_DATE,\n" +
            "  MODIFICATION_DATE,\n" +
            "  AUTHOR_ID,\n" +
            "  AUTHOR_NAME,\n" +
            "  NEXT_ID,\n" +
            "  PRE_ID\n" +
            "FROM (\n" +
            "  SELECT\n" +
            "    NEWS_ID,\n" +
            "    TITLE,\n" +
            "    SHORT_TEXT,\n" +
            "    FULL_TEXT,\n" +
            "    CREATION_DATE,\n" +
            "    MODIFICATION_DATE,\n" +
            "    AUTHOR_ID,\n" +
            "    AUTHOR_NAME,\n" +
            "    POPULARITY,\n" +
            "    lead(NEWS_ID)\n" +
            "    OVER (ORDER BY POPULARITY)\n" +
            "      AS NEXT_ID,\n" +
            "    lag(NEWS_ID)\n" +
            "    OVER (ORDER BY POPULARITY)\n" +
            "      AS PRE_ID\n" +
            "  FROM (\n" +
            "    SELECT DISTINCT\n" +
            "      N.NEWS_ID,\n" +
            "      N.TITLE,\n" +
            "      N.SHORT_TEXT,\n" +
            "      N.FULL_TEXT,\n" +
            "      N.CREATION_DATE,\n" +
            "      N.MODIFICATION_DATE,\n" +
            "      A.AUTHOR_ID,\n" +
            "      A.AUTHOR_NAME,\n" +
            "      (SELECT COUNT(*)\n" +
            "       FROM Comments C\n" +
            "       WHERE C.NEWS_ID = N.NEWS_ID) AS POPULARITY\n" +
            "    FROM News N\n" +
            "      JOIN News_Tag NT ON N.NEWS_ID = NT.NEWS_ID\n" +
            "      JOIN News_Author NA ON N.NEWS_ID = NA.NEWS_ID\n" +
            "      JOIN AUTHOR A ON NA.AUTHOR_ID = A.AUTHOR_ID\n" +
            "  )\n" +
            ")\n" +
            "WHERE NEWS_ID = ?";
    private String INSERT_CONTACT_AUTHOR = "INSERT INTO News_Author (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
    private String INSERT_CONTACT_TAG = "INSERT INTO News_Tag (NEWS_ID, TAG_ID) VALUES (?, ?)";
    private String UPDATE_NEWS = "UPDATE News " +
            "SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, " +
            "CREATION_DATE = ?, MODIFICATION_DATE = ? " +
            "WHERE NEWS_ID = ?";
    private String DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
    private String UNBIND_AUTHOR_BY_NEWS = "DELETE FROM News_Author WHERE NEWS_ID = ?";
    private String UNBIND_TAGS_BY_NEWS = "DELETE FROM News_Tag WHERE NEWS_ID = ?";
    private String GET_AUTHOR_FOR_NEWS = "SELECT A.AUTHOR_ID, A.AUTHOR_NAME, A.EXPIRED FROM News_Author NA " +
            "JOIN AUTHOR A ON A.AUTHOR_ID = NA.AUTHOR_ID " +
            "WHERE NA.NEWS_ID = ? ORDER BY A.AUTHOR_ID";
    private String GET_TAGS_FOR_NEWS = "SELECT T.TAG_ID, T.TAG_NAME FROM News_Tag NT " +
            "JOIN Tag T ON T.TAG_ID = NT.TAG_ID " +
            "WHERE NT.NEWS_ID = ? ORDER BY T.TAG_ID";
    private String SELECT_ALL_NEWS = "SELECT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, " +
            "N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, " +
            "(SELECT COUNT(*) FROM Comments C " +
            "WHERE C.NEWS_ID = N.NEWS_ID) AS POPULARITY " +
            "FROM News N";
    private String SELECT_ALL_INFORMATION = " SELECT DISTINCT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, " +
            "N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, " +
            "A.AUTHOR_ID, A.AUTHOR_NAME, " +
            "(SELECT COUNT(*) FROM Comments C " +
            "WHERE C.NEWS_ID = N.NEWS_ID) AS POPULARITY " +
            "FROM News N " +
            "JOIN News_Tag NT ON N.NEWS_ID = NT.NEWS_ID " +
            "JOIN News_Author NA ON N.NEWS_ID = NA.NEWS_ID " +
            "JOIN AUTHOR A ON NA.AUTHOR_ID = A.AUTHOR_ID ";
    private String SEARCH_NEWS = "SELECT DISTINCT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, " +
            "N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, " +
            "(SELECT COUNT(*) FROM Comments C " +
            "WHERE C.NEWS_ID = N.NEWS_ID) AS POPULARITY " +
            "FROM News N " +
            "JOIN News_Tag NT ON N.NEWS_ID = NT.NEWS_ID " +
            "JOIN News_Author NA ON N.NEWS_ID = NA.NEWS_ID ";
    private String SEARCH_ALL_INFORMATION = "SELECT DISTINCT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, " +
            "N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, " +
            "A.AUTHOR_ID, A.AUTHOR_NAME, " +
            "(SELECT COUNT(*) FROM Comments C " +
            "WHERE C.NEWS_ID = N.NEWS_ID) AS POPULARITY " +
            "FROM News N " +
            "JOIN News_Tag NT ON N.NEWS_ID = NT.NEWS_ID " +
            "JOIN News_Author NA ON N.NEWS_ID = NA.NEWS_ID " +
            "JOIN AUTHOR A ON NA.AUTHOR_ID = A.AUTHOR_ID ";
    private String SORTING = " ORDER BY POPULARITY";
    private String COUNT_NEWS = "SELECT COUNT(DISTINCT N.NEWS_ID) AS QUANTITY" +
            " FROM NEWS N " +
            "JOIN NEWS_TAG NT ON N.NEWS_ID = NT.NEWS_ID " +
            "JOIN NEWS_AUTHOR NA ON N.NEWS_ID = NA.NEWS_ID ";

    private ConnectionManager manager;

    public JDBCNewsDao(ConnectionManager manager) {
        this.manager = manager;
    }

    public long create(News object) throws DAOException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        long id = 0;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(INSERT, new String[]{"NEWS_ID"});
            preparedStatement.setString(1, object.getTitle());
            preparedStatement.setString(2, object.getShortText());
            preparedStatement.setString(3, object.getFullText());
            preparedStatement.setTimestamp(4, object.getCreationDate());
            preparedStatement.setDate(5, object.getModificationDate());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, preparedStatement, c);
        }
        return id;
    }

    public void update(News object) throws DAOException {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        try {
            c = manager.getConnection();
            preparedStatement = c.prepareStatement(UPDATE_NEWS);
            preparedStatement.setString(1, object.getTitle());
            preparedStatement.setString(2, object.getShortText());
            preparedStatement.setString(3, object.getFullText());
            preparedStatement.setTimestamp(4, object.getCreationDate());
            preparedStatement.setDate(5, object.getModificationDate());
            preparedStatement.setLong(6, object.getNewsId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, preparedStatement, c);
        }
    }

    public void delete(List<News> objects) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(DELETE_NEWS);
            for (News obj : objects) {
                ps.setLong(1, obj.getNewsId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public void unbindAuthors(List<News> news) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UNBIND_AUTHOR_BY_NEWS);
            for (News obj : news) {
                ps.setLong(1, obj.getNewsId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public void unbindTags(List<News> news) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UNBIND_TAGS_BY_NEWS);
            for (News obj : news) {
                ps.setLong(1, obj.getNewsId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public void contactAuthor(News news, Author author) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UNBIND_AUTHOR_BY_NEWS);
            ps.setLong(1, news.getNewsId());
            ps.executeUpdate();
            ps = c.prepareStatement(INSERT_CONTACT_AUTHOR);
            ps.setLong(1, news.getNewsId());
            ps.setLong(2, author.getAuthorId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public void contactTags(News news, List<Tag> tags) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(UNBIND_TAGS_BY_NEWS);
            ps.setLong(1, news.getNewsId());
            ps.executeUpdate();
            ps = c.prepareStatement(INSERT_CONTACT_TAG);
            for (Tag t : tags) {
                ps.setLong(1, news.getNewsId());
                ps.setLong(2, t.getTagId());
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(null, ps, c);
        }
    }

    public List<News> getAll() throws DAOException {
        Connection c = null;
        Statement st = null;
        List<News> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(SELECT_ALL_NEWS + SORTING);
            list = new ArrayList<News>();
            while (rs.next()) {
                News news = new News();
                news.setNewsId(rs.getLong("NEWS_ID"));
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    public List<NewsForList> getAllInformation() throws DAOException {
        Connection c = null;
        Statement st = null;
        List<NewsForList> list = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            st = c.createStatement();
            rs = st.executeQuery(SELECT_ALL_INFORMATION + SORTING);
            list = new ArrayList<NewsForList>();
            while (rs.next()) {
                News news = new News();
                Author author = new Author();
                NewsForList newsForList = new NewsForList();

                news.setNewsId(rs.getLong("NEWS_ID"));
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
                author.setAuthorId(rs.getLong("AUTHOR_ID"));
                author.setAuthorName(rs.getString("AUTHOR_NAME"));
                long countOfComments = rs.getLong("POPULARITY");

                newsForList.setNews(news);
                newsForList.setAuthor(author);
                newsForList.setCountOfComments(countOfComments);
                newsForList.setTagList(getTagsByNewsId(news.getNewsId()));
                list.add(newsForList);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, st, c);
        }
        return list;
    }

    public News getByPK(long id) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(SELECT_BY_PK);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            News news = null;
            if (rs.next()) {
                news = new News();
                news.setNewsId(id);
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
            }
            return news;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public NewsForView getAllByPK(long id) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(SELECT_ALL_BY_PK);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            NewsForView returnNews = new NewsForView();
            if (rs.next()) {
                News news = new News();
                Author author = new Author();

                news.setNewsId(rs.getLong("NEWS_ID"));
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
                author.setAuthorId(rs.getLong("AUTHOR_ID"));
                author.setAuthorName(rs.getString("AUTHOR_NAME"));
               // Long next=rs.getLong("NEXT_ID");
               // Long pre=rs.getLong("PRE_ID");
              returnNews.setNextNewsId(rs.getLong("NEXT_ID"));
                returnNews.setPreNewsId(rs.getLong("PRE_ID"));

                returnNews.setTagList(getTagsByNewsId(news.getNewsId()));
                returnNews.setNews(news);
                returnNews.setAuthor(author);
            }
            return returnNews;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
    }

    public Author getAuthorByNewsID(Long newsId) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Author author = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(GET_AUTHOR_FOR_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            while (rs.next()) {
                author = new Author();
                author.setAuthorId(rs.getLong("AUTHOR_ID"));
                author.setAuthorName(rs.getString("AUTHOR_NAME"));
                author.setExpired(rs.getTimestamp("EXPIRED"));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return author;
    }

    public List<Tag> getTagsByNewsId(Long newsId) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Tag> tagList = null;
        try {
            c = manager.getConnection();
            ps = c.prepareStatement(GET_TAGS_FOR_NEWS);
            ps.setLong(1, newsId);
            rs = ps.executeQuery();
            tagList = new ArrayList<Tag>();
            while (rs.next()) {
                Tag tag = new Tag();
                tag.setTagId(rs.getLong("TAG_ID"));
                tag.setTagName(rs.getString("TAG_NAME"));
                tagList.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return tagList;
    }

    public List<News> search(NewsSeachCriteria criteria) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<News> list = null;
        try {
            c = manager.getConnection();
            ps = getPreparedStatementForSearchQuery(criteria, c, SEARCH_NEWS);
            rs = ps.executeQuery();
            list = new ArrayList<News>();
            while (rs.next()) {
                News news = new News();
                news.setNewsId(rs.getLong("NEWS_ID"));
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return list;
    }

    public List<NewsForList> searchAllInformation(NewsSeachCriteria criteria) throws DAOException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<NewsForList> list = null;
        try {
            c = manager.getConnection();
            ps = getPreparedStatementForSearchQuery(criteria, c, SEARCH_ALL_INFORMATION);
            rs = ps.executeQuery();
            list = new ArrayList<NewsForList>();
            while (rs.next()) {
                News news = new News();
                Author author = new Author();
                NewsForList newsForList = new NewsForList();

                news.setNewsId(rs.getLong("NEWS_ID"));
                news.setTitle(rs.getString("TITLE"));
                news.setShortText(rs.getString("SHORT_TEXT"));
                news.setFullText(rs.getString("FULL_TEXT"));
                news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
                news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
                author.setAuthorId(rs.getLong("AUTHOR_ID"));
                author.setAuthorName(rs.getString("AUTHOR_NAME"));
                long countOfComments = rs.getLong("POPULARITY");

                newsForList.setNews(news);
                newsForList.setAuthor(author);
                newsForList.setCountOfComments(countOfComments);
                newsForList.setTagList(getTagsByNewsId(news.getNewsId()));
                list.add(newsForList);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return list;
    }

    public long countNews(NewsSeachCriteria criteria) throws DAOException {
        Author author = null;
        List<Tag> tagList = null;
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        long count = 0;
        try {
            c = manager.getConnection();
            ps = getPreparedStatementForCount(criteria, c);
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getLong("QUANTITY");
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            manager.releaseResources(rs, ps, c);
        }
        return count;
    }

    private PreparedStatement getPreparedStatementForSearchQuery(NewsSeachCriteria criteria, Connection c, String searchQuery) throws SQLException {

        StringBuffer sb = new StringBuffer("");
        PreparedStatement ps = null;
        sb.append(searchQuery);
        if ((criteria == null) || (criteria.getAuthor() == null && (criteria.getTagList() == null || criteria.getTagList().size() == 0))) {
            sb.append(SORTING);
            return c.prepareStatement(sb.toString());
        } else {
            sb.append("WHERE ");
            if (criteria.getAuthor() != null) {
                sb.append("NA.AUTHOR_ID = ?");
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    sb.append(" AND ");
                }
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (int i = 0; i < criteria.getTagList().size(); i++) {
                    if (i == 0) {
                        sb.append("NT.TAG_ID = ?");
                    } else {
                        sb.append(" OR NT.TAG_ID = ?");
                    }
                }
            }
            sb.append(SORTING);
            ps = c.prepareStatement(sb.toString());
            int numberFlag = 1;
            if (criteria.getAuthor() != null) {
                ps.setLong(numberFlag++, criteria.getAuthor().getAuthorId());
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (Tag obj : criteria.getTagList()) {
                    ps.setLong(numberFlag++, obj.getTagId());
                }
            }
            return ps;

        }
    }


    private PreparedStatement getPreparedStatementForCount(NewsSeachCriteria criteria, Connection c) throws SQLException {

        StringBuffer sb = new StringBuffer("");
        PreparedStatement ps = null;
        sb.append(COUNT_NEWS);
        if ((criteria == null) || (criteria.getAuthor() == null && (criteria.getTagList() == null || criteria.getTagList().size() == 0))) {
            return c.prepareStatement(sb.toString());
        } else {
            sb.append("WHERE ");
            if (criteria.getAuthor() != null) {
                sb.append("NA.AUTHOR_ID = ?");
                if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                    sb.append(" AND ");
                }
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (int i = 0; i < criteria.getTagList().size(); i++) {
                    if (i == 0) {
                        sb.append("NT.TAG_ID = ?");
                    } else {
                        sb.append(" OR NT.TAG_ID = ?");
                    }
                }
            }
            ps = c.prepareStatement(sb.toString());
            int numberFlag = 1;
            if (criteria.getAuthor() != null) {
                ps.setLong(numberFlag++, criteria.getAuthor().getAuthorId());
            }
            if (criteria.getTagList() != null && criteria.getTagList().size() != 0) {
                for (Tag obj : criteria.getTagList()) {
                    ps.setLong(numberFlag++, obj.getTagId());
                }
            }
            return ps;

        }
    }
}