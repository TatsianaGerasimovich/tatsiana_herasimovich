package com.epam.gerasimovich.newsPortal.service;

import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;

import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 10/6/2015
 */
public interface IServiceTags {
    long create(Tag tag) throws ServiceException;

    void update(Tag tag) throws ServiceException;

    List<Tag> getAll() throws ServiceException;

    void delete(List<Tag> tags) throws ServiceException;

    void unbindNews(List<Tag> tags) throws ServiceException;
}
