package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.NewsDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.dao.util.NewsSeachCriteria;
import com.epam.gerasimovich.newsPortal.model.NewsForList;
import com.epam.gerasimovich.newsPortal.model.NewsForView;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Tatiana
 * @version 1.00 10/7/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

    private final static Logger LOG = Logger.getLogger(NewsServiceTest.class);
    @Mock
    private News news;
    @Mock
    private NewsDao newsDao;
    @Mock
    private List<News> newsList;
    @Mock
    private Author author;
    @Mock
    private List<Tag> tagList;
    @Mock
    private NewsSeachCriteria criteria;
    @Mock
    private List<NewsForList> listNewsAll;
    @Mock
    private NewsForView newsAll;

    private NewsService newsService;

    @Before
    public void initialize() {
        newsService = new NewsService(newsDao);
    }

    @Test
    public void createTest() {
        try {
            long id = 1L;
            when(newsDao.create(any(News.class))).thenReturn(id);
            long resId = newsService.create(news);
            Assert.assertEquals(id, resId);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void updateTest() {
        try {
            doNothing().when(newsDao).update(any(News.class));
            newsService.update(news);
            verify(newsDao).update(news);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(newsDao).delete(anyList());
            newsService.delete(newsList);
            verify(newsDao).delete(newsList);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void unbindAuthorsTest() {
        try {
            doNothing().when(newsDao).unbindAuthors(anyList());
            newsService.unbindAuthors(newsList);
            verify(newsDao).unbindAuthors(newsList);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }


    @Test
    public void unbindTagsTest() {
        try {
            doNothing().when(newsDao).unbindTags(anyList());
            newsService.unbindTags(newsList);
            verify(newsDao).unbindTags(newsList);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getById() {
        try {
            long id = 1L;
            when(newsDao.getByPK(anyLong())).thenReturn(news);
            News news = newsService.getNewsById(id);
            verify(newsDao).getByPK(id);
            Assert.assertEquals(this.news, news);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getAllById() {
        try {
            long id = 1L;
            when(newsDao.getAllByPK(anyLong())).thenReturn(newsAll);
            NewsForView news = newsService.getAllByNewsPK(id);
            verify(newsDao).getAllByPK(id);
            Assert.assertEquals(this.newsAll, news);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getAllTest() {
        try {
            when(newsDao.getAll()).thenReturn(newsList);
            List<News> newsList = newsService.getAllNews();
            Assert.assertEquals(this.newsList, newsList);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getAllInformationTest() {
        try {
            when(newsDao.getAllInformation()).thenReturn(listNewsAll);
            List<NewsForList> newsList = newsService.getAllInformation();
            Assert.assertEquals(this.listNewsAll, newsList);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void contactAuthorTest() {
        try {
            doNothing().when(newsDao).contactAuthor(any(News.class), any(Author.class));
            newsService.contactAuthor(news, author);
            verify(newsDao).contactAuthor(news, author);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void contactTagsTest() {
        try {
            doNothing().when(newsDao).contactTags(any(News.class), anyList());
            newsService.contactTags(news, tagList);
            verify(newsDao).contactTags(news, tagList);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getAuthorTest() {
        try {
            long id = 1L;
            when(newsDao.getAuthorByNewsID(anyLong())).thenReturn(author);
            Author author = newsService.getAuthorByNewsId(id);
            Assert.assertEquals(this.author, author);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void getTagsByNewsIdTest() {
        try {
            long id = 1L;
            when(newsDao.getTagsByNewsId(anyLong())).thenReturn(tagList);
            List<Tag> list = newsService.getTagsByNewsId(id);
            Assert.assertEquals(list, tagList);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void searchTest() {
        try {
            when(newsDao.search(any(NewsSeachCriteria.class))).thenReturn(newsList);
            List<News> list = newsService.searchNews(criteria);
            verify(newsDao).search(criteria);
            Assert.assertEquals(newsList, list);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void searchAllInformationTest() {
        try {
            when(newsDao.searchAllInformation(any(NewsSeachCriteria.class))).thenReturn(listNewsAll);
            List<NewsForList> list = newsService.searchAllInformation(criteria);
            verify(newsDao).searchAllInformation(criteria);
            Assert.assertEquals(listNewsAll, list);
        } catch (DAOException | ServiceException e) {
            LOG.error(e, e.getCause());
        }
    }

    @Test
    public void countTest() {
        try {
            long count = 25L;
            when(newsDao.countNews(any(NewsSeachCriteria.class))).thenReturn(count);
            long countRes = newsDao.countNews(criteria);
            verify(newsDao).countNews(criteria);
            Assert.assertEquals(count, countRes);
        } catch (DAOException e) {
            LOG.error(e, e.getCause());
        }
    }

}