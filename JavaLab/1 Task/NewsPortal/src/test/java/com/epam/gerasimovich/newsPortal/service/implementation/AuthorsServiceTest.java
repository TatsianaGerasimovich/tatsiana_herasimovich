package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.AuthorDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.entity.Comments;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Tatiana
 * @version 1.00 10/7/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorsServiceTest {
    private final static Logger lOG = Logger.getLogger(AuthorsServiceTest.class);

    @Mock
    private Author author;
    @Mock
    private AuthorDao authorDao;
    @Mock
    private List<Author> authorList;

    private AuthorsService commentsService;

    @Before
    public void initialize() {
        commentsService=new AuthorsService(authorDao);
    }

    @Test
    public void getAllAuthorsTest() {
        try {
            when(authorDao.getAll()).thenReturn(authorList);
            List<Author> listService = commentsService.getAll();
            Assert.assertEquals(authorList, listService);
        } catch (DAOException | ServiceException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void createTest() {
        try {
            long id = 1L;
            when(authorDao.create(any(Author.class))).thenReturn(id);
            long idService = commentsService.create(author);
            Assert.assertEquals(id, idService);
        } catch (DAOException | ServiceException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void updateTest() {
        try {
            doNothing().when(authorDao).update(any(Author.class));
            commentsService.update(author);
            verify(authorDao).update(author);
        } catch (DAOException | ServiceException e) {
            lOG.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        try {
            doNothing().when(authorDao).delete(anyList());
            commentsService.delete(authorList);
            verify(authorDao).delete(authorList);
        } catch (DAOException | ServiceException e) {
            lOG.error(e, e.getCause());
        }
    }
}