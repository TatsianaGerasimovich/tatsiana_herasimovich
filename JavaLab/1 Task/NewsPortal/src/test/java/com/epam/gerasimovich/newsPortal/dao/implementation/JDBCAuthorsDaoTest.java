package com.epam.gerasimovich.newsPortal.dao.implementation;

import com.epam.gerasimovich.newsPortal.dao.AuthorDao;
import com.epam.gerasimovich.newsPortal.dao.entity.Author;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tatiana
 * @version 1.00 04.10.2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-module-test.xml"})
@DatabaseSetup("classpath:inputData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class JDBCAuthorsDaoTest {
        private static final Logger LOG = Logger.getLogger(JDBCAuthorsDaoTest.class);
        @Autowired
        private AuthorDao authorsDao;

        @Test
        @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED,value="classpath:authorTest/updateExpectedDatabase.xml")
        public void updateTest() {
                Author author = new Author();
                author.setAuthorId(1L);
                author.setAuthorName("Elly");
                author.setExpired(new Timestamp(1444392600L));
                try {
                        authorsDao.update(author);
                } catch (DAOException e) {
                        LOG.error(e, e.getCause());
                }
        }
        @Test
        @ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED, value = "classpath:authorTest/deleteExpectedDatabase.xml")
        public void deleteTest() {
                Author author = new Author();
                author.setAuthorId(2L);
                author.setExpired(new Timestamp(1441879200L));
                List<Author> list = new ArrayList<>();
                list.add(author);
                try {
                        authorsDao.delete(list);
                } catch (DAOException e) {
                        LOG.error(e, e.getCause());
                }
        }
        @Test
        public void getAllTest() {
                List<Author> resList = null;
                try {
                        resList = authorsDao.getAll();
                } catch (DAOException e) {
                        LOG.error(e, e.getCause());
                }
                Assert.assertNotNull(resList);
                Assert.assertEquals(3, resList.size());
        }
        @Test
        public void insertTest() {
                Author author = new Author();
                author.setAuthorName("Jenny");
                Author resAuthor = null;
                try {
                        long resId = authorsDao.create(author);
                        author.setAuthorId(resId);
                        resAuthor=authorsDao.getByPK(resId);

                } catch (DAOException e) {
                        LOG.error(e, e.getCause());
                }
                Assert.assertEquals(author, resAuthor);
        }
}