package com.epam.gerasimovich.newsPortal.service.implementation;

import com.epam.gerasimovich.newsPortal.dao.TagDao;
import com.epam.gerasimovich.newsPortal.dao.entity.News;
import com.epam.gerasimovich.newsPortal.dao.entity.Tag;
import com.epam.gerasimovich.newsPortal.dao.exception.DAOException;
import com.epam.gerasimovich.newsPortal.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Tatiana
 * @version 1.00 10/7/2015
 */
@RunWith(MockitoJUnitRunner.class)
public class TagsServiceTest {
    private final static Logger lOG = Logger.getLogger(TagsServiceTest.class);
    @Mock
    private Tag tag;
    @Mock
    private TagDao tagsDao;
    @Mock
    private List<Tag> tagsList;
    @Mock
    private List<News> newsList;

    @Test
    public void getAllAuthorsTest() {
        TagsService tagsService=new TagsService(tagsDao);
        try {
            when(tagsDao.getAll()).thenReturn(tagsList);
            List<Tag> listService = tagsService.getAll();
            Assert.assertEquals(tagsList, listService);
        } catch (DAOException | ServiceException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void createTest() {
        TagsService tagsService=new TagsService(tagsDao);
        try {
            long id = 1L;
            when(tagsDao.create(any(Tag.class))).thenReturn(id);
            long idService = tagsService.create(tag);
            Assert.assertEquals(id, idService);
        } catch (DAOException | ServiceException e) {
            lOG.error(e, e.getCause());
        }
    }
    @Test
    public void updateTest() {
        TagsService tagsService=new TagsService(tagsDao);
        try {
            doNothing().when(tagsDao).update(any(Tag.class));
            tagsService.update(tag);
            verify(tagsDao).update(tag);
        } catch (DAOException | ServiceException e) {
            lOG.error(e, e.getCause());
        }
    }

    @Test
    public void deleteTest() {
        TagsService tagsService=new TagsService(tagsDao);
        try {
            doNothing().when(tagsDao).delete(anyList());
            tagsService.delete(tagsList);
            verify(tagsDao).delete(tagsList);
        } catch (DAOException | ServiceException e) {
            lOG.error(e, e.getCause());
        }
    }

    @Test
    public void unbindTest() {
        TagsService tagsService=new TagsService(tagsDao);
        try {
            doNothing().when(tagsDao).delete(anyList());
            tagsService.unbindNews(tagsList);
            verify(tagsDao).unbindNews(tagsList);
        } catch (DAOException | ServiceException e) {
            lOG.error(e, e.getCause());
        }
    }
}